package com.olacabs.olamoney.utils;

import org.apache.log4j.Logger;

import java.util.Properties;

/**
 * <p>This class is intended to abstract the properties loading
 * responsibilities.</p>
 *
 * @author ashifa sunar
 */
public enum PropertyLoader {
    TEST_ENVIRONMENT("/environment.properties");

    private Logger logger;
    private Properties properties;

    private PropertyLoader(String propertyFile) {
        logger = Logger.getLogger(this.name());
        System.out.println(" ============  Running Test Cases  =================");
        properties = new Properties();
        try {
            properties.load(PropertyLoader.class.getResourceAsStream(propertyFile));
            logger.info(propertyFile + " has been loaded successfully");
        } catch (Exception e) {
            logger.error("unable to load " + propertyFile, e);
        }
    }

    public String getProperty(String key, String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }
}
