package com.olacabs.olamoney.utils;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class DataSource {

    private ComboPooledDataSource cpds;

    private int initialPoolSize = 10;
    private int acquireIncrement = 10;
    private int maxPoolSize = 200;
    private int minPoolSize = 10;
    private int maxStatements = 200;

    public DataSource(SqlConnectionParam sqlConnectionParam) throws IOException, SQLException, PropertyVetoException {
        cpds = new ComboPooledDataSource();

        cpds.setDriverClass(String.valueOf(sqlConnectionParam.getJdbcDriver()));
        cpds.setJdbcUrl(sqlConnectionParam.getUrl());
        cpds.setUser(sqlConnectionParam.getDbUserName());
        cpds.setPassword(sqlConnectionParam.getDbPassword());

        cpds.setInitialPoolSize(initialPoolSize);
        cpds.setMinPoolSize(minPoolSize);
        cpds.setAcquireIncrement(acquireIncrement);
        cpds.setMaxPoolSize(maxPoolSize);
        cpds.setMaxStatements(maxStatements);
    }

    /**
     * @return initialPoolSize
     */
    public int getInitialPoolSize() {
        return initialPoolSize;
    }

    /**
     * @param initialPoolSize
     */
    public void setInitialPoolSize(int initialPoolSize) {
        this.initialPoolSize = initialPoolSize;
    }

    /**
     * @return acquireIncrement
     */
    public int getAcquireIncrement() {
        return acquireIncrement;
    }

    /**
     * @param acquireIncrement
     */
    public void setAcquireIncrement(int acquireIncrement) {
        this.acquireIncrement = acquireIncrement;
    }

    /**
     * @return maxPoolSize
     */
    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    /**
     * @param maxPoolSize
     */
    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    /**
     * @return minPoolSize
     */
    public int getMinPoolSize() {
        return minPoolSize;
    }

    /**
     * @param minPoolSize
     */
    public void setMinPoolSize(int minPoolSize) {
        this.minPoolSize = minPoolSize;
    }

    /**
     * @return maxStatement
     */
    public int getMaxStatements() {
        return maxStatements;
    }

    /**
     * @param maxStatements
     */
    public void setMaxStatements(int maxStatements) {
        this.maxStatements = maxStatements;
    }

    /**
     * Gets connection out of pool
     *
     * @return connection
     * @throws SQLException
     */
    public Connection getConnection() throws SQLException {
        return this.cpds.getConnection();
    }

    /**
     * Closes the pool
     */
    public void close() {
        this.cpds.close();
    }
}