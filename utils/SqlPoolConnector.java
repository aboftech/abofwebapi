package com.olacabs.olamoney.utils;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlPoolConnector implements Connector {

    private SqlConnectionParam sqlConnectionParam = null;
    private Connection connection = null;
    private Statement statement = null;
    private DataSource dataSource = null;

    public SqlPoolConnector(SqlConnectionParam sqlConnectionParam) {
        this.sqlConnectionParam = sqlConnectionParam;
    }

    //    @Override
    public void connect() {
        logger.info("[Sql Pool Connector] Connecting to URL: " + sqlConnectionParam.getUrl() + " with username [" + sqlConnectionParam.getDbUserName()
                + "] and password as: [" + sqlConnectionParam.getDbPassword() + "]");
        try {
            dataSource = new DataSource(sqlConnectionParam);
            connection = dataSource.getConnection();
            statement = connection.createStatement();
        } catch (SQLException e) {
            throw new ConnectorException(e.getMessage());
        } catch (IOException e) {
            throw new ConnectorException(e.getMessage());
        } catch (PropertyVetoException e) {
            throw new ConnectorException(e.getMessage());
        }
        logger.info("[Sql Pool Connector] Connected.");
    }

    //    @Override
    public void disconnect() {
        try {
            if (statement != null)
                statement.close();
            if (connection != null)
                connection.close();
            dataSource.close();
        } catch (SQLException e) {
            throw new ConnectorException(e.getMessage());
        }
        logger.info("[Sql Pool Connector] Disconnected from Database");
    }

    /**
     * @param query
     * @return resultSet
     */
    public ResultSet select(String query) throws SQLException {
        return executeReadQuery(query);
    }

    /**
     * @param query
     * @return
     */
    public boolean insert(String query) throws SQLException {
        return executeWriteQuery(query);
    }

    /**
     * @param query
     * @return
     */
    public boolean delete(String query) throws SQLException {
        return executeWriteQuery(query);
    }

    /**
     * @param query
     * @return
     */
    public boolean update(String query) throws SQLException {
        return executeWriteQuery(query);
    }

    private ResultSet executeReadQuery(String query) throws SQLException {
        validateConnection();
        logger.info("[Sql Pool Connector] Executing query: {}", query);
        return statement.executeQuery(query);
    }

    private boolean executeWriteQuery(String query) throws SQLException {
        validateConnection();
        logger.info("[Sql Pool Connector] Executing query: {}", query);
        return statement.execute(query);
    }

    private void validateConnection() {
        if (this.connection == null)
            throw new ConnectorException("[Sql Pool Connector] Connection is null. Please call connector.connect() to connect.");
    }
}