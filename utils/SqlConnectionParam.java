package com.olacabs.olamoney.utils;


public class SqlConnectionParam {
    private JdbcDriver jdbcDriver = JdbcDriver.MYSQL_DRIVER;
    private String host = "localhost";
    private int port = 3306;
    private String dbName;
    private String dbUserName = "root";
    private String dbPassword = "";
    private String url = "";

    /**
     * Default constructor
     */
    public SqlConnectionParam() {
    }

    /**
     * @param jdbcDriver
     */
    public SqlConnectionParam(JdbcDriver jdbcDriver) {
        this.jdbcDriver = jdbcDriver;
    }

    /**
     * @param jdbcDriver
     * @param host
     * @param port
     * @param dbName
     * @param dbUserName
     * @param dbPassword
     */
    public SqlConnectionParam(JdbcDriver jdbcDriver, String host, int port, String dbName, String dbUserName, String dbPassword) {
        this.jdbcDriver = jdbcDriver;
        this.host = host;
        this.port = port;
        this.dbName = dbName;
        this.dbUserName = dbUserName;
        this.dbPassword = dbPassword;
    }

    /**
     * @param url
     * @param dbUserName
     * @param dbPassword
     */
    public SqlConnectionParam(String url, String dbUserName, String dbPassword) {
        this.url = url;
        this.dbUserName = dbUserName;
        this.dbPassword = dbPassword;
    }

    /**
     * @return jdbc driver
     */
    public JdbcDriver getJdbcDriver() {
        return jdbcDriver;
    }

    /**
     * @param jdbcDriver
     */
    public SqlConnectionParam setJdbcDriver(JdbcDriver jdbcDriver) {
        this.jdbcDriver = jdbcDriver;
        return this;
    }

    /**
     * @return host
     */
    public String getHost() {
        return host;
    }

    /**
     * @param host
     */
    public SqlConnectionParam setHost(String host) {
        this.host = host;
        return this;
    }

    /**
     * @return port
     */
    public int getPort() {
        return port;
    }

    /**
     * @param port
     */
    public SqlConnectionParam setPort(int port) {
        this.port = port;
        return this;
    }

    /**
     * @return database name
     */
    public String getDbName() {
        return dbName;
    }

    /**
     * @param dbName
     */
    public SqlConnectionParam setDbName(String dbName) {
        this.dbName = dbName;
        return this;
    }

    /**
     * @return database username
     */
    public String getDbUserName() {
        return dbUserName;
    }

    /**
     * @param dbUserName
     */
    public SqlConnectionParam setDbUserName(String dbUserName) {
        this.dbUserName = dbUserName;
        return this;
    }

    /**
     * @return database password
     */
    public String getDbPassword() {
        return dbPassword;
    }

    /**
     * @param dbPassword
     */
    public SqlConnectionParam setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
        return this;
    }

    /**
     * @return connection url
     */
    public String getUrl() {
        return this.url.isEmpty() ? constructURL() : this.url;
    }

    /**
     * @param url
     */
    public SqlConnectionParam setUrl(String url) {
        this.url = url;
        return this;
    }

    /**
     * @return connection url
     */
    private String constructURL() {
        if (this.jdbcDriver.equals(JdbcDriver.MYSQL_DRIVER))
            return "jdbc:mysql://" + this.host + ":" + this.port + "/" + this.dbName;
        else if (this.jdbcDriver.equals(JdbcDriver.ORACLE_DRIVER))
            return "jdbc:oracle:thin:@" + this.host + ":" + this.port + ":" + this.dbName;
        else if (this.jdbcDriver.equals(JdbcDriver.DB2_DRIVER))
            return "jdbc:db2:" + this.host + ":" + this.port + "/" + this.dbName;
        else if (this.jdbcDriver.equals(JdbcDriver.SYBASE_DRIVER))
            return "jdbc:sybase:Tds:" + this.host + ":" + this.port + "/" + this.dbName;
        else
            throw new ConnectorException("JDBC driver is not set");
    }
}
