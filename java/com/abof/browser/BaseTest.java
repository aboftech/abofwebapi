package com.abof.browser;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.abof.library.BaseLib;
import com.abof.library.GenericLib;
import com.abof.pageobjects.BrowsePlpPdpPO;
import com.abof.pageobjects.ShoppingBagPO;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;


public class BaseTest {
	public static WebDriver driver;
	static WebElement wElement;
	static Dimension dSize;
	Wait wait;
	static int sStatusCnt = 0;

	@BeforeClass
	public void setUp() throws MalformedURLException {
		System.setProperty("webdriver.chrome.driver", "/Users/asifasunar/Downloads/chromedriver");
		ChromeOptions options = new ChromeOptions ();
		//		options.addExtensions (new File(“/path/to/extension.crx”));
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}


	public static void elementStatus(WebElement element, String elementName, String checkType)

	{
		switch (checkType) {
		case "displayed":
			try {
				element.isDisplayed();
				NXGReports.addStep(elementName + " is displayed", LogAs.PASSED, null);
			} catch (Exception e) {
				sStatusCnt++;
				NXGReports.addStep(elementName + " is not displayed", LogAs.FAILED,
						new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			break;
		case "enabled":
			try {
				element.isEnabled();
				NXGReports.addStep(elementName + " is enabled", LogAs.PASSED, null);
			} catch (Exception e) {
				sStatusCnt++;
				NXGReports.addStep(elementName + " is not enabled", LogAs.FAILED,
						new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			break;
		case "selected":
			try {
				element.isSelected();
				NXGReports.addStep(elementName + " is selected", LogAs.PASSED, null);
			} catch (Exception e) {
				sStatusCnt++;
				NXGReports.addStep(elementName + " is not selected", LogAs.FAILED,
						new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			break;
		}
	}

	public void visibilityOfElementWait(WebElement webElement, String elementName) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOf(webElement));
		} catch (Exception e) {
			NXGReports.addStep(elementName + " is not Visible", LogAs.FAILED, null);
		}
	}

	/*
	 * @author:Prerana Bhatt Description:check the list of webelemtns.
	 */
	public static void IsListDisplayed(List lst, String lstName) {
		try {
			if (lst.size() > 0) {
				NXGReports.addStep(lstName + " List is displayed", LogAs.PASSED, null);
			}
		} catch (Exception e) {
			NXGReports.addStep(lstName + " List is not displayed", LogAs.FAILED, null);
		}

	}

	public static void handleNavigation(WebElement ele) throws InterruptedException {
		while (!ele.isDisplayed()) {
			driver.navigate().back();
		}
	}

	@AfterMethod
	public void restapp() {
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}


}
