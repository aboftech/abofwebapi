package com.abof.api.tests;

import com.abof.api.utils.Constants;
import com.abof.api.utils.JdbcDriver;
import com.abof.api.utils.PropertyLoader;
import com.abof.api.utils.SqlConnectionParam;
import com.abof.api.utils.SqlPoolConnector;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.ResponseTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;

import org.json.JSONObject;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Random;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static com.jayway.restassured.RestAssured.given;


public class BaseTest {

    static JdbcDriver jdbcDriver;
    static SqlConnectionParam sqp;
    static SqlPoolConnector spc;

    public static String notifyResponse = "";
    public static String returnResponse = "";
    public static WireMockServer wireMockServer;


    @AfterMethod
    public void afterTestFun() {
        notifyResponse = "";
        returnResponse = "";
    }

    @AfterTest
    public void afterTest() {
        notifyResponse = "";
        returnResponse = "";

    }

    @BeforeSuite
    public void startWireMock() {
        jdbcDriver = JdbcDriver.MYSQL_DRIVER;
        System.out.println("Inside");
        sqp = new SqlConnectionParam(jdbcDriver, PropertyLoader.TEST_ENVIRONMENT.getProperty("db.host"),
                Integer.valueOf(PropertyLoader.TEST_ENVIRONMENT.getProperty("db.port")),
                PropertyLoader.TEST_ENVIRONMENT.getProperty("db.name"),
                PropertyLoader.TEST_ENVIRONMENT.getProperty("db.user"),
                PropertyLoader.TEST_ENVIRONMENT.getProperty("db.password"));
        System.out.println("Outside");
        spc = new SqlPoolConnector(sqp);
        spc.connect();
        System.setProperty("java.net.preferIPv4Stack", "true");
        wireMockServer = new WireMockServer(wireMockConfig().port(1251).extensions(new MyTransformer(), new GetTransformer()));
        wireMockServer.start();
        WireMock.configureFor(PropertyLoader.TEST_ENVIRONMENT.getProperty("wiremock.host"), 1251);
        WireMock.stubFor(WireMock.post(WireMock.urlEqualTo("/notify"))
                .willReturn(WireMock.aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200).withTransformers("mytransformer")));
        WireMock.stubFor(WireMock.post(WireMock.urlEqualTo("/return"))
                .willReturn(WireMock.aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200).withTransformers("mytransformer")));
        WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/getnotify"))
                .willReturn(WireMock.aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200).withTransformers("gettransformer")));
        WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/getreturn"))
                .willReturn(WireMock.aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200).withTransformers("gettransformer")));
    }

    @AfterSuite
    public void stopWireMock() {
        wireMockServer.stop();
        spc.disconnect();
    }


    public Response put(int retCode, String url) {
        Response putResponse = given()
                .contentType(ContentType.JSON)
                .log()
                .everything()
                .expect()
                .statusCode(retCode)
                .log()
                .ifError()
                .when()
                .put(url);
        return putResponse;
    }

    public Response get(int retCode, String url) {
        Response getResponse = given()
                .contentType(ContentType.JSON)
                .log()
                .everything()
                .expect()
                .statusCode(retCode)
                .log()
                .ifError()
                .when()
                .get(url);
        return getResponse;
    }

    public static Response postRequest(String url, String body) {
        Response PostResponse = given()
                .contentType(ContentType.JSON)
                .body(body)
                .log()
                .everything()
                .expect()
                .log()
                .ifError()
                .when()
                .post(url);
        return PostResponse;
    }

    public static Response postCallWithBearerToken(String header, String accessToken, String url,
                                                   String bdy) {
        Response PostResponse = given().contentType(ContentType.JSON)
                .header(new Header(header, "Bearer " + accessToken)).body(bdy)
                .log().everything().expect().log().ifError().when().post(url);
        return PostResponse;
    }

    public static Response getCallWithBearerToken(String header, String accessToken, String url) {
        Response getResponse = given().contentType(ContentType.JSON)
                .header(new Header(header, "Bearer " + accessToken)).expect().log().ifError().when().get(url);
        return getResponse;
    }


    public static Response postCallWithBasicToken(String accessToken, String url,
                                                  String bdy) {
        Response PostResponse = given().contentType(ContentType.JSON)
                .header(new Header("Authorization", "Basic " + accessToken)).body(bdy)
                .log().everything().expect().log().ifError().when().post(url);
        return PostResponse;
    }

    public String getuserId() {
        Random random = new Random();
        String mobile = String.format("%9d", random.nextInt(999999999));
        mobile = "1" + mobile.trim();
        JSONObject payload = new JSONObject();
        payload.put("name", "Amit");
        payload.put("password", "JunkPassword");
        payload.put("mobile", mobile);
        payload.put("email", "junk@junk.junk");
        Response signupResponse = postRequest(Constants.SIGNUP_URL, payload.toString());
        String verificationId = signupResponse.jsonPath().getString("verificationId");
        payload.put("verificationId", verificationId);
        payload.put("code", "JunkCode");
        Response verifyResponse = postRequest(Constants.VERIFY_URL, payload.toString());
        String user_id = verifyResponse.jsonPath().getString("session.user_id");
        return user_id;

    }


    public JSONObject getAccessTokenForUserId() {
        Random random = new Random();
        JSONObject userDetails = new JSONObject();
        String mobile = String.format("%010d", random.nextInt(999999999));
        //mobile ="1" + mobile.trim();
        JSONObject payload = new JSONObject();
        payload.put("name", "Amit");
        payload.put("password", "JunkPassword");
        payload.put("mobile", mobile);
        payload.put("email", "junk@junk.junk");
        Response signupResponse = postRequest(Constants.SIGNUP_URL, payload.toString());
        String verificationId = signupResponse.jsonPath().getString("verificationId");
        payload.put("verificationId", verificationId);
        payload.put("code", "JunkCode");
        Response verifyResponse = postRequest(Constants.VERIFY_URL, payload.toString());
        String user_id = verifyResponse.jsonPath().getString("session.user_id");
        String accessToken = verifyResponse.jsonPath().getString("session.id");
        userDetails.put("user_id", user_id);
        userDetails.put("accessToken", accessToken);
        return userDetails;

    }

    public JSONObject createAccountForSelf() {
        Random random = new Random();
        JSONObject userDetails = new JSONObject();
        String mobile = "8861430514";
        //mobile ="1" + mobile.trim();
        JSONObject payload = new JSONObject();
        payload.put("name", "Amit");
        payload.put("password", "JunkPassword");
        payload.put("mobile", mobile);
        payload.put("email", "amit.agarwal@olacabs.com");
        Response signupResponse = postRequest(Constants.SIGNUP_URL, payload.toString());
        String verificationId = signupResponse.jsonPath().getString("verificationId");
        payload.put("verificationId", verificationId);
        payload.put("code", "JunkCode");
        Response verifyResponse = postRequest(Constants.VERIFY_URL, payload.toString());
        String user_id = verifyResponse.jsonPath().getString("session.user_id");
        String accessToken = verifyResponse.jsonPath().getString("session.id");
        userDetails.put("user_id", user_id);
        userDetails.put("accessToken", accessToken);
        return userDetails;

    }

    public Response signUp(String name, String password, String mobile, String emailId) {
        JSONObject payload = new JSONObject();
        payload.put("name", name);
        payload.put("password", password);
        payload.put("mobile", mobile);
        payload.put("email", emailId);
        Response signupResponse = postRequest(Constants.SIGNUP_URL, payload.toString());
        return signupResponse;
    }

    public Response verifyUser(String name, String password, String mobile, String emailId, String verificationId) {
        JSONObject payload = new JSONObject();
        payload.put("name", name);
        payload.put("password", password);
        payload.put("mobile", mobile);
        payload.put("email", emailId);
        payload.put("verificationId", verificationId);
        payload.put("code", "JunkCode");
        Response verifyResponse = postRequest(Constants.VERIFY_URL, payload.toString());
        return verifyResponse;
    }

    public String getAccessTokenForUser(JSONObject payload) {
        Response signupResponse = postRequest(Constants.SIGNUP_URL, payload.toString());
        String verificationId = signupResponse.jsonPath().getString("verificationId");
        payload.put("verificationId", verificationId);
        payload.put("code", "JunkCode");
        Response verifyResponse = postRequest(Constants.VERIFY_URL, payload.toString());
        String accessToken = verifyResponse.jsonPath().getString("session.id");
        return accessToken;
    }

    public String getUserId(JSONObject payload) {
        Response signupResponse = postRequest(Constants.SIGNUP_URL, payload.toString());
        String verificationId = signupResponse.jsonPath().getString("verificationId");
        payload.put("verificationId", verificationId);
        payload.put("code", "JunkCode");
        Response verifyResponse = postRequest(Constants.VERIFY_URL, payload.toString());
        String user_id = verifyResponse.jsonPath().getString("session.user_id");
        return user_id;
    }

    public Response post(String header, String accessToken, String url,
                         String bdy) {
        Response postResponse = given().contentType(ContentType.JSON)
                .header(new Header(header, "Bearer " + accessToken)).body(bdy)
                .log().everything().expect().log().ifError().when().post(url);
        return postResponse;
    }

    public synchronized void exeQuery(String query) throws SQLException {
        java.sql.ResultSet rs = spc.select(query);
        rs.close();
    }

    public synchronized String exeQueryGetStatus(String query) throws SQLException {
        String status = "";
        java.sql.ResultSet rs = spc.select(query);
        while (rs.next()) {
            status = rs.getString("status");

        }
        rs.close();
        return status;
    }


    public synchronized static String getLatestOTP(String phone) throws SQLException {
        String otp = "";
        java.sql.ResultSet rs = spc.select("select otp from OTP where phone_number='" + phone + "' order by id desc limit 1");
        while (rs.next()) {
            otp = rs.getString("otp");

        }
        rs.close();
        return otp;
    }


    public synchronized static String getMerchantLinkOTP(String phone) throws SQLException {
        String otp = "";
        java.sql.ResultSet rs = spc.select("select otp from UserMerchantLinkOTP where otp_key=(select account_id from Account where user_id='" + phone + "')");
        while (rs.next()) {
            otp = rs.getString("otp");

        }
        rs.close();
        return otp;
    }

    public synchronized static String getMonthlyLimitReachedUsers(String userId) throws SQLException {
        String user_id = "";
        java.sql.ResultSet rs = spc.select("select * from MonthlyLimitReachedUsers where user_id=" + userId);
        while (rs.next()) {
            user_id = rs.getString("user_id");

        }
        rs.close();
        return user_id;
    }

    public synchronized static String getAccountId(String userId) throws SQLException {
        String account_id = "";
        java.sql.ResultSet rs = spc.select("select account_id from Account where user_id=" + userId);
        while (rs.next()) {
            account_id = rs.getString("account_id");

        }
        rs.close();
        return account_id;
    }

    public synchronized static String[] getTransactionIdAndOTP() throws SQLException {
        String otp = "";
        String otp_key = "";
        java.sql.ResultSet rs = spc.select("select otp_key,otp from TransactionOTP order by transaction_id desc limit 1");
        while (rs.next()) {
            otp_key = rs.getString("otp_key");
            otp = rs.getString("otp");

        }
        rs.close();
        String ar[] = new String[2];
        ar[0] = otp_key;
        ar[1] = otp;
        return ar;
    }

    public synchronized boolean exeInsert(String query) throws SQLException {
        boolean rs = spc.insert(query);
        return rs;
    }

    public synchronized boolean executeQuery(String query) throws SQLException {
        boolean rs = spc.insert(query);
        return rs;

    }

    public synchronized boolean exeDelete(String query) throws SQLException {
        boolean rs = spc.delete(query);
        return rs;

    }

    public synchronized boolean exeUpdate(String query) throws SQLException {
        boolean rs = spc.update(query);
        return rs;
    }

    public synchronized void setKYC(String idWithKYCSet) throws SQLException {
        String UpdateQry = "update Account set is_kyc_done=1 where user_id='" + idWithKYCSet + "';";
        exeUpdate(UpdateQry);
    }

    public synchronized static int checkFailedSignupLoginDetailsDb(String email_id) throws SQLException {
        int count = 0;
        java.sql.ResultSet rs = spc.select("select count(id) from FailedSignupLoginDetails where email_id=\"" + email_id + "\"");
        while (rs.next()) {
            count = Integer.parseInt(rs.getString("count(id)"));

        }
        rs.close();
        return count;


    }

    public synchronized static int getOlaKPBalance() throws SQLException {
        int balance = 0;
        java.sql.ResultSet rs = spc.select("SELECT ifnull(sum(if(type='debit', amount, -1*amount)),0) AS balance FROM MerchantTransaction mt LEFT JOIN TransactionSettlement ts ON ts.merchant_transaction_id=mt.id WHERE ts.id IS NULL AND mt.merchant_id=3 AND mt.status='completed' ");
        while (rs.next()) {
            balance = Integer.parseInt(rs.getString("balance"));

        }
        rs.close();
        return balance;


    }

    public synchronized static int getOlaRideBalance() throws SQLException {
        int balance = 0;
        java.sql.ResultSet rs = spc.select("SELECT ifnull(sum(if(type='debit', amount, -1*amount)),0) AS balance FROM MerchantTransaction mt LEFT JOIN TransactionSettlement ts ON ts.merchant_transaction_id=mt.id WHERE ts.id IS NULL AND mt.merchant_id=2 AND mt.status='completed' ");
        while (rs.next()) {
            balance = Integer.parseInt(rs.getString("balance"));

        }
        rs.close();
        return balance;


    }


    public synchronized static int getTransactionCount(String userId) throws SQLException {
        int count = 0;
        java.sql.ResultSet rs = spc.select("select count(id) from Transactions,Balance, Account where Account.user_id=" + userId + " and Account.account_id=Balance.account_id and Balance.balance_id=Transactions.balance_id");
        while (rs.next()) {
            count = Integer.parseInt(rs.getString("count(id)"));

        }
        rs.close();
        return count;


    }

    public synchronized static int getPGTransactionCount(String userId) throws SQLException {
        int count = 0;
        java.sql.ResultSet rs = spc.select("select count(id) from PGTransactions,Balance, Account where Account.user_id=" + userId + " and Account.account_id=Balance.account_id and Balance.balance_id=PGTransactions.balance_balance_id");
        while (rs.next()) {
            count = Integer.parseInt(rs.getString("count(id)"));

        }
        rs.close();
        return count;


    }

    public synchronized static int getMerchantTransactionCount(String userId) throws SQLException {
        int count = 0;
        java.sql.ResultSet rs = spc.select("select count(id) from MerchantTransaction where user_id=" + userId + "");
        while (rs.next()) {
            count = Integer.parseInt(rs.getString("count(id)"));

        }
        rs.close();
        return count;


    }

    public synchronized static int checkDeviceLoginUserAssociationDb(String email_id) throws SQLException {
        int count = 0;
        java.sql.ResultSet rs = spc.select("select count(id) from DeviceLoginUserAssociation where email_id=\"" + email_id + "\"");
        while (rs.next()) {
            count = Integer.parseInt(rs.getString("count(id)"));

        }
        rs.close();
        return count;


    }

    public synchronized static int p2pTransactionCount(String initiator_original_id) throws SQLException {
        int count = 0;
        java.sql.ResultSet rs = spc.select("select count(id) from P2PTransactions where initiator_original_id=\"" + initiator_original_id + "\"");
        while (rs.next()) {
            count = Integer.parseInt(rs.getString("count(id)"));

        }
        rs.close();
        return count;


    }


    public synchronized static double getCumulativeCreditTotalCurrentMonth(String id) throws SQLException {
        Calendar now = Calendar.getInstance();
        int month = now.get(Calendar.MONTH) + 1;
        double cumulativeBalance = 0;
        java.sql.ResultSet bid = spc.select("select balance_id from Balance, Account where Account.user_id=" + id + " and Account.account_id=Balance.account_id");
        String balance_id = "";
        while (bid.next()) {
            balance_id = bid.getString("balance_id");
        }
        bid.close();
        java.sql.ResultSet rs = spc.select("select Sum(amount) from Transactions where transactionType=\"credit\" and balance_id=" + balance_id + " and Month(created_at) = " + month);
        while (rs.next()) {
            cumulativeBalance = Double.parseDouble(rs.getString("Sum(amount)"));

        }
        rs.close();
        return cumulativeBalance / 100;
    }

    public synchronized static int getBalance(String id) throws SQLException {
        int Balance = 0;
        java.sql.ResultSet rs = spc.select("select amount from Balance, Account where Account.user_id=" + id + " and Account.account_id=Balance.account_id");
        while (rs.next()) {
            Balance = Integer.parseInt(rs.getString("amount"));

        }
        rs.close();
        return Balance / 100;
    }


    public synchronized static int checkComments(String id) throws SQLException {
        String tranasctionComment = null;
        String merchantComment = null;
        java.sql.ResultSet rsFromTranactionsTable = spc.select("select comments from Transactions,Balance, Account where Account.user_id=" + id + " and Account.account_id=Balance.account_id and Balance.balance_id=Transactions.balance_id");
        while (rsFromTranactionsTable.next()) {
            tranasctionComment = rsFromTranactionsTable.getString("comments");
        }
        java.sql.ResultSet rsFromMerchantTransactionTable = spc.select("select comments from MerchantTransaction where MerchantTransaction.user_id=" + id + " ");
        while (rsFromMerchantTransactionTable.next()) {
            merchantComment = rsFromMerchantTransactionTable.getString("comments");
        }
        rsFromTranactionsTable.close();
        rsFromMerchantTransactionTable.close();
        boolean result = tranasctionComment.equals(merchantComment);
        if (result == true)
            return 1;
        else
            return 0;

    }

    public synchronized static JSONObject getTransactionDetail(String id, String txnType) throws SQLException {
        String transactionSubType = "";
        String subTransactionType = "";
        java.sql.ResultSet rs = spc.select("select transactionSubType,subTransactionType from TransactionDetails, Account, MerchantTransaction where Account.user_id=" + id + " and Account.user_id=MerchantTransaction.user_id and MerchantTransaction.transaction_link_id=TransactionDetails.transactionLinkId and MerchantTransaction.type='" + txnType + "' order by TransactionDetails.id desc limit 1");
        while (rs.next()) {
            transactionSubType = rs.getString("transactionSubType");
            subTransactionType = rs.getString("subTransactionType");

        }
        rs.close();
        JSONObject obj = new JSONObject();
        obj.put("transactionSubType", transactionSubType);
        obj.put("subTransactionType", subTransactionType);
        return obj;
    }

    public synchronized static int getMerchantBalance(String merchantAccessToken) throws SQLException {
        int balance = 0;
        java.sql.ResultSet rs = spc.select("select balance from Merchant, Merchant_roles  where Merchant.access_token=\"" + merchantAccessToken + "\" and Merchant.auto_debit_allowed=1 and Merchant_roles.roles=\"ADMIN\" and Merchant.id=Merchant_roles.Merchant_id ");
        while (rs.next()) {
            balance = Integer.parseInt(rs.getString("balance"));

        }
        rs.close();
        return balance / 100;
    }


    public class MyTransformer extends ResponseTransformer {

        @Override
        public ResponseDefinition transform(Request request, ResponseDefinition responseDefinition, FileSource fileSource) {
            String responseStr = "";
            if (request.getMethod().isOneOf(RequestMethod.POST)) {
                if (request.getUrl().endsWith("notify")) {
                    notifyResponse = request.getBodyAsString();
                    responseStr = notifyResponse;

                } else {
                    returnResponse = request.getBodyAsString();
                    responseStr = returnResponse;
                }
            }
            return ResponseDefinitionBuilder.like(responseDefinition)
                    .withBody(responseStr.getBytes())
                    .build();
        }

        public String name() {
            return "mytransformer";
        }
    }

    ;

    public class GetTransformer extends ResponseTransformer {

        @Override
        public ResponseDefinition transform(Request request, ResponseDefinition responseDefinition, FileSource fileSource) {
            if (request.getUrl().endsWith("notify"))
                return ResponseDefinitionBuilder.like(responseDefinition)
                        .withBody(notifyResponse.getBytes())
                        .build();
            else
                return ResponseDefinitionBuilder.like(responseDefinition)
                        .withBody(returnResponse.getBytes())
                        .build();
        }

        public String name() {
            return "gettransformer";
        }
    }

    ;

    @Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
    @Target({ElementType.METHOD, ElementType.TYPE})
    public @interface Priority {
        int value() default 0;
    }
}