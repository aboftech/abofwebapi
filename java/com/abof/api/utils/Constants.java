package com.abof.api.utils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * Created by amit.agarwal on 3/25/16.
 */
public class Constants {

    public static final String OLAMONEY_HOST = PropertyLoader.TEST_ENVIRONMENT.getProperty("olamoney.host");
    public static final String BASE_URL = OLAMONEY_HOST + "/v1";
    public static final String V2BASE_URL = OLAMONEY_HOST + "/v2";
    public static final String V3BASE_URL = OLAMONEY_HOST + "/v3";

    // P2P API's
    public static final String P2P_URL = BASE_URL + "/peer/initiate";
    public static final String RETRACT_URL = BASE_URL + "/peer/act";

    // Signup API's
    public static final String SIGNUP_URL = V2BASE_URL + "/users/signup";
    public static final String VERIFY_URL = V2BASE_URL + "/users/verify";
    public static final String V3_SIGNUP_URL = V3BASE_URL + "/users/signup";
    public static final String V3_VERIFY_URL = V3BASE_URL + "/users/verify";
    public static final String V3_LOGIN_URL = V3BASE_URL + "/users/login";
    public static final String V2_SIGNUP_URL = V2BASE_URL + "/users/signup";
    public static final String V2_VERIFY_URL = V2BASE_URL + "/users/verify";
    public static final String V2_LOGIN_URL = V2BASE_URL + "/users/login";

    // Txn API's
    public static final String DEBIT_URL = BASE_URL + "/debit";
    public static final String REFUND_URL = BASE_URL + "/refund";
    public static final String SUMMARY_URL = BASE_URL+"/account/summary";
    public static final String GETTXN_URL= Constants.BASE_URL+"/account/transactions?";

    // Mobile Recharge API's
    public static final String POSTPAID_URL = V2BASE_URL + "/servicePayments/payMobilePostpaidBill";
    public static final String PREPAID_URL = V2BASE_URL + "/servicePayments/rechargeMobile";
    public static final String GETPLANS_URL = V3BASE_URL + "/servicePayments/getPlans?";
    public static final String GETTRANSACTIONS_URL = V3BASE_URL + "/servicePayments/getTransactions?";

    // V1 PAYU API's
    public static final String GENERATEBILL_URL = BASE_URL + "/payuint/generateBill?amount=";
    public static final String PAYUSUCCESS_URL = BASE_URL + "/payuint/success";
    public static final String GENERATEWEBBILL_URL = BASE_URL + "/payuint/generateWebBill?";
    public static final String CALLBACK_URL = BASE_URL + "/payuint/callback";
    public static final String SUCCESSWEB_URL =  BASE_URL + "/payuint/success_web";
    public static final String SUCCESSPAYU_URL =  BASE_URL + "/payuint/success_payu";

    // V3 PAYU API's
    public static final String V3GENERATEBILL_URL = V3BASE_URL + "/payuint/generateBill?amount=";
    public static final String V3PAYUSUCCESS_URL = V3BASE_URL + "/payuint/success";
    public static final String V3CALLBACK_URL = V3BASE_URL + "/payuint/callback";
    public static final String V3FAILURE_URL = V3BASE_URL + "/payuint/failure";

    // Bill Constants
    public static final String CURRENCY = "INR";
    public static final String OLA_ACCESS_TOKEN = "ola_access_token";
    public static final String OLA_SALT = "ola_salt";
    public static final String RIDES_ACCESS_TOKEN = "ola_rides_access_token";
    public static final String RIDES_SALT = "rides_salt";
    public static final String KP_ACCESS_TOKEN = "ola_kp_access_token";
    public static final String KP_SALT = "kp_salt";
    public static final String BMS_ACCESS_TOKEN = "bms_access_token";
    public static final String BMS_SALT = "bms_salt";
    public static final String UDF = "Test User";
    public static final String RETURN_URL = "http://"+ PropertyLoader.TEST_ENVIRONMENT.getProperty("wiremock.host")+":1251/return";
    public static final String NOTIFY_URL = "http://"+ PropertyLoader.TEST_ENVIRONMENT.getProperty("wiremock.host")+":1251/notify";
    public static final String BALANCE_NAME = "cash";
    public static final String COMMENT = "Test Comment";
    public static final String INVALID_OLA_ACCESS_TOKEN = "ola_access";

    public static final String TOKEN = "2DPPoRB1H6Ls77NqjDjO";
    public static final String SALT="MbCDCko9tbcvrvxANPZg";

    public static String generateBasicToken() {
        Base64.Encoder encoder = Base64.getEncoder();
        String normalString = "OLA:test";
        String encodedString = encoder.encodeToString(
                normalString.getBytes(StandardCharsets.UTF_8));
        return encodedString;
    }


    // Merchant console credentials 
    public static final String MC_URL =  PropertyLoader.TEST_ENVIRONMENT.getProperty("merchantconsole.url");
    public static final String ADMIN = PropertyLoader.TEST_ENVIRONMENT.getProperty("merchantconsole.admin");
    public static final String ADMIN_PWD = PropertyLoader.TEST_ENVIRONMENT.getProperty("merchantconsole.pwd");
    
}
