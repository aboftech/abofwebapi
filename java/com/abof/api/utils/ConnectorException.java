package com.abof.api.utils;

public class ConnectorException extends RuntimeException {

    /**
     * Default constructor
     */
    public ConnectorException() {
        super();
    }

    /**
     * @param message
     */
    public ConnectorException(String message) {
        super(message);
    }

    /**
     * @param message
     * @param throwable
     */
    public ConnectorException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
