package com.abof.api.utils;

public enum JdbcDriver {

    MYSQL_DRIVER {
        public String toString() {
            return "com.mysql.jdbc.Driver";
        }
    },

    ORACLE_DRIVER {
        public String toString() {
            return "oracle.jdbc.driver.OracleDriver";
        }
    },

    DB2_DRIVER {
        public String toString() {
            return "COM.ibm.db2.jdbc.net.DB2Driver";
        }
    },

    SYBASE_DRIVER {
        public String toString() {
            return "com.sybase.jdbc.SybDriver";
        }
    }
}