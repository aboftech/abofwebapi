package com.abof.api.clients;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import org.testng.Assert;

import static com.jayway.restassured.RestAssured.given;

public class AccountMemberCheckClient {


	public static Response accountMemberCheck(String url,String body) {

		Response GetResponse = given()
				.contentType(ContentType.JSON)
				.body(body)
				.log()
				.everything()
				.expect()
				.log()
				.ifError()
				.when()
				.get(url);

		return GetResponse;
	}

	public static void verifyResponse(Response verifyOTP, int retCode, String errMsg, String errorCode) {
		Assert.assertEquals(verifyOTP.getStatusCode(), retCode);
		if (retCode == 200) {
			JsonPath jsonPath = new JsonPath(verifyOTP.asString());
			Assert.assertFalse(jsonPath.getString("userId").isEmpty());
			Assert.assertEquals(jsonPath.getString("status"), "success");
			Assert.assertFalse(jsonPath.getString("accessToken").isEmpty());
		} else {
			JsonPath jsonPath = new JsonPath(verifyOTP.asString());
			Assert.assertEquals(jsonPath.getString("message"), errMsg);
			Assert.assertEquals(jsonPath.getString("status"), "error");
			Assert.assertEquals(jsonPath.getString("errorCode"), errorCode);
		}
	}

	public static void verifyErrResponse(Response verifyOTP, int retCode, String errMsg) {
		Assert.assertEquals(verifyOTP.getStatusCode(), retCode);
		JsonPath jsonPath = new JsonPath(verifyOTP.asString());
		Assert.assertEquals(jsonPath.getString("message"), errMsg);
		Assert.assertEquals(jsonPath.getString("status"), "error");
	}

}
