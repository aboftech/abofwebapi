package com.abof.scripts;


import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.abof.library.BaseLib;
import com.abof.library.GenericLib;
import com.abof.pageobjects.BrowsePlpPdpPO;
import com.abof.pageobjects.HamburgerMenuPO;
import com.abof.pageobjects.HomePagePO;
import com.abof.pageobjects.LoginPagePO;
import com.abof.pageobjects.MyOrdersPagePO;
import com.abof.pageobjects.ShoppingBagPO;
import com.abof.pageobjects.WhatsHotLandingPO;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;

import io.appium.java_client.android.AndroidKeyCode;

public class FilterScript extends BaseLib {
	LoginPagePO loginPo = null;
	HomePagePO homePagePo = null;
	BrowsePlpPdpPO browsePlpPdpPo = null;
	String[] sData = null;
	int productFound=0;
	int  count=0;
	int i=0;
	String ProductfoundTxt=null;
	 
	
	@BeforeMethod
	public void init() {
		loginPo = new LoginPagePO(driver);
		homePagePo = new HomePagePO(driver);
		browsePlpPdpPo = new BrowsePlpPdpPO(driver);
	}
	
	/* @Description:Check the functionality of Popularity,Discount,Price High-to-Low,Price Low-to-Highgender ,Products ,brands,sizes,price-ranges, brand filter sort option
	 * @Author:Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = true, priority = 1, description = "Check the functionality of Popularity,Discount,Price High-to-Low,Price Low-to-High,gender ,Products ,brands,sizes,price-ranges, brand filter sort option")
	public void testPerformDifferentFilter(String device) throws Exception
	{
		
        try{
		loginPo.getEleLoginCloseBtn().click();
		BaseLib.waitForElement(loginPo.getEleOkayBtn(),"Okay Btn is displayed"," Okay Btn is not displayed"); 
		loginPo.handleOkayBtn();
		sData = GenericLib.toReadExcelData("Login", "TC_PLPFilter_001");
		homePagePo.searchOption(sData[2]);	
		loginPo.handleOkayBtn();
		BaseLib.waitForElement(browsePlpPdpPo.getElePlpFilterIcon()," Plp Filter Icon is displayed"," Plp Filter Icon is not displayed");
		browsePlpPdpPo.getElePlpFilterIcon().click();
		BaseLib.waitForElement(browsePlpPdpPo.getEleFilterCloseBtn(),"FilterCloseBtn is displayed"," FilterCloseBtn is not displayed");
		//applying filter for popularity
		NXGReports.addStep("applying filter for popularity", LogAs.INFO,null);
		browsePlpPdpPo.toValidateFilterOptions(sData[3], sData[4]);
		browsePlpPdpPo.getEleFilterTickBtn().click();
		loginPo.handleOkayBtn();
		browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
		//applying filter for discount
		NXGReports.addStep("applying filter for discount", LogAs.INFO,null);
		browsePlpPdpPo.getElePlpFilterIcon().click();
		browsePlpPdpPo.toValidateFilterOptions(sData[3],sData[5]);
		browsePlpPdpPo.getEleFilterTickBtn().click();
		loginPo.handleOkayBtn();
		ProductfoundTxt=browsePlpPdpPo.getElePlpProductFoundTxt().getText();
		productFound=browsePlpPdpPo.convertPriceToIntValue(ProductfoundTxt);
		while(count<productFound)
		{
			
			if(!(browsePlpPdpPo.getEleDiscountPriceLst().get(i).isDisplayed()))
			{
				NXGReports.addStep("list of product with discount is  not displayed", LogAs.FAILED,new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			   break;
			}
	    	count++;
				if(i%2==1)
				{
					swipeBottomToTop(.80,.40);
					loginPo.handleOkayBtn();
					 if(i==3)
						{
							i=-1;
						}
				}
								
			i++;
		}
		//applying filter for gender "Men"
		NXGReports.addStep("applying filter for gender Men", LogAs.INFO,null);
		browsePlpPdpPo.getElePlpFilterIcon().click();
		browsePlpPdpPo.toValidateFilterOptions(sData[6],sData[7]);
		Thread.sleep(2000);
		browsePlpPdpPo.getEleFilterTickBtn().click();
		loginPo.handleOkayBtn();
		ProductfoundTxt=browsePlpPdpPo.getElePlpProductFoundTxt().getText();
		productFound=browsePlpPdpPo.convertPriceToIntValue(ProductfoundTxt);
		System.out.println(productFound);
		i=0;
		count=1;
		while(count<productFound)
		{
			if(browsePlpPdpPo.getEleProductNameLst().get(i).getText().contains("women"))
			{
				NXGReports.addStep("female product is available in male gender search",LogAs.FAILED,new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			   break;
			}
	    	count++;
	    	
				if(i%2==1)
				{
					swipeBottomToTop(.80,.40);
					loginPo.handleOkayBtn();
					 if(i==3)
						{
							i=-1;
						}
				}
								
			i++;
		}
		//applying filter for  gender "Women"
		NXGReports.addStep("applying filter for gender Women", LogAs.INFO,null);
		browsePlpPdpPo.getElePlpFilterIcon().click();
		browsePlpPdpPo.getEleFilterClearAll().click();
		browsePlpPdpPo.toValidateFilterOptions(sData[6],sData[8]);
		browsePlpPdpPo.getEleFilterTickBtn().click();
		loginPo.handleOkayBtn();
		ProductfoundTxt=browsePlpPdpPo.getElePlpProductFoundTxt().getText();
		productFound=browsePlpPdpPo.convertPriceToIntValue(ProductfoundTxt);
		i=0;
		count=1;
		while(count<productFound)
		{
			if(browsePlpPdpPo.getEleProductNameLst().get(i).getText().contains("Men"))
			{
				
				NXGReports.addStep("Men product is available in female gender search", LogAs.FAILED,new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			   break;
			}
	    	count++;
				if(i%2==1)
				{
					swipeBottomToTop(.80,.40);
					 if(i==3)
						{
							i=-1;
						}
				}
			i++;
		}
		NXGReports.addStep("applying filter for size", LogAs.INFO,null);
		//applying filter for size
		browsePlpPdpPo.getElePlpFilterIcon().click();
		browsePlpPdpPo.getEleFilterClearAll().click();
		browsePlpPdpPo.toValidateFilterOptions(sData[9], sData[10]);
		Thread.sleep(2000);
		browsePlpPdpPo.getEleFilterTickBtn().click();
		loginPo.handleOkayBtn();
		ProductfoundTxt=browsePlpPdpPo.getElePlpProductFoundTxt().getText();
		productFound=browsePlpPdpPo.convertPriceToIntValue(ProductfoundTxt);
		System.out.println(productFound);
		i=0;
		count=1;
		while(count<productFound)
		{
			
				browsePlpPdpPo.getEleProductNameLst().get(i).click();
				BaseLib.scrollToElement(4,"UP",.80,.20,browsePlpPdpPo.getElePdpSizeGuideLnk());
				//wait for size list
				if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+sData[10]+"')]")).isDisplayed())
				{
					NXGReports.addStep("given size  is  available", LogAs.PASSED,null);
				}
			loginPo.handleOkayBtn();
			browsePlpPdpPo.getEleBackBtn().click();
			Thread.sleep(5000);
	    	count++;
				if(i%2==1)
				{
					
					swipeBottomToTop(.80,.40);
					
					 if(i==3)
						{
							i=-1;
						}
				}
								
			i++;
		}
		//applying filter for Brand
		NXGReports.addStep("applying filter for Brand", LogAs.INFO,null);
		browsePlpPdpPo.getElePlpFilterIcon().click();
		browsePlpPdpPo.getEleFilterClearAll().click();
		browsePlpPdpPo.toValidateFilterOptions(sData[11],sData[12]);
		browsePlpPdpPo.getEleFilterTickBtn().click();
		loginPo.handleOkayBtn();
		ProductfoundTxt=browsePlpPdpPo.getElePlpProductFoundTxt().getText();
		productFound=browsePlpPdpPo.convertPriceToIntValue(ProductfoundTxt);
		i=0;
		count=1;
		while(count<productFound)
		{
			if(!(browsePlpPdpPo.getEleProductNameLst().contains(sData[12])))
			{
				NXGReports.addStep("List of product is  not as per given filter", LogAs.FAILED,new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			   break;
			}
	    	count++;
				if(i%2==1)
				{
					swipeBottomToTop(.80,.40);
					 if(i==3)
						{
							i=-1;
						}
				}
								
			i++;
		}
		//applying filter for Price-Range
		NXGReports.addStep("applying filter for Price-Range", LogAs.INFO,null);
		browsePlpPdpPo.getElePlpFilterIcon().click();
		browsePlpPdpPo.getEleFilterClearAll().click();
		browsePlpPdpPo.toValidateFilterOptions(sData[13], sData[14]);
		browsePlpPdpPo.getEleFilterTickBtn().click();
		loginPo.handleOkayBtn();
		ProductfoundTxt=browsePlpPdpPo.getElePlpProductFoundTxt().getText();
		productFound=browsePlpPdpPo.convertPriceToIntValue(ProductfoundTxt);
		System.out.println(productFound);
		i=0;
		count=1;
		 String []range = sData[14].split("-"); 
		  String lowerRange=range[0];
		  String UpperRange=range[1];
		  while(count<=productFound)
			{
				int Price=browsePlpPdpPo.convertPriceToIntValue(browsePlpPdpPo.getElePlpOfferPriceLst().get(i).getText());
				  if(Price>=Integer.parseInt(lowerRange) && Price<=Integer.parseInt(UpperRange))
				  {
					  System.out.println(" price displayed, as per given range filter ");
				  }
		    	count++;
					if(i%2==1)
					{
						swipeBottomToTop(.80,.40);
						
						 if(i==3)
							{
								i=-1;
							}
					}
				i++;
			}
		//applying filter for sort:PriceHighToLow
		NXGReports.addStep("applying filter for sort:PriceHighToLow", LogAs.INFO,null);
		browsePlpPdpPo.getElePlpFilterIcon().click();
		browsePlpPdpPo.getEleFilterClearAll().click();
		browsePlpPdpPo.toValidateFilterOptions(sData[3], sData[15]);
		browsePlpPdpPo.getEleFilterTickBtn().click();
		loginPo.handleOkayBtn();
		ProductfoundTxt=browsePlpPdpPo.getElePlpProductFoundTxt().getText();
		productFound=browsePlpPdpPo.convertPriceToIntValue(ProductfoundTxt);
		i=0;
		count=1;
		int firstOfferPriceHighestValue=browsePlpPdpPo.convertPriceToIntValue(browsePlpPdpPo.getElePlpOfferPriceLst().get(0).getText());
	
		while(count<productFound)
		{
			int secondOfferPriceLowestValue=browsePlpPdpPo.convertPriceToIntValue(browsePlpPdpPo.getElePlpOfferPriceLst().get(i).getText());
			browsePlpPdpPo.checkPriceFilter( sData[15],firstOfferPriceHighestValue,secondOfferPriceLowestValue);
			firstOfferPriceHighestValue=secondOfferPriceLowestValue;
	    	count++;
	    	
				if(i%2==1)
				{				
					swipeBottomToTop(.80,.40);
					
					 if(i==3)
						{
							i=-1;
						}
				}
								
			i++;
		}
		//applying filter for sort:PriceLowTohigh
		NXGReports.addStep("applying filter for sort:PriceLowTohigh", LogAs.INFO,null);
		browsePlpPdpPo.getElePlpFilterIcon().click();
		browsePlpPdpPo.getEleFilterClearAll().click();
		browsePlpPdpPo.toValidateFilterOptions(sData[3], sData[16]);
		browsePlpPdpPo.getEleFilterTickBtn().click();
		loginPo.handleOkayBtn();
		ProductfoundTxt=browsePlpPdpPo.getElePlpProductFoundTxt().getText();
		productFound=browsePlpPdpPo.convertPriceToIntValue(ProductfoundTxt);
		i=0;
		count=1;
		int firstOfferPriceLowestValue=browsePlpPdpPo.convertPriceToIntValue(browsePlpPdpPo.getElePlpOfferPriceLst().get(0).getText());
		while(count<productFound)
		{
			int secondOfferPriceLowestValue=browsePlpPdpPo.convertPriceToIntValue(browsePlpPdpPo.getElePlpOfferPriceLst().get(i).getText());
			browsePlpPdpPo.checkPriceFilter( sData[16],firstOfferPriceHighestValue,secondOfferPriceLowestValue);
			firstOfferPriceHighestValue=secondOfferPriceLowestValue;
	    	count++;
				if(i%2==1)
				{
					
					swipeBottomToTop(.80,.40);
		
					 if(i==3)
						{
							i=-1;
						}
				}
								
			i++;
		}
		
	    }catch(Exception e){
        	throw e;
        }
		
	
	}
	
	/* @Description:Check the refine further filter options
	 * @Author:Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled =true, priority = 2, description = "Check the refine further filter options")
	public void testPerformFilterForTwoCombination(String device) throws Exception
	{
		  NXGReports.addStep("reports for combination for two filter", LogAs.INFO,null);	
		try{
			loginPo.getEleLoginCloseBtn().click();
			BaseLib.waitForElement(loginPo.getEleOkayBtn(),"Okay Btn is displayed"," Okay Btn is not displayed"); 
			loginPo.handleOkayBtn();
			sData = GenericLib.toReadExcelData("Login", "TC_PLPFilter_002");
			homePagePo.searchOption(sData[2]);
			loginPo.handleOkayBtn();
			BaseLib.waitForElement(browsePlpPdpPo.getElePlpFilterIcon()," Plp Filter Icon is displayed"," Plp Filter Icon is not displayed");
			browsePlpPdpPo.getElePlpFilterIcon().click();
			BaseLib.waitForElement(browsePlpPdpPo.getEleFilterCloseBtn(),"FilterCloseBtn is not displayed" ," Plp CategoryName is not displayed");
			browsePlpPdpPo.toValidateFilterOptions(sData[3],sData[4]);
			browsePlpPdpPo.toValidateFilterOptions(sData[5], sData[6]);
			BaseLib.waitForElement(browsePlpPdpPo.getEleFilterTickBtn(),"FilterTickBtn is not displayed","FilterTickBtn is displayed");
			browsePlpPdpPo.getEleFilterTickBtn().click();
			loginPo.handleOkayBtn();
			ProductfoundTxt=browsePlpPdpPo.getElePlpProductFoundTxt().getText();
			productFound=browsePlpPdpPo.convertPriceToIntValue(ProductfoundTxt);
			count=0;
			while(count<productFound)
			{
				if(browsePlpPdpPo.getEleProductNameLst().get(i).getText().contains(sData[4]) && browsePlpPdpPo.getEleProductNameLst().get(i).getText().contains(sData[6]))
				{
					
					
					NXGReports.addStep( "both filters applied succesfully", LogAs.PASSED,null);
				}
				else 
				{
					
					NXGReports.addStep( "search is not as per given keyword", LogAs.FAILED,new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				    break;
				}		    	
		    	count++;
		    	
					if(i%2==1)
					{
						
						swipeBottomToTop(.80,.40);
						
						loginPo.handleOkayBtn();
						
						 if(i==3)
							{
								i=-1;
							}
					}
									
				i++;
			}
			
		 	}catch(Exception e){
		 		throw e;
		 	}
			}
	/* @Description:Check if the user can able to refine the products based on combination of different filters & Sort options
	 * @Author:Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = true, priority = 3, description = "Check if the user can able to refine the products based on combination of different filters & Sort options")
	public void testPerformFilterForDifferentCombinations(String device) throws Exception
	{
		try
		{
		
		  NXGReports.addStep("reports for combination for five filter", LogAs.INFO,null);	
			loginPo.getEleLoginCloseBtn().click();
			BaseLib.waitForElement(loginPo.getEleOkayBtn(),"Okay Btn is displayed"," Okay Btn is not displayed"); 
			loginPo.handleOkayBtn();
			sData = GenericLib.toReadExcelData("Login", "TC_PLPFilter_003");
			homePagePo.searchOption(sData[2]);	
			loginPo.handleOkayBtn();
			BaseLib.waitForElement(browsePlpPdpPo.getElePlpFilterIcon()," Plp Filter Icon is displayed"," Plp Filter Icon is not displayed");
			browsePlpPdpPo.getElePlpFilterIcon().click();
			BaseLib.waitForElement(browsePlpPdpPo.getEleFilterCloseBtn(),"FilterCloseBtn is displayed","FilterCloseBtn  is not displayed");
			browsePlpPdpPo.toValidateFilterOptions(sData[3],sData[4]);
			
			browsePlpPdpPo.toValidateFilterOptions(sData[5],sData[6]);
			browsePlpPdpPo.toValidateFilterOptions(sData[7],sData[8]);
			
			browsePlpPdpPo.toValidateFilterOptions(sData[9], sData[10]);
		//	browsePlpPdpPo.toValidateFilterOptions(sData[11],sData[12]);
			Thread.sleep(1000);
			browsePlpPdpPo.getEleFilterTickBtn().click();
			loginPo.handleOkayBtn();
			ProductfoundTxt=browsePlpPdpPo.getElePlpProductFoundTxt().getText();
			productFound=browsePlpPdpPo.convertPriceToIntValue(ProductfoundTxt);
			count=0;
			
			while(count<productFound)
			{
				
				if(browsePlpPdpPo.getEleProductNameLst().get(i).getText().contains(sData[4]) )// need to add brand condition check,will done after demo/whn issue resolved
				{
					
				  //check range
				  String []range = sData[8].split("-"); 
				  String lowerRange=range[0];
				
				  String UpperRange=range[1];
					int Price=browsePlpPdpPo.convertPriceToIntValue(browsePlpPdpPo.getElePlpOfferPriceLst().get(i).getText());
					  if(Price>=Integer.parseInt(lowerRange) && Price<=Integer.parseInt(UpperRange))
					  {
						  NXGReports.addStep("price displayed, as per given range filter", LogAs.PASSED,null);
						
					  }
			    //check for size
			    browsePlpPdpPo.getEleProductNameLst().get(i).click();	
				if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+sData[10]+"')]")).isDisplayed())
				{
					NXGReports.addStep("given size  is  available", LogAs.PASSED,null);
				}
				browsePlpPdpPo.getEleBackBtn().click(); 
		
				//check for 'Price-High to Low
				
				
				    int firstOfferPriceHighestValue=browsePlpPdpPo.convertPriceToIntValue(browsePlpPdpPo.getElePlpOfferPriceLst().get(0).getText());
				
					int secondOfferPriceLowestValue=browsePlpPdpPo.convertPriceToIntValue(browsePlpPdpPo.getElePlpOfferPriceLst().get(i).getText());
					browsePlpPdpPo.checkPriceFilter( sData[6],firstOfferPriceHighestValue,secondOfferPriceLowestValue);
				}
				
						    	
		    	count++;
		    	
					if(i%2==1)
					{	
						swipeBottomToTop(.80,.40);
						
						loginPo.handleOkayBtn();
						
						 if(i==3)
							{
								i=-1;
							}
					}
									
				i++;
			}
		}catch(Exception e)
		{
			throw e;
		}
		
	}
	
}
