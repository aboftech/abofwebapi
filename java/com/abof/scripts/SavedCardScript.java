package com.abof.scripts;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.abof.library.BaseLib;
import com.abof.library.GenericLib;
import com.abof.pageobjects.BrowsePlpPdpPO;
import com.abof.pageobjects.CheckOutPagePO;
import com.abof.pageobjects.HamburgerMenuPO;
import com.abof.pageobjects.HomePagePO;
import com.abof.pageobjects.LoginPagePO;
import com.abof.pageobjects.MyFavouritesPagePO;
import com.abof.pageobjects.PaymentPagePO;
import com.abof.pageobjects.ShoppingBagPO;
import com.abof.pageobjects.SkultPO;
import com.abof.pageobjects.WhatsHotLandingPO;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;

public class SavedCardScript extends BaseLib {

	LoginPagePO loginPo = null;
	HomePagePO homePagePo = null;
	HamburgerMenuPO hamburgerMenuPo = null;
	WhatsHotLandingPO whatsHotLandingPo = null;
	BrowsePlpPdpPO browsePlpPdpPo = null;
	ShoppingBagPO shoppingBagPo = null;
	CheckOutPagePO checkOutPagePo = null;
	PaymentPagePO paymentPagePo = null;
	MyFavouritesPagePO myfavouritespagePo;
	String productSaveForLater = null;
	String removedProductFromFav = null;
	String[] sData = null;
	String size = null;
	String pin = null;
	String[] ss = null;
	String text = null;
	String PDP = null;
	String ProductName = null;
	
	@BeforeMethod
	public void init() {
		loginPo = new LoginPagePO(driver);
		homePagePo = new HomePagePO(driver);
		hamburgerMenuPo = new HamburgerMenuPO(driver);
		whatsHotLandingPo = new WhatsHotLandingPO(driver);
		browsePlpPdpPo = new BrowsePlpPdpPO(driver);
		shoppingBagPo = new ShoppingBagPO(driver);
		checkOutPagePo = new CheckOutPagePO(driver);
		paymentPagePo = new PaymentPagePO(driver);
		myfavouritespagePo = new MyFavouritesPagePO(driver);
	}
	@Parameters("device")
	@Test(enabled = false, priority = 1)
	public void chromebrowser(){
		try{
			callAPIService("http://www.google.com");
		}catch(Exception e){
			
		}
	}
	
	/* @Description:Login as normal user men (abof),Checkbox gets de-selected.
	  * @Author:RaghuKiran MR/Srinivas Hippargi /Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = false, priority = 2, description = "Login as normal user men (abof),Checkbox gets de-selected.")
	public void testNoSavecardfunctionality() throws Exception{
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_005");
		size = sData[2];
		ss = size.split(",");
		try{
		loginPo.loginApp("ABOF", "TC_Login_004");
		Thread.sleep(8000);
		loginPo.handleOkayBtn();
		Thread.sleep(8000);
		shoppingBagPo.removeItemInShopping();
		sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
		homePagePo.searchOption(sData[3]);
		loginPo.handleOkayBtn();
		loginPo.handleOkayBtn();
		Thread.sleep(40000);
		browsePlpPdpPo.getElePlpProductName().click();
		loginPo.handleOkayBtn();
		Thread.sleep(40000);
		shoppingBagPo.browserselectSize(ss);
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		shoppingBagPo.getEleViewBagBtn().click();
		Thread.sleep(10000);
		
			BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
			shoppingBagPo.getElePlaceOrderBtn().click();
			Thread.sleep(10000);
			checkOutPagePo.fillCheckOutDetails();
			
			Thread.sleep(8000);

			paymentPagePo.addpaymentdetailswithoutsSaveCard("Debit Card","TC_PMT_001");
			Thread.sleep(10000);
			browsePlpPdpPo.getEleBackBtn().click();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleUserProfileNameTxt().click();
			Assert.assertFalse(hamburgerMenuPo.getEleSaveCardText().isDisplayed(), "The Savecard text is displayed");
			NXGReports.addStep("The saved card is not displayed", LogAs.PASSED, null);
		}catch(Exception e){
		throw e;
	}
		
	}
	/* @Description:Login as normal User and save the card and verify the content in the savecard section
	  * @Author:RaghuKiran MR/Srinivas Hippargi /Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = false, priority = 3, description = "Login as normal User and save the card and verify the content in the savecard section")
	public void testVerifyContentSavedSection() throws Exception{
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_005");
		size = sData[2];
		ss = size.split(",");
		try{
		loginPo.loginApp("ABOF", "TC_Login_004");
		loginPo.handleOkayBtn();
		shoppingBagPo.removeItemInShopping();
		sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
		homePagePo.searchOption(sData[3]);
		loginPo.handleOkayBtn();
		Thread.sleep(10000);
		browsePlpPdpPo.getElePlpProductName().click();
		loginPo.handleOkayBtn();
		Thread.sleep(10000);
		shoppingBagPo.browserselectSize(ss);
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		Thread.sleep(10000);
		shoppingBagPo.getEleViewBagBtn().click();
		Thread.sleep(10000);
		BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
		shoppingBagPo.getElePlaceOrderBtn().click();
		Thread.sleep(10000);
		checkOutPagePo.fillCheckOutDetails();
		Thread.sleep(8000);
		paymentPagePo.addpaymentdetailsWithSaveCard("Credit card","TC_PMT_003");
		Thread.sleep(80000);
		paymentPagePo.verifyContentPaymentThankYouMessage("Master");
	 
	}catch(Exception e){
		throw e;
	}
		
	}
	
	/* @Description:Login as normal User and User has an active saved card and abof bucks.
	  * @Author:RaghuKiran MR/Srinivas Hippargi /Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = false, priority = 4, description = "Login as normal User and User has an active saved card and abof bucks.")
	public void testAbofBucksSavedCard() throws Exception{
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_005");
		size = sData[2];
		ss = size.split(",");
		try{
			loginPo.loginApp("ABOF", "TC_Login_005");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleUserProfileNameTxt().click();
			BaseLib.scrollToElement(5, "UP", .90, .50, hamburgerMenuPo.getEleSaveCardText());
			Assert.assertTrue(hamburgerMenuPo.getEleSaveCardText().isDisplayed(), "The Savecard text is displayed");
			NXGReports.addStep("The saved card is not displayed", LogAs.PASSED, null);
			for (WebElement eleSaveCardInfoTxt:hamburgerMenuPo.getEleSavedCardInfoTxt()) {

				Assert.assertTrue(eleSaveCardInfoTxt.isDisplayed(),
						eleSaveCardInfoTxt.getText() + " is not dispalyed in the savedcard section page");
				NXGReports.addStep(
						eleSaveCardInfoTxt.getText() + "is dispalyed in the savedcard section page",
						LogAs.PASSED, null);
			}
			shoppingBagPo.removeItemInShopping();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleAbofBucksAndGiftCardLnk().click();
			BaseLib.elementStatus(hamburgerMenuPo.getEleAbofBucksCreditsTxt(), "the abof bucks is", "displayed");
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[3]);
			loginPo.handleOkayBtn();
			Thread.sleep(10000);
			browsePlpPdpPo.getElePlpProductName().click();
			loginPo.handleOkayBtn();
			Thread.sleep(10000);
			shoppingBagPo.browserselectSize(ss);
			browsePlpPdpPo.getElePdpAddToBagBtn().click();
			Thread.sleep(10000);
			shoppingBagPo.getEleViewBagBtn().click();
			Thread.sleep(5000);
			BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
			shoppingBagPo.getElePlaceOrderBtn().click();
			Thread.sleep(10000);
			checkOutPagePo.getEleDeliverAddressBtn().click();
			Assert.assertTrue(paymentPagePo.getEleAbofBucksTxt().isDisplayed(), "The abof Bucks text is not displayed");
			NXGReports.addStep("The Abof Bucks is displayed", LogAs.PASSED, null);
			GenericLib.isVisible(GenericLib.path + "Mastercard.PNG");
			paymentPagePo.savecard("MasterCard");
			Thread.sleep(80000);
			paymentPagePo.verifyContentPaymentThankYouMessage("Master");
	}catch(Exception e){
		throw e;
	}
		
	}
	
	/* @Description:Login as normal User and User has an active saved card and GV balance
	  * @Author:RaghuKiran MR/Srinivas Hippargi /Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = false, priority = 5, description = "Login as normal User and User has an active saved card and GV balance")
	public void testGVSavedCard() throws Exception{
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_005");
		size = sData[2];
		ss = size.split(",");
		try{
			loginPo.loginApp("ABOF", "TC_Login_005");
			loginPo.handleOkayBtn();
			shoppingBagPo.removeItemInShopping();
			paymentPagePo.addGvAmount("TC_GiftVoucher_001");
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[3]);
			loginPo.handleOkayBtn();
			Thread.sleep(10000);
			browsePlpPdpPo.getElePlpProductName().click();
			loginPo.handleOkayBtn();
			Thread.sleep(10000);
			shoppingBagPo.browserselectSize(ss);
			browsePlpPdpPo.getElePdpAddToBagBtn().click();
			Thread.sleep(5000);
			shoppingBagPo.getEleViewBagBtn().click();
			Thread.sleep(10000);
			String PriceTxt=checkOutPagePo.getEleOfferPriceTxt().getText();
			System.out.println(PriceTxt);
			int iPayableAmount=browsePlpPdpPo.convertPriceToIntValue(PriceTxt);
			System.out.println(iPayableAmount);
			Thread.sleep(5000);
			BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
			shoppingBagPo.getElePlaceOrderBtn().click();
			Thread.sleep(10000);
			checkOutPagePo.getEleDeliverAddressBtn().click();
			Thread.sleep(10000);
			Assert.assertTrue(paymentPagePo.getEleGiftBuckdBalanceTxt().isDisplayed(), "The Gift Bucks Balance text is not displayed");
			NXGReports.addStep("The Gift Bucks Balance is displayed", LogAs.PASSED, null);
			GenericLib.isVisible(GenericLib.path + "Mastercard.PNG");
			Thread.sleep(4000);
			paymentPagePo.abofGVSegmentVerification(iPayableAmount);
			BaseLib.swipeBottomToTop(.22, .90);
			BaseLib.swipeBottomToTop(.22, .90);
			paymentPagePo.savecard("Visa");
			Thread.sleep(40000);
			paymentPagePo.verifyContentPaymentThankYouMessage("Master");
			BaseLib.swipeBottomToTop(.90, .22);
			String sOrderText=driver.findElement(By.id("com.abof.android:id/txtGiftCardValue")).getText();
			System.out.println(sOrderText);
			String sOderGitText=driver.findElement(By.id("com.abof.android:id/txtGiftCardValue")).getText();
			System.out.println(sOrderText);
			String sOderTotalAmount=driver.findElement(By.id("com.abof.android:id/txtOrderTotalValue")).getText();
			System.out.println(sOderTotalAmount);
			Assert.assertNotEquals(sOrderText, sOderTotalAmount);
			Assert.assertTrue(paymentPagePo.getEleContinueShoppingBtn().isDisplayed(), "The Continue shopping button is not displayed");
			NXGReports.addStep("The Continue shopping button is displayed", LogAs.PASSED, null);
			paymentPagePo.getEleContinueShoppingBtn().click();
			Assert.assertTrue(whatsHotLandingPo.getEleWhatsHotModule().isDisplayed(), "The Whats Hot Module text is not displayed");
			NXGReports.addStep("The Whats Hot Module text is displayed", LogAs.PASSED, null);
			
		}catch(Exception e){
		throw e;
	}
		
	}
	/* @Description:Login as normal user and Delete the saved card in the savecard section
	  * @Author:RaghuKiran MR/Srinivas Hippargi /Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = false, priority =6, description = "Login as abof user add to cart select save card and Check the contents of Payment screen")
	public void testDeleteSavecard(String device) throws Exception {
		try {
			loginPo.loginApp("ABOF", "TC_Login_005");
			loginPo.handleOkayBtn();
			Thread.sleep(5000);
			homePagePo.getEleHamburgerMenuIcon().click();
			Thread.sleep(5000);
			hamburgerMenuPo.getEleUserProfileNameTxt().click();
			Thread.sleep(5000);
			for(int i=0;i<=4;i++){
			BaseLib.swipeBottomToTop(.90, .22);
			}
			Thread.sleep(5000);
			hamburgerMenuPo.getEleProSavedCardIcon().click();
			Thread.sleep(5000);
			BaseLib.swipeBottomToTop(.90, .22);
			BaseLib.elementStatus(hamburgerMenuPo.getEleDeleteCardBtn(), "The Delete Saved Card Button", "displayed");
			Thread.sleep(5000);
			hamburgerMenuPo.getEleDeleteCardBtn().click();
			hamburgerMenuPo.getEleDeleteConfirmBtn().click();
			String Deletetext=BaseLib.verifyToastMessage(driver);
			System.out.println(Deletetext);
			Assert.assertTrue(Deletetext.contains("credit card deleted successfully."), "The Saved card is not deleted in the saved card section");
			NXGReports.addStep("The Saved card is deleted in the saved card section", LogAs.PASSED, null);
			Thread.sleep(4000);
		} catch (Exception e) {
			throw e;

		}

	}
	
	/* @Description:Login as normal User and User has an active saved card,GV balance,Abof bucks balance.
	  * @Author:RaghuKiran MR/Srinivas Hippargi /Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = false, priority = 7, description = "Login as normal User and User has an active saved card and GV balance")
	public void testAbofBucksGVSavedCard() throws Exception{
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_005");
		size = sData[2];
		ss = size.split(",");
		try{
			loginPo.loginApp("ABOF", "TC_Login_005");
			loginPo.handleOkayBtn();
			Thread.sleep(5000);
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[3]);
			loginPo.handleOkayBtn();
			Thread.sleep(10000);
			browsePlpPdpPo.getElePlpProductName().click();
			loginPo.handleOkayBtn();
			Thread.sleep(10000);
			shoppingBagPo.browserselectSize(ss);
			browsePlpPdpPo.getElePdpAddToBagBtn().click();
			Thread.sleep(5000);
			shoppingBagPo.getEleViewBagBtn().click();
			Thread.sleep(10000);
			String PriceTxt=checkOutPagePo.getEleOfferPriceTxt().getText();
			System.out.println(PriceTxt);
			int iPayableAmount=browsePlpPdpPo.convertPriceToIntValue(PriceTxt);
			System.out.println(iPayableAmount);
			Thread.sleep(5000);
			BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
			shoppingBagPo.getElePlaceOrderBtn().click();
			Thread.sleep(10000);
			checkOutPagePo.getEleDeliverAddressBtn().click();
			Thread.sleep(10000);
			Assert.assertTrue(paymentPagePo.getEleGiftBuckdBalanceTxt().isDisplayed(), "The Gift Bucks Balance text is not displayed");
			NXGReports.addStep("The Gift Bucks Balance is displayed", LogAs.PASSED, null);
			Assert.assertTrue(paymentPagePo.getEleAbofBuckBalance().isDisplayed(), "The Abof Bucks Balance text is not displayed");
			NXGReports.addStep("The Abof Bucks Balance is displayed", LogAs.PASSED, null);
			BaseLib.swipeBottomToTop(.90, .22);
			BaseLib.swipeBottomToTop(.90, .22);
			String AbofValueTxt=paymentPagePo.getEleAbofValueTxt().getText();
			int AbofValueAmount=browsePlpPdpPo.convertPriceToIntValue(AbofValueTxt);
			System.out.println(AbofValueAmount);
			String AbofGiftValueTxt=paymentPagePo.getEleGiftCrdValueTxt().getText();
			int GiftValueAmount=browsePlpPdpPo.convertPriceToIntValue(AbofGiftValueTxt);
			System.out.println(GiftValueAmount);
			int AmountPaidAbofBuckGiftVoucher=AbofValueAmount+GiftValueAmount;
			int remainingBalanceToBePaid=iPayableAmount-AmountPaidAbofBuckGiftVoucher;
			String TotalOrderValueTxt=paymentPagePo.getEleOrderTotalValueTxt().getText();
			int TotalOrderAmount=browsePlpPdpPo.convertPriceToIntValue(TotalOrderValueTxt);
			if(remainingBalanceToBePaid==TotalOrderAmount){
				NXGReports.addStep("after deduction of abof bucks and gift voucher,remaing payable amount displayed is correct", LogAs.PASSED,null);
			}
			BaseLib.swipeBottomToTop(.22, .90);
			BaseLib.swipeBottomToTop(.22, .90);
			paymentPagePo.savecard("Mastercard");
			
		}catch(Exception e){
			
		}
	}
	
	/* @Description:Login as normal user User has 0 abof bucks and 0 voucher balance, Saved card.
	  * @Author:RaghuKiran MR/Srinivas Hippargi /Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = false, priority = 8, description = "Login as normal user User has 0 abof bucks and 0 voucher balance, Saved card.")
	public void testVerifySavedCard(String device){
		try{
			loginPo.loginApp("ABOF", "TC_Login_005");
			loginPo.handleOkayBtn();
			myfavouritespagePo.handleFavourite();
			shoppingBagPo.removeItemInShoppingBag();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[2]);
			loginPo.handleOkayBtn();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			loginPo.handleOkayBtn();
			browsePlpPdpPo.checkPagination();
			Thread.sleep(3000);
			browsePlpPdpPo.getEleProductImageLst().get(0).click();
			visibilityOfElementWait(browsePlpPdpPo.getElePdpSizeGuideLnk(), "sizeGuideLink");
			browsePlpPdpPo.toVerifyPageContentsDisplay("Pp");
			browsePlpPdpPo.getElePdpSizeGuideLnk().click();
			browsePlpPdpPo.getEleSizeGuideCloseBtn().click();
			sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
			browsePlpPdpPo.pdpFlow(sData[6],"", sData[5]);
			BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
			browsePlpPdpPo.getElePdpAddToBagBtn().click();
			Thread.sleep(1000);
			browsePlpPdpPo.verifyViewBagStaus();
			shoppingBagPo.getEleViewBagBtn().click();
			shoppingBagPo.verifyShoppingBagContent();
			Thread.sleep(5000);
			BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
			shoppingBagPo.getElePlaceOrderBtn().click();
			Thread.sleep(4000);
			checkOutPagePo.getEleDeliverAddressBtn().click();
			Thread.sleep(4000);
			try{
				paymentPagePo.getEleAbofBuckCheckbox().click();
				paymentPagePo.getEleGiftVoucherCheckBx().click();
			}catch(RuntimeException e){
				
			}
			Thread.sleep(4000);
			BaseLib.swipeBottomToTop(.90, .22);
			Thread.sleep(4000);
			BaseLib.swipeBottomToTop(.90, .22);
			paymentPagePo.getEleOrderSummaryOption().click();
			BaseLib.swipeBottomToTop(.90, .22);
			for (int i = 1; i <= 5; i++) {
				List<WebElement> Ordersummarydetails = driver.findElements(
						By.xpath("//android.widget.RelativeLayout[@index='" + i + "']//android.widget.TextView"));
				for (WebElement eleOrdersummarydetailstext : Ordersummarydetails) {
					Assert.assertTrue(eleOrdersummarydetailstext.isDisplayed(),
							eleOrdersummarydetailstext.getText() + " is dispalyed in the oder summary");
					NXGReports.addStep(eleOrdersummarydetailstext.getText() + "is dispalyed in the oder summary",
							LogAs.PASSED, null);
				}

			}
			Thread.sleep(4000);
			BaseLib.swipeBottomToTop(.22, .90);
			Thread.sleep(4000);
			BaseLib.swipeBottomToTop(.22, .90);
			Thread.sleep(4000);
			paymentPagePo.savecard("Visa");
			Thread.sleep(40000);
			paymentPagePo.verifyContentPaymentThankYouMessage("Visa");
		}catch(Exception e){
			
		}
	}
	/* @Description:Login as normal User and User has an active saved card,GV balance,Abof bucks balance.
	  * @Author:RaghuKiran MR/Srinivas Hippargi /Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = true, priority = 9, description = "Login as normal User and User has an active saved card and GV balance")
	public void testGvCombinationSavedCard(){
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		size = sData[2];
		ss = size.split(",");
		
		try{
			loginPo.loginApp("ABOF", "TC_Login_005");
			Thread.sleep(5000);
			loginPo.handleOkayBtn();
			sData=GenericLib.toReadExcelData("Login", "TC_WHT_004");
			whatsHotLandingPo.getwomenSubmenu(sData[2],sData[3]);
			loginPo.handleOkayBtn();
			Thread.sleep(10000);
			sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
			browsePlpPdpPo.pdpFlow(sData[6],"","0");
			
			
		}catch(Exception e){
			
		}
	}
}
