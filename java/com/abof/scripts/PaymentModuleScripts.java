/***********************************************************************
* @author 			:		Raghukiran MR /Srinivas Hippargi /Prerana Bhatt
* @description		: 		Test scripts of Payment Module
* @module			:		Payment
* @testmethod		:	   	testPaymentScreenContent()
*/
package com.abof.scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.abof.library.BaseLib;
import com.abof.library.GenericLib;
import com.abof.pageobjects.BrowsePlpPdpPO;
import com.abof.pageobjects.CheckOutPagePO;
import com.abof.pageobjects.HamburgerMenuPO;
import com.abof.pageobjects.HomePagePO;
import com.abof.pageobjects.LoginPagePO;
import com.abof.pageobjects.MyFavouritesPagePO;
import com.abof.pageobjects.PaymentPagePO;
import com.abof.pageobjects.ShoppingBagPO;
import com.abof.pageobjects.WhatsHotLandingPO;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;

import io.appium.java_client.android.AndroidKeyCode;

public class PaymentModuleScripts extends BaseLib {

	LoginPagePO loginPo = null;
	HomePagePO homePagePo = null;
	HamburgerMenuPO hamburgerMenuPo = null;
	WhatsHotLandingPO whatsHotLandingPo = null;
	BrowsePlpPdpPO browsePlpPdpPo = null;
	ShoppingBagPO shoppingBagPo = null;
	CheckOutPagePO checkOutPagePo = null;
	PaymentPagePO paymentPagePo = null;
	MyFavouritesPagePO myfavouritespagePo;
	String productSaveForLater = null;
	String removedProductFromFav = null;
	String[] sData = null;
	String size = null;
	String pin = null;
	String[] ss = null;
	String text = null;
	String PDP = null;
	String ProductName = null;

	@BeforeMethod
	public void init() {
		loginPo = new LoginPagePO(driver);
		homePagePo = new HomePagePO(driver);
		hamburgerMenuPo = new HamburgerMenuPO(driver);
		whatsHotLandingPo = new WhatsHotLandingPO(driver);
		browsePlpPdpPo = new BrowsePlpPdpPO(driver);
		shoppingBagPo = new ShoppingBagPO(driver);
		checkOutPagePo = new CheckOutPagePO(driver);
		paymentPagePo = new PaymentPagePO(driver);
		myfavouritespagePo = new MyFavouritesPagePO(driver);
	}
	
	 /* @Description:Login as ABOF user, add to cart and Check the contents of Payment screen
	  * @Author:RaghuKiran MR/Srinivas Hippargi /Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = true, priority = 1, description = "Login as ABOF user, add to cart and Check the contents of Payment screen")
	public void testPaymentScreenContent(String device) throws Exception {
		try {

			loginPo.loginApp("ABOF", "TC_Login_001");
			loginPo.handleOkayBtn();
			myfavouritespagePo.handleFavourite();
			shoppingBagPo.removeItemInShoppingBag();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[2]);
			loginPo.handleOkayBtn();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			browsePlpPdpPo.checkPagination();
			browsePlpPdpPo.getEleProductImageLst().get(0).click();
			visibilityOfElementWait(browsePlpPdpPo.getElePdpSizeGuideLnk(), "sizeGuideLink");
			browsePlpPdpPo.toVerifyPageContentsDisplay("Pdp");
			browsePlpPdpPo.getElePdpSizeGuideLnk().click();
			browsePlpPdpPo.getEleSizeGuideCloseBtn().click();
			sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
			browsePlpPdpPo.pdpFlow(sData[2], sData[3], sData[4]);
			browsePlpPdpPo.pdpFlow(sData[6],"", sData[5]);
			BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
			browsePlpPdpPo.getElePdpAddToBagBtn().click();
			Thread.sleep(1000);
			browsePlpPdpPo.verifyViewBagStaus();
			shoppingBagPo.getEleShoppingBagIcon().click();
			shoppingBagPo.verifyShoppingBagContent();
			BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
			shoppingBagPo.getElePlaceOrderBtn().click();
			checkOutPagePo.fillCheckOutDetails();
			paymentPagePo.paymentoption();
			paymentPagePo.addpaymentdetailswithoutsSaveCard("Debit Card","TC_PMT_001");;
		} catch (Exception e) {
			throw e;
		}
	}
	 /* @Description:Login as normal user men (abof),Check the LDP,Edit size and qtyin shopping bag,Edit and delete in the checkoutpage and Check the savecard checkbox 
	  * @Author:RaghuKiran MR/Srinivas Hippargi /Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = true, priority = 3, description = "Login as Abof user,Check the LDP,Edit size and qtyin shopping bag,Edit and delete in the checkoutpage and Check the savecard checkbox ")
	public void testWhatHotLandingLookDetails(String device) throws Exception {
		try {

			loginPo.loginApp("ABOF", "TC_Login_001");
			loginPo.handleOkayBtn();
			myfavouritespagePo.handleFavourite();
			browsePlpPdpPo.getEleBackBtn().click();
			whatsHotLandingPo.handleLookDetailsPage();
			Thread.sleep(5000);
			shoppingBagPo.getEleShoppingBagIcon().click();
			shoppingBagPo.editShoppingBag();
			loginPo.handleOkayBtn();
			BaseLib.swipeBottomToTop(.90, .22);
			Thread.sleep(2000);
			browsePlpPdpPo.getEleInfoCareExpandableIcon().click();
			Thread.sleep(2000);
			browsePlpPdpPo.getEleInfoCareExpandableIcon().click();
			Thread.sleep(5000);
			BaseLib.swipeBottomToTop(.90, .22);
			Thread.sleep(5000);
			for (WebElement eleInfoCareText : browsePlpPdpPo.getEleInfoCareTxt()) {

				Assert.assertTrue(eleInfoCareText.isDisplayed(),
						eleInfoCareText.getText() + " is dispalyed in the info care details");
				NXGReports.addStep(eleInfoCareText.getText() + "is dispalyed in the info care details", LogAs.PASSED,
						null);
			}
			Thread.sleep(3000);
			BaseLib.swipeBottomToTop(.22, .90);
			browsePlpPdpPo.getEleInfoCareExpandableIcon().click();
			BaseLib.swipeBottomToTop(.22, .90);
			shoppingBagPo.getEleShoppingBagIcon().click();
			Thread.sleep(5000);
			BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
			Thread.sleep(5000);
			shoppingBagPo.getElePlaceOrderBtn().click();
			Thread.sleep(8000);
			checkOutPagePo.handleEditDelete();
			paymentPagePo.paymentoption();
			
			paymentPagePo.addpaymentdetailswithoutsSaveCard("Debit Card","TC_PMT_001");
		} catch (Exception e) {
			throw e;

		}

	}
	 /* @Description:Login as normal user men (abof) add to cart select save card and Check the contents of Payment screen
	  * @Author:RaghuKiran MR/Srinivas Hippargi /Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = true, priority = 2, description = "Login as abof user add to cart select save card and Check the contents of Payment screen")
	public void testSavecardPaymentScreenContent(String device) throws Exception {
		try {
			loginPo.loginApp("ABOF", "TC_Login_001");
			loginPo.handleOkayBtn();
			myfavouritespagePo.handleFavourite();
			shoppingBagPo.removeItemInShoppingBag();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[2]);
			loginPo.handleOkayBtn();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			loginPo.handleOkayBtn();
			browsePlpPdpPo.checkPagination();
			Thread.sleep(3000);
			browsePlpPdpPo.getEleProductImageLst().get(0).click();
			visibilityOfElementWait(browsePlpPdpPo.getElePdpSizeGuideLnk(), "sizeGuideLink");
			browsePlpPdpPo.toVerifyPageContentsDisplay("Pp");
			browsePlpPdpPo.getElePdpSizeGuideLnk().click();
			browsePlpPdpPo.getEleSizeGuideCloseBtn().click();
			sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
			browsePlpPdpPo.pdpFlow(sData[6],"", sData[5]);
			BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
			browsePlpPdpPo.getElePdpAddToBagBtn().click();
			Thread.sleep(1000);
			browsePlpPdpPo.verifyViewBagStaus();
			shoppingBagPo.getEleViewBagBtn().click();
			shoppingBagPo.verifyShoppingBagContent();
			Thread.sleep(5000);
			BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
			shoppingBagPo.getElePlaceOrderBtn().click();
			Thread.sleep(4000);
			checkOutPagePo.getEleDeliverAddressBtn().click();
			Thread.sleep(4000);
			paymentPagePo.savecard("Visa");
			// paymentPagePo.verifyContentPaymentThankYouMessage();
		} catch (Exception e) {
			throw e;

		}

	}
	 /* @Description:Login as Abof user,Check product add from cart to Favourites,Check delete from cart Page 
	  * @Author:RaghuKiran MR/Srinivas Hippargi /Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = true, priority = 4, description = "Login as Abof user,Check product add from cart to Favourites and add from Favourites to Cart,Check delete from cart Page ")
	public void testAddFavoritesShoppingbag(String device) throws Exception {
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		size = sData[2];
		ss = size.split(",");
		try {
			loginPo.loginApp("ABOF", "TC_Login_002");
			loginPo.handleOkayBtn();
			myfavouritespagePo.handleFavourite();
			shoppingBagPo.removeItemInShoppingBag();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[2]);
			loginPo.handleOkayBtn();
			browsePlpPdpPo.getElePlpProductName().click();
			loginPo.handleOkayBtn();
			shoppingBagPo.browserselectSize(ss);
			browsePlpPdpPo.getElePdpAddToBagBtn().click();
			shoppingBagPo.getEleViewBagBtn().click();
			String producttext = shoppingBagPo.getEleBrandName().getText();
			Thread.sleep(5000);
			shoppingBagPo.getEleMoveToFavouritesIcon().click();
			browsePlpPdpPo.getEleBackBtn().click();
			browsePlpPdpPo.getEleBackBtn().click();
			homePagePo.getEleHamburgerMenuIcon().click();
			Thread.sleep(3000);
			hamburgerMenuPo.getEleMyFavouritesLnk().click();
			Thread.sleep(3000);
			WebElement ele = driver.findElement(By.xpath("//android.widget.TextView[@text='" + producttext + "']"));
			Thread.sleep(3000);
			String elementtext = ele.getText();
			Thread.sleep(3000);
			Assert.assertTrue(producttext.contains(elementtext), "the value");
			NXGReports.addStep(text + " The Favorites product text is  displayed", LogAs.PASSED, null);
			Thread.sleep(3000);
			driver.findElement(
					By.xpath("//android.widget.LinearLayout[@index='0']//android.widget.TextView[@text='" + producttext
							+ "']/..//android.widget.LinearLayout[@resource-id='com.abof.android:id/btnAddToBag']"))
					.click();
			shoppingBagPo.browserselectSize(ss);
			myfavouritespagePo.getEleDoneBtn().click();
			shoppingBagPo.getEleShoppingBagIcon().click();
			String Shoppingproducttext = shoppingBagPo.getEleBrandName().getText();
			Assert.assertTrue(elementtext.contains(Shoppingproducttext), "The Product name is not displayed");
			NXGReports.addStep(elementtext + " he Product name text is  displayed", LogAs.PASSED, null);
			shoppingBagPo.getEleShoppingBagDeleteIcon().click();
			Assert.assertTrue(elementtext.contains(Shoppingproducttext), "The Product name is displayed");
			NXGReports.addStep(elementtext + "The Product name is displayed", LogAs.PASSED, null);
			homePagePo.getEleHomeSearchIcon().click();
			BaseLib.elementStatus(homePagePo.getEleSearchEdtBx(), "The Search Text ", "displayed");
			Thread.sleep(5000);
		} catch (Exception e) {
			throw e;

		}

	}
	 /* @Description:Login as normal Abof user,Make payment though credit card
	  * @Author:RaghuKiran MR/Srinivas Hippargi /Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled =true, priority = 5, description = "Login as normal Abof user,Make payment though credit card")
	public void testPaymentCreditCard(String device) throws Exception {
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		size = sData[2];
		ss = size.split(",");
		try {
			loginPo.loginApp("ABOF", "TC_Login_001");
			loginPo.handleOkayBtn();
			myfavouritespagePo.handleFavourite();
			shoppingBagPo.removeItemInShoppingBag();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[2]);
			loginPo.handleOkayBtn();
			browsePlpPdpPo.getElePlpProductName().click();
			loginPo.handleOkayBtn();
			Thread.sleep(4000);
			shoppingBagPo.browserselectSize(ss);
			browsePlpPdpPo.getElePdpAddToBagBtn().click();
			shoppingBagPo.getEleViewBagBtn().click();
			Thread.sleep(5000);
			BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
			shoppingBagPo.getElePlaceOrderBtn().click();
			Thread.sleep(4000);
			checkOutPagePo.getEleDeliverAddressBtn().click();
			paymentPagePo.addpaymentdetailsWithSaveCard("Credit card","TC_PMT_003");
		} catch (Exception e) {
			throw e;

		}
	}
	 /* @Description:Login as normal user men (abof),Apply the filter option,verify the more text,verify the RecentlyViewedTxt,Apply Coupon and apply payment
	  * @Author:RaghuKiran MR/Srinivas Hippargi /Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = true, priority = 6, description = "Login as normal user men (abof),Apply the filter option,verify the more text,verify the RecentlyViewedTxt,Apply Coupon and apply payment")
	public void testApplyFilterMoreTxtAppliedTxtCoupon(String device) throws Exception {
		try {
			loginPo.loginApp("ABOF", "TC_Login_001");
			Thread.sleep(4000);
			loginPo.handleOkayBtn();
			myfavouritespagePo.handleFavourite();
			shoppingBagPo.removeItemInShopping();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[3]);
			loginPo.handleOkayBtn();
			BaseLib.waitForElement(browsePlpPdpPo.getElePlpFilterIcon(), "Plp Filter Icon is displayed", "Plp Filter Icon is not displayed");
			browsePlpPdpPo.getElePlpFilterIcon().click();
			BaseLib.waitForElement(browsePlpPdpPo.getEleFilterCloseBtn(), "Plp CategoryName is displayed", "Plp CategoryName is not displayed");
			browsePlpPdpPo.toVerifyPageContentsDisplay("Filter");
			browsePlpPdpPo.toValidateFilterOptions("Sort", "Newest");
			browsePlpPdpPo.getEleFilterTickBtn().click();
			BaseLib.waitForElement( browsePlpPdpPo.getElePlpCategoryName(),"Plp CategoryName is displayed",
					" Plp CategoryName is not displayed");
			browsePlpPdpPo.getEleProductImageLst().get(1).click();
			loginPo.handleOkayBtn();
			/*browsePlpPdpPo.verifyBottomSegementCarousal(browsePlpPdpPo.getEleMoreLikeItTxt());
			browsePlpPdpPo.verifyBottomSegementCarousal(browsePlpPdpPo.getEleRecentlyViewedTxt());*/
			sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
			BaseLib.waitForElement( browsePlpPdpPo.getElePdpSizeGuideLnk(),"sizeGuideLink is displayed", " sizeGuideLink is not displayed");
			browsePlpPdpPo.pdpFlow(sData[6],"", sData[5]);
			BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());

			BaseLib.waitForElement(browsePlpPdpPo.getElePdpShareLnk(), "PdpShareLink is displayed"," PdpShareLink is not displayed");
			BaseLib.waitForElement(browsePlpPdpPo.getElePdpShareLnk(), "PdpShareLink is displayed"," PdpShareLink is not displayed");
			browsePlpPdpPo.getElePdpShareLnk().click();
			BaseLib.scrollToElement(2, "UP", .80, .20, browsePlpPdpPo.getEleShareFacebookLnk());
			BaseLib.waitForElement(browsePlpPdpPo.getEleShareFacebookLnk()," PdpShareFacebookLink is displayed",
					" PdpShareFacebookLink is not displayed");
			browsePlpPdpPo.getEleShareFacebookLnk().click();
			BaseLib.elementStatus(browsePlpPdpPo.getEleSharedProductInfo(), "SharedProductInfo", "displayed");
			BaseLib.waitForElement(browsePlpPdpPo.getEleFacebookPostBtn(),"FacebookPostBtn is displayed", " FacebookPostBtn is not displayed");
			browsePlpPdpPo.getEleFacebookPostBtn().click();

			String postMsg = verifyToastMessage(driver);
			if (postMsg.contains("You sharedthis post")) {
				NXGReports.addStep(" product is shared in facebook", LogAs.PASSED, null);
			}

			BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
			BaseLib.waitForElement(browsePlpPdpPo.getElePdpAddToBagBtn(), "Add To Bag btn is displayed"," Add To Bag btn is not displayed");
			browsePlpPdpPo.getElePdpAddToBagBtn().click();
			BaseLib.waitForElement(browsePlpPdpPo.getElePdpViewBagStatusMsg(),"View Bag Status Msg is displayed",
					" View Bag Status Msg is not displayed");
			browsePlpPdpPo.verifyViewBagStaus();
			BaseLib.waitForElement(shoppingBagPo.getEleShoppingBagIcon(),"ShoppingBagIcon is displayed", " ShoppingBagIcon is not displayed");
			shoppingBagPo.getEleShoppingBagIcon().click();
			productSaveForLater = shoppingBagPo.getEleShoppingCartProductLst().get(0).getText();
			Thread.sleep(4000);
			shoppingBagPo.getEleShoppingBagApplycouponIcon().click();
			sData = GenericLib.toReadExcelData("Login", "TC_APCOU_002");
			shoppingBagPo.applyCoupon(sData[6], sData[3], sData[4]);
			BaseLib.waitForElement(shoppingBagPo.getEleShoppingBagApplycouponIcon(),"EditIcon is displayed",
					" EditIcon is not displayed");
			shoppingBagPo.getEleShoppingBagApplycouponIcon().click();
			sData = GenericLib.toReadExcelData("Login", "TC_APCOU_002");
			shoppingBagPo.applyCoupon(sData[2], sData[3], sData[4]);
			scrollToElement(2, "UP", .80, .20, shoppingBagPo.getElePlaceOrderBtn());
			shoppingBagPo.getElePlaceOrderBtn().click();
			Thread.sleep(2000);
			checkOutPagePo.getEleDeliverAddressBtn().click();
			paymentPagePo.savecard("Visa");
		} catch (Exception e) {
			throw e;
		}
	}
	 /* @Description:Login as normal user men (abof),Checkbox gets de-selected.
	  * @Author:RaghuKiran MR/Srinivas Hippargi /Prerana Bhatt*/
	@Parameters("device")
	@Test(enabled = false, priority = 7, description = "Login as normal user men (abof),Checkbox gets de-selected.")
	public void testNoSavecardfunctionality() throws Exception{
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		size = sData[2];
		ss = size.split(",");
		try{
		loginPo.loginApp("ABOF", "TC_Login_005");
		Thread.sleep(8000);
		loginPo.handleOkayBtn();
		shoppingBagPo.removeItemInShopping();
		sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
		homePagePo.searchOption(sData[3]);
		loginPo.handleOkayBtn();
		loginPo.handleOkayBtn();
		Thread.sleep(40000);
		browsePlpPdpPo.getElePlpProductName().click();
		loginPo.handleOkayBtn();
		Thread.sleep(40000);
		shoppingBagPo.browserselectSize(ss);
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		shoppingBagPo.getEleViewBagBtn().click();
		Thread.sleep(10000);
		try{
		if(driver.findElement(By.id("com.abof.android:id/out_of_stock_txt")).getText().equals("Out-of-stock")){
		shoppingBagPo.getEleShoppingBagDeleteIcon().click();
		Thread.sleep(10000);
		browsePlpPdpPo.getEleBackBtn().click();
		sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
		homePagePo.searchOption(sData[2]);
		loginPo.handleOkayBtn();
		Thread.sleep(10000);
		browsePlpPdpPo.getEleProductImageLst().get(2).click();
		loginPo.handleOkayBtn();
		Thread.sleep(10000);
		shoppingBagPo.browserselectSize(ss);
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		shoppingBagPo.getEleViewBagBtn().click();
		Thread.sleep(5000);
		BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
		shoppingBagPo.getElePlaceOrderBtn().click();
		Thread.sleep(4000);
		}
		else{
			Thread.sleep(5000);
			BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
			shoppingBagPo.getElePlaceOrderBtn().click();
		}
		}catch(RuntimeException e){
			Thread.sleep(5000);
			BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
			shoppingBagPo.getElePlaceOrderBtn().click();
			Thread.sleep(10000);
			checkOutPagePo.fillCheckOutDetails();
			paymentPagePo.addpaymentdetailswithoutsSaveCard("Credit card","TC_PMT_003");
			Thread.sleep(10000);
			browsePlpPdpPo.getEleBackBtn().click();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleUserProfileNameTxt().click();
			Assert.assertFalse(hamburgerMenuPo.getEleSaveCardText().isDisplayed(), "The Savecard text is displayed");
			NXGReports.addStep("The saved card is not displayed", LogAs.PASSED, null);
		}
		}catch(Exception e){
		throw e;
	}
		
	}
}
