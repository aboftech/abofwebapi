/***********************************************************************
* @author 			:		Srinivas Hippargi
* @description		: 		Test scripts of HamburgerMenu Module
* @module			:		Hamburger Menu
* @testmethod		:	   	testHamburgerOprions()
*/
package com.abof.scripts;

import org.junit.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.abof.library.BaseLib;
import com.abof.library.GenericLib;
import com.abof.pageobjects.BrowsePlpPdpPO;
import com.abof.pageobjects.HamburgerMenuPO;
import com.abof.pageobjects.HomePagePO;
import com.abof.pageobjects.LoginPagePO;
import com.abof.pageobjects.MyFavouritesPagePO;
import com.abof.pageobjects.MyOrdersPagePO;
import com.abof.pageobjects.ProfilePagePO;
import com.abof.pageobjects.WhatsHotLandingPO;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;

public class HamburgerMenuScripts extends BaseLib {
	LoginPagePO loginPo = null;
	HomePagePO homePagePo = null;
	HamburgerMenuPO hamburgerMenuPo = null;
	ProfilePagePO profilePagePo = null;
	WhatsHotLandingPO whatsHotLandingPo = null;
	MyOrdersPagePO myOrdersPagePo = null;
	MyFavouritesPagePO myFavouritePagePo = null;
	BrowsePlpPdpPO browsePlpPdpPo = null;
	String sData[] = null;

	@BeforeMethod
	public void init() {
		loginPo = new LoginPagePO(driver);
		homePagePo = new HomePagePO(driver);
		hamburgerMenuPo = new HamburgerMenuPO(driver);
		profilePagePo = new ProfilePagePO(driver);
		whatsHotLandingPo = new WhatsHotLandingPO(driver);
		myOrdersPagePo = new MyOrdersPagePO(driver);
		myFavouritePagePo = new MyFavouritesPagePO(driver);
		browsePlpPdpPo = new BrowsePlpPdpPO(driver);

	}

	/*
	 * @Description:Validate the hamburger menu for Guset,registered normal user
	 * and social user *
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Test(dataProvider = "getUsers", enabled = true, priority = 1, description = "Validate the hamburger menu for Guset,registered normal user and social use")
	public void testValidateHamburgerMenu(String sUser, String device) throws Exception {
		try {
			if (sUser.equalsIgnoreCase("ABOF")) {
				NXGReports.addStep("ABOF user Hamburger menu validation", LogAs.INFO, null);
				loginPo.loginApp(sUser, "TC_Login_001");
			} else if (sUser.equalsIgnoreCase("FBLogged")) {
				NXGReports.addStep("FB user Hamburger menu validation", LogAs.INFO, null);
				loginPo.loginApp(sUser, "TC_FBLogin_001");
			} else {
				NXGReports.addStep("Guest user Hamburger menu validation", LogAs.INFO, null);
				loginPo.getEleLoginCloseBtn().click();
			}
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay! got it coach is displayed",
					"Okay! got it coach is not displayed");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			BaseLib.elementStatus(hamburgerMenuPo.getEleUserProfileNameTxt(),
					hamburgerMenuPo.getEleUserProfileNameTxt().getText(), "displayed");
			BaseLib.elementStatus(hamburgerMenuPo.getEleUserProfileImg(), "User Profile Image", "displayed");
			BaseLib.elementStatus(hamburgerMenuPo.getElewhatshotLnk(), hamburgerMenuPo.getElewhatshotLnk().getText(),
					"displayed");
			BaseLib.elementStatus(hamburgerMenuPo.getEleShopMenLnk(), hamburgerMenuPo.getEleShopMenLnk().getText(),
					"displayed");
			BaseLib.elementStatus(hamburgerMenuPo.getEleShopWomenLnk(), hamburgerMenuPo.getEleShopWomenLnk().getText(),
					"displayed");
			BaseLib.elementStatus(hamburgerMenuPo.getEleMyOrdersLnk(), hamburgerMenuPo.getEleMyOrdersLnk().getText(),
					"displayed");
			BaseLib.elementStatus(hamburgerMenuPo.getEleMyFavouritesLnk(),
					hamburgerMenuPo.getEleMyFavouritesLnk().getText(), "displayed");
			BaseLib.elementStatus(hamburgerMenuPo.getEleReferaFriendLnk(),
					hamburgerMenuPo.getEleReferaFriendLnk().getText(), "displayed");
			BaseLib.elementStatus(hamburgerMenuPo.getEleReturnAndExchangeLnk(),
					hamburgerMenuPo.getEleReturnAndExchangeLnk().getText(), "displayed");
			BaseLib.elementStatus(hamburgerMenuPo.getEleAbofBucksAndGiftCardLnk(),
					hamburgerMenuPo.getEleAbofBucksAndGiftCardLnk().getText(), "displayed");
			BaseLib.elementStatus(hamburgerMenuPo.getEleAbofSupportLnk(),
					hamburgerMenuPo.getEleAbofSupportLnk().getText(), "displayed");
			BaseLib.elementStatus(hamburgerMenuPo.getEleAboutAbofLnk(), hamburgerMenuPo.getEleAboutAbofLnk().getText(),
					"displayed");
			swipeBottomToTop(.90, .70);
			BaseLib.elementStatus(hamburgerMenuPo.getEleTermsAndConditionsLnk(),
					hamburgerMenuPo.getEleTermsAndConditionsLnk().getText(), "displayed");
			BaseLib.elementStatus(hamburgerMenuPo.getElePrivacyPolicyLnk(),
					hamburgerMenuPo.getElePrivacyPolicyLnk().getText(), "displayed");

		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:SignUp as abof user and verify Hamburger Menu Links
	 * 
	 * @Author:Srinivas Hippargi
	 */
	@Parameters("device")
	@Test(enabled = true, priority = 2, description = "SignUp as abof user and verify Hamburger Menu Links")
	public void testValidateHamburgerMenuLinks(String device) throws InterruptedException {
		try {
			loginPo.signUpUser("ABOF", "TC_SignUp_002");
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Ok Got coach is displayed",
					"Ok Got coach is not displayed");
			loginPo.handleOkayBtn();
			BaseLib.elementStatus(homePagePo.getEleHamburgerMenuIcon(), "Hamburger Menu", "displayed");
			homePagePo.getEleHamburgerMenuIcon().click();
			Thread.sleep(2000);
			hamburgerMenuPo.validateHamburgerMenuOptions(hamburgerMenuPo.getElewhatshotLnk(),
					whatsHotLandingPo.getEleWhatsHotModule());
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.validateHamburgerMenuOptions(hamburgerMenuPo.getEleShopMenLnk(),
					whatsHotLandingPo.getEleMenModule());
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.validateHamburgerMenuOptions(hamburgerMenuPo.getEleShopWomenLnk(),
					whatsHotLandingPo.getEleWomenModule());
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.validateHamburgerMenuOptions(hamburgerMenuPo.getEleMyOrdersLnk(),
					myOrdersPagePo.getEleMyOrdersTxt());
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.validateHamburgerMenuOptions(hamburgerMenuPo.getEleMyFavouritesLnk(),
					myFavouritePagePo.getEleFavouritesTitleTxt());
			driver.navigate().back();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.validateHamburgerMenuOptions(hamburgerMenuPo.getEleReferaFriendLnk(),
					hamburgerMenuPo.getEleReferaFriendTitleTxt());
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.validateHamburgerMenuOptions(hamburgerMenuPo.getEleReturnAndExchangeLnk(),
					hamburgerMenuPo.getEleReturnAndExchangeTitleTxt());
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.validateHamburgerMenuOptions(hamburgerMenuPo.getEleAbofBucksAndGiftCardLnk(),
					hamburgerMenuPo.getEleAbofBucksandGiftCardTitleTxt());
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.validateHamburgerMenuOptions(hamburgerMenuPo.getEleAbofSupportLnk(),
					hamburgerMenuPo.getEleAbofSupportTitleTxt());
			driver.navigate().back();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.validateHamburgerMenuOptions(hamburgerMenuPo.getEleAboutAbofLnk(),
					hamburgerMenuPo.getEleAboutAbofTitleTxt());
			homePagePo.getEleHamburgerMenuIcon().click();
			Thread.sleep(3000);
			swipeBottomToTop(.90, .70);
			hamburgerMenuPo.validateHamburgerMenuOptions(hamburgerMenuPo.getEleTermsAndConditionsLnk(),
					hamburgerMenuPo.getEleTermsAndConditionsTitleTxt());
			homePagePo.getEleHamburgerMenuIcon().click();
			Thread.sleep(3000);
			swipeBottomToTop(.90, .70);
			hamburgerMenuPo.validateHamburgerMenuOptions(hamburgerMenuPo.getElePrivacyPolicyLnk(),
					hamburgerMenuPo.getElePrivacyPolicyTitleTxt());
			homePagePo.getEleHamburgerMenuIcon().click();
			Thread.sleep(3000);
			swipeTopToBottm(.50, .90);
			BaseLib.elementStatus(hamburgerMenuPo.getEleUserProfileImg(), "User profile Image ", "displayed");
			hamburgerMenuPo.getEleUserProfileImg().click();
			BaseLib.elementStatus(profilePagePo.getEleAbofLogoutBtn(), "Abof logout button ", "displayed");
			profilePagePo.getEleAbofLogoutBtn().click();
		} catch (Exception e) {
			throw e;
		}
	}

	@DataProvider
	public Object[][] getUsers() {
		Object[][] user = new Object[3][2];
		String sDevice = GenericLib.getCongigValue(BaseLib.sConfigFile, "DeviceName");
		user[0][0] = "Guest";
		user[0][1] = sDevice;
		user[1][0] = "ABOF";
		user[1][1] = sDevice;
		user[2][0] = "FBLogged";
		user[2][1] = sDevice;
		return user;
	}

}
