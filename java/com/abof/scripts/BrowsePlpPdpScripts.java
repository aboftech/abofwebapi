/***********************************************************************
* @author 			:		Srinivas Hippargi 
* @description		: 		Test scripts of Search valid and invalid
* @module			:		Payment
* @testmethod		:	   	testPaymentScreenContent()
*/
package com.abof.scripts;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.abof.library.BaseLib;
import com.abof.library.GenericLib;
import com.abof.pageobjects.BrowsePlpPdpPO;
import com.abof.pageobjects.HamburgerMenuPO;
import com.abof.pageobjects.HomePagePO;
import com.abof.pageobjects.LoginPagePO;
import com.abof.pageobjects.MyFavouritesPagePO;
import com.abof.pageobjects.MyOrdersPagePO;
import com.abof.pageobjects.ProfilePagePO;
import com.abof.pageobjects.WhatsHotLandingPO;

public class BrowsePlpPdpScripts extends BaseLib{
	LoginPagePO loginPo = null;
	HomePagePO homePagePo = null;
	HamburgerMenuPO hamburgerMenuPo = null;
	ProfilePagePO profilePagePo=null;
	WhatsHotLandingPO whatsHotLandingPo=null;
	MyOrdersPagePO myOrdersPagePo=null;
	MyFavouritesPagePO myFavouritePagePo=null;
	BrowsePlpPdpPO browsePlpPdpPo=null;
	String sData[]=null;
	@BeforeMethod
	public void init() {
		loginPo = new LoginPagePO(driver);
		homePagePo = new HomePagePO(driver);
		hamburgerMenuPo = new HamburgerMenuPO(driver);
		profilePagePo=new ProfilePagePO(driver);
		whatsHotLandingPo=new WhatsHotLandingPO(driver);
		myOrdersPagePo=new MyOrdersPagePO(driver);
		myFavouritePagePo=new MyFavouritesPagePO(driver);
		browsePlpPdpPo=new BrowsePlpPdpPO(driver);
	}
	 /* @Description:Login as FB user in FB app user is not logged in and validate search options for valid and invalid inputs
	  * @Author: Srinivas Hippargi*/
	
	@Parameters("device")
	@Test(enabled = true, priority = 1, description = "Validating search Oprions for Valid and Invaild inputs")
	public void testSearchOptions(String device) throws Exception {
		sData = GenericLib.toReadExcelData("Login", "TC_Search_001");
		try {
			loginPo.loginApp("ABOF", "TC_Login_001");
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Ok Got coach is displayed", "Ok Got coach is not displayed");
			loginPo.handleOkayBtn();
			for(int i=2;i<sData.length;i++){
				browsePlpPdpPo.validateSearchOptions(sData[i]);
			}
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleUserProfileImg().click();
			profilePagePo.getEleAbofLogoutBtn().click();
		}
		catch (Exception e) {
			throw e;
		}
			
		}
		
}
