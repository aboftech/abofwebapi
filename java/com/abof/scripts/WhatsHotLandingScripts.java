/***********************************************************************
* @author 			:		Raghukiran MR
* @description		: 		Test scripts of WhatsHotLanding Module
* @module			:		Payment
* @testmethod		:	   	testPaymentScreenContent()
*/
package com.abof.scripts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.abof.library.BaseLib;
import com.abof.library.GenericLib;
import com.abof.pageobjects.BrowsePlpPdpPO;
import com.abof.pageobjects.HamburgerMenuPO;
import com.abof.pageobjects.HomePagePO;
import com.abof.pageobjects.LoginPagePO;
import com.abof.pageobjects.MyFavouritesPagePO;
import com.abof.pageobjects.MyOrdersPagePO;
import com.abof.pageobjects.ProfilePagePO;
import com.abof.pageobjects.WhatsHotLandingPO;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;

public class WhatsHotLandingScripts extends BaseLib{
	LoginPagePO loginPo = null;
	HomePagePO homePagePo = null;
	HamburgerMenuPO hamburgerMenuPo = null;
	ProfilePagePO profilePagePo=null;
	WhatsHotLandingPO whatsHotLandingPo=null;
	MyOrdersPagePO myOrdersPagePo=null;
	MyFavouritesPagePO myFavouritePagePo=null;
	BrowsePlpPdpPO browsePlpPdpPo=null;
	String sData[]=null;
	@BeforeMethod
	public void init() {
		loginPo = new LoginPagePO(driver);
		homePagePo = new HomePagePO(driver);
		hamburgerMenuPo = new HamburgerMenuPO(driver);
		profilePagePo=new ProfilePagePO(driver);
		whatsHotLandingPo=new WhatsHotLandingPO(driver);
		myOrdersPagePo=new MyOrdersPagePO(driver);
		myFavouritePagePo=new MyFavouritesPagePO(driver);
		browsePlpPdpPo=new BrowsePlpPdpPO(driver);
	}
	
	/* @Description:Signup and Login as Abof Normal User and verify the contenst on whatshot landing page as female user
	 * @Author:RaghuKiran MR*/
	@Parameters("device")
	@Test(enabled = true, priority = 1, description = "SignUP as Abof Normal User and verify the contenst on whatshot landing page as female user")
	public void testVerifyWhatsHotLandingAsFemale(String device) throws Exception {
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_002");
		try {
			loginPo.loginApp("ABOF", "TC_Login_002");
			//loginPo.signUpUser("ABOF", "TC_SignUp_001");
			loginPo.handleOkayBtn();
			BaseLib.elementStatus(whatsHotLandingPo.getEleWhatsHotModule(),"The WhatsHot Text","displayed");
			whatsHotLandingPo.whatsHotLandingPage();
			for(int i=0;i<=4;i++){
				BaseLib.swipeBottomToTop(.22, .90);
			}
			whatsHotLandingPo.getEleBannerViewTab().click();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			driver.navigate().back();
			whatsHotLandingPo.whatsHotlookDetailsPage();
		}
		catch (Exception e) {
			throw e;
		}
			
		}
	/* @Description:Check if the each category menu tiles are expanding and showing the sub-category options by tapping on them
	 * @Author:RaghuKiran MR*/
	@Parameters("device")
	@Test(enabled = true, priority = 2, description = "Check if the each category menu tiles are expanding and showing the sub-category options by tapping on them")
	public void testVerifyWomenCategoryMenuSubCategoryMenu(String device) throws Exception {
		try{
			loginPo.loginApp("ABOF", "TC_Login_002");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleShopWomenLnk().click();
			homePagePo.womencategoryTab();
		}catch(Exception e){
			throw e;
		}
	}
	/* @Description:Check the new-arrival,Trending brands and different brands  tile in women landing page and  App should display the list of new products in PLP
	 * @Author:RaghuKiran MR*/
	@Parameters("device")
	@Test(enabled = true, priority = 3, description = "Check the new-arrival,Trending brands and different brands  tile in women landing page and  App should display the list of new products in PLP")
	public void testWomenSubcategoryPLPPage(String device) throws Exception {
		try{
			loginPo.loginApp("ABOF", "TC_Login_002");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleShopWomenLnk().click();
			homePagePo.womensubcategory();
		}catch(Exception e){
			throw e;
		}
	}
	/* @Description:Check the Clearence sale tile in the women-landing page
	 * @Author:RaghuKiran MR*/
	@Parameters("device")
	@Test(enabled = true, priority = 4, description = "Check the Clearence sale tile in the women-landing page")
	public void testWomenClearenceSale(String device) throws Exception {
		try{
			loginPo.loginApp("ABOF", "TC_Login_002");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleShopWomenLnk().click();
			homePagePo.getEleSaleList().click();
			loginPo.handleOkayBtn();
			for(int k=0;k<=4;k++){
				BaseLib.swipeBottomToTop(.90, .22);
				loginPo.handleOkayBtn();
				for (WebElement elewomenBrandList:browsePlpPdpPo.getEleProductNameLst()){
				Assert.assertTrue(elewomenBrandList.isDisplayed(),
						elewomenBrandList.getText() + "the women product name is  not displayed in the women sale page");
				NXGReports.addStep(
						elewomenBrandList.getText() + "the women product name is displayed in the women sale page",
						LogAs.PASSED, null);
			}
			}
			for(int k=0;k<=4;k++){
				BaseLib.swipeBottomToTop(.22, .90);
				
			}
			browsePlpPdpPo.getEleBackBtn().click();
			for(int i=0;i<=1;i++){
				BaseLib.swipeBottomToTop(.90, .22);
			}
			 for(int i=1;i<=2;i++){
				 int k=i--;
				 BaseLib.swipeBottomToTop(.90,.22);
			 WebElement ele=driver.findElement(By.xpath("//android.widget.RelativeLayout[@index='"+k+"']"));
			 ele.click();
			 Thread.sleep(3000);
			 whatsHotLandingPo.closeviewBtn();
			 Thread.sleep(3000);
			 loginPo.handleOkayBtn();
			  if(homePagePo.getEleProductText().getText().equals("Clearance Sale")){
				Thread.sleep(4000);
				browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
				Assert.assertTrue(browsePlpPdpPo.getEleWomenPlpPage().isDisplayed(), "The women text is not displayed in the plp page");
				NXGReports.addStep("the women text is displayed in the plp page", LogAs.PASSED, null);
				browsePlpPdpPo.getEleBackBtn().click(); 
				BaseLib.swipeBottomToTop(.22,.90);
				homePagePo.getEleviewAllBtn().click();
				Thread.sleep(2000);
				 for (WebElement eleBrandList:homePagePo.getEleBrandList()) {

						Assert.assertTrue(eleBrandList.isDisplayed(),
								eleBrandList.getText() + "Brand A-Z screen is not displayed in the women brand page");
						NXGReports.addStep(
								eleBrandList.getText() + "Brand A-Z screen is displayed in the women brand page",
								LogAs.PASSED, null);
					}
				 for(int j=0;j<=2;j++){
				 BaseLib.swipeBottomToTop(.90,.22);
				 for (WebElement eleBrandList:homePagePo.getEleBrandList()) {

						Assert.assertTrue(eleBrandList.isDisplayed(),
								eleBrandList.getText() + "Brand A-Z screen is not displayed in the women brand page");
						NXGReports.addStep(
								eleBrandList.getText() + "Brand A-Z screen is displayed in the women brand page",
								LogAs.PASSED, null);
					}
				 }
				 sData = GenericLib.toReadExcelData("Login", "TC_WHT_002");
				 homePagePo.getEleBrandSearch().sendKeys(sData[3]);
				 Thread.sleep(2000);
				driver.findElement(By.xpath("//android.widget.ListView[@index='5']//android.widget.LinearLayout//android.widget.TextView[@text='"+sData[3]+"']")).click();
				 Assert.assertTrue(browsePlpPdpPo.getElePlpProductName().isDisplayed(),
						 browsePlpPdpPo.getElePlpProductName().getText() + "The product name is not displayed");
					NXGReports.addStep(
							 browsePlpPdpPo.getElePlpProductName().getText() + "The product name is  displayed",
							LogAs.PASSED, null);
				break;
			  }
			  else{
				  Thread.sleep(3000);
				  BaseLib.swipeBottomToTop(.90,.22);
				  driver.navigate().back();
				  Thread.sleep(3000);
				  BaseLib.swipeBottomToTop(.90,.22);
			  }
			 }
		}catch(Exception e){
			throw e;
		}
	}
	/* @Description:Login As men user,Tap on new-in category verify the list of new-in,Tap on Trending brands verify the list of brands
	 * @Author:RaghuKiran MR*/
	@Parameters("device")
	@Test(enabled = true, priority = 5, description = "Login As men user,Tap on new-in category verify the list of new-in,Tap on Trending brands verify the list of brands")
	public void testMenSubcategoryPLPPage(String device) throws Exception {
		try{
			loginPo.loginApp("ABOF", "TC_Login_002");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleShopMenLnk().click();
			homePagePo.mensubcategory();
		}catch(Exception e){
			throw e;
		}
	}
	
	/* @Description:Check the Clearence sale tile in the women-landing page
	 * @Author:RaghuKiran MR*/
	@Parameters("device")
	@Test(enabled = true, priority = 6, description = "Check the Clearence sale tile in the women-landing page")
	public void testMenClearenceSale(String device) throws Exception {
		try{
			loginPo.loginApp("ABOF", "TC_Login_002");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleShopMenLnk().click();
			homePagePo.getEleSaleList().click();
			loginPo.handleOkayBtn();
			for(int k=0;k<=4;k++){
				BaseLib.swipeBottomToTop(.90, .22);
				Assert.assertTrue(browsePlpPdpPo.getElePlpProductName().isDisplayed(), "The men text is not displayed in the sale plp page");
				NXGReports.addStep("the men text is displayed in the sale plp page", LogAs.PASSED, null);
			}
			for(int k=0;k<=4;k++){
				BaseLib.swipeBottomToTop(.22, .90);
				
			}
			browsePlpPdpPo.getEleBackBtn().click();
			BaseLib.swipeBottomToTop(.90, .22);
			 for(int i=1;i<=2;i++){
				 int k=i--;
				 BaseLib.swipeBottomToTop(.90,.22);
			 WebElement ele=driver.findElement(By.xpath("//android.widget.RelativeLayout[@index='"+k+"']"));
			 ele.click();
			 Thread.sleep(3000);
			 whatsHotLandingPo.closeviewBtn();
			 Thread.sleep(3000);
			 loginPo.handleOkayBtn();
			  if(homePagePo.getEleProductText().getText().equals("Clearance Sale")){
				Thread.sleep(4000);
				browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
				Assert.assertTrue(browsePlpPdpPo.getEleMenPlpPage().isDisplayed(), "The men text is not displayed in the plp page");
				NXGReports.addStep("the men text is displayed in the plp page", LogAs.PASSED, null);
				
				browsePlpPdpPo.getEleBackBtn().click(); 
				homePagePo.getEleviewAllBtn().click();
				Thread.sleep(2000);
				 for (WebElement eleBrandList:homePagePo.getEleBrandList()) {

						Assert.assertTrue(eleBrandList.isDisplayed(),
								eleBrandList.getText() + "Brand A-Z screen is not displayed in the men brand page");
						NXGReports.addStep(
								eleBrandList.getText() + "Brand A-Z screen is displayed in the men brand page",
								LogAs.PASSED, null);
					}
				 for(int j=0;j<=4;j++){
				 BaseLib.swipeBottomToTop(.90,.22);
				 for (WebElement eleBrandList:homePagePo.getEleBrandList()) {

						Assert.assertTrue(eleBrandList.isDisplayed(),
								eleBrandList.getText() + "Brand A-Z screen is not displayed in the men brand page");
						NXGReports.addStep(
								eleBrandList.getText() + "Brand A-Z screen is displayed in the men brand page",
								LogAs.PASSED, null);
					}
				 }
				 sData = GenericLib.toReadExcelData("Login", "TC_WHT_003");
				 homePagePo.getEleBrandSearch().sendKeys(sData[3]);
				 Thread.sleep(2000);
				driver.findElement(By.xpath("//android.widget.ListView[@index='5']//android.widget.LinearLayout//android.widget.TextView[@text='"+sData[3]+"']")).click();
				 Assert.assertTrue(browsePlpPdpPo.getElePlpProductName().isDisplayed(),
						 browsePlpPdpPo.getElePlpProductName().getText() + "The product name is not displayed");
					NXGReports.addStep(
							 browsePlpPdpPo.getElePlpProductName().getText() + "The product name is  displayed",
							LogAs.PASSED, null);
				break;
			  }
			  else{
				  Thread.sleep(3000);
				  BaseLib.swipeBottomToTop(.90,.22);
				  driver.navigate().back();
				  Thread.sleep(3000);
				  BaseLib.swipeBottomToTop(.90,.22);
			  }
			 }
		}catch(Exception e){
			throw e;
		}
	}
	/* @Description:Check if the each category menu tiles are expanding and showing the sub-category options by tapping on them
	 * @Author:RaghuKiran MR*/
	@Parameters("device")
	@Test(enabled = true, priority = 7, description = "Check if the each category menu tiles are expanding and showing the sub-category options by tapping on them")
	public void testVerifymenCategoryMenuSubCategoryMenu(String device) throws Exception {
		try{
			loginPo.loginApp("ABOF", "TC_Login_002");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleShopMenLnk().click();
			homePagePo.mencategoryTab();
		}catch(Exception e){
			throw e;
		}
	}
	
	/* @Description:Check if the user can able to share the what's hot story using social mediums like facebook, Twitter & google +,like stories,increased/decreased the like button.
	 * @Author:RaghuKiran MR*/
	@Parameters("device")
	@Test(enabled = true, priority = 8, description = "Check if the user can able to share the what's hot story using social mediums like facebook, Twitter & google +,like stories,increased/decreased the like button.")
	public void testWhatsHotShareStoriesLike(String device) throws Exception {
		try{
			loginPo.loginApp("ABOF", "TC_Login_002");
			loginPo.handleOkayBtn();
			Thread.sleep(2000); 
			 for(int i=1;i<=2;i++){
				 int k=i--;
				 BaseLib.swipeBottomToTop(.90,.22);
				 WebElement ele=driver.findElement(By.xpath("//android.widget.RelativeLayout[@index='"+k+"']"));
				 ele.click();
				 Thread.sleep(2000);
				 whatsHotLandingPo.closeviewBtn();
				 Thread.sleep(2000);
				 loginPo.handleOkayBtn();
				 try{ 
					 System.out.println(whatsHotLandingPo.getEleTrendingStoryTitle().getText());
					 if(whatsHotLandingPo.getEleTrendingStoryTitle().getText().equals("Back to what's hot")){
						  Thread.sleep(2000);
						 BaseLib.swipeBottomToTop(.90,.22);
						 Assert.assertTrue(whatsHotLandingPo.getEleLikesIcon().isDisplayed(), "the Like icon is not displayed");
						 NXGReports.addStep("the Like icon is displayed", LogAs.PASSED, null);
						 Assert.assertTrue(whatsHotLandingPo.getEleWhatHotShareIcon().isDisplayed(), "the share icon is not displayed");
						 NXGReports.addStep("the share icon is displayed", LogAs.PASSED, null);
						 Assert.assertTrue(whatsHotLandingPo.getEleReadMoreIcon().isDisplayed(), "the Readmore icon is not displayed");
						 NXGReports.addStep("the Readmore icon is displayed", LogAs.PASSED, null);
						 Thread.sleep(3000);
						 whatsHotLandingPo.getEleReadMoreIcon().click();
						 Assert.assertTrue(whatsHotLandingPo.getEleReadLessIcon().isDisplayed(), "the ReadLess icon is not displayed");
						 NXGReports.addStep("the ReadLess icon is displayed", LogAs.PASSED, null);
						 Thread.sleep(3000);
						 whatsHotLandingPo.getEleReadLessIcon().click();
						 BaseLib.elementStatus(whatsHotLandingPo.getEleSimilarLooksTxt(), "the Similar looks text ", "displayed");
						 whatsHotLandingPo.getEleLikesIcon().click();
						 whatsHotLandingPo.getEleWhatHotShareIcon().click();
						 //precondtion to run the share the story in the Twitter you have to login first in the twitter app
						 scrollToElement(2,"UP", .80, .20,whatsHotLandingPo.getEleTweetIcon());
						 whatsHotLandingPo.getEleTweetIcon().click();
						BaseLib.elementStatus(whatsHotLandingPo.getEleWCMTxt(), " details page of that particular WCM card", "displayed");
						whatsHotLandingPo.getEleTweetComposerPostBtn().click();
						 BaseLib.elementStatus(whatsHotLandingPo.getEleWhatHotShareIcon(), "the share icon ", "displayed");
						break;
					 }
					  else {
							Thread.sleep(3000);
							 BaseLib.swipeBottomToTop(.90,.22);
							 driver.navigate().back();
							 whatsHotLandingPo.handlenoButton();
							Thread.sleep(3000);
							BaseLib.swipeBottomToTop(.90,.22);
						}
			 }catch(RuntimeException e){
				 Thread.sleep(3000);
				 BaseLib.swipeBottomToTop(.90,.22);
				 driver.navigate().back();
				 whatsHotLandingPo.handlenoButton();
				Thread.sleep(3000);
				BaseLib.swipeBottomToTop(.90,.22);
			 }
				 break; 
			 }
			 
		}catch(Exception e){
			throw e;
		}
	}
	
	/* @Description:Verify the What's Hot contents for a newly registered Male user through Google sign-up
	 * @Author:RaghuKiran MR*/
	@Parameters("device")
	@Test(enabled = true, priority = 9, description = "Verify the What's Hot contents for a newly registered Male user through Google sign-up")
	public void testMenWhatsHotContentsGamilSignup(String device) throws Exception {
		try{
			sData = GenericLib.toReadExcelData("Login", "TC_AddGmail_002");
			loginPo.getEleSignInGoogleLnkTab().click();
			loginPo.getEleChooseAcc(sData[2]).click();
			loginPo.getEleChooseAccOKBtn().click();
			Thread.sleep(5000);
			loginPo.handleOkayBtn();
			BaseLib.elementStatus(whatsHotLandingPo.getEleWhatsHotModule(),"The WhatsHot Text","displayed");
			whatsHotLandingPo.whatsHotLandingPage();
			for(int i=0;i<=4;i++){
				BaseLib.swipeBottomToTop(.22, .90);
			}
			whatsHotLandingPo.getEleBannerViewTab().click();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			driver.navigate().back();
			whatsHotLandingPo.whatsHotlookDetailsPage();
		}catch(Exception e){
			throw e;
		}
	}
	
	/* @Description:Verify the What's Hot contents for a newly registered Female user through Google sign-up
	 * @Author:RaghuKiran MR*/
	@Parameters("device")
	@Test(enabled = true, priority = 10, description = "Verify the What's Hot contents for a newly registered Female user through Google sign-up")
	public void testWomenWhatsHotContentsGamilSignup(String device) throws Exception {
		try{
			sData = GenericLib.toReadExcelData("Login", "TC_AddGmail_003");
			loginPo.getEleSignInGoogleLnkTab().click();
			loginPo.getEleChooseAcc(sData[2]).click();
			loginPo.getEleChooseAccOKBtn().click();
			Thread.sleep(5000);
			loginPo.handleOkayBtn();
			BaseLib.elementStatus(whatsHotLandingPo.getEleWhatsHotModule(),"The WhatsHot Text","displayed");
			whatsHotLandingPo.whatsHotLandingPage();
			for(int i=0;i<=4;i++){
				BaseLib.swipeBottomToTop(.22, .90);
			}
			whatsHotLandingPo.getEleBannerViewTab().click();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			driver.navigate().back();
			whatsHotLandingPo.whatsHotlookDetailsPage();
		}catch(Exception e){
			throw e;
		}
	}
	
	
	/* @Description:Verify the What's Hot contents for a newly registered Male user through Facebook sign-up
	 * @Author:RaghuKiran MR*/
	@Parameters("device")
	@Test(enabled = true, priority = 11, description = "Verify the What's Hot contents for a newly registered Male user through Facebook sign-up")
	public void testMaleWhatsHotFaceBookSignUp(String device) throws Exception {
		try{
				
				/*sData = GenericLib.toReadExcelData("Login", "TC_AddGmail_002");
				driver.startActivity("com.facebook.katana","com.facebook.katana.LoginActivity" );
				loginPo.facebookLogout();
				Thread.sleep(8000);
				loginPo.facebookSign(sData[2],sData[3]);
				driver.startActivity("com.abof.android","com.abof.android.landingpage.view.LandingPageView" );*/
				loginPo.getEleSignInFacebookLnkTab().click();
				NXGReports.addStep("The SignUp as the male user", LogAs.PASSED, null);
				try{
					Thread.sleep(6000);
					loginPo.getEleContinueBtn().click();
					loginPo.handleOkayBtn();
					BaseLib.elementStatus(whatsHotLandingPo.getEleWhatsHotModule(),"The WhatsHot Text","displayed");
					whatsHotLandingPo.whatsHotLandingPage();
					for(int i=0;i<=4;i++){
						BaseLib.swipeBottomToTop(.22, .90);
					}
					whatsHotLandingPo.getEleBannerViewTab().click();
					browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
					driver.navigate().back();
					whatsHotLandingPo.whatsHotlookDetailsPage();
				}catch(RuntimeException e){
					Thread.sleep(5000);
					loginPo.handleOkayBtn();
					BaseLib.elementStatus(whatsHotLandingPo.getEleWhatsHotModule(),"The WhatsHot Text","displayed");
					whatsHotLandingPo.whatsHotLandingPage();
					for(int i=0;i<=4;i++){
						BaseLib.swipeBottomToTop(.22, .90);
					}
					whatsHotLandingPo.getEleBannerViewTab().click();
					browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
					driver.navigate().back();
					whatsHotLandingPo.whatsHotlookDetailsPage();
				}
		}catch(Exception e){
			throw e;
		}
	}
	/* @Description:Verify the What's Hot contents for a newly registered Female user through Facebook sign-up
	 * @Author:RaghuKiran MR*/
	@Parameters("device")
	@Test(enabled = true, priority = 12, description = "Verify the What's Hot contents for a newly registered Male user through Facebook sign-up")
	public void testFemaleWhatsHotFaceBookSignUp(String device) throws Exception {
		try{
				
				sData = GenericLib.toReadExcelData("Login", "TC_AddGmail_003");
				driver.startActivity("com.facebook.katana","com.facebook.katana.LoginActivity" );
				loginPo.facebookLogout();
				Thread.sleep(8000);
				loginPo.facebookSign(sData[2],sData[3]);
				driver.startActivity("com.abof.android","com.abof.android.landingpage.view.LandingPageView" );
				loginPo.getEleSignInFacebookLnkTab().click();
				NXGReports.addStep("The SignUp as the Female user", LogAs.PASSED, null);
				try{
					Thread.sleep(6000);
					loginPo.getEleContinueBtn().click();
					loginPo.handleOkayBtn();
					BaseLib.elementStatus(whatsHotLandingPo.getEleWhatsHotModule(),"The WhatsHot Text","displayed");
					whatsHotLandingPo.whatsHotLandingPage();
					for(int i=0;i<=4;i++){
						BaseLib.swipeBottomToTop(.22, .90);
					}
					whatsHotLandingPo.getEleBannerViewTab().click();
					browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
					driver.navigate().back();
					whatsHotLandingPo.whatsHotlookDetailsPage();
				}catch(RuntimeException e){
					Thread.sleep(5000);
					loginPo.handleOkayBtn();
					BaseLib.elementStatus(whatsHotLandingPo.getEleWhatsHotModule(),"The WhatsHot Text","displayed");
					whatsHotLandingPo.whatsHotLandingPage();
					for(int i=0;i<=4;i++){
						BaseLib.swipeBottomToTop(.22, .90);
					}
					whatsHotLandingPo.getEleBannerViewTab().click();
					browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
					driver.navigate().back();
					whatsHotLandingPo.whatsHotlookDetailsPage();
				}
		}catch(Exception e){
			throw e;
		}
	}
}

