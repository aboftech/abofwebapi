package com.abof.api.clients;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;

import org.testng.Assert;
import static com.jayway.restassured.RestAssured.given;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class cashbackClient {


	public static Response productDetails(String url,String WCToken,String WCTrustedToken, String userId) {

		Response getResponse = given()
				.contentType(ContentType.JSON)
				.header(new Header("WCToken", WCToken))
				.header(new Header("WCTrustedToken", WCTrustedToken))
				.header(new Header("userId", userId))
				.log()
				.everything()
				.expect()
				.log()
				.ifError()
				.when()
				.get(url);

		return getResponse;
	}


	public static void verifyProductDetailsResponse(Response productDetailsResponse) {

		Assert.assertEquals(productDetailsResponse.getStatusCode(), 200);


	}

	public static String getRandomCatalogueSkuIds(Response productDetailsResponse) {
		JsonPath productDetailsResponsePath = new JsonPath(productDetailsResponse.asString());

		Random rand = new Random();
		int catalogue_entry_size = productDetailsResponsePath.getList("catalogEntryView").size();
		String catalogueSkuIds = productDetailsResponsePath.getList("catalogEntryView.sKUs.uniqueID").get(0).toString();
		return catalogueSkuIds.replace("[", "").replace("]", "").replace(" ", "");

	}


	public static String getAvailableProductsId(Response InventoryAvailabilityResponse) {
		JsonPath InventoryAvailabilityResponsePath = new JsonPath(InventoryAvailabilityResponse.asString());

		List<String> productId = InventoryAvailabilityResponsePath.getList("InventoryAvailability.productId");
		List<String> inventoryStatus = InventoryAvailabilityResponsePath.getList("InventoryAvailability.inventoryStatus");
		String product_id = "";

		for(int i=0;i<productId.size();i++){
			if(inventoryStatus.get(i).equals("Available")){
				product_id = productId.get(i);
			}
		}
		
		return product_id;
	}
	
	public static String getAnyProductId(Response InventoryAvailabilityResponse) {
		JsonPath InventoryAvailabilityResponsePath = new JsonPath(InventoryAvailabilityResponse.asString());

		List<String> productId = InventoryAvailabilityResponsePath.getList("InventoryAvailability.productId");
		String product_id = productId.get(0);
		
		return product_id;
	}

	public static void verifyAddToCartResponse(Response addToCartResponse) {
		
		Assert.assertEquals(addToCartResponse.getStatusCode(), 201);
		JsonPath addToCartResponsePath = new JsonPath(addToCartResponse.asString());
		Assert.assertNotNull(addToCartResponsePath.getString("orderId"));

	}
	
public static String getOrderID(Response addToCartResponse) {
		
		Assert.assertEquals(addToCartResponse.getStatusCode(), 201);
		JsonPath addToCartResponsePath = new JsonPath(addToCartResponse.asString());
		return addToCartResponsePath.getString("orderId");

	}

	public static Double getBagAmountFromPaymentORCashbackMethodAPI(Response calculateAPIResponse, Boolean is_aggregate_api) {

		String cashback_field;
		JsonPath calculateAPIResponsePath = new JsonPath(calculateAPIResponse.asString());
		if (is_aggregate_api){
			Assert.assertEquals(calculateAPIResponse.getStatusCode(), 200);
			cashback_field = "cashback.orderTotal";
		}
		else{
			Assert.assertEquals(calculateAPIResponse.getStatusCode(), 200);
			cashback_field = "orderTotal";
		}
		
		return Double.parseDouble(calculateAPIResponsePath.getString(cashback_field));
	}

	public static void verifyCashbackInCalculateAPIResponse(Response calculateAPIResponse, double expected_cashback_amount, String CashbackId, String Cashback_name, String long_desc, String short_desc) {

		Assert.assertEquals(calculateAPIResponse.getStatusCode(), 200);
		JsonPath calculateAPIResponsePath = new JsonPath(calculateAPIResponse.asString());

		Assert.assertTrue(Double.parseDouble(calculateAPIResponsePath.getString("cashback.cashBackAmount")) == expected_cashback_amount, "Cashback Amount does not match,expected="+expected_cashback_amount+",acutal="+Double.parseDouble(calculateAPIResponsePath.getString("cashback.cashBackAmount")));
		Assert.assertTrue(calculateAPIResponsePath.getList("cashback.cashbacks.cashBackPromoName").get(0).toString().startsWith(Cashback_name), "Cashback PromoNAme mismatch");

		Assert.assertEquals(calculateAPIResponsePath.getList("cashback.cashbacks.couponShortDescription").get(0),short_desc, "Cashback Shot Description mismatch");
		Assert.assertEquals(calculateAPIResponsePath.getList("cashback.cashbacks.couponLongDescription").get(0),long_desc, "Cashback Long Description mismatch");
		Assert.assertEquals(calculateAPIResponsePath.getList("cashback.cashbacks.cashbackBucket").get(0),"Bucket A", "Cashback PromoNAme mismatch");

		// Performing extra checks
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		Assert.assertNotNull(calculateAPIResponsePath.getList("cashback.cashbacks.cashbackDate").get(0), "Cashback Date is null");
		Assert.assertEquals(calculateAPIResponsePath.getList("cashback.cashbacks.cashbackExpiry").get(0),1, "Cashback Expiry mismatch");
		Assert.assertNotNull(calculateAPIResponsePath.getString("cashback.orderId"), "Cashback Order Id is null");
		Assert.assertEquals(calculateAPIResponsePath.getString("cashback.event"),"Cashback", "Cashback event is incorrent");
		Assert.assertNotNull(calculateAPIResponsePath.getString("cashback.reevaluationTime"), "Cashback reevaluationTime is null");
		Assert.assertNotNull(calculateAPIResponsePath.getString("cashback.orderDate"), "Cashback order date is null");

	}

	public static void verifyCashbackAPIResponse(Response cashbackAPIResponse, String expected_cashback_amount, String CashbackId, 
			String Cashback_name, String long_desc, String short_desc,String bucket) {

		Assert.assertEquals(cashbackAPIResponse.getStatusCode(), 200);
		JsonPath cashbackAPIResponsePath = new JsonPath(cashbackAPIResponse.asString());
		
		Assert.assertTrue(Double.valueOf(cashbackAPIResponsePath.getString("cashBackAmount"))==Double.parseDouble(expected_cashback_amount), 
				"Cashback Amount does not match" + cashbackAPIResponsePath.getString("cashBackAmount"));
		Assert.assertTrue(cashbackAPIResponsePath.getList("cashbacks.cashBackPromoName").get(0).toString().startsWith(Cashback_name), 
				"Cashback PromoNAme mismatch");

		Assert.assertEquals(cashbackAPIResponsePath.getList("cashbacks.couponShortDescription").get(0),short_desc, "Cashback Shot Description mismatch");
		Assert.assertEquals(cashbackAPIResponsePath.getList("cashbacks.couponLongDescription").get(0),long_desc, "Cashback Long Description mismatch");
		Assert.assertEquals(cashbackAPIResponsePath.getList("cashbacks.cashbackBucket").get(0),bucket, "Cashback PromoNAme mismatch");
	}


	public static void verifyCahbackNotapplicableResponse(Response calculateAPIResponse, double expected_cashback_amount)
	{
		Assert.assertEquals(calculateAPIResponse.getStatusCode(), 200);
		JsonPath calculateAPIResponsePath = new JsonPath(calculateAPIResponse.asString());

		Assert.assertTrue(Double.parseDouble(calculateAPIResponsePath.getString("cashback.cashBackAmount"))==expected_cashback_amount, 
				"Cashback Amount does not match" + calculateAPIResponsePath.getString("cashback.cashBackAmount"));
	}

	public static void readErrorMessageWhenNoCashback(Response calculateAPIResponse) {

		JsonPath calculateAPIResponsePath = new JsonPath(calculateAPIResponse.asString());
		Assert.assertTrue(calculateAPIResponsePath.getString("cashback.errorMessage").startsWith("No Cashback Promotions applicable on this order for"),
				"Cashback response expected to have errorMessage in case of no applicable cahsback but it does not have");
	}

}
