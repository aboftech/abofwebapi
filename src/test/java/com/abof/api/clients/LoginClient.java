package com.abof.api.clients;

import com.abof.api.tests.BaseAPITest;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import org.testng.Assert;

public class LoginClient {

	public static Response login(String url, String email, String password) {

		String login_body = "{\"AppVersion\":\"2.0.0\",\"ClientName\":\"Automation\",\"logonId\":\"" + email 
				+"\",\"logonPassword\":\""+password+"\"}";

		return BaseAPITest.genericPostRequest(url, login_body);
	}
	
	
	

	public static String getWCToken(Response loginResponse) {
		Assert.assertEquals(loginResponse.getStatusCode(), 201);
		JsonPath loginPath = new JsonPath(loginResponse.asString());
		return loginPath.getString("WCToken");
	}
	
	public static String getWCTrustedToken(Response loginResponse) {
		Assert.assertEquals(loginResponse.getStatusCode(), 201);
		JsonPath loginPath = new JsonPath(loginResponse.asString());
		return loginPath.getString("WCTrustedToken");
	}
	
	public static String getUserId(Response loginResponse) {
		Assert.assertEquals(loginResponse.getStatusCode(), 201);
		JsonPath loginPath = new JsonPath(loginResponse.asString());
		return loginPath.getString("userId");
	}

	public static void verifyErrResponse(Response verifyOTP, int retCode, String errMsg) {

	}

}
