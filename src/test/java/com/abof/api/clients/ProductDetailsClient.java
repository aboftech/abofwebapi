package com.abof.api.clients;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;

import org.testng.Assert;
import static com.jayway.restassured.RestAssured.given;

import java.util.Random;

public class ProductDetailsClient {


	public static Response productDetails(String url){

		Response getResponse = given()
				.contentType(ContentType.JSON)
				.log()
				.everything()
				.expect()
				.log()
				.ifError()
				.when()
				.get(url);

		return getResponse;
	}
	
	
	public static void verifyProductDetailsResponse(Response productDetailsResponse) {

		Assert.assertEquals(productDetailsResponse.getStatusCode(), 200);
		

	}
	
	public static String getRandomCatalogueSkuIds(Response productDetailsResponse) {
		JsonPath productDetailsResponsePath = new JsonPath(productDetailsResponse.asString());

		Random rand = new Random();
		int catalogue_entry_size = productDetailsResponsePath.getList("catalogEntryView").size();
		String catalogueSkuIds = productDetailsResponsePath.getList("catalogEntryView.sKUs.uniqueID").get(0).toString();
		return catalogueSkuIds.replace("[", "").replace("]", "").replace(" ", "");
		
	}

}
