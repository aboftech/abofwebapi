package com.abof.api.clients;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.Header;

import org.testng.Assert;

import static com.jayway.restassured.RestAssured.given;

public class AccountMemberCheckClient {


	public static Response accountMemberCheck(String url,String WCToken,String WCTrustedToken, String userId) {

		Response getResponse = given()
				.contentType(ContentType.JSON)
				.header(new Header("WCToken", WCToken))
				.header(new Header("WCTrustedToken", WCTrustedToken))
				.header(new Header("userId", userId))
				.log()
				.everything()
				.expect()
				.log()
				.ifError()
				.when()
				.get(url);

		return getResponse;
	}

	public static void verifyMemberCheckResponse(Response memberCheck, Boolean SocialUser, Boolean Exist) {
		Assert.assertEquals(memberCheck.getStatusCode(), 200);
		JsonPath MemberCheckResponsePath = new JsonPath(memberCheck.asString());
		Assert.assertEquals(MemberCheckResponsePath.getString("isSocialUser"), SocialUser.toString());
		Assert.assertEquals(MemberCheckResponsePath.getString("isExist"), Exist.toString());

	}

	public static void verifyErrResponse(Response verifyOTP, int retCode, String errMsg) {

	}

}
