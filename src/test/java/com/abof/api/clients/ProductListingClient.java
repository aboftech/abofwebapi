package com.abof.api.clients;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import org.testng.Assert;
import static com.jayway.restassured.RestAssured.given;

import java.util.Random;

public class ProductListingClient {


	public static Response productList(String url)
	{

		Response getResponse = given()
				.contentType(ContentType.JSON)
				.log()
				.everything()
				.expect()
				.log()
				.ifError()
				.when()
				.get(url);

		return getResponse;
	}


	public static void verifyProductListResponse(Response productListResponse,int page_no,int page_size) {

		Assert.assertEquals(productListResponse.getStatusCode(), 200);
		JsonPath productListResponsePath = new JsonPath(productListResponse.asString());

		Assert.assertTrue(productListResponsePath.getList("catalogEntryView.uniqueID").size() <= page_size, 
				"Product list API is returning more items than the page size");

		Assert.assertTrue(productListResponsePath.getList("catalogEntryView.uniqueID").size() > 0, 
				"Product list API is returning non positive items");

	}

	public static int getrandomProductId(Response productListResponse,int page_no,int page_size) {

		Assert.assertEquals(productListResponse.getStatusCode(), 200);
		JsonPath productListResponsePath = new JsonPath(productListResponse.asString());
		int list_size = productListResponsePath.getList("catalogEntryView.uniqueID").size();
		Random rand = new Random();
		return Integer.parseInt(productListResponsePath.getList("catalogEntryView.uniqueID").get(rand.nextInt(list_size)).toString());

	}


}
