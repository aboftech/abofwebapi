package com.abof.api.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class db2Connector {
	
		public static Statement stmt;
		public static void connect(String host, String port, String DBName, String username, String password) {
			try {
				Class.forName("com.ibm.db2.jcc.DB2Driver");

				String url = "jdbc:db2://"+host+":"+port+"/"+DBName+":" + "user="+username+";password="+password+";";
				Connection conn = DriverManager.getConnection(url);
				System.out.println("Connected");

				stmt = conn.createStatement();
				//return stmt;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public static Statement connect2QA(String host, String port, String DBName, String username, String password) {
			try {
				Class.forName("com.ibm.db2.jcc.DB2Driver");

				String url = "jdbc:db2://"+host+":"+port+"/"+DBName+":" + "user="+username+";password="+password+";";
				Connection conn = DriverManager.getConnection(url);
				System.out.println("Connected");

				stmt = conn.createStatement();
				return stmt;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public static void quitDBConnection() {
			try {
				stmt.close();
				//System.out.println("DataBase Disconnected");

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		public static List<String> listOfURLs(String sql){
			List<String> urls = new ArrayList<String>();
			connect("wcs-db2.prod.abof.com","50000","ABOFDB","automation","VmZ3fNKgp");
			try{
				ResultSet rs = stmt.executeQuery(sql);
				while(rs.next())
                {
					urls.add(rs.getString(1));
                }
				return urls;
			}
			catch (Exception e){
				e.printStackTrace();
				System.out.println("Exception occured.");
				return urls;
			}
			finally {
				quitDBConnection();
			}
		}
		public static Map<String, String> getQueryResult(String sql){
			Map<String, String> result = new HashMap<String, String>();
			connect("wcs-db2.prod.abof.com","50000","ABOFDB","automation","VmZ3fNKgp");
			try{
				ResultSet rs = stmt.executeQuery(sql);
				while(rs.next()){
					//System.out.println("TITLE :"+ rs.getString(1));
					result.put("TITLE", rs.getString(1));
					//System.out.println("META_DESC :"+ rs.getString(2));
					result.put("META_DESC", rs.getString(2));
					//System.out.println("CANONICAL_TAG :"+ rs.getString(3));
					result.put("CANONICAL_TAG", rs.getString(3));
					//System.out.println("FOOTER :"+ rs.getString(4));
					result.put("FOOTER", rs.getString(4));
					//System.out.println("HEADER :"+ rs.getString(5));
					result.put("HEADER", rs.getString(5));
				}
				return result;
			}
			catch (Exception e){
				e.printStackTrace();
				System.out.println("Exception occured.");
				return result;
			}
			finally {
				quitDBConnection();
			}
		}
		
		public static Map<String, String> verifyCashBackEntryRecordinWCSDB(String orderId){
			Map<String, String> result = new HashMap<String, String>();
			stmt = db2Connector.connect2QA("wcsdb1.qa.abof.com","60000","ABSITDB","db2inst1","db2inst1");
			
			try{
				ResultSet rs = stmt.executeQuery("SELECT ORDERS_ID,CASHBACK_ID,AMOUNT,DESCRIPTION,CASHBACKDATE FROM WCSADMIN.XPROMOARG where ORDERS_ID IN ('"+orderId+"')");
				while(rs.next()){
					result.put("ORDERS_ID", rs.getString(1));
					result.put("CASHBACK_ID", rs.getString(2));
					result.put("AMOUNT", rs.getString(3));
					result.put("DESCRIPTION", rs.getString(4));
					result.put("CASHBACKDATE", rs.getString(5));
				}
				return result;
			}
			catch (Exception e){
				e.printStackTrace();
				System.out.println("Exception occured.");
				return result;
			}
			finally {
				quitDBConnection();
			}
		}
		
}
