package com.abof.api.utils;

import java.util.HashMap;
import java.util.Map;

//import java.nio.charset.StandardCharsets;
//import java.util.Base64;

/**
 * Created by ashifa.sunar on 3/5/2017.
 */
public class Constants {

	public static final String ABOF_HOST = PropertyLoader.TEST_ENVIRONMENT.getProperty("abof.host");
	public static final String BASE_URL = ABOF_HOST + "/wcs/resources/store/"+PropertyLoader.TEST_ENVIRONMENT.getProperty("abof_store_key");


	public static final String ABOF_CATALOGUE_ID = PropertyLoader.TEST_ENVIRONMENT.getProperty("abof_catalogId");


	public static final String MEMBER_CHECK_URL = BASE_URL + "/account/memberCheck?";
	public static final String LOGIN_URL = BASE_URL + "/loginidentity?catalogId=" + ABOF_CATALOGUE_ID;


	public static final String ABOF_PRODUCT_LIST_URL = ABOF_HOST + "/search/resources/store/"
			+PropertyLoader.TEST_ENVIRONMENT.getProperty("abof_store_key") 
			+"/productview/bySearchTerm/search_keyword?searchType=1000&pageNumber=page_no&pageSize=page_size"
			+"&profileName=mABOF_findProductsBySearchTerm&catalogId=" + ABOF_CATALOGUE_ID;


	public static final String ABOF_PRODUCT_DETAILS_URL = ABOF_HOST + "/search/resources/store/"
			+PropertyLoader.TEST_ENVIRONMENT.getProperty("abof_store_key") + "/productview"+
			"/byId/product_id?profileName=mABOF_findProductsById&catalogId="+ ABOF_CATALOGUE_ID;

	public static final String MEN_FORMAL_SHOES_CATEGORY_ID = "51483";
	public static final String WOMEN_ETHINIC_WEAR_CATEGORY_ID = "51473";
	public static final String KURTA_AND_KURTI = "51515";
	public static final String WOMEN_FOOTWEAR = "51474";
	public static final String WOMEN_SANDALS_AND_HEELS = "51520";
	public static final String WOMEN_TOPS_AND_TEES = "51513";
	public static final String WOMEN_SHORTS_AND_SKIRTS = "51514";
	public static final String WOMEN_DRESSES = "51534";

	public static final String ABOF_SEARCH_BY_CATGEORY_URL = ABOF_HOST.replace("https", "http") + "/search/resources/store/10154/productview/byCategory/"+
			"category_id" +"?profileName=mABOF_findProductsByCategory&catalogId="+ ABOF_CATALOGUE_ID;

	public static final String ABOF_INVENTORY_AVAILABILITY_URL = BASE_URL + 
			"/inventoryavailability/productSKUIds?catalogId=10101";

	public static final String ABOF_AGG_CALCULATE_URL = BASE_URL + "/cart/aggregate/calculate";
	public static final String ABOF_PERSON_URL = BASE_URL + "/person/@self";

	public static final String ABOF_CALCULATE_COD_URL = BASE_URL + "/cashback?paymentMethod=COD";
	public static final String ABOF_CALCULATE_PREPAID_URL = BASE_URL + "/cashback?paymentMethod=PREPAID";

	public static final String ABOF_ADD_TO_CART_URL = BASE_URL + "/cart?catalogId=" + ABOF_CATALOGUE_ID;
	public static final String ABOF_DELETE_CART_URL = BASE_URL + "/cart/@self?catalogId=" + ABOF_CATALOGUE_ID;

	public static final String ABOF_GET_WCS_ORDER_DETAILS = BASE_URL + "/order/orderID";

	public static final String ABOF_GET_CART_URL = BASE_URL + "/cart/aggregate/calculate?catalogId=" + ABOF_CATALOGUE_ID;


	public static final String ABOF_GET_PRODUCT_INFO_URL = BASE_URL + 
			"/productview/byIds?profileName=IBM_findProductByIds_Summary&id=434974&catalogId="+ ABOF_CATALOGUE_ID;

	public static final String ABOF_EDIT_CART_URL = BASE_URL + "/cart/aggregate/update_cart_item?catalogId=" + ABOF_CATALOGUE_ID;

	public static final String ABOF_DELETE_CART_ITEM_URL = BASE_URL + "/cart/aggregate/delete_cart_item?catalogId=" + ABOF_CATALOGUE_ID;

	public static final String ABOF_MOVE_TO_FAV_URL = BASE_URL + "/cart/aggregate/moveItemToFav?catalogId=" + ABOF_CATALOGUE_ID;

	public static final String ABOF_ADD_PI_URL = BASE_URL + "/cart/@self/payment_instruction?catalogId=" + ABOF_CATALOGUE_ID;

	public static final String ABOF_MEGAMENU_CATEGORY_LIST_URL = ABOF_HOST + "/MegaMenuJson?catalogId" + ABOF_CATALOGUE_ID+"&storeId=" 
			+ PropertyLoader.TEST_ENVIRONMENT.getProperty("abof_store_key");
	public static final String ABOF_SEO_BASE_URL = ABOF_HOST + "/seo/api";

	public static final String ABOF_SOLR_SEARCH_API = "http://solr.api.abof.com/search/resources/store/10154/productview"
			+ "/byCategory/categoryID?profileName=mABOF_findProductsByCategory&searchSource=E&catalogId=10101";

	public static final String ADD_INVENTORY_URL = "http://srv01.qa.abof.com:8099/service/testdata/addinventory/itemid/"+"available_product_sku_id"+"/qty/10";
	public static final String addtoCArtbody = "{\"orderItem\":[{\"productId\":\""+"available_product_sku_id"+"\",\"quantity\":\"1\"}],\"x_device\":\"mobile\"}";
	public static final String CALCULATE_API_URL = "https://wcs11.qa.abof.com/wcs/resources/store/10154/calculateamtpayable?responseFormat=json&catalogId=10101";

	public static final String JustPayBody = "{\"addressId\":\"246567\",\"catalogId\":\"10101\",\"channel\":\""+"platform"+"-"+"version"+"-"+"major_version"+"\","
			+ "\"emailId\":\""+"user_email"+"\",\"firstname\":\"cashback\",\"mobileNo\":\"9003092667\","
			+ "\"newPgSelect\":\"true\",\"orderId\":\""+"order_id"+"\",\"payMethod\":\"DebitCard\","
			+ "\"productinfo\":\""+"product_info"+"\",\"qcWalletUsed\":\"0\""
			+ ",\"saveCard\":\"false\",\"orderAmount\":\""+"order_Amount"+"\",\"storeCredit\":\"0\"}";

	public static final String ABOF_PRODUCT_VIEW_BY_ID = "https://wcs11.qa.abof.com/search/resources/store/10154/productview/byId/"
			+"ProductId"+"?profileName=mABOF_findProductsById&catalogId=10101";

	public static final String checkout_body = "{\"inferred_payment_status\":\"UNKNOWN\",\"signature_algorithm\":\"HMAC-SHA256\","
			+ "\"signature\":\"Xw4qYnA9LRMQctG2pvVqHsd2t9XbNWSsZzdyprY9THM=\","
			+ "\"order_id\":\""+"justPay_OrderId"+"\",\"status\":\"CHARGED\",\"pg\":\"Juspay\",\"status_id\":\"21\",\"clientVATenabled\":\"true\","
			+ "\"notifyOrderSubmitted\":\"1\",\"catalogId\":\"10101\",\"firstName\":\"cashback\",\"amount\":\"1195.0\",\"addressStreet1\":\"\","
			+ "\"addressState\":\"Karnataka\",\"giftCardTransID\":\"\",\"paymentMethod\":\"CreditCard\",\"channel\":\"Android-39-2.0.0\","
			+ "\"addressCountry\":\"IN\",\"addressStreet2\":\"13\nSpartan Heights Apartment, 16/17, Richmond Road, Shanthala Nagar, Ashok Nagar\","
			+ "\"shippingaddressid\":\"246567\",\"orderId\":\""+"orderid"+"\",\"giftCardPayMethod\":\"0\",\"storeCreditTransID\":\"\","
			+ "\"email\":\"cashback.6106@gmail.com\",\"purchaseorder_id\":\"\",\"storeId\":\"10154\",\"storeCreditPayMethod\":\"0\","
			+ "\"addressCity\":\"Bengaluru\",\"addressZip\":\"560004\",\"isCOD\":\"false\",\"lastName\":\"Test\","
			+ "\"notifyShopper\":\"1\",\"notifyMerchant\":\"1\",\"langId\":\"-1\"}";
	
	public static final String justPay_redirection_url = "http://wcs11.qa.abof.com/";
	public static final String preCheckoutAPIBody = "{\"catalogId\":\"10101\",\"clientVATenabled\":\"true\",\"langId\":-1,\"storeId\":10154,\"abofBucksUsed\":\"0\",\"qcWalletUsed\":\"0\"}";

	public static final String preCheckoutAPI = "https://wcs11.qa.abof.com/wcs/resources/store/10154/cart/@self/precheckout?catalogId=10101";

	public static final String JusPayCommandLineParams = "?order_id="+"juspay_order_id"+"&merchant_id=abof&payment_method_type=CARD"
			+ "&payment_method=MASTERCARD&card_number="+"cardNumber"
			+ "&card_exp_month="+"cardExpMonth"+"&card_exp_year="+"cardExpYear"+"&name_on_card=Name&card_security_code="+"cardSecurityCode"
			+ "&save_to_locker=true&redirect_after_payment=false&format=json";
	
	public static final String ABOF_CHECKOUT_URL ="https://wcs11.qa.abof.com/wcs/resources/store/10154/cart/@self/checkout?orderId="+"order_id";
	public static final String WCS_ORDER_DETAIL_API = "https://wcs11.qa.abof.com/wcs/resources/store/10154/order/"+"order_id";
	
	public static final String JUSTPAY_SANDBOX_URL = "https://sandbox.juspay.in/txns"+"CommandLineParams";
	public static final String JUSTPAY_SANDBOX_ORDERS_URL ="https://sandbox.juspay.in/orders/"+ "justPay_OrderId";
	
	public static final String RazorPay_success_button = "//input[@class='success']";
	
	public static final String ABOF_STORECONF_URL = "https://wcs11.qa.abof.com/wcs/resources/store/10154/mobile_store?profileName=IBM_Admin_All";
	public static final String ABOF_PROFILE_URL = "http://wcs11.qa.abof.com/wcs/resources/store/10154/person/@self";

}
