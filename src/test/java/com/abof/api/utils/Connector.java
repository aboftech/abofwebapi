package com.abof.api.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface Connector {

    public static final Logger logger = LoggerFactory.getLogger(Connector.class);

    public void connect();
    
    public void disconnect();
}