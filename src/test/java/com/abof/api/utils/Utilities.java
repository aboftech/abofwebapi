package com.abof.api.utils;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.StringWriter;
import java.util.Map;

/**
 * Created by ashifa.sunar on 05/05/17.
 */
public class Utilities {

    public static String buildRequest(String templateFile, Map<String, String> bodyParams, Map<String, String> defaults) throws Exception
    {
        VelocityEngine ve = new VelocityEngine();
        ve.init();
        Template t = ve.getTemplate(templateFile);
        VelocityContext context = new VelocityContext();

        if(! (defaults == null)) {
            for(String key: defaults.keySet())
            {
                context.put(key, defaults.get(key));
            }
        }

        for(String key: bodyParams.keySet())
        {
            context.put(key, bodyParams.get(key));
        }

        StringWriter writer = new StringWriter();
        t.merge(context, writer);
        return writer.toString();

    }
}
