package com.abof.api.utils;

public enum JdbcDriver {

    MYSQL_DRIVER {
        @Override
		public String toString() {
            return "com.mysql.jdbc.Driver";
        }
    },

    ORACLE_DRIVER {
        @Override
		public String toString() {
            return "oracle.jdbc.driver.OracleDriver";
        }
    },

    DB2_DRIVER {
        @Override
		public String toString() {
        	//return "COM.ibm.db2.jdbc.net.DB2Driver";
            return "com.ibm.db2.jcc.DB2Driver";
        }
    },

    SYBASE_DRIVER {
        @Override
		public String toString() {
            return "com.sybase.jdbc.SybDriver";
        }
    }
}