package com.abof.api.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MysqlConnector {

	public static Statement stmt;
	public static void connect(String host, String port, String DBName, String username, String password) {
		try {
			Class.forName("com.mysql.jdbc.Driver");

			String url = "jdbc:mysql://"+host+":"+port+"/"+DBName+":" + "user="+username+";password="+password+";";
			Connection conn = DriverManager.getConnection(url);
			System.out.println("Connected");

			stmt = conn.createStatement();
			//return stmt;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Statement connect2QA(String host, String port, String DBName, String username, String password) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://"+host+":"+port+"/"+DBName+":" + "user="+username+";password="+password+";";
			Connection conn = DriverManager.getConnection(url);
			System.out.println("Connected");

			stmt = conn.createStatement();
			return stmt;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void quitDBConnection() {
		try {
			stmt.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Map<String, String> verifyEntryInCashbackDB(String order_id){
		Map<String, String> result = new HashMap<String, String>();
		connect("srv01.qa.abof.com","3306","abofqa","abofdev","abof@tech");
		try{
			ResultSet rs = stmt.executeQuery("SELECT * FROM abofqa.REWARD where ORDERID='"+order_id+"';");
			System.out.println("resultset=");
			System.out.println(rs);
			while(rs.next()){
				result.put("TITLE", rs.getString(1));
				result.put("META_DESC", rs.getString(2));
				result.put("CANONICAL_TAG", rs.getString(3));
				result.put("FOOTER", rs.getString(4));
				result.put("HEADER", rs.getString(5));
			}
			return result;
		}
		catch (Exception e){
			e.printStackTrace();
			System.out.println("Exception occured.");
			return result;
		}
		finally {
			quitDBConnection();
		}
	}

}
