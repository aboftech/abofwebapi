package com.abof.api.tests;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;
import org.testng.Assert;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import com.abof.api.clients.LoginClient;
import com.abof.api.clients.ProductDetailsClient;
import com.abof.api.clients.ProductListingClient;
import com.abof.api.clients.cashbackClient;
import com.abof.api.utils.Constants;
import com.abof.api.utils.MysqlConnector;
import com.abof.api.utils.db2Connector;

public class CashbackTest extends BaseAPITest{


	String user_email = "cashback.6106@gmail.com";
	String addtoCArtbody = "{\"orderItem\":[{\"productId\":\""+"available_product_sku_id"+"\",\"quantity\":\"1\"}],\"x_device\":\"mobile\"}";
	int page_size=10;
	int page_no=1;

	@Test(enabled = true, priority = 1)
	public void cashback4AbofJeansWithPercentageAsDiscount() throws  IOException, SQLException, InvalidParameterException {

		Response loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "password123");
		String wc_token = LoginClient.getWCToken(loginResponse);
		String trusted_token = LoginClient.getWCTrustedToken(loginResponse);
		String user_id = LoginClient.getUserId(loginResponse);

		//Product List API
		Response ProductListResponse = ProductListingClient.productList(Constants.ABOF_PRODUCT_LIST_URL.replace("search_keyword", "abof jeans").replace("page_no", 
				String.valueOf(page_no)).replace("page_size",String.valueOf(page_size)));

		int product_id = ProductListingClient.getrandomProductId(ProductListResponse,page_no,page_size);

		//Product details API
		Response ProductDetailsResponse = ProductDetailsClient.productDetails(Constants.ABOF_PRODUCT_DETAILS_URL.replace("product_id", String.valueOf(product_id)));
		ProductDetailsClient.verifyProductDetailsResponse(ProductDetailsResponse);

		//Inventory Availability API
		Response InventoryAvailabilityResponse = BaseAPITest.getRequest(Constants.ABOF_INVENTORY_AVAILABILITY_URL.replace("productSKUIds", 
				ProductDetailsClient.getRandomCatalogueSkuIds(ProductDetailsResponse)),wc_token,trusted_token, user_id);

		String available_product_sku_id = cashbackClient.getAvailableProductsId(InventoryAvailabilityResponse);

		//delete all the items in the cart by hitting the below API
		Response deleteCartResponse = BaseAPITest.deleteRequest(Constants.ABOF_DELETE_CART_URL, wc_token, trusted_token, user_id);

		Assert.assertEquals(deleteCartResponse.statusCode(),204, "Delete cart response is:"+deleteCartResponse.statusCode());

		//Add to cart API
		Response AddtoCartResponse = BaseAPITest.postRequest(Constants.ABOF_ADD_TO_CART_URL, addtoCArtbody.replace("available_product_sku_id", available_product_sku_id), wc_token,trusted_token, user_id);
		cashbackClient.verifyAddToCartResponse(AddtoCartResponse);

		//Order Calculate API - This should have 4 api call reponse, the last one being the cashback
		Response calculateAPIResponse = BaseAPITest.getRequest(Constants.ABOF_AGG_CALCULATE_URL, wc_token,trusted_token, user_id);
		calculateAPIResponse = addItemsToBagForCashbackEligibility(ProductListResponse, AddtoCartResponse, calculateAPIResponse, loginResponse, InventoryAvailabilityResponse, Constants.ABOF_AGG_CALCULATE_URL, true);

		cashbackClient.verifyCashbackInCalculateAPIResponse(calculateAPIResponse, Math.min((cashbackClient.getBagAmountFromPaymentORCashbackMethodAPI(calculateAPIResponse,true)*20)/100,400.0), "10030102", 
				"CASHBACK_UAT_PROMO1", "Make a minimum purchase of Rs. 1495 using any payment mode &amp; get 20 % cashback, up to Rs.400",
				"Cashback only eligible for Jeans");

	}

	public Response addItemsToBagForCashbackEligibility(Response ProductListResponse, Response AddtoCartResponse, Response calculateAPIResponse,
			Response loginResponse, Response InventoryAvailabilityResponse, String AGG_OR_PAYMENT_URL, Boolean isAggregate)
	{
		String wc_token = LoginClient.getWCToken(loginResponse);
		String trusted_token = LoginClient.getWCTrustedToken(loginResponse);
		String user_id = LoginClient.getUserId(loginResponse);
		int i=0;
		Response ProductDetailsResponse, deleteCartResponse, getCartResponse;
		String available_product_sku_id, order_id;

		while(cashbackClient.getBagAmountFromPaymentORCashbackMethodAPI(calculateAPIResponse,isAggregate)<2000){

			if(i==3){
				deleteCartResponse = BaseAPITest.deleteRequest(Constants.ABOF_DELETE_CART_URL,wc_token, trusted_token, user_id);
				Assert.assertEquals(deleteCartResponse.statusCode(),204, "Delete cart response is:"+deleteCartResponse.statusCode());
				i=0;
			}
			int product_id = ProductListingClient.getrandomProductId(ProductListResponse,page_no,page_size);

			ProductDetailsResponse = ProductDetailsClient.productDetails(Constants.ABOF_PRODUCT_DETAILS_URL.replace("product_id", String.valueOf(product_id)));
			ProductDetailsClient.verifyProductDetailsResponse(ProductDetailsResponse);

			InventoryAvailabilityResponse = BaseAPITest.getRequest(
					Constants.ABOF_INVENTORY_AVAILABILITY_URL.replace("productSKUIds", ProductDetailsClient.getRandomCatalogueSkuIds(ProductDetailsResponse)),
					wc_token, trusted_token, user_id);

			available_product_sku_id = cashbackClient.getAvailableProductsId(InventoryAvailabilityResponse);

			String user_email = "cashback.6106@gmail.com";
			LoginClient loginClient = new LoginClient();
			loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "password123");

			AddtoCartResponse = BaseAPITest.postRequest(Constants.ABOF_ADD_TO_CART_URL, addtoCArtbody.replace("available_product_sku_id", available_product_sku_id), 
					wc_token, trusted_token, user_id);
			cashbackClient.verifyAddToCartResponse(AddtoCartResponse);
			i++;

			calculateAPIResponse = BaseAPITest.getRequest(AGG_OR_PAYMENT_URL,wc_token, trusted_token, user_id);
		}
		return calculateAPIResponse;
	}

	@Test(enabled = true, priority = 2)
	public void cashback4MenFormalShoesWithFixedAmountAsDiscount() throws  IOException, SQLException, InvalidParameterException {

		Response loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "password123");
		String wc_token = LoginClient.getWCToken(loginResponse);
		String trusted_token = LoginClient.getWCTrustedToken(loginResponse);
		String user_id = LoginClient.getUserId(loginResponse);

		int page_size=30;
		int page_no=1;

		//Product List API
		Response ProductListResponse = ProductListingClient.productList(Constants.ABOF_SEARCH_BY_CATGEORY_URL.replace("category_id", Constants.MEN_FORMAL_SHOES_CATEGORY_ID)
				.replace("page_no", String.valueOf(page_no)).replace("page_size",String.valueOf(page_size)));

		int product_id = ProductListingClient.getrandomProductId(ProductListResponse,page_no,page_size);

		//Product details API
		Response ProductDetailsResponse = ProductDetailsClient.productDetails(Constants.ABOF_PRODUCT_DETAILS_URL.replace("product_id", String.valueOf(product_id)));

		ProductDetailsClient.verifyProductDetailsResponse(ProductDetailsResponse);

		//Inventory Availability API
		Response InventoryAvailabilityResponse = BaseAPITest.getRequest(Constants.ABOF_INVENTORY_AVAILABILITY_URL.replace("productSKUIds", 
				ProductDetailsClient.getRandomCatalogueSkuIds(ProductDetailsResponse)), wc_token, trusted_token, user_id);

		String available_product_sku_id = cashbackClient.getAvailableProductsId(InventoryAvailabilityResponse);

		//delete all the items in the cart by hitting the below API
		BaseAPITest.deleteRequest(Constants.ABOF_DELETE_CART_URL, wc_token,trusted_token, user_id);

		//Add to cart API
		Response AddtoCartResponse = BaseAPITest.postRequest(Constants.ABOF_ADD_TO_CART_URL, addtoCArtbody.replace("available_product_sku_id", available_product_sku_id), 
				wc_token, trusted_token, user_id);

		cashbackClient.verifyAddToCartResponse(AddtoCartResponse);

		//Order Calculate API - This should have 4 api call reponse, the last one being the cashback
		Response calculateAPIResponse = BaseAPITest.getRequest(Constants.ABOF_AGG_CALCULATE_URL,wc_token, trusted_token, user_id);

		calculateAPIResponse = addItemsToBagForCashbackEligibility(ProductListResponse,AddtoCartResponse, calculateAPIResponse,loginResponse, InventoryAvailabilityResponse, Constants.ABOF_AGG_CALCULATE_URL,true);
		cashbackClient.verifyCashbackInCalculateAPIResponse(calculateAPIResponse, 500.0, "10030102", 
				"CASHBACK_UAT_PROMO2", "Make a minimum purchase of Rs. 1495 using any payment mode &amp; get Rs.500 cashback",
				"Cashback only eligible for Men - Formal Shoes");


	}


	@Test(enabled = true, priority = 3, expectedExceptions={NullPointerException.class })
	public void cashbackNegativeTest() throws  IOException, SQLException, InvalidParameterException {


		Response loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "password123");

		int page_size=30;
		int page_no=1;
		String wc_token = LoginClient.getWCToken(loginResponse);
		String trusted_token = LoginClient.getWCTrustedToken(loginResponse);
		String user_id = LoginClient.getUserId(loginResponse);

		//Product List API
		Response ProductListResponse = ProductListingClient.productList(Constants.ABOF_PRODUCT_LIST_URL.replace("search_keyword", "pepe jeans").replace("page_no", 
				String.valueOf(page_no)).replace("page_size",String.valueOf(page_size)));

		int product_id = ProductListingClient.getrandomProductId(ProductListResponse,page_no,page_size);

		//Product details API
		Response ProductDetailsResponse = ProductDetailsClient.productDetails(Constants.ABOF_PRODUCT_DETAILS_URL.replace("product_id", String.valueOf(product_id)));
		ProductDetailsClient.verifyProductDetailsResponse(ProductDetailsResponse);

		//Inventory Availability API
		Response InventoryAvailabilityResponse = BaseAPITest.getRequest(
				Constants.ABOF_INVENTORY_AVAILABILITY_URL.replace("productSKUIds", ProductDetailsClient.getRandomCatalogueSkuIds(ProductDetailsResponse)),
				wc_token, trusted_token, user_id);

		String available_product_sku_id = cashbackClient.getAvailableProductsId(InventoryAvailabilityResponse);

		//delete all the items in the cart by hitting the below API
		BaseAPITest.deleteRequest(Constants.ABOF_DELETE_CART_URL, wc_token, trusted_token, user_id);


		//Add to cart API
		Response AddtoCartResponse = BaseAPITest.postRequest(Constants.ABOF_ADD_TO_CART_URL, addtoCArtbody.replace("available_product_sku_id", available_product_sku_id), 
				wc_token, trusted_token, user_id);

		cashbackClient.verifyAddToCartResponse(AddtoCartResponse);

		//Order Calculate API - This should have 4 api call reponse, the last one being the cashback
		Response calculateAPIResponse = BaseAPITest.getRequest(Constants.ABOF_AGG_CALCULATE_URL, wc_token, trusted_token, user_id);

		cashbackClient.verifyCahbackNotapplicableResponse(calculateAPIResponse, Math.min((cashbackClient.getBagAmountFromPaymentORCashbackMethodAPI(calculateAPIResponse,true)*20)/100,400.0));
		cashbackClient.readErrorMessageWhenNoCashback(calculateAPIResponse);

	}


	@Test(enabled = true, priority = 4)
	public void PercentageCashbackTest4PrepaidPayment() throws  IOException, SQLException, InvalidParameterException {

		Response loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "password123");
		String wc_token = LoginClient.getWCToken(loginResponse);
		String trusted_token = LoginClient.getWCTrustedToken(loginResponse);
		String user_id = LoginClient.getUserId(loginResponse);

		//Product List API
		Response ProductListResponse = 
				BaseAPITest.getRequestWithParams(Constants.ABOF_SEARCH_BY_CATGEORY_URL.
						replace("category_id", Constants.WOMEN_ETHINIC_WEAR_CATEGORY_ID).replace("page_no", 
								String.valueOf(page_no)).replace("page_size",String.valueOf(page_size)), wc_token, trusted_token, user_id);

		int product_id = ProductListingClient.getrandomProductId(ProductListResponse,page_no,page_size);

		//Product details API
		Response ProductDetailsResponse = ProductDetailsClient.productDetails(Constants.ABOF_PRODUCT_DETAILS_URL.replace("product_id", String.valueOf(product_id)));

		ProductDetailsClient.verifyProductDetailsResponse(ProductDetailsResponse);

		//Inventory Availability API
		Response InventoryAvailabilityResponse = BaseAPITest.getRequest(
				Constants.ABOF_INVENTORY_AVAILABILITY_URL.replace("productSKUIds", ProductDetailsClient.getRandomCatalogueSkuIds(ProductDetailsResponse)),
				wc_token, trusted_token, user_id);

		String available_product_sku_id = cashbackClient.getAvailableProductsId(InventoryAvailabilityResponse);

		//delete all the items in the cart by hitting the below API
		BaseAPITest.deleteRequest(Constants.ABOF_DELETE_CART_URL, wc_token, trusted_token, user_id);


		//Add to cart API
		Response AddtoCartResponse = BaseAPITest.postRequest(Constants.ABOF_ADD_TO_CART_URL, addtoCArtbody.replace("available_product_sku_id", available_product_sku_id), 
				wc_token, trusted_token, user_id);
		cashbackClient.verifyAddToCartResponse(AddtoCartResponse);

		//Order Calculate API - This should have 4 api call reponse, the last one being the cashback
		Response paymentCashbackAPIResponse = BaseAPITest.getRequest(Constants.ABOF_CALCULATE_PREPAID_URL, wc_token, trusted_token, user_id);

		paymentCashbackAPIResponse = addItemsToBagForCashbackEligibility(ProductListResponse, AddtoCartResponse, paymentCashbackAPIResponse, 
				loginResponse, InventoryAvailabilityResponse, Constants.ABOF_CALCULATE_PREPAID_URL,false);
		cashbackClient.verifyCashbackAPIResponse(paymentCashbackAPIResponse, String.valueOf(Math.round(cashbackClient.getBagAmountFromPaymentORCashbackMethodAPI(paymentCashbackAPIResponse,false)/10)), "10030106", 
				"CASHBACK_UAT_PROMO5", "Make a minimum purchase of Rs. 1000 using Prepaid payment mode &amp; get 10 % cashback, No max limit ",
				"Cashback only eligible for women - Ethinc wear", "Bucket B");

	}

	@Test(enabled = true, priority = 5)
	public void FixedCashbackTest4PrepaidPayment() throws  IOException, SQLException, InvalidParameterException {

		Response loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "password123");
		String wc_token = LoginClient.getWCToken(loginResponse);
		String trusted_token = LoginClient.getWCTrustedToken(loginResponse);
		String user_id = LoginClient.getUserId(loginResponse);

		//Product List API
		Response ProductListResponse = BaseAPITest.getRequestWithParams(Constants.ABOF_SEARCH_BY_CATGEORY_URL.replace("category_id", 
				Constants.WOMEN_SANDALS_AND_HEELS).replace("page_no", String.valueOf(page_no)).replace("page_size",String.valueOf(page_size)),wc_token, trusted_token, user_id);

		int product_id = ProductListingClient.getrandomProductId(ProductListResponse,page_no,page_size);

		//Product details API
		Response ProductDetailsResponse = ProductDetailsClient.productDetails(
				Constants.ABOF_PRODUCT_DETAILS_URL.replace("product_id", String.valueOf(product_id)));
		ProductDetailsClient.verifyProductDetailsResponse(ProductDetailsResponse);

		//Inventory Availability API
		Response InventoryAvailabilityResponse = BaseAPITest.getRequest(
				Constants.ABOF_INVENTORY_AVAILABILITY_URL.replace("productSKUIds", ProductDetailsClient.getRandomCatalogueSkuIds(ProductDetailsResponse)),
				wc_token, trusted_token, user_id);

		String available_product_sku_id = cashbackClient.getAvailableProductsId(InventoryAvailabilityResponse);
		//delete all the items in the cart by hitting the below API
		BaseAPITest.deleteRequest(Constants.ABOF_DELETE_CART_URL, wc_token, trusted_token, user_id);


		//Add to cart API
		Response AddtoCartResponse = BaseAPITest.postRequest(Constants.ABOF_ADD_TO_CART_URL, addtoCArtbody.replace("available_product_sku_id", available_product_sku_id), 
				wc_token, trusted_token, user_id);
		cashbackClient.verifyAddToCartResponse(AddtoCartResponse);

		//Prepaid payment API
		Response paymentCashbackAPIResponse = BaseAPITest.getRequest(Constants.ABOF_CALCULATE_PREPAID_URL,wc_token, trusted_token, user_id);

		paymentCashbackAPIResponse = addItemsToBagForCashbackEligibility(ProductListResponse,AddtoCartResponse, paymentCashbackAPIResponse, 
				loginResponse, InventoryAvailabilityResponse, Constants.ABOF_CALCULATE_PREPAID_URL,false);

		cashbackClient.verifyCashbackAPIResponse(paymentCashbackAPIResponse, String.valueOf(200), "10030107", 
				"CASHBACK_UAT_PROMO6", "Make a minimum purchase of Rs. 1000 using Prepaid payment mode &amp; get Rs.200 cashback",
				"Cashback only eligible for Women - Footwear", "Bucket B");

	}


	@Test(enabled = true, priority = 6)
	public void PercentageCashbackTest4CODPayment() throws  IOException, SQLException, InvalidParameterException {

		Response loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "password123");

		String wc_token = LoginClient.getWCToken(loginResponse);
		String trusted_token = LoginClient.getWCTrustedToken(loginResponse);
		String user_id = LoginClient.getUserId(loginResponse);

		//Product List API
		Response ProductListResponse = ProductListingClient.productList(Constants.ABOF_SEARCH_BY_CATGEORY_URL.
				replace("category_id", Constants.WOMEN_SHORTS_AND_SKIRTS).replace("page_no", String.valueOf(page_no)).replace("page_size",String.valueOf(page_size)));

		int product_id = ProductListingClient.getrandomProductId(ProductListResponse,page_no,page_size);

		//Product details API
		Response ProductDetailsResponse = ProductDetailsClient.productDetails(
				Constants.ABOF_PRODUCT_DETAILS_URL.replace("product_id", String.valueOf(product_id)));

		ProductDetailsClient.verifyProductDetailsResponse(ProductDetailsResponse);

		//Inventory Availability API
		Response InventoryAvailabilityResponse = BaseAPITest.getRequest(Constants.ABOF_INVENTORY_AVAILABILITY_URL.replace("productSKUIds", 
				ProductDetailsClient.getRandomCatalogueSkuIds(ProductDetailsResponse)), wc_token, trusted_token, user_id);

		String available_product_sku_id = cashbackClient.getAvailableProductsId(InventoryAvailabilityResponse);
		//delete all the items in the cart by hitting the below API
		BaseAPITest.deleteRequest(Constants.ABOF_DELETE_CART_URL, wc_token, trusted_token, user_id);


		//Add to cart API
		Response AddtoCartResponse = BaseAPITest.postRequest(Constants.ABOF_ADD_TO_CART_URL, addtoCArtbody.replace("available_product_sku_id", available_product_sku_id), 
				wc_token, trusted_token, user_id);
		cashbackClient.verifyAddToCartResponse(AddtoCartResponse);

		//Cashback API
		Response paymentCashbackAPIResponse = BaseAPITest.getRequest(Constants.ABOF_CALCULATE_COD_URL, wc_token, trusted_token, user_id);
		paymentCashbackAPIResponse = addItemsToBagForCashbackEligibility(ProductListResponse,AddtoCartResponse, paymentCashbackAPIResponse, loginResponse, InventoryAvailabilityResponse, Constants.ABOF_CALCULATE_COD_URL,false);

		cashbackClient.verifyCashbackAPIResponse(paymentCashbackAPIResponse, String.valueOf(Math.min(Math.round(cashbackClient.getBagAmountFromPaymentORCashbackMethodAPI(paymentCashbackAPIResponse, false)*18/100),450.0)), "10030106", 
				"CASHBACK_UAT_PROMO11", "Make a minimum purchase of Rs. 2000 using COD payment mode &amp; get 18 % cashback, up to Rs.450",
				"Cashback only eligible for Women - Short & Skirts","Bucket A");

	}


	@Test(enabled = true, priority = 7)
	public void FixedCashbackTest4CODPayment() throws  IOException, SQLException, InvalidParameterException {

		Response loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "password123");

		String wc_token = LoginClient.getWCToken(loginResponse);
		String trusted_token = LoginClient.getWCTrustedToken(loginResponse);
		String user_id = LoginClient.getUserId(loginResponse);

		//Product List API
		Response ProductListResponse = 
				BaseAPITest.getRequestWithParams(Constants.ABOF_SEARCH_BY_CATGEORY_URL.replace("category_id", Constants.WOMEN_DRESSES).replace("page_no", 
						String.valueOf(page_no)).replace("page_size",String.valueOf(page_size)),wc_token, trusted_token, user_id);

		int product_id = ProductListingClient.getrandomProductId(ProductListResponse,page_no,page_size);

		//Product details API
		Response ProductDetailsResponse = ProductDetailsClient.productDetails(Constants.ABOF_PRODUCT_DETAILS_URL.replace("product_id", String.valueOf(product_id)));

		ProductDetailsClient.verifyProductDetailsResponse(ProductDetailsResponse);

		//Inventory Availability API
		Response InventoryAvailabilityResponse = BaseAPITest.getRequest(Constants.ABOF_INVENTORY_AVAILABILITY_URL.replace("productSKUIds", 
				ProductDetailsClient.getRandomCatalogueSkuIds(ProductDetailsResponse)), wc_token, trusted_token, user_id);

		String available_product_sku_id = cashbackClient.getAvailableProductsId(InventoryAvailabilityResponse);

		//delete all the items in the cart by hitting the below API
		BaseAPITest.deleteRequest(Constants.ABOF_DELETE_CART_URL,wc_token, trusted_token, user_id);


		//Add to cart API
		Response AddtoCartResponse = BaseAPITest.postRequest(Constants.ABOF_ADD_TO_CART_URL,
				addtoCArtbody.replace("available_product_sku_id", available_product_sku_id),wc_token, trusted_token, user_id);

		cashbackClient.verifyAddToCartResponse(AddtoCartResponse);

		//Cashback API 
		Response paymentCashbackAPIResponse = BaseAPITest.getRequest(Constants.ABOF_CALCULATE_COD_URL,wc_token, trusted_token, user_id);
		paymentCashbackAPIResponse = addItemsToBagForCashbackEligibility(ProductListResponse,AddtoCartResponse, paymentCashbackAPIResponse, loginResponse, 
				InventoryAvailabilityResponse, Constants.ABOF_CALCULATE_COD_URL,false);

		cashbackClient.verifyCashbackAPIResponse(paymentCashbackAPIResponse, String.valueOf(400.0), "10030105", 
				"CASHBACK_UAT_PROMO4", "Make a minimum purchase of Rs. 2000 using COD payment mode &amp; get Rs.400 cashback",
				"Cashback only eligible for Women - Dress", "Bucket B");

	}

	@Test(enabled = true, priority = 8)
	public void percentCashbackWithMobileVerificationTurnedON() throws  IOException, SQLException, InvalidParameterException {

		Response loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "password123");

		String wc_token = LoginClient.getWCToken(loginResponse);
		String trusted_token = LoginClient.getWCTrustedToken(loginResponse);
		String user_id = LoginClient.getUserId(loginResponse);

		//Product List API
		Response ProductListResponse = BaseAPITest.getRequestWithParams(Constants.ABOF_SEARCH_BY_CATGEORY_URL.
				replace("category_id", Constants.WOMEN_TOPS_AND_TEES).replace("page_no", String.valueOf(page_no)).replace("page_size",String.valueOf(page_size)),
				wc_token, trusted_token, user_id);

		int product_id = ProductListingClient.getrandomProductId(ProductListResponse,page_no,page_size);

		//Product details API
		Response ProductDetailsResponse = ProductDetailsClient.productDetails(Constants.ABOF_PRODUCT_DETAILS_URL.replace("product_id", String.valueOf(product_id)));
		ProductDetailsClient.verifyProductDetailsResponse(ProductDetailsResponse);

		//Inventory Availability API
		Response InventoryAvailabilityResponse = BaseAPITest.getRequest(Constants.ABOF_INVENTORY_AVAILABILITY_URL.replace("productSKUIds", 
				ProductDetailsClient.getRandomCatalogueSkuIds(ProductDetailsResponse)), wc_token, trusted_token, user_id);

		String available_product_sku_id = cashbackClient.getAvailableProductsId(InventoryAvailabilityResponse);

		//delete all the items in the cart by hitting the below API
		BaseAPITest.deleteRequest(Constants.ABOF_DELETE_CART_URL, wc_token, trusted_token, user_id);


		//Add to cart API
		Response AddtoCartResponse = BaseAPITest.postRequest(Constants.ABOF_ADD_TO_CART_URL, addtoCArtbody.replace("available_product_sku_id", available_product_sku_id),wc_token, trusted_token, user_id);
		cashbackClient.verifyAddToCartResponse(AddtoCartResponse);

		//Cashback API 
		Response paymentCashbackAPIResponse = BaseAPITest.getRequest(Constants.ABOF_CALCULATE_COD_URL, wc_token, trusted_token, user_id);
		paymentCashbackAPIResponse = addItemsToBagForCashbackEligibility(ProductListResponse,AddtoCartResponse, paymentCashbackAPIResponse, loginResponse, InventoryAvailabilityResponse, Constants.ABOF_CALCULATE_PREPAID_URL,false);

		JsonPath paymentCashbackAPIResponsePath = new JsonPath(paymentCashbackAPIResponse.asString());
		Assert.assertTrue(paymentCashbackAPIResponsePath.getString("errorMessage").startsWith("No Cashback Promotions applicable on this order for"));

	}

	@Test(enabled = true, priority = 9)
	public void verifyCashbackRecordGetsCreatedInWCSNCashbackDB() throws  IOException, SQLException, InvalidParameterException, InterruptedException {

		Response loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "password123");

		String wc_token = LoginClient.getWCToken(loginResponse);
		String trusted_token = LoginClient.getWCTrustedToken(loginResponse);
		String user_id = LoginClient.getUserId(loginResponse);

		//Product List API
		Response ProductListResponse = ProductListingClient.productList(Constants.ABOF_PRODUCT_LIST_URL.replace("search_keyword", "abof jeans").replace("page_no", 
				String.valueOf(page_no)).replace("page_size",String.valueOf(page_size)));

		int product_id = ProductListingClient.getrandomProductId(ProductListResponse,page_no,page_size);

		//Product details API
		Response ProductDetailsResponse = ProductDetailsClient.productDetails(Constants.ABOF_PRODUCT_DETAILS_URL.replace("product_id", String.valueOf(product_id)));
		ProductDetailsClient.verifyProductDetailsResponse(ProductDetailsResponse);

		//Inventory Availability API
		Response InventoryAvailabilityResponse = BaseAPITest.getRequest(Constants.ABOF_INVENTORY_AVAILABILITY_URL.replace("productSKUIds", 
				ProductDetailsClient.getRandomCatalogueSkuIds(ProductDetailsResponse)),wc_token,trusted_token, user_id);

		String available_product_sku_id = cashbackClient.getAvailableProductsId(InventoryAvailabilityResponse);

		//delete all the items in the cart by hitting the below API
		Response deleteCartResponse = BaseAPITest.deleteRequest(Constants.ABOF_DELETE_CART_URL, wc_token, trusted_token, user_id);

		Assert.assertEquals(deleteCartResponse.statusCode(),204, "Delete cart response is:"+deleteCartResponse.statusCode());

		//Add to cart API
		Response AddtoCartResponse = BaseAPITest.postRequest(Constants.ABOF_ADD_TO_CART_URL, addtoCArtbody.replace("available_product_sku_id", available_product_sku_id), wc_token,trusted_token, user_id);
		cashbackClient.verifyAddToCartResponse(AddtoCartResponse);

		//Order Calculate API - This should have 4 api call reponse, the last one being the cashback
		Response calculateAPIResponse = BaseAPITest.getRequest(Constants.ABOF_AGG_CALCULATE_URL, wc_token,trusted_token, user_id);

		calculateAPIResponse = addItemsToBagForCashbackEligibility(ProductListResponse,AddtoCartResponse, 
				calculateAPIResponse,loginResponse, InventoryAvailabilityResponse, Constants.ABOF_AGG_CALCULATE_URL,true);

		Response GetCartResponse =BaseAPITest.getRequest(Constants.ABOF_GET_CART_URL, wc_token,trusted_token, user_id);

		List<String> returnListFromplaceAJustPayOrder = new ArrayList<String>();
		returnListFromplaceAJustPayOrder= JustPayTests.placeAJustPayOrder(wc_token,trusted_token,user_id, "abof jeans", GetCartResponse);

		String order_id = returnListFromplaceAJustPayOrder.get(0);

		System.out.println("returnListFromplaceAJustPayOrder.get(1)");
		System.out.println(returnListFromplaceAJustPayOrder.get(1));

		JsonPath getCarResponsePath = new JsonPath(returnListFromplaceAJustPayOrder.get(1));

		JustPayTests.verifyOrderFlowfromWCStoOMS(wc_token,trusted_token,user_id,order_id);

		//		//Place an order worth 2000 and above, copy the code from this file till addTocart 
		//		//and then modify the justpayfunction to see if the cart value is 2000 already
		Map<String, String> result = new HashMap<String, String>();
		result = db2Connector.verifyCashBackEntryRecordinWCSDB(order_id);
		System.out.println(result);

		//		compareCashbackEntryValuesWithThatOfAgrregateAPI(result);

		System.out.println(returnListFromplaceAJustPayOrder.get(1));

		//		String order_id = "653004";
		System.out.println(MysqlConnector.verifyEntryInCashbackDB(order_id));

	}

	public static  void compareCashbackEntryValuesWithThatOfAgrregateAPI(){

	}


}
