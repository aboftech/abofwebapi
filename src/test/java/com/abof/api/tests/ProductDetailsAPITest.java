package com.abof.api.tests;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.sql.SQLException;

import org.testng.annotations.Test;
import com.jayway.restassured.response.Response;

import com.abof.api.clients.LoginClient;
import com.abof.api.clients.ProductDetailsClient;
import com.abof.api.clients.ProductListingClient;
import com.abof.api.utils.Constants;

public class ProductDetailsAPITest extends BaseAPITest{


	@Test(enabled = true, priority = 1)
	public void TestProductDetailsTest() throws  IOException, SQLException, InvalidParameterException {

		String user_email = "saravanakumar.d@abof.adityabirla.com";
		LoginClient loginClient = new LoginClient();
		Response loginResponse = LoginClient.login(Constants.LOGIN_URL, user_email, "qwer1234$#");
		int page_size=30;
		int page_no=1;

		Response ProductListResponse = ProductListingClient.productList(Constants.ABOF_PRODUCT_LIST_URL.replace("page_no", 
				String.valueOf(page_no)).replace("page_size",String.valueOf(page_size)));

		int product_id = ProductListingClient.getrandomProductId(ProductListResponse,page_no,page_size);

		Response ProductDetailsResponse = ProductDetailsClient.productDetails(
				Constants.ABOF_PRODUCT_DETAILS_URL.replace("product_id", String.valueOf(product_id)));

		ProductDetailsClient.verifyProductDetailsResponse(ProductDetailsResponse);

	}

}
