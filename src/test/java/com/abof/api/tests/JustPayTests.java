package com.abof.api.tests;

import static com.jayway.restassured.RestAssured.given;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;

import com.abof.api.clients.LoginClient;
import com.abof.api.clients.ProductDetailsClient;
import com.abof.api.clients.ProductListingClient;
import com.abof.api.clients.cashbackClient;
import com.abof.api.utils.Constants;
import com.abof.api.utils.db2Connector;
import com.abof.web.testscripts.BaseTest;

public class JustPayTests extends BaseAPITest{

	public static Statement stmt;
	public static String user_email = "cashback.6106@gmail.com";
	public static String[] card_number={"5123456789012346","4242424242424242","4111111111111111"};
	public static String[] card_exp_month={"05","10","11"};
	public static String[] card_exp_year={"2020","2020","2020"};
	public static String[] card_security_code={"123","111","123"};
	public static int page_size=10;
	public static int page_no=1;



	@Test(enabled = true, priority = 1)
	public void calculateAPI4JustPay() throws  IOException, SQLException, InvalidParameterException {


		Response loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "password123");

		String wc_token = LoginClient.getWCToken(loginResponse);
		String trusted_token = LoginClient.getWCTrustedToken(loginResponse);
		String user_id = LoginClient.getUserId(loginResponse);

		//Product List API
		Response ProductListResponse = ProductListingClient.productList(Constants.ABOF_PRODUCT_LIST_URL
				.replace("search_keyword", "abof jeans").replace("page_no",String.valueOf(page_no)).replace("page_size",String.valueOf(page_size)));

		int product_id = ProductListingClient.getrandomProductId(ProductListResponse,page_no,page_size);

		//Product details API
		Response ProductDetailsResponse = ProductDetailsClient.productDetails(Constants.ABOF_PRODUCT_DETAILS_URL.replace("product_id", String.valueOf(product_id)));
		ProductDetailsClient.verifyProductDetailsResponse(ProductDetailsResponse);

		//Inventory Availability API
		Response InventoryAvailabilityResponse = BaseAPITest.getRequest(Constants.ABOF_INVENTORY_AVAILABILITY_URL.replace("productSKUIds", 
				ProductDetailsClient.getRandomCatalogueSkuIds(ProductDetailsResponse)),wc_token,trusted_token, user_id);

		String available_product_sku_id = cashbackClient.getAvailableProductsId(InventoryAvailabilityResponse);

		//delete all the items in the cart by hitting the below API
		Response deleteCartResponse = BaseAPITest.deleteRequest(Constants.ABOF_DELETE_CART_URL, wc_token, trusted_token, user_id);

		Assert.assertEquals(deleteCartResponse.statusCode(),204, "Delete cart response is:"+deleteCartResponse.statusCode());

		//Add to cart API
		Response AddtoCartResponse = BaseAPITest.postRequest(Constants.ABOF_ADD_TO_CART_URL, 
				Constants.addtoCArtbody.replace("available_product_sku_id", available_product_sku_id), wc_token,trusted_token, user_id);
		cashbackClient.verifyAddToCartResponse(AddtoCartResponse);
		String order_id = cashbackClient.getOrderID(AddtoCartResponse);

		Response Get_cart_response = BaseAPITest.getRequest(Constants.ABOF_GET_CART_URL, wc_token,trusted_token, user_id);
		JsonPath Get_cart_response_path = new JsonPath(Get_cart_response.asString());

		String amount = Get_cart_response_path.getString("getCartResponse.grandTotal");

		for (int version= 38; version >=31;version--){

			Response CalculateAPIResponse = JustPayTests.calculateAPICall(user_email,String.valueOf(version),"2.1.0",order_id,
					wc_token, trusted_token, user_id,"Android",available_product_sku_id, amount);
			JsonPath JustPayResponsePath = new JsonPath(CalculateAPIResponse.asString());
			Assert.assertNotNull(JustPayResponsePath.getString("razorPayServiceStatus"));
		}

		for (int version= 30; version >=0;version--){

			Response CalculateAPIResponse = JustPayTests.calculateAPICall(user_email,String.valueOf(version),"2.1.0",order_id,
					wc_token, trusted_token, user_id,"Android",available_product_sku_id, amount);
			JsonPath CalculateAPIResponsePath = new JsonPath(CalculateAPIResponse.asString());
			Assert.assertNotNull(CalculateAPIResponsePath.getString("PAYU_MERCHANT_KEY"));
			Assert.assertEquals(CalculateAPIResponsePath.getString("paymentGatway"), "PayU","Payment Gateway is not payU");
		}

		for (int version= 39; version <=50;version++){

			Response CalculateAPIResponse = JustPayTests.calculateAPICall(user_email,String.valueOf(version),"2.1.0",order_id,
					wc_token, trusted_token, user_id,"Android",available_product_sku_id, amount);
			JsonPath CalculateAPIResponsePath = new JsonPath(CalculateAPIResponse.asString());
			Assert.assertNotNull(CalculateAPIResponsePath.getString("juspayOrderId"));
		}

		for (int version= 34; version >=24;version--){

			Response CalculateAPIResponse = JustPayTests.calculateAPICall(user_email,String.valueOf(version),"1.8.0",order_id,
					wc_token, trusted_token, user_id,"iOS",available_product_sku_id, amount);
			JsonPath CalculateAPIResponsePath = new JsonPath(CalculateAPIResponse.asString());
			Assert.assertNotNull(CalculateAPIResponsePath.getString("razorPayServiceStatus"));
		}

		for (int version= 36; version >=24;version--){

			Response CalculateAPIResponse = JustPayTests.calculateAPICall(user_email,String.valueOf(version),"1.8.0",order_id,
					wc_token, trusted_token, user_id,"iOS",available_product_sku_id, amount);
			JsonPath CalculateAPIResponsePath = new JsonPath(CalculateAPIResponse.asString());
			Assert.assertNotNull(CalculateAPIResponsePath.getString("razorPayOrderId"));
		}

		for (int version= 23; version >=0;version--){

			Response CalculateAPIResponse = JustPayTests.calculateAPICall(user_email,String.valueOf(version),"1.8.0",order_id,
					wc_token, trusted_token, user_id,"iOS",available_product_sku_id, amount);
			JsonPath CalculateAPIResponsePath = new JsonPath(CalculateAPIResponse.asString());
			Assert.assertNotNull(CalculateAPIResponsePath.getString("PAYU_MERCHANT_KEY"));
		}

		for (int version= 37; version <=50; version++){

			Response CalculateAPIResponse = JustPayTests.calculateAPICall(user_email,String.valueOf(version),"1.8.0",order_id,
					wc_token, trusted_token, user_id,"iOS",available_product_sku_id, amount);
			JsonPath CalculateAPIResponsePath = new JsonPath(CalculateAPIResponse.asString());
			Assert.assertNotNull(CalculateAPIResponsePath.getString("juspayOrderId"));

		}

	}

	/*
	 * Calculate API function call
	 */
	public static Response calculateAPICall(String user_email, String version,String major_version,String order_id, String wc_token,
			String trusted_token,String user_id, String platform,String product_info, String order_Amount){ 

		String JustPayBody = Constants.JustPayBody.replace("platform", platform).replace("version", version).replace("major_version", major_version)
				.replace("user_email", user_email).replace("order_id", order_id).replace("product_info", product_info).replace("order_Amount", order_Amount);

		Response CalculateResponse = BaseTest.postRequest(Constants.CALCULATE_API_URL, JustPayBody, wc_token, trusted_token, user_id);
		return CalculateResponse;
	}


	/*
	 * Test for placing an order using JustPay payment Gateway
	 * 1. finds the item with the available inventory
	 * 2. If no item found then it adds the inventory to a random
	 * 3. Places the order with an item
	 * 4. Tries with various cards so that at least one txn goes through fine for any routing logic at JustPay
	 * 5. Makes the payment with JusPay PG
	 * 6. Once the payment is done, checks the order status in WCS
	 * 7. Checks various statuses of the order in WCS
	 */

	@Test(enabled = true, priority = 2)
	public void OrderPlacementUSingJustpayPG() throws  IOException, SQLException, InvalidParameterException, InterruptedException {


		Response loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "password123");
		String wc_token = LoginClient.getWCToken(loginResponse);
		String trusted_token = LoginClient.getWCTrustedToken(loginResponse);
		String user_id = LoginClient.getUserId(loginResponse);

		String order_id = placeAJustPayOrder(wc_token,trusted_token,user_id, "abof jeans", null).get(0);
		verifyOrderFlowfromWCStoOMS(wc_token,trusted_token,user_id,order_id);

	}

	@Test(enabled = true, priority = 3)
	public void PlacenOrderswithjustPay() throws  IOException, SQLException, InvalidParameterException, InterruptedException {

		Response loginResponse = LoginClient.login(Constants.LOGIN_URL, user_email, "password123");
		String wc_token = LoginClient.getWCToken(loginResponse);
		String trusted_token = LoginClient.getWCTrustedToken(loginResponse);
		String user_id = LoginClient.getUserId(loginResponse);

		int numberOfOrders = 1;
		List<String> Order_ids = new ArrayList<String>();
		String order_id;
		
		for(int i=0;i<numberOfOrders;i++){
			order_id = placeAJustPayOrder(wc_token,trusted_token,user_id, "abof jeans", null).get(0);
			Order_ids.add(order_id);
		}
		//		verifyOrderFlowfromWCStoOMS(wc_token,trusted_token,user_id,order_id);
		System.out.println(Order_ids);

	}

	public static void verifyOrderFlowfromWCStoOMS(String wc_token,String trusted_token, String user_id,String order_id) throws InterruptedException{
		Response wcsODerDetailResponse = BaseAPITest.getRequest(Constants.WCS_ORDER_DETAIL_API.replace("order_id", order_id),
				wc_token, trusted_token, user_id);
		JsonPath wcsODerDetailResponsePath = new JsonPath(wcsODerDetailResponse.asString());

		Assert.assertEquals(wcsODerDetailResponsePath.getString("orderStatus"),"M","Order has not been submitted");
		System.out.println("Order has been submitted");

		int j=0;
		while(j<10){
			if(wcsODerDetailResponsePath.getString("orderStatus").equals("F") || wcsODerDetailResponsePath.getString("orderStatus").equals("G")){
				System.out.println("Order sent to oms");
				break;

			}
			else{
				Thread.sleep(60000);
				wcsODerDetailResponse = BaseAPITest.getRequest(Constants.WCS_ORDER_DETAIL_API.replace("order_id", order_id),
						wc_token, trusted_token, user_id);
				wcsODerDetailResponsePath = new JsonPath(wcsODerDetailResponse.asString());
			}
		}
		Assert.assertTrue(wcsODerDetailResponsePath.getString("orderStatus").equals("F") || wcsODerDetailResponsePath.getString("orderStatus").equals("G"),
				"Order was not sent to oms");

		int k=0;
		while(k<10){
			if(wcsODerDetailResponsePath.getString("orderStatus").equals("G")){
				System.out.println("Order confirmed by oms");
				break;
			}
			else{
				Thread.sleep(60000);
				wcsODerDetailResponse = BaseAPITest.getRequest(Constants.WCS_ORDER_DETAIL_API.replace("order_id", order_id),
						wc_token, trusted_token, user_id);
				wcsODerDetailResponsePath = new JsonPath(wcsODerDetailResponse.asString());
			}
		}
		Assert.assertTrue(wcsODerDetailResponsePath.getString("orderStatus").equals("G"),
				"Order was not created in OMS or was created but not acknowledged back to WCS "
						+ "due to integration issue Or order did not reach OMS due to integration issue");
	}


	public static String find_available_product_sku_id(String wc_token,String trusted_token, String user_id,String search_keyword){
		//Product List API
		Response ProductListResponse = ProductListingClient.productList(Constants.ABOF_PRODUCT_LIST_URL
				.replace("search_keyword", search_keyword).replace("page_no", 
						String.valueOf(page_no)).replace("page_size",String.valueOf(page_size)));

		int product_id = ProductListingClient.getrandomProductId(ProductListResponse,page_no,page_size);

		//Product details API
		Response ProductDetailsResponse = ProductDetailsClient.productDetails(Constants.ABOF_PRODUCT_DETAILS_URL.replace("product_id", String.valueOf(product_id)));
		ProductDetailsClient.verifyProductDetailsResponse(ProductDetailsResponse);

		//Inventory Availability API
		Response InventoryAvailabilityResponse = BaseAPITest.getRequest(Constants.ABOF_INVENTORY_AVAILABILITY_URL.replace("productSKUIds", 
				ProductDetailsClient.getRandomCatalogueSkuIds(ProductDetailsResponse)),wc_token,trusted_token, user_id);

		String available_product_sku_id = cashbackClient.getAvailableProductsId(InventoryAvailabilityResponse);
		if(available_product_sku_id.equals(""))
		{
			String anyProductId = cashbackClient.getAnyProductId(InventoryAvailabilityResponse);
			String anyProductIdURL = Constants.ABOF_PRODUCT_VIEW_BY_ID.replace("ProductId", anyProductId);

			Response productViewIdResponse = BaseTest.getRequest(anyProductIdURL, wc_token,trusted_token, user_id);
			JsonPath productViewIdResponsePath = new JsonPath(productViewIdResponse.asString());

			available_product_sku_id = productViewIdResponsePath.getList("catalogEntryView.partNumber").get(0).toString();
			driver.get(Constants.ADD_INVENTORY_URL.replace("available_product_sku_id", available_product_sku_id));
		}
		return available_product_sku_id;
	}

	public static Response AddItemsToCart(String wc_token,String trusted_token, String user_id, String available_product_sku_id){

		//delete all the items in the cart by hitting the below API
		Response deleteCartResponse = BaseAPITest.deleteRequest(Constants.ABOF_DELETE_CART_URL, wc_token, trusted_token, user_id);

		Assert.assertEquals(deleteCartResponse.statusCode(),204, "Delete cart response is:"+deleteCartResponse.statusCode());

		//Add to cart API
		Response AddtoCartResponse = BaseAPITest.postRequest(Constants.ABOF_ADD_TO_CART_URL, 
				Constants.addtoCArtbody.replace("available_product_sku_id", available_product_sku_id), wc_token,trusted_token, user_id);
		cashbackClient.verifyAddToCartResponse(AddtoCartResponse);

		return AddtoCartResponse;
	}

	public static List<String> placeAJustPayOrder(String wc_token,String trusted_token,String user_id, String search_keyword, Response getCartResponse) throws InterruptedException, IOException{

		String checkout_body = "";
		String order_id = "";
		String available_product_sku_id  = "";
		List<String> returnList = new ArrayList<String> ();

		for(int i=2;i>=0;i--)
		{

			if(getCartResponse == null){

				available_product_sku_id = find_available_product_sku_id(wc_token,trusted_token,user_id,search_keyword);
				Response AddtoCartResponse = AddItemsToCart(wc_token,trusted_token,user_id,available_product_sku_id);
				order_id = cashbackClient.getOrderID(AddtoCartResponse);
			}
			Response Get_cart_response = BaseAPITest.getRequest(Constants.ABOF_GET_CART_URL, wc_token,trusted_token, user_id);
			JsonPath Get_cart_response_path = new JsonPath(Get_cart_response.asString());

			if(order_id.equals(""))
				order_id = Get_cart_response_path.getString("getCartResponse.orderId");

			if(available_product_sku_id.equals(""))
				available_product_sku_id = Get_cart_response_path.getList("getCartResponse.orderItem.productId").get(0).toString();

			String amount = Get_cart_response_path.getString("getCartResponse.grandTotal");

			Response JustPayResponse = JustPayTests.calculateAPICall(user_email,String.valueOf(39),"2.1.0",order_id,wc_token, 
					trusted_token, user_id,"Android", available_product_sku_id, amount);

			JsonPath JustPayResponsePath = new JsonPath(JustPayResponse.asString());
			Assert.assertNotNull(JustPayResponsePath.getString("juspayOrderId"),"juspayOrderId is null in API call");

			String juspay_order_id = JustPayResponsePath.getString("juspayOrderId");

			String CommandLineParams = Constants.JusPayCommandLineParams.replace("juspay_order_id", juspay_order_id).replace("cardNumber", card_number[i])
					.replace("cardExpMonth", card_exp_month[i]).replace("cardExpYear", card_exp_year[i])
					.replace("cardSecurityCode", card_security_code[i]);

			Response preCheckoutAPIResponse = BaseAPITest.putRequest(Constants.preCheckoutAPI,Constants.preCheckoutAPIBody,wc_token,trusted_token, user_id);

			JsonPath preCheckoutAPIResponsePath = new JsonPath(preCheckoutAPIResponse.asString());
			String expected_orderId = preCheckoutAPIResponsePath.getString("orderId");

			Assert.assertEquals(order_id, expected_orderId);

			Response juspayPaymentResponse =  given().contentType(ContentType.JSON)
					.header(new Header("Authorization", "Basic NjlDREI0QUE5NkZGNDM3QjhGQUJBMUJGQUQ4RkQxQzg="))
					.header(new Header("Content-Type", "application/x-www-form-urlencoded")).log().everything().expect().log()
					.ifError().when().post(Constants.JUSTPAY_SANDBOX_URL.replace("CommandLineParams",CommandLineParams));

			JsonPath juspayPaymentResponsePath = new JsonPath(juspayPaymentResponse.asString());
			String justPay_OrderId = juspayPaymentResponsePath.getString("order_id");

			System.out.println(Constants.checkout_body);

			checkout_body = Constants.checkout_body.replace("justPay_OrderId", justPay_OrderId).replace("orderid",order_id);

			String redirection_url = juspayPaymentResponsePath.getString("payment.authentication.url");
			JustPayTests.hitUrl(redirection_url,Constants.justPay_redirection_url);

			int ct=0;
			Boolean is_charged = false;
			while (true){
				try{
					WebElement RazorPay_success_button_element = driver.findElement(By.xpath(Constants.RazorPay_success_button));
					if(RazorPay_success_button_element.isDisplayed())
						RazorPay_success_button_element.click();
				}
				catch(Exception e){}

				Response justPayOrderDetailsResponse =  justpayOrderStatus(justPay_OrderId);
				JsonPath justPayOrderDetailsResponsePath = new JsonPath(justPayOrderDetailsResponse.asString());
				String payment_status = justPayOrderDetailsResponsePath.getString("status");
				ct++;
				if(payment_status.equals("CHARGED") )	
					is_charged=true;
				else if(payment_status.equals("AUTHORIZATION_FAILED"))	
					break;
				else if (ct>10)	
					break;
				if(is_charged==true) 
					break;
				Thread.sleep(1000);
			}


			if(is_charged==true){ 
				Response checkoutResponse = BaseAPITest.postRequest(Constants.ABOF_CHECKOUT_URL.replace("order_id", order_id),
						checkout_body, wc_token,trusted_token, user_id);
				Assert.assertEquals(checkoutResponse.getStatusCode(), 201, "Checkout API did not exit with 201");

				JsonPath checkoutResponsePath = new JsonPath(checkoutResponse.asString());
				try{
					System.out.println(checkoutResponsePath.getString("orderId"));
				}
				catch(Exception e){
					System.out.println("Checkout API response does not have OrderId");
					continue;
				}

				break;
			}
		}
		driver.quit();
		returnList.add(order_id);
		returnList.add(getCartResponse.asString());
		return returnList;
	}


	public static Response justpayOrderStatus(String justPay_OrderId){
		Response justPayOrderStatusResponse = given().contentType(ContentType.JSON)
				.header(new Header("Authorization", "Basic NjlDREI0QUE5NkZGNDM3QjhGQUJBMUJGQUQ4RkQxQzg="))
				.header(new Header("Content-Type", "application/x-www-form-urlencoded"))
				.log().everything().expect().log().ifError().when()
				.get(Constants.JUSTPAY_SANDBOX_ORDERS_URL.replace("justPay_OrderId", justPay_OrderId));

		return justPayOrderStatusResponse;
	}

	public static void hitUrl(String url,String homePageUrl) throws InterruptedException, IOException {
		driver.get(url);
		int i=0;
		if(driver.getTitle()!=homePageUrl){
			Thread.sleep(1000);
			i++;
			if(i>20){
				return;
			}

		}
	}

}
