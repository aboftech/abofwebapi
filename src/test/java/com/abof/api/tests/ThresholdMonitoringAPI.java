package com.abof.api.tests;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Logger;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.abof.api.clients.LoginClient;
import com.abof.api.utils.Constants;
import com.abof.web.testscripts.BaseTest;
import com.abof.web.utils.ExcelUtility;
import com.google.gson.Gson;

public class ThresholdMonitoringAPI extends BaseAPITest{
	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	ExcelUtility exeUtil = null;
	LoginClient loginClient = null;
	Response loginResponse;
	static int productsCount ;
	public static class entryCount4Facet{
		int count;
		String label;

		public int getCount() {
			return count;
		}
		public void setCount(int count) {
			this.count = count;
		}
		public String getLabel() {
			return label;
		}
		public void setLabel(String label) {
			this.label = label;
		}

	}
	@BeforeClass
	public void init() {
		BaseTest.className = "Threshold Monitoring Suite";
		exeUtil = new ExcelUtility();
	}

	@Test(enabled = true, priority = 1)
	public void  thresholdMonitoring() throws IOException, SQLException, InvalidParameterException, Exception{
		String result="", message="";
		boolean status=true;
		List<String> urls = getMegaMenuUrls();
		ExcelUtility.moveYesterdayCategoryURLs("C:\\Opkey\\MonitoringThresholdQuantity", "Categories");
		ExcelUtility.saveCategoriesFromAPI("C:\\Opkey\\MonitoringThresholdQuantity", "Categories", urls);
		ExcelUtility.verifyDroppedCategories("C:\\Opkey\\MonitoringThresholdQuantity", "Categories");
		ExcelUtility.verifyNewAddedCategories("C:\\Opkey\\MonitoringThresholdQuantity", "Categories");
		List<String> commonUrls = ExcelUtility.commonCategories("C:\\Opkey\\MonitoringThresholdQuantity", "Categories");
		for(int i=1; i<commonUrls.size(); i++){
			logger.info((i+1)+") "+commonUrls.get(i));
			if(!commonUrls.get(i).contains("/clearance-sale")&&!commonUrls.get(i).endsWith("accessories/belts")){
				String[] gender = commonUrls.get(i).split("/");
				List<entryCount4Facet> genderData = genderOptionsList(commonUrls.get(i));
				List<entryCount4Facet> typeData = typeOptionsList(commonUrls.get(i));
				List<entryCount4Facet> priceData = priceOptionsList(commonUrls.get(i));
				List<entryCount4Facet> sizeData = sizeOptionsList(commonUrls.get(i));
				List<entryCount4Facet> manufacturerNameData = manufacturerNameOptionsList(commonUrls.get(i));
				result = ExcelUtility.thresholdMonitoring("C:\\Opkey\\MonitoringThresholdQuantity", "Categories", commonUrls.get(i), gender[1],genderData,typeData,priceData,sizeData,manufacturerNameData);
				if(result.equalsIgnoreCase("Threshold Monitoring passed")){
					NXGReports.addStep("Verify threshold count for "+"http://abof.com"+commonUrls.get(i), "http://abof.com"+commonUrls.get(i), "It should verify filter options threshold count for "+ "http://abof.com"+commonUrls.get(i),result, LogAs.PASSED, null);
				} else {
					message = message+result;
					status = false;
					NXGReports.addStep("Verify threshold count for "+"http://abof.com"+commonUrls.get(i), "http://abof.com"+commonUrls.get(i), "It should verify filter options threshold count for "+ "http://abof.com"+commonUrls.get(i),result, LogAs.FAILED, null);
				}
			}
			else{
				if(!commonUrls.get(i).endsWith("accessories/belts")){
					genderOptionsList(commonUrls.get(i));
					if(productsCount>0){
						NXGReports.addStep("Verify threshold count for "+"http://abof.com"+commonUrls.get(i),"http://abof.com"+commonUrls.get(i),"Atleast one product should be there. But found: "+productsCount, LogAs.PASSED, null);
					}else{
						NXGReports.addStep("Verify threshold count for "+"http://abof.com"+commonUrls.get(i),"http://abof.com"+commonUrls.get(i),"Atleast one product should be there. But found: "+productsCount, LogAs.FAILED, null);
					}
				}
			}
		}

		Assert.assertTrue(status, message);
	}
	public List<String> getMegaMenuUrls() {
		String user_email = "saravanakumar.d@abof.adityabirla.com";
		new LoginClient();
		loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "qwer1234$#");
		int page_size=30;
		int page_no=1;

		Response MegaMenuCategoryListResponse = BaseAPITest.getRequest(
				Constants.ABOF_MEGAMENU_CATEGORY_LIST_URL.replace("page_no", String.valueOf(page_no)).replace("page_size",String.valueOf(page_size)),
				LoginClient.getWCToken(loginResponse),
				LoginClient.getWCTrustedToken(loginResponse),
				LoginClient.getUserId(loginResponse));

		List<String> category_urls = ThresholdMonitoringAPI.getRandomCatalogueSeoUrlsandNames(MegaMenuCategoryListResponse);
		return category_urls;
	}

	public List<entryCount4Facet> genderOptionsList(String url){


		List<entryCount4Facet> genderList = getFacetEntriesNCounts(url, loginResponse, "ads_f10143_ntk_cs");
		return genderList;
	}
	public List<entryCount4Facet> typeOptionsList(String url){


		List<entryCount4Facet> typeList = getFacetEntriesNCounts(url, loginResponse, "ads_f10142_ntk_cs");
		return typeList;
	}
	public List<entryCount4Facet> priceOptionsList(String url){
		List<entryCount4Facet> priceList = getPriceFacetEntriesNCounts(url, loginResponse, "price_INR_10021");
		return priceList;
	}
	public List<entryCount4Facet> sizeOptionsList(String url){
		List<entryCount4Facet> sizeList = getFacetEntriesNCounts(url, loginResponse, "adinv_size");
		return sizeList;
	}
	public List<entryCount4Facet> manufacturerNameOptionsList(String url){
		List<entryCount4Facet> manufacturerNameList = getFacetEntriesNCounts(url, loginResponse, "mfName_ntk_cs");
		return manufacturerNameList;
	}

	@Test(enabled = false, priority = 1)
	public void getMegaMenuJson() throws  IOException, SQLException, InvalidParameterException {

		String user_email = "saravanakumar.d@abof.adityabirla.com";
		new LoginClient();
		Response loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "qwer1234$#");
		int page_size=30;
		int page_no=1;

		Response MegaMenuCategoryListResponse = BaseAPITest.getRequest(
				Constants.ABOF_MEGAMENU_CATEGORY_LIST_URL.replace("page_no", String.valueOf(page_no)).replace("page_size",String.valueOf(page_size)),
				LoginClient.getWCToken(loginResponse),
				LoginClient.getWCTrustedToken(loginResponse),
				LoginClient.getUserId(loginResponse));

		List<String> category_urls = ThresholdMonitoringAPI.getRandomCatalogueSeoUrlsandNames(MegaMenuCategoryListResponse);
		String Gender="ads_f10143_ntk_cs";
		String Type="ads_f10142_ntk_cs";
		String Price="price_INR_10021";
		String Size="adinv_size";
		String ManufacturerName = "mfName_ntk_cs";
		for(String s: category_urls)
		{
			getFacetEntriesNCounts(s, loginResponse, Gender);
			getFacetEntriesNCounts(s, loginResponse, Type);
			getPriceFacetEntriesNCounts(s, loginResponse, Price);
			getFacetEntriesNCounts(s, loginResponse, Size);
			getFacetEntriesNCounts(s, loginResponse, ManufacturerName);

		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<entryCount4Facet> getFacetEntriesNCounts(String s, Response loginResponse, String facet)
	{
		List<entryCount4Facet>  entryWithCounts = new ArrayList<entryCount4Facet>();
		if (!(Constants.ABOF_SEO_BASE_URL+s).endsWith("/seo/api/men/sale")&&!(Constants.ABOF_SEO_BASE_URL+s).endsWith("/seo/api/women/sale")&& !(Constants.ABOF_SEO_BASE_URL+s).contains("/shop-by-look"))
		{

			Response seoReponse = BaseAPITest.getRequest(Constants.ABOF_SEO_BASE_URL+s,LoginClient.getWCToken(loginResponse),
					LoginClient.getWCTrustedToken(loginResponse),
					LoginClient.getUserId(loginResponse));
			JsonPath seoResponsePath = new JsonPath(seoReponse.asString());
			String categoryId=seoResponsePath.getString("categoryId");

			Response solrAPIresponse = BaseAPITest.getRequest(Constants.ABOF_SOLR_SEARCH_API.replace("categoryID", categoryId),LoginClient.getWCToken(loginResponse),
					LoginClient.getWCTrustedToken(loginResponse),
					LoginClient.getUserId(loginResponse));

			JsonPath solrAPIresponsePath = new JsonPath(solrAPIresponse.asString());
			productsCount = Integer.valueOf(solrAPIresponsePath.getString("recordSetTotal"));
			solrAPIresponsePath.getString("recordSetTotal");

			new HashMap<String,Object>();
			List<Map<String,Object>> jsonObjList = new ArrayList();

			for(int k=0;k<solrAPIresponsePath.getList("facetView").size();k++){
				jsonObjList.add((HashMap)(solrAPIresponsePath.getList("facetView").get(k)));
			}


			Iterator<Map<String,Object>> itr = jsonObjList.iterator();


			while(itr.hasNext()){
				try{

					Map<String,Object> jo=itr.next();
					if(jo.get("value").toString().equals(facet)){

						String json = new Gson().toJson(jo.get("entry"));
						JsonPath entryPath = new JsonPath(json);
						List<String> counts = new ArrayList<String>();

						counts = entryPath.getList("count");
						List<String> labels = new ArrayList<String>();
						labels = entryPath.getList("label");
						for(int i=0;i<labels.size();i++){
							entryCount4Facet entry= new entryCount4Facet();
							entry.setLabel(labels.get(i));
							entry.setCount(Integer.valueOf(counts.get(i)));
							entryWithCounts.add(entry);
						}
					}
				}
				catch(NullPointerException e){
					continue;
				}
			}

		}
		return entryWithCounts;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<entryCount4Facet> getPriceFacetEntriesNCounts(String s, Response loginResponse, String facet)
	{
		List<entryCount4Facet>  entryWithCounts = new ArrayList<entryCount4Facet>();
		if (!(Constants.ABOF_SEO_BASE_URL+s).endsWith("/seo/api/men/sale") && !(Constants.ABOF_SEO_BASE_URL+s).endsWith("/seo/api/women/sale")&& !(Constants.ABOF_SEO_BASE_URL+s).contains("/shop-by-look"))
		{

			Response seoReponse = BaseAPITest.getRequest(Constants.ABOF_SEO_BASE_URL+s,LoginClient.getWCToken(loginResponse),
					LoginClient.getWCTrustedToken(loginResponse),
					LoginClient.getUserId(loginResponse));
			JsonPath seoResponsePath = new JsonPath(seoReponse.asString());
			String categoryId=seoResponsePath.getString("categoryId");


			Response solrAPIresponse = BaseAPITest.getRequest(Constants.ABOF_SOLR_SEARCH_API.replace("categoryID", categoryId),LoginClient.getWCToken(loginResponse),
					LoginClient.getWCTrustedToken(loginResponse),
					LoginClient.getUserId(loginResponse));

			JsonPath solrAPIresponsePath = new JsonPath(solrAPIresponse.asString());
			solrAPIresponsePath.getString("recordSetTotal");

			new HashMap<String,Object>();
			List<Map<String,Object>> jsonObjList = new ArrayList();

			for(int k=0;k<solrAPIresponsePath.getList("facetView").size();k++){
				jsonObjList.add((HashMap)(solrAPIresponsePath.getList("facetView").get(k)));
			}


			Iterator<Map<String,Object>> itr = jsonObjList.iterator();


			while(itr.hasNext()){
				try{

					Map<String,Object> jo=itr.next();
					if(jo.get("value").toString().equals(facet)){
						String json = new Gson().toJson(jo.get("entry"));
						JsonPath entryPath = new JsonPath(json);
						List<Integer> priceCounts = new ArrayList<Integer>();
						priceCounts = entryPath.getList("count");
						List<String> labels = new ArrayList<String>();
						labels = entryPath.getList("label");
						for(int i=0;i<labels.size();i++){
							entryCount4Facet entry= new entryCount4Facet();
							entry.setLabel(labels.get(i));
							entry.setCount(Integer.valueOf(priceCounts.get(i)));
							entryWithCounts.add(entry);
						}
					}
				}
				catch(NullPointerException e){
					continue;
				}
			}

		}
		return entryWithCounts;
	}


	public static List<String> getRandomCatalogueSeoUrlsandNames(Response MegaMenuCategoryListResponse) {
		JsonPath MegaMenuCategoryListResponsepath = new JsonPath(MegaMenuCategoryListResponse.asString());

		new Random();
		MegaMenuCategoryListResponsepath.getList("catalogGroupView").size();

		List<String> listofurls = new ArrayList<>();
		listofurls.addAll(ThresholdMonitoringAPI.getSeoURLs(MegaMenuCategoryListResponsepath
				.getList("catalogGroupView.catalogGroupView").get(0).toString()));

		listofurls.addAll(ThresholdMonitoringAPI.getSeoURLs(MegaMenuCategoryListResponsepath
				.getList("catalogGroupView.catalogGroupView").get(1).toString()));

		return listofurls;
	}


	public static List<String> getSeoURLs(String specificGenderCategoryList){
		String[] list1=specificGenderCategoryList.split("seoUrl=");
		List<String> list2 = new ArrayList<String>();

		for(int i=1;i<list1.length;i++){
			if(!list1[i].toString().contains("/shop-by-look")){
				list2.add(list1[i].split(", resourceId=")[0]);
			}
		}
		return list2;
	}

}

