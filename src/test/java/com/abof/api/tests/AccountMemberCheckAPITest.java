package com.abof.api.tests;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.sql.SQLException;

import org.testng.annotations.Test;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import com.abof.api.clients.AccountMemberCheckClient;
import com.abof.api.clients.LoginClient;
import com.abof.api.utils.Constants;

public class AccountMemberCheckAPITest extends BaseAPITest{

	public static String member_check_url=Constants.MEMBER_CHECK_URL+"logonId=user_email&catalogId="+Constants.ABOF_CATALOGUE_ID;


	@Test(enabled = true, priority = 1)
	public void TestMemberCheckUrl() throws  IOException, SQLException, InvalidParameterException {

		String user_email = "saravanakumar.d@abof.adityabirla.com";
		LoginClient loginClient = new LoginClient();
		Response loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "qwer1234$#");

		member_check_url=member_check_url.replace("user_email", user_email);
		Response MemberCheckResponse = AccountMemberCheckClient.accountMemberCheck(member_check_url,LoginClient.getWCToken(loginResponse),
				LoginClient.getWCTrustedToken(loginResponse),LoginClient.getUserId(loginResponse));
		JsonPath creditResponseStr = new JsonPath(MemberCheckResponse.asString());
		System.out.println(MemberCheckResponse.asString());

		AccountMemberCheckClient.verifyMemberCheckResponse(MemberCheckResponse, false,true);


	}


	//{"orderItem":[{"productId":"358482","quantity":"1"}],"x_device":"mobile"}

	//TestMemberCheckUrl() by passing the tokens which are generated using the older version of the app
	@Test(enabled = true, priority = 2)
	public void TestMemberCheckUrlwithOlderVersionAppLogin() throws  IOException, SQLException, InvalidParameterException {

		String user_email = "saravanakumar.d@abof.adityabirla.com";
		LoginClient loginClient = new LoginClient();
		Response loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "qwer1234$#");

		member_check_url=member_check_url.replace("user_email", user_email);
		Response MemberCheckResponse = AccountMemberCheckClient.accountMemberCheck(member_check_url,LoginClient.getWCToken(loginResponse),
				LoginClient.getWCTrustedToken(loginResponse),LoginClient.getUserId(loginResponse));
		JsonPath creditResponseStr = new JsonPath(MemberCheckResponse.asString());

		//		Rewrite the verification
		//		AccountMemberCheckClient.verifyMemberCheckResponse(MemberCheckResponse, false,false);


	}


	





}
