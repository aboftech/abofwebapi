package com.abof.api.tests;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.abof.api.clients.LoginClient;
import com.abof.api.utils.Constants;
import com.abof.api.utils.db2Connector;
import com.abof.web.testscripts.BaseTest;
import com.abof.web.utils.ExcelUtility;

public class SEO_Sanity extends BaseAPITest{
	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	static int productsCount ;
	ExcelUtility exeUtil = null;
	LoginClient loginClient = null;
	static Response loginResponse;
	boolean difference=false;
	String message = "";
	
	@BeforeClass
	public void init() {
		BaseTest.className = "SEO Sanity Suite";
		exeUtil = new ExcelUtility();
	}
	
	@Test(enabled = true, priority = 1)
	public void  seoSanityVerification() throws IOException, SQLException, InvalidParameterException, Exception{
		//String result="", message="";
		String lastOne="";
		boolean status=true;
		Map<String, String> tableData = new HashMap<String, String>();
		Map<String, String> apiData = new HashMap<String, String>();
		List<String> urls = db2Connector.listOfURLs("SELECT PATH FROM WCSADMIN.SEO_PATH_CONF");
		List<String> brandsAPI = getBrandsAvailable();
		
		for(int c=1; c<urls.size(); c++){
			//Constants.ABOF_SEO_BASE_URL+s;
			//if(!s.contains("/brand/Inc-5")){

			 logger.info(c+". "+urls.get(c));
			 String[] bits = urls.get(c).split("/");
			 lastOne = bits[bits.length-1];
			if(brandsAPI.contains(lastOne.toLowerCase())){
			tableData = db2Connector.getQueryResult("SELECT TITLE,META_DESC,CANONICAL_TAG,FOOTER,HEADER from WCSADMIN.SEO_PATH_CONF WHERE PATH='"+urls.get(c)+"'");
			/*System.out.println(tableData.get("TITLE"));
			System.out.println(tableData.get("META_DESC"));
			System.out.println(tableData.get("CANONICAL_TAG"));
			System.out.println(tableData.get("FOOTER"));
			System.out.println(tableData.get("HEADER"));*/
			String title_DB = tableData.get("TITLE");
			String desc_DB = tableData.get("META_DESC");
			String cononical_DB = tableData.get("CANONICAL_TAG");
			String footer_DB = tableData.get("FOOTER");
			String header_DB = tableData.get("HEADER");
			apiData = seoDataWithAPI(urls.get(c));
			String title_api = apiData.get("TITLE");
			String desc_api = apiData.get("META_DESC");
			String cononical_api = apiData.get("CANONICAL_TAG");
			String footer_api = apiData.get("FOOTER");
			String header_api = apiData.get("HEADER");
				if(((title_DB!=null&&title_DB .equalsIgnoreCase( title_api))==false)&&(title_DB==null&&title_api==null)==false){
					System.out.println("Both null? ");
					message = message+ "Title in DB: <b><font color="+'"'+"Green"+'"'+">"+title_DB+"</font></b><br> Title in API: <b><font color="+'"'+"Green"+'"'+">"+title_api+"</font></b><br>";
					difference=true;
				}
				if(((desc_DB!=null&&desc_DB.equalsIgnoreCase(desc_api))==false)&&(desc_DB==null&&desc_api==null)==false){
					System.out.println((desc_DB!=null&&desc_DB.equalsIgnoreCase(desc_api))==false);
					message =  message+ "META_DESC in DB: <b><font color="+'"'+"Blue"+'"'+">"+desc_DB+"</font></b><br> META_DESC in API: <b><font color="+'"'+"Blue"+'"'+">"+desc_api+"</font></b><br> ";
					difference=true;
				}
				if(((cononical_DB!=null&&cononical_DB .equalsIgnoreCase(cononical_api))==false)&&(cononical_DB==null&&cononical_api==null)==false){
					System.out.println((cononical_DB!=null&&cononical_DB .equalsIgnoreCase(cononical_api))==false);
					message =  message+ "CANONICAL_TAG in DB: <b><font color="+'"'+"Orange"+'"'+">"+cononical_DB+"</font></b><br>"+" CANONICAL_TAG in API: <b><font color="+'"'+"Orange"+'"'+">"+cononical_api+"</font></b><br>";
					difference=true;
				}
				if(((footer_DB!=null&&footer_DB.equalsIgnoreCase(footer_api))==false)&&(footer_DB==null&&footer_api==null)==false){
					System.out.println((footer_DB!=null&&footer_DB.equalsIgnoreCase(footer_api))==false);
					message =  message+ "FOOTER in DB: <b><font color="+'"'+"Red"+'"'+">"+footer_DB+"</font></b><br>"+" FOOTER in API: <b><font color="+'"'+"Red"+'"'+">"+footer_api+"</font></b><br>";
					difference=true;
				}
				if(((header_DB!=null&&header_DB .equalsIgnoreCase( header_api))==false)&&(header_DB==null&&header_api==null)==false){
					System.out.println((header_DB!=null&&header_DB .equalsIgnoreCase( header_api))==false);
					message =  message+ "HEADER in DB: <b><font color="+'"'+"Brown"+'"'+">"+header_DB+"</font></b><br>"+" HEADER in API: <b><font color="+'"'+"Brown"+'"'+">"+header_api+"</font></b><br>";
					difference=true;
				}
				if(!difference){
					NXGReports.addStep("Verify SEO content for "+"http://abof.com"+urls.get(c), "http://abof.com"+urls.get(c), "SEO content should match with Database values.","SEO content verified with Database values.", LogAs.PASSED, null);
				}
				else{
					BaseTest.emailMessageBody.put("http://abof.com"+urls.get(c), message);
					NXGReports.addStep("Verify SEO content for "+"http://abof.com"+urls.get(c), "http://abof.com"+urls.get(c),"SEO content should match with Database values.",message, LogAs.FAILED, null);
					status=false;
				}
				
			}
			 else{
				 try{
					 
					 Response seoReponse = BaseAPITest.getRequest(Constants.ABOF_SEO_BASE_URL+urls.get(c),LoginClient.getWCToken(loginResponse),
							LoginClient.getWCTrustedToken(loginResponse),
							LoginClient.getUserId(loginResponse));
					JsonPath seoResponsePath = new JsonPath(seoReponse.asString());
					String categoryId=seoResponsePath.getString("categoryId");
					Response solrAPIresponse = BaseAPITest.getRequest(Constants.ABOF_SOLR_SEARCH_API.replace("categoryID", categoryId),LoginClient.getWCToken(loginResponse),
							LoginClient.getWCTrustedToken(loginResponse),
							LoginClient.getUserId(loginResponse));

					JsonPath solrAPIresponsePath = new JsonPath(solrAPIresponse.asString());
					productsCount = Integer.valueOf(solrAPIresponsePath.getString("recordSetTotal"));
					System.out.println(Constants.ABOF_SEO_BASE_URL+urls.get(c)+" has "+productsCount+" products");
					
					if(brandsAPI.contains(lastOne.toLowerCase())){
					tableData = db2Connector.getQueryResult("SELECT TITLE,META_DESC,CANONICAL_TAG,FOOTER,HEADER from WCSADMIN.SEO_PATH_CONF WHERE PATH='"+urls.get(c)+"'");
					
					String title_DB = tableData.get("TITLE");
					String desc_DB = tableData.get("META_DESC");
					String cononical_DB = tableData.get("CANONICAL_TAG");
					String footer_DB = tableData.get("FOOTER");
					String header_DB = tableData.get("HEADER");
					apiData = seoDataWithAPI(urls.get(c));
					String title_api = apiData.get("TITLE");
					String desc_api = apiData.get("META_DESC");
					String cononical_api = apiData.get("CANONICAL_TAG");
					String footer_api = apiData.get("FOOTER");
					String header_api = apiData.get("HEADER");
						if(((title_DB!=null&&title_DB .equalsIgnoreCase( title_api))==false)&&(title_DB==null&&title_api==null)==false){
							System.out.println("Both null? ");
							message = message+ "Title in DB: <b><font color="+'"'+"Green"+'"'+">"+title_DB+"</font></b><br> Title in API: <b><font color="+'"'+"Green"+'"'+">"+title_api+"</font></b><br>";
							difference=true;
						}
						if(((desc_DB!=null&&desc_DB.equalsIgnoreCase(desc_api))==false)&&(desc_DB==null&&desc_api==null)==false){
							System.out.println((desc_DB!=null&&desc_DB.equalsIgnoreCase(desc_api))==false);
							message =  message+ "META_DESC in DB: <b><font color="+'"'+"Blue"+'"'+">"+desc_DB+"</font></b><br> META_DESC in API: <b><font color="+'"'+"Blue"+'"'+">"+desc_api+"</font></b><br> ";
							difference=true;
						}
						if(((cononical_DB!=null&&cononical_DB .equalsIgnoreCase(cononical_api))==false)&&(cononical_DB==null&&cononical_api==null)==false){
							System.out.println((cononical_DB!=null&&cononical_DB .equalsIgnoreCase(cononical_api))==false);
							message =  message+ "CANONICAL_TAG in DB: <b><font color="+'"'+"Orange"+'"'+">"+cononical_DB+"</font></b><br>"+" CANONICAL_TAG in API: <b><font color="+'"'+"Orange"+'"'+">"+cononical_api+"</font></b><br>";
							difference=true;
						}
						if(((footer_DB!=null&&footer_DB.equalsIgnoreCase(footer_api))==false)&&(footer_DB==null&&footer_api==null)==false){
							System.out.println((footer_DB!=null&&footer_DB.equalsIgnoreCase(footer_api))==false);
							message =  message+ "FOOTER in DB: <b><font color="+'"'+"Red"+'"'+">"+footer_DB+"</font></b><br>"+" FOOTER in API: <b><font color="+'"'+"Red"+'"'+">"+footer_api+"</font></b><br>";
							difference=true;
						}
						if(((header_DB!=null&&header_DB .equalsIgnoreCase( header_api))==false)&&(header_DB==null&&header_api==null)==false){
							System.out.println((header_DB!=null&&header_DB .equalsIgnoreCase( header_api))==false);
							message =  message+ "HEADER in DB: <b><font color="+'"'+"Brown"+'"'+">"+header_DB+"</font></b><br>"+" HEADER in API: <b><font color="+'"'+"Brown"+'"'+">"+header_api+"</font></b><br>";
							difference=true;
						}
						if(!difference){
							NXGReports.addStep("Verify SEO content for "+"http://abof.com"+urls.get(c), "http://abof.com"+urls.get(c), "SEO content should match with Database values.","SEO content verified with Database values.", LogAs.PASSED, null);
						}
						else{
							BaseTest.emailMessageBody.put("http://abof.com"+urls.get(c), message);
							NXGReports.addStep("Verify SEO content for "+"http://abof.com"+urls.get(c), "http://abof.com"+urls.get(c),"SEO content should match with Database values.",message, LogAs.FAILED, null);
							status=false;
						}
					}
				 }
				 catch(Exception e){
				 BaseTest.emailMessageBody.put("http://abof.com"+urls.get(c), urls.get(c)+" is not listed in brands");
				 NXGReports.addStep("Verify SEO content for "+"http://abof.com"+urls.get(c), "http://abof.com"+urls.get(c),"SEO content should match with Database values.",lastOne+" is not listed in brands", LogAs.WARNING, null); 
				 }
			 }
			tableData = new HashMap<String, String>();
			apiData = new HashMap<String, String>();
			message = "";
			difference=false;
		}
		Assert.assertTrue(status);
	}
	

	
	
	public  Map<String, String> seoDataWithAPI(String path){
		Map<String,String> seoDatafromAPI = new HashMap<String, String>();
		try{
			String user_email = "saravanakumar.d@abof.adityabirla.com";
			new LoginClient();
			loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "qwer1234$#");
			Response apiReponse = BaseAPITest.getRequest(Constants.ABOF_SEO_BASE_URL+path,LoginClient.getWCToken(loginResponse),
					LoginClient.getWCTrustedToken(loginResponse),
					LoginClient.getUserId(loginResponse));
			JsonPath apiResponsePath = new JsonPath(apiReponse.asString());
			seoDatafromAPI.put("TITLE", apiResponsePath.getString("title"));
			seoDatafromAPI.put("META_DESC", apiResponsePath.getString("metaDescription"));
			seoDatafromAPI.put("CANONICAL_TAG", apiResponsePath.getString("canonicalTag"));
			seoDatafromAPI.put("FOOTER", apiResponsePath.getString("footer"));
			seoDatafromAPI.put("HEADER", apiResponsePath.getString("searchHeader"));
			return seoDatafromAPI;
		}catch(Exception e){
			throw e;
		}
	}

	public List<String> getBrandsAvailable(){
	//public static void main(String args[]){
		List<String> list = new ArrayList<String>();
		String user_email = "saravanakumar.d@abof.adityabirla.com";
		new LoginClient();
		loginResponse = LoginClient.login( Constants.LOGIN_URL, user_email, "qwer1234$#");
		Response apiReponse = BaseAPITest.getRequest(Constants.ABOF_SEO_BASE_URL+"/brands",LoginClient.getWCToken(loginResponse),
				LoginClient.getWCTrustedToken(loginResponse),
				LoginClient.getUserId(loginResponse));
				String seoResponsePath = apiReponse.asString().trim();
				String[] arr_Of_json = seoResponsePath.split(",");
                Map<String,String> map= new HashMap<String,String>();
                for (String s:arr_Of_json){
                      map.put(s.split(":")[0],s.split(":")[1]);         
                }
                Set<Entry<String, String>> entry = map.entrySet();
                Iterator<Entry<String, String>> ite = entry.iterator();
                while(ite.hasNext()){
                	list.add(ite.next().getValue().replace("\"", ""));
                }
                
                return list;
	}
	
	
}

