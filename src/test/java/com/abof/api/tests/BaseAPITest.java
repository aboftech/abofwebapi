package com.abof.api.tests;

import com.abof.api.utils.JdbcDriver;
import com.abof.api.utils.PropertyLoader;
import com.abof.api.utils.SqlConnectionParam;
import com.abof.api.utils.SqlPoolConnector;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.ResponseTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.SSLConfig;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;

import org.json.JSONObject;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static com.jayway.restassured.RestAssured.given;
import org.openqa.selenium.JavascriptExecutor;

public class BaseAPITest {

	static JdbcDriver jdbcDriver;
	static SqlConnectionParam sqp;
	static SqlPoolConnector spc;

	public static String notifyResponse = "";
	public static String returnResponse = "";
	public static WireMockServer wireMockServer;

	public static WebDriver driver;
	static WebElement wElement;
	static Dimension dSize;
	Wait wait;
	static int sStatusCnt = 0;

	@BeforeClass
	public void setUp() throws MalformedURLException {
		if (System.getProperty("os.name").toLowerCase().contains("windows")) {
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		} else {
			System.setProperty("webdriver.chrome.driver", "chromedriver");
		}
		ChromeOptions options = new ChromeOptions ();
		DesiredCapabilities caps = DesiredCapabilities.chrome();
		LoggingPreferences logPrefs = new LoggingPreferences();
		logPrefs.enable(LogType.BROWSER, Level.ALL);
		logPrefs.enable(LogType.DRIVER, Level.ALL);
		logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
		caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
		driver = new ChromeDriver(caps);

		String currentWindowHandle = driver.getWindowHandle();

		//run your javascript and alert code
		((JavascriptExecutor)driver).executeScript("alert('Test')"); 
		driver.switchTo().alert().accept();

		//Switch back to to the window using the handle saved earlier
		driver.switchTo().window(currentWindowHandle);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@AfterMethod
	public void afterTestFun() {
		notifyResponse = "";
		returnResponse = "";
	}

	@AfterTest
	public void afterTest() {
		notifyResponse = "";
		returnResponse = "";
		driver.quit();
	}

	//	@BeforeSuite
	//	public void startWireMock() {
	//		jdbcDriver = JdbcDriver.MYSQL_DRIVER;
	//		System.out.println("Inside");
	//		sqp = new SqlConnectionParam(jdbcDriver, PropertyLoader.TEST_ENVIRONMENT.getProperty("db.host"),
	//				Integer.valueOf(PropertyLoader.TEST_ENVIRONMENT.getProperty("db.port")),
	//				PropertyLoader.TEST_ENVIRONMENT.getProperty("db.name"),
	//				PropertyLoader.TEST_ENVIRONMENT.getProperty("db.user"),
	//				PropertyLoader.TEST_ENVIRONMENT.getProperty("db.password"));
	//		System.out.println("Outside");
	//		spc = new SqlPoolConnector(sqp);
	//		spc.connect();
	//		System.setProperty("java.net.preferIPv4Stack", "true");
	//		wireMockServer = new WireMockServer(wireMockConfig().port(1251).extensions(new MyTransformer(), new GetTransformer()));
	//		wireMockServer.start();
	//		WireMock.configureFor(PropertyLoader.TEST_ENVIRONMENT.getProperty("wiremock.host"), 1251);
	//		WireMock.stubFor(WireMock.post(WireMock.urlEqualTo("/notify"))
	//				.willReturn(WireMock.aResponse()
	//						.withHeader("Content-Type", "application/json")
	//						.withStatus(200).withTransformers("mytransformer")));
	//		WireMock.stubFor(WireMock.post(WireMock.urlEqualTo("/return"))
	//				.willReturn(WireMock.aResponse()
	//						.withHeader("Content-Type", "application/json")
	//						.withStatus(200).withTransformers("mytransformer")));
	//		WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/getnotify"))
	//				.willReturn(WireMock.aResponse()
	//						.withHeader("Content-Type", "application/json")
	//						.withStatus(200).withTransformers("gettransformer")));
	//		WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/getreturn"))
	//				.willReturn(WireMock.aResponse()
	//						.withHeader("Content-Type", "application/json")
	//						.withStatus(200).withTransformers("gettransformer")));
	//	}
	//
	//	@AfterSuite
	//	public void stopWireMock() {
	//		wireMockServer.stop();
	//		spc.disconnect();
	//	}


	public Response put(int retCode, String url) {
		Response putResponse = given()
				.contentType(ContentType.JSON)
				.log()
				.everything()
				.expect()
				.statusCode(retCode)
				.log()
				.ifError()
				.when()
				.put(url);
		return putResponse;
	}

	public Response get(int retCode, String url) {
		Response getResponse = given()
				.contentType(ContentType.JSON)
				.log()
				.everything()
				.expect()
				.statusCode(retCode)
				.log()
				.ifError()
				.when()
				.get(url);
		return getResponse;
	}

	public static Response postRequest(String url, String body, String WCToken, String WCTrustedToken, String userId) {
		Response PostResponse = given()
				.contentType(ContentType.JSON)
				.header(new Header("WCToken", WCToken))
				.header(new Header("WCTrustedToken", WCTrustedToken))
				.header(new Header("userId", userId))
				.body(body)
				.log()
				.everything()
				.expect()
				.log()
				.ifError()
				.when()
				.post(url);
		return PostResponse;
	}

	public static Response genericPostRequest(String url, String body) {
		Response PostResponse = given()
				.contentType(ContentType.JSON)
				.body(body)
				//.log()
				//.everything()
				.expect()
				.log()
				.ifError()
				.when()
				.post(url);
		return PostResponse;
	}

	public static Response deleteRequest(String url, String WCToken, String WCTrustedToken, String userId) {
		Response deleteResponse = given()
				.contentType(ContentType.JSON)
				.header(new Header("WCToken", WCToken))
				.header(new Header("WCTrustedToken", WCTrustedToken))
				.header(new Header("userId", userId))
				.log()
				.everything()
				.expect()
				.log()
				.ifError()
				.when()
				.delete(url);
		return deleteResponse;
	}


	public static Response getRequestWithParams(String url,String WCToken,String WCTrustedToken, String userId) {

		Response getResponse = given()
				.contentType(ContentType.JSON)
				.header(new Header("WCToken", WCToken))
				.header(new Header("WCTrustedToken", WCTrustedToken))
				.header(new Header("userId", userId))
				.parameters("searchSource", "E")
				.parameters("channel", "Desktop")
				.parameters("currency", "INR")
				.parameters("responseFormat", "json")
				.parameters("langId", "-1")
				.parameters("pageNumber", "1")
				.parameters("pageSize", "100")
				.log()
				.everything()
				.expect()
				.log()
				.ifError()
				.when()
				.get(url);

		return getResponse;
	}

	public static Response getRequest(String url, String WCToken, String WCTrustedToken, String userId) {
		Response getResponse = given()
				.contentType(ContentType.JSON)
				.header(new Header("WCToken", WCToken))
				.header(new Header("WCTrustedToken", WCTrustedToken))
				.header(new Header("userId", userId))
				.log()
				.everything()
				.expect()
				.log()
				.ifError()
				.when()
				.get(url);
		return getResponse;
	}

	public static Response putRequest(String url, String body,String WCToken, String WCTrustedToken, String userId) {
		Response putResponse = given()
				.contentType(ContentType.JSON)
				.header(new Header("WCToken", WCToken))
				.header(new Header("WCTrustedToken", WCTrustedToken))
				.header(new Header("userId", userId))
				.body(body)
				.log()
				.everything()
				.expect()
				.log()
				.ifError()
				.when()
				.put(url);
		return putResponse;
	}

	public static Response getRequestwithoutTrustedToken(String url, String WCToken, String userId) {
		Response getResponse = given().contentType(ContentType.JSON).header(new Header("WCToken", WCToken))
				.header(new Header("userId", userId)).log()
				.everything().expect().log().ifError().when().get(url);
		return getResponse;
	}

	public synchronized void exeQuery(String query) throws SQLException {
		java.sql.ResultSet rs = spc.select(query);
		rs.close();
	}

	public synchronized String exeQueryGetStatus(String query) throws SQLException {
		String status = "";
		java.sql.ResultSet rs = spc.select(query);
		while (rs.next()) {
			status = rs.getString("status");

		}
		rs.close();
		return status;
	}


	public synchronized boolean exeInsert(String query) throws SQLException {
		boolean rs = spc.insert(query);
		return rs;
	}

	public synchronized boolean executeQuery(String query) throws SQLException {
		boolean rs = spc.insert(query);
		return rs;

	}

	public synchronized boolean exeDelete(String query) throws SQLException {
		boolean rs = spc.delete(query);
		return rs;

	}

	public synchronized boolean exeUpdate(String query) throws SQLException {
		boolean rs = spc.update(query);
		return rs;
	}


	public class MyTransformer extends ResponseTransformer {

		@Override
		public ResponseDefinition transform(Request request, ResponseDefinition responseDefinition, FileSource fileSource) {
			String responseStr = "";
			if (request.getMethod().isOneOf(RequestMethod.POST)) {
				if (request.getUrl().endsWith("notify")) {
					notifyResponse = request.getBodyAsString();
					responseStr = notifyResponse;

				} else {
					returnResponse = request.getBodyAsString();
					responseStr = returnResponse;
				}
			}
			return ResponseDefinitionBuilder.like(responseDefinition)
					.withBody(responseStr.getBytes())
					.build();
		}

		public String name() {
			return "mytransformer";
		}
	}

	;

	public class GetTransformer extends ResponseTransformer {

		@Override
		public ResponseDefinition transform(Request request, ResponseDefinition responseDefinition, FileSource fileSource) {
			if (request.getUrl().endsWith("notify"))
				return ResponseDefinitionBuilder.like(responseDefinition)
						.withBody(notifyResponse.getBytes())
						.build();
			else
				return ResponseDefinitionBuilder.like(responseDefinition)
						.withBody(returnResponse.getBytes())
						.build();
		}

		public String name() {
			return "gettransformer";
		}
	}

}