/***********************************************************************
* @author 			:		Srinivas Hippargi
* @description		: 		Scripts for Guest/Gmail/Googgle+/FB user Login/Sign up scripts
* @module			:		LoginScripts
* */

package com.abof.android.testscripts;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.abof.android.pageobjects.BrowsePlpPdpPO;
import com.abof.android.pageobjects.HamburgerMenuPO;
import com.abof.android.pageobjects.HomePagePO;
import com.abof.android.pageobjects.LoginPagePO;
import com.abof.android.pageobjects.MyFavouritesPagePO;
import com.abof.android.pageobjects.MyOrdersPagePO;
import com.abof.android.pageobjects.ProfilePagePO;
import com.abof.android.pageobjects.ShoppingBagPO;
import com.abof.android.pageobjects.WhatsHotLandingPO;
import com.abof.library.BaseLib;
import com.abof.library.GenericLib;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;

public class LoginScripts extends BaseLib {

	LoginPagePO loginPo = null;
	HomePagePO homePagePo = null;
	HamburgerMenuPO hamburgerMenuPo = null;
	ProfilePagePO profilePagePo = null;
	WhatsHotLandingPO whatsHotLandingPo = null;
	MyOrdersPagePO myOrdersPagePo = null;
	MyFavouritesPagePO myFavouritePagePo = null;
	BrowsePlpPdpPO browsePlpPdpPo = null;
	ShoppingBagPO shoppingBagPo = null;
	String sData[] = null;

	@BeforeMethod
	public void init() {
		loginPo = new LoginPagePO(driver);
		homePagePo = new HomePagePO(driver);
		hamburgerMenuPo = new HamburgerMenuPO(driver);
		profilePagePo = new ProfilePagePO(driver);
		whatsHotLandingPo = new WhatsHotLandingPO(driver);
		myOrdersPagePo = new MyOrdersPagePO(driver);
		myFavouritePagePo = new MyFavouritesPagePO(driver);
		browsePlpPdpPo = new BrowsePlpPdpPO(driver);
		shoppingBagPo = new ShoppingBagPO(driver);
	}

	/*
	 * @Description:To Check the normal Guest login functionality while adding
	 * an item to favories from PLP as Normal/FB/Gmail user and Validate show
	 * password option for login via my abof")
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Test(dataProvider = "getUsers", enabled = true, priority = 1, description = "Guest user Fav from PLP for Normal/FB/Gmail user")
	public void testGuestUserFavItemPLP(String sUser, String device) throws Exception {
		try {
			loginPo.getEleLoginCloseBtn().click();
			loginPo.handleOkayBtn();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[3]);
			BaseLib.tapOnElement(.98, .99);
			loginPo.handleOkayBtn();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			browsePlpPdpPo.getElePlpFavIcon().click();
			Thread.sleep(2000);
			GenericLib.isVisible(GenericLib.path + "SignInToAddToFav.PNG");
			if (sUser.equalsIgnoreCase("ABOF")) {
				loginPo.verifyPasewordVisibility("TC_Login_001");
				NXGReports.addStep("Guest user Fav from PLP for Normal (ABOF) user login", LogAs.INFO, null);
				loginPo.loginApp(sUser, "TC_Login_001");
			} else if (sUser.equalsIgnoreCase("FBLogged")) {
				NXGReports.addStep("Guest user Fav from PLP for FB user login", LogAs.INFO, null);
				loginPo.loginApp("FB", "TC_FBLogin_001");
			} else {
				NXGReports.addStep("Guest user Fav from PLP for GMAIL user login", LogAs.INFO, null);
				loginPo.loginApp(sUser, "TC_GmailLogin_001");
			}
			String toastMessage = BaseLib.verifyToastMessage(driver);
			Assert.assertTrue(toastMessage.contains("Cha-ching! Added to your"),
					"Cha Ching ! Added to your favourites toast message is not displayed");
			NXGReports.addStep("Cha Ching ! Added to your favourites toast message is successfully displayed",
					LogAs.PASSED, null);
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:To Check the normal Guest login functionality while adding
	 * an item to favories from PDP as a abof/FB/Gmail user , Validate show
	 * password option for login via my abof
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Test(dataProvider = "getUsers", enabled = true, priority = 2, description = "Guest user Fav from PDP for Normal/FB/Gmail user")
	public void testGuestUserFavItemPDP(String sUser, String device) throws Exception {
		try {
			loginPo.getEleLoginCloseBtn().click();
			loginPo.handleOkayBtn();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[2]);
			BaseLib.tapOnElement(.98, .99);
			loginPo.handleOkayBtn();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			browsePlpPdpPo.getEleProductImageLst().get(1).click();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Pdp");
			browsePlpPdpPo.getElePdpSaveForLaterLnk().click();
			Thread.sleep(2000);
			GenericLib.isVisible(GenericLib.path + "SignInToAddToFav.PNG");
			if (sUser.equalsIgnoreCase("ABOF")) {
				NXGReports.addStep("Guest user Fav from PDP for Normal (ABOF) user login", LogAs.INFO, null);
				loginPo.verifyPasewordVisibility("TC_Login_001");
				loginPo.loginApp(sUser, "TC_Login_001");
			} else if (sUser.equalsIgnoreCase("FBLogged")) {
				NXGReports.addStep("Guest user Fav from PDP for FB user login", LogAs.INFO, null);
				loginPo.loginApp(sUser, "TC_FBLogin_001");
			} else {
				NXGReports.addStep("Guest user Fav from PDP for Gmail user login", LogAs.INFO, null);
				loginPo.loginApp(sUser, "TC_GmailLogin_001");
			}
			String toastMessage = BaseLib.verifyToastMessage(driver);
			Assert.assertTrue(toastMessage.contains("Cha-ching! Added to your"),
					"Cha Ching ! Added to your favourites toast message is not displayed");
			NXGReports.addStep("Cha Ching ! Added to your favourites toast message is successfully displayed",
					LogAs.PASSED, null);
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description: Check whether the cart merge is working fine when guest
	 * user logging as normal/FB/Gmail user,Validate show password option for
	 * login via my abof
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Test(dataProvider = "getUsers", enabled = true, priority = 3, description = "Guest user Cart merge for Normal/FB/Gmail user")
	public void testGuestUserCartMerge(String sUser, String device) throws Exception {
		try {
			loginPo.getEleLoginCloseBtn().click();
			loginPo.handleOkayBtn();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[4]);
			BaseLib.tapOnElement(.98, .99);
			loginPo.handleOkayBtn();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			browsePlpPdpPo.getEleProductImageLst().get(1).click();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Pdp");
			driver.findElement(By
					.xpath("//android.widget.LinearLayout[@resource-id='com.abof.android:id/sizeLayout']//android.widget.LinearLayout//android.widget.TextView[@index='0']"))
					.click();
			browsePlpPdpPo.getElePdpAddToBagBtn().click();
			int prevBadgeCount = Integer.parseInt(browsePlpPdpPo.getEleBadgeCount().getText());
			browsePlpPdpPo.getElePdpViewBagLnk().click();
			BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
			shoppingBagPo.getElePlaceOrderBtn().click();
			Thread.sleep(2000);
			GenericLib.isVisible(GenericLib.path + "SignInToProceedToCheckout.PNG");
			if (sUser.equalsIgnoreCase("ABOF")) {
				NXGReports.addStep("Guest user Cart merge for Normal (ABOF) user login", LogAs.INFO, null);
				loginPo.verifyPasewordVisibility("TC_Login_001");
				loginPo.loginApp(sUser, "TC_CartMerge_001");
			} else if (sUser.equalsIgnoreCase("FBLogged")) {
				NXGReports.addStep("Guest user Cart merge for FB user login", LogAs.INFO, null);
				loginPo.loginApp(sUser, " ");
			} else {
				NXGReports.addStep("Guest user Cart merge for GMAIL user login", LogAs.INFO, null);
				loginPo.loginApp(sUser, "TC_CartMerge_002");
			}
			BaseLib.waitForElement(shoppingBagPo.getEleDeliveryAddressTxt(), "Develery address text is displayed ",
					"Develery address text is not displayed");
			Assert.assertTrue(shoppingBagPo.getEleDeliveryAddressTxt().isDisplayed());
			driver.navigate().back();
			int currBadgeCount = Integer.parseInt(shoppingBagPo.getEleShoppingBagCount().getText());
			Assert.assertTrue(prevBadgeCount < currBadgeCount,
					"cart merge is not working fine/user doesn't have existing item");
			NXGReports.addStep("cart merge is working fine ", LogAs.PASSED, null);
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:To Verify that the user can able to return back to the
	 * Sign-up screen from T&C screen
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Parameters("device")
	@Test(enabled = true, priority = 4, description = "Verify that the user can able to return back to the Sign-up screen from T&C screen")
	public void testTermsAndConditionScreen(String device) throws Exception {
		try {
			BaseLib.elementStatus(loginPo.getEleJoinToday(), "New user? Join toady text", "displayed");
			loginPo.getEleJoinToday().click();
			BaseLib.elementStatus(loginPo.getEleSignUpTermsAndCondLnk(),
					"By joining you agree our Terms and Conditions", "displayed");
			loginPo.getEleSignUpTermsAndCondLnk().click();
			BaseLib.elementStatus(loginPo.getEleLoginCloseBtn(), "Terms and Conditions of use WebView", "displayed");
			loginPo.getEleLoginCloseBtn().click();
			BaseLib.elementStatus(loginPo.getEleSignUpJoinAbofBtn(), "Terms and Conditions of use", "displayed");
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:Check the normal Guest login functionality while accessing
	 * the my abof option/signin option in hamburger menu
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Test(dataProvider = "getUsers", enabled = true, priority = 5, description = "Guest user Profile Name verification for Normal/FB/Gmail user")
	public void testGuestUserLoginInHamburgerMenu(String sUser, String device) throws Exception {
		try {
			loginPo.getEleLoginCloseBtn().click();
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			BaseLib.elementStatus(hamburgerMenuPo.getEleUserProfileNameTxt(), "Hi Guest ", "displayed");
			String prevUserName = hamburgerMenuPo.getEleUserProfileNameTxt().getText();
			hamburgerMenuPo.getEleUserProfileImg().click();
			Thread.sleep(2000);
			GenericLib.isVisible(GenericLib.path + "SignInToAccessProfile.PNG");
			if (sUser.equalsIgnoreCase("ABOF")) {
				NXGReports.addStep("Guest user Profile Name verification for  Normal (ABOF) user login", LogAs.INFO,
						null);
				loginPo.loginApp(sUser, "TC_Login_001");
			} else if (sUser.equalsIgnoreCase("FBLogged")) {
				loginPo.verifyPasewordVisibility("TC_FBLogin_001");
				NXGReports.addStep("Guest user Profile Name verification for FB user login", LogAs.INFO, null);
				loginPo.loginApp(sUser, "TC_FBLogin_001");
			} else {
				NXGReports.addStep("Guest user Profile Name verification for GMAIL user login", LogAs.INFO, null);
				loginPo.loginApp(sUser, "TC_GmailLogin_001");
			}
			BaseLib.waitForElement(homePagePo.getEleHamburgerMenuIcon(), "Hamburger Menu icon is displayed",
					"Hamburger Menu icon not is displayed");
			homePagePo.getEleHamburgerMenuIcon().click();
			NXGReports.addStep(hamburgerMenuPo.getEleUserProfileNameTxt().getText(), LogAs.INFO, null);
			String currUserName = hamburgerMenuPo.getEleUserProfileNameTxt().getText();
			Assert.assertNotEquals(prevUserName, currUserName, "Guest User name is same after logging in");
			NXGReports.addStep("Guest Login is success", LogAs.PASSED, null);
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:Check the normal Guest login functionality while moving an
	 * item to favories from shopping bag as Normal/FB/Gmail user
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Test(dataProvider = "getUsers", enabled = true, priority = 6, description = "Guest user Move to Fav from cart for Noraml/FB/Gmailuser")
	public void testGuestUserMoveToFavouritesFromCart(String sUser, String device) throws Exception {
		try {
			loginPo.getEleLoginCloseBtn().click();
			loginPo.handleOkayBtn();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[4]);
			BaseLib.tapOnElement(.98, .99);
			loginPo.handleOkayBtn();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			browsePlpPdpPo.getEleProductImageLst().get(1).click();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Pdp");
			driver.findElement(By
					.xpath("//android.widget.LinearLayout[@resource-id='com.abof.android:id/sizeLayout']//android.widget.LinearLayout//android.widget.TextView[@index='2']"))
					.click();
			browsePlpPdpPo.getElePdpAddToBagBtn().click();
			BaseLib.elementStatus(browsePlpPdpPo.getElePdpViewBagLnk(), "SuccesFully added to bag View Bag",
					"displayed");
			browsePlpPdpPo.getElePdpViewBagLnk().click();
			String prodMoveToFav = shoppingBagPo.getEleBrandName().getText();
			shoppingBagPo.getEleMoveToFavouritesIcon().click();
			NXGReports.addStep(prodMoveToFav + " prodcut is moved to favourites", LogAs.INFO, null);
			Thread.sleep(2000);
			GenericLib.isVisible(GenericLib.path + "SignInToAddToFav.PNG");
			if (sUser.equalsIgnoreCase("ABOF")) {
				NXGReports.addStep("Guest user Move to Fav from cart for Normal (ABOF) user login", LogAs.INFO, null);
				loginPo.loginApp(sUser, "TC_Login_001");
			} else if (sUser.equalsIgnoreCase("FBLogged")) {
				loginPo.verifyPasewordVisibility("TC_FBLogin_001");
				NXGReports.addStep("Guest user Move to Fav from cart for FB user login", LogAs.INFO, null);
				loginPo.loginApp(sUser, "TC_FBLogin_001");
			} else {
				NXGReports.addStep("Guest user Move to Fav from cart for GMAIL user login", LogAs.INFO, null);
				loginPo.loginApp(sUser, "TC_GmailLogin_001");
			}
			if (BaseLib.verifyToastMessage(driver).equals("Cha-ching added  to your favorites")) {
				NXGReports.addStep("Cha-ching added  to your favorites", LogAs.PASSED, null);
			} else {
				NXGReports.addStep("Cha-ching added  to your favorites toast meassge is not displayed", LogAs.INFO,
						new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			navigateBackToHomePage();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyFavouritesLnk().click();
			Thread.sleep(5000);
			hamburgerMenuPo.isProductAvailableInFavList(prodMoveToFav);
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:Verify Google Login functionality by adding new account
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Parameters("device")
	@Test(enabled = true, priority = 7, description = "Verify Google Login functionality by adding new account and Profile image")
	public void testAddNewGoogleAccount(String device) throws Exception {
		try {
			loginPo.addGmailAcc("TC_AddGmail_001");
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay, got it button is displayed",
					"Okay, got it button is not displayed");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			Assert.assertTrue(hamburgerMenuPo.getEleUserProfileImg().isDisplayed(),
					"Gmail user profile iamge is not displayed");
			NXGReports.addStep("Gmail user profile iamge is displayed", LogAs.PASSED, null);
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:Verify normal login functionality using invalid abof
	 * credentials
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Parameters("device")
	@Test(enabled = true, priority = 8, description = "Verify normal login functionality using invalid abof credentials")
	public void testInavlidLoginUser(String device) throws Exception {
		try {
			loginPo.loginApp("ABOF", "TC_Login_003");
			GenericLib.isVisible(GenericLib.path + "testInavlidLoginUser.PNG");
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:Verify Forgot Password functionality for non registered user
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Parameters("device")
	@Test(enabled = true, priority = 9, description = "Verify Forgot Password functionality for non registered user")
	public void testForgotPasswordForNonRegisteredUser(String device) throws Exception {
		try {
			BaseLib.elementStatus(loginPo.getEleAbofForgotYourPasswordtLnk(), "Forgot your password? ", "displayed");
			loginPo.getEleAbofForgotYourPasswordtLnk().click();
			loginPo.getEleAbofForgotPasswordEmailTxtBx().sendKeys("cbtcrowd237349@gmail.com");
			loginPo.handleKeyboard();
			BaseLib.elementStatus(loginPo.getEleAbofEmailResetLinkBtn(), "Email Reset Link ", "displayed");
			loginPo.getEleAbofEmailResetLinkBtn().click();
			GenericLib.isVisible(GenericLib.path + "testForgotPasswordForNonRegisteredUser.PNG");

		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:Verify the Sign-up functionality for already registered
	 * Normal/FB/G+ user
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Parameters("device")
	@Test(enabled = true, priority = 10, description = "Verify the Sign-up functionality for already registered Normal/FB/G+ user")
	public void testRegisteredNormalUser(String device) throws Exception {
		try {
			loginPo.signUpUser("ABOF", "TC_SignUp_003");
			Assert.assertTrue(loginPo.getEleEmailAlreadyUseTxt().getText().contains("Ah shoot! Email already in use!"),
					"Ah shoot! Email already in use! error message is not displayed");
			NXGReports.addStep("Ah shoot! Email already in use! error message is displayed", LogAs.PASSED, null);

		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:Verify FB Login functionality with and without app
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Parameters("device")
	@Test(enabled = false, priority = 11, description = "Verify FB Login functionality with and without app")
	public void testFBLogin(String device) throws Exception {
		try {
			loginPo.loginApp("FB", "TC_FBLogin_001");
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay got it is displayed", "Okay got it is not displayed");

		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:Verify Forgot Password functionality for registered user
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Parameters("device")
	@Test(enabled = true, priority = 12, description = "Verify Forgot Password functionality for registered user")
	public void testForgotPasswordForRegisteredUser(String device) throws Exception {
		try {
			sData = GenericLib.toReadExcelData("Login", "TC_ForgotPassword_001");
			BaseLib.elementStatus(loginPo.getEleAbofForgotYourPasswordtLnk(), "Forgot your password? ", "displayed");
			loginPo.getEleAbofForgotYourPasswordtLnk().click();
			loginPo.getEleAbofForgotPasswordEmailTxtBx().sendKeys(sData[2]);
			loginPo.handleKeyboard();
			BaseLib.elementStatus(loginPo.getEleAbofEmailResetLinkBtn(), "Email Reset Link ", "displayed");
			loginPo.getEleAbofEmailResetLinkBtn().click();
			GenericLib.isVisible(GenericLib.path + "testForgotPassword.PNG");

			driver.startActivity("com.google.android.gm", "com.android.mail.ui.MailActivity");
			Thread.sleep(10000);
			loginPo.switchToGmailAcc(sData[2]);
			Thread.sleep(5000);
			BaseLib.swipeTopToBottm(.20, .50);
			BaseLib.waitForElement(loginPo.getEleAbofPassResetGmailLnk(), "Abof password link mail is diplayed",
					"Abof password link mail is not diplayed");
			loginPo.getEleAbofPassResetGmailLnk().click();
			try {
				BaseLib.waitForElement(loginPo.getEleGmailResetPassLnk(), "Gmail Reset password link  is diplayed",
						"Gmail Reset password link  is not diplayed");
				loginPo.getEleGmailResetPassLnk().isDisplayed();
				loginPo.getEleGmailResetPassLnk().click();
				loginPo.handlePasswordResetChoice();
				loginPo.getEleNewPasswordTxtbx().sendKeys(sData[3]);
				loginPo.getEleConfirmPasswordTxtbx().sendKeys(sData[3]);
				loginPo.getEleResetPassSubmitBtn().click();
				loginPo.loginApp("ABOF", "TC_ForgotPassword_001");
				BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay, got it button is displayed",
						"Okay, got it button is not displayed");
			} catch (RuntimeException e) {
				BaseLib.waitForElement(loginPo.getEleShowQuotedTxtLnk(), "show quoted text ",
						"show quoted text is not");
				loginPo.getEleShowQuotedTxtLnk().click();
				BaseLib.swipeBottomToTop(.70, .20);
				BaseLib.waitForElement(loginPo.getEleGmailResetPassLnk(), "Reset password link is displayed",
						"Reset password link is not displayed");
				loginPo.getEleGmailResetPassLnk().click();
				loginPo.handlePasswordResetChoice();
				loginPo.getEleNewPasswordTxtbx().sendKeys(sData[3]);
				loginPo.getEleConfirmPasswordTxtbx().sendKeys(sData[3]);
				loginPo.getEleResetPassSubmitBtn().click();
				driver.startActivity("com.abof.android", "com.abof.android.landingpage.view.LandingPageView");
				loginPo.loginApp("ABOF", "TC_ForgotPassword_001");
				loginPo.handleOkayBtn();
				BaseLib.waitForElement(whatsHotLandingPo.getEleWhatsHotModule(), "Whats Hot module is displayed",
						"Whats Hot module is not displayed");

			}
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:Verify normal login functionality using G+/FB abof
	 * credentials
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Parameters("device")
	@Test(enabled = true, priority = 13, description = "Verify normal login functionality using G+/FB abof credentials")
	public void testGuestLoginForgotPassword(String device) throws Exception {
		try {
			loginPo.loginApp("ABOF", "TC_ForgotPassword_002");
			GenericLib.isVisible(GenericLib.path + "testGuestLoginForgotPassword.PNG");
			BaseLib.elementStatus(loginPo.getEleAbofForgotYourPasswordtLnk(), "Forgot your password? ", "displayed");
			loginPo.getEleAbofForgotYourPasswordtLnk().click();
			loginPo.getEleAbofForgotPasswordEmailTxtBx().sendKeys(sData[2]);
			loginPo.handleKeyboard();
			BaseLib.elementStatus(loginPo.getEleAbofEmailResetLinkBtn(), "Email Reset Link ", "displayed");
			loginPo.getEleAbofEmailResetLinkBtn().click();
			GenericLib.isVisible(GenericLib.path + "testForgotPassword.PNG");
			driver.startActivity("com.google.android.gm", "com.android.mail.ui.MailActivity");
			Thread.sleep(10000);
			loginPo.switchToGmailAcc(sData[2]);
			Thread.sleep(2000);
			BaseLib.swipeTopToBottm(.20, .50);
			BaseLib.waitForElement(loginPo.getEleAbofPassResetGmailLnk(), "Abof password link mail is diplayed",
					"Abof password link mail is not diplayed");
			loginPo.getEleAbofPassResetGmailLnk().click();
			Thread.sleep(8000);
			try {
				BaseLib.waitForElement(loginPo.getEleGmailResetPassLnk(), "Gmail Reset password link  is diplayed",
						"Gmail Reset password link  is not diplayed");
				loginPo.getEleGmailResetPassLnk().click();
				loginPo.handlePasswordResetChoice();
				loginPo.getEleNewPasswordTxtbx().sendKeys(sData[3]);
				loginPo.getEleConfirmPasswordTxtbx().sendKeys(sData[3]);
				loginPo.getEleResetPassSubmitBtn().click();
				loginPo.loginApp("ABOF", "TC_ForgotPassword_002");
				BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay, got it button is displayed",
						"Okay, got it button is not displayed");
			} catch (RuntimeException e) {
				BaseLib.waitForElement(loginPo.getEleShowQuotedTxtLnk(), "show quoted text ",
						"show quoted text is not");
				loginPo.getEleShowQuotedTxtLnk().click();
				BaseLib.swipeBottomToTop(.70, .20);
				BaseLib.waitForElement(loginPo.getEleGmailResetPassLnk(), "Reset password link is displayed",
						"Reset password link is not displayed");
				loginPo.getEleGmailResetPassLnk().click();
				loginPo.handlePasswordResetChoice();
				loginPo.getEleNewPasswordTxtbx().sendKeys(sData[3]);
				loginPo.getEleConfirmPasswordTxtbx().sendKeys(sData[3]);
				loginPo.getEleResetPassSubmitBtn().click();
				driver.startActivity("com.abof.android", "com.abof.android.landingpage.view.LandingPageView");
				loginPo.loginApp("ABOF", "TC_ForgotPassword_002");
				loginPo.handleOkayBtn();
				BaseLib.waitForElement(whatsHotLandingPo.getEleWhatsHotModule(), "Whats Hot module is displayed",
						"Whats Hot module is not displayed");
			}
		} catch (Exception e) {
			throw e;
		}
	}
	/*
	 * @Description:Check the Change password functionality- Normal login
	 * 
	 * @Author: Srinivas Hippargi
	 */

	@Parameters("device")
	@Test(enabled = true, priority = 14, description = "Check the Change password functionality- Normal login")
	public void testChangePassword(String device) throws Exception {
		try {
			sData = GenericLib.toReadExcelData("Login", "TC_ForgotPassword_003");
			loginPo.loginApp("ABOF", "TC_ForgotPassword_003");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleUserProfileImg().click();
			profilePagePo.getEleChangePasswordLnk().click();
			NXGReports.addStep(profilePagePo.getEleAlertDescTxt().getText(), LogAs.PASSED, null);
			profilePagePo.getEleYesBtn().click();
			Assert.assertTrue(
					profilePagePo.getEleAlertDescTxt().getText()
							.contains("You will receive email shortly, you will be logged out now!"),
					"You will receive email shortly, you will be logged out now! alert message is not displayed");
			NXGReports.addStep("You will receive email shortly, you will be logged out now! alert message is diaplayed",
					LogAs.PASSED, null);
			profilePagePo.getEleYesBtn().click();
			driver.startActivity("com.google.android.gm", "com.android.mail.ui.MailActivity");
			Thread.sleep(2000);
			loginPo.switchToGmailAcc(sData[2]);
			Thread.sleep(2000);
			BaseLib.swipeTopToBottm(.20, .50);
			BaseLib.waitForElement(loginPo.getEleAbofPassResetGmailLnk(), "Abof password link mail is diplayed",
					"Abof password link mail is not diplayed");
			loginPo.getEleAbofPassResetGmailLnk().click();
			try {
				BaseLib.waitForElement(loginPo.getEleGmailResetPassLnk(), "Gmail Reset password link  is diplayed",
						"Gmail Reset password link  is not diplayed");
				loginPo.getEleGmailResetPassLnk().click();
				loginPo.handlePasswordResetChoice();
				loginPo.getEleNewPasswordTxtbx().sendKeys(sData[4]);
				loginPo.getEleConfirmPasswordTxtbx().sendKeys(sData[4]);
				loginPo.getEleResetPassSubmitBtn().click();
				loginPo.loginApp("ABOF", "TC_ForgotPassword_004");
				BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay, got it button is displayed",
						"Okay, got it button is not displayed");
			} catch (RuntimeException e) {
				BaseLib.waitForElement(loginPo.getEleShowQuotedTxtLnk(), "show quoted text ",
						"show quoted text is not");
				loginPo.getEleShowQuotedTxtLnk().click();
				BaseLib.swipeBottomToTop(.70, .20);
				BaseLib.waitForElement(loginPo.getEleGmailResetPassLnk(), "Reset password link is displayed",
						"Reset password link is not displayed");
				loginPo.getEleGmailResetPassLnk().click();
				loginPo.handlePasswordResetChoice();
				loginPo.getEleNewPasswordTxtbx().sendKeys(sData[4]);
				loginPo.getEleConfirmPasswordTxtbx().sendKeys(sData[4]);
				loginPo.getEleResetPassSubmitBtn().click();
				driver.startActivity("com.abof.android", "com.abof.android.landingpage.view.LandingPageView");
				loginPo.loginApp("ABOF", "TC_ForgotPassword_004");
				loginPo.handleOkayBtn();
				BaseLib.waitForElement(whatsHotLandingPo.getEleWhatsHotModule(), "Whats Hot module is displayed",
						"Whats Hot module is not displayed");

			}
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:To Check the normal Guest Sign-up functionality while adding
	 * an item to favorites from PLP for Normal/FB/Google+ user
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Test(dataProvider = "getUsers", enabled = true, priority = 15, description = "Guest Sign-up functionality while adding an item to favorites from PLP for Normal/FB/Google+ user")
	public void testGuestSignUPFavItemFromPLP(String sUser, String device) throws Exception {
		try {
			loginPo.getEleLoginCloseBtn().click();
			loginPo.handleOkayBtn();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[3]);
			BaseLib.tapOnElement(.98, .99);
			loginPo.handleOkayBtn();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			browsePlpPdpPo.getElePlpFavIcon().click();
			Thread.sleep(2000);
			GenericLib.isVisible(GenericLib.path + "SignInToAddToFav.PNG");
			if (sUser.equalsIgnoreCase("ABOF")) {
				NXGReports.addStep("Guest user Fav from PLP for Normal (ABOF) user SignUp", LogAs.INFO, null);
				loginPo.signUpUser("ABOF", "TC_SignUp_004");
			} else if (sUser.equalsIgnoreCase("GMAIL")) {
				NXGReports.addStep("Guest user Fav from PLP for Google+ user SignUp", LogAs.INFO, null);
				loginPo.addGmailAcc("TC_SignUp_006");

			} else {
				NXGReports.addStep("Guest user Fav from PLP for FB user SignUp", LogAs.INFO, null);
				driver.startActivity("com.facebook.katana", "com.facebook.katana.LoginActivity");
				Thread.sleep(10000);
				loginPo.facebookLogout();
				Thread.sleep(5000);
				driver.startActivity("com.abof.android", "com.abof.android.login.views.LoginActivity");
				loginPo.loginApp("FB", "TC_SignUp_005");
			}
			String toastMessage = BaseLib.verifyToastMessage(driver);
			Assert.assertTrue(toastMessage.contains("Cha-ching! Added to your Favourites message is displayed"),
					"Cha Ching ! Added to your favourites toast message is not displayed");
			NXGReports.addStep("Cha Ching ! Added to your favourites toast message is successfully displayed",
					LogAs.PASSED, null);
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description: Check the normal Guest Sign-up functionality while adding
	 * an item to favorites from PDP for Normal/FB/Google+ user
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Test(dataProvider = "getUsers", enabled = false, priority = 16, description = "Guest Sign-up functionality while adding an item to favorites from PDP for Normal/FB/Google+user")
	public void testGuestSignUPFavItemFromPDP(String sUser, String device) throws Exception {
		try {
			loginPo.getEleLoginCloseBtn().click();
			Thread.sleep(2000);
			loginPo.handleOkayBtn();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[2]);
			BaseLib.tapOnElement(.98, .99);
			loginPo.handleOkayBtn();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			browsePlpPdpPo.getEleProductImageLst().get(1).click();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Pdp");
			browsePlpPdpPo.getElePdpSaveForLaterLnk().click();
			Thread.sleep(2000);
			GenericLib.isVisible(GenericLib.path + "SignInToAddToFav.PNG");
			if (sUser.equalsIgnoreCase("ABOF")) {
				NXGReports.addStep("Guest user Fav from PDP for Normal (ABOF) user SignUp", LogAs.INFO, null);
				loginPo.signUpUser("ABOF", "TC_SignUp_007");
			} else if (sUser.equalsIgnoreCase("FBLogged")) {
				NXGReports.addStep("Guest user Fav from PLP for FB user SignUp", LogAs.INFO, null);
				driver.startActivity("com.facebook.katana", "com.facebook.katana.LoginActivity");
				Thread.sleep(5000);
				loginPo.facebookLogout();
				Thread.sleep(5000);
				driver.startActivity("com.abof.android", "com.abof.android.login.views.LoginActivity");
				loginPo.loginApp("FB", "TC_SignUp_008");
				Thread.sleep(50000);
			} else {
				NXGReports.addStep("Guest user Fav from PDP for GMAIL user SignUp", LogAs.INFO, null);
				loginPo.addGmailAcc("TC_SignUp_009");
				Thread.sleep(25000);
			}
			String toastMessage = BaseLib.verifyToastMessage(driver);
			Assert.assertTrue(toastMessage.contains("Cha-ching! Added to your"),
					"Cha Ching ! Added to your favourites toast message is not displayed");
			NXGReports.addStep("Cha Ching ! Added to your favourites toast message is successfully displayed",
					LogAs.PASSED, null);
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:Check the normal Guest Sign-up functionality while moving an
	 * item to favorites from shopping bag for Normal/FB/Google+ user
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Test(dataProvider = "getUsers", enabled = false, priority = 17, description = "Guest Sign-up moving an item to favorites from Shopping Bag for Noraml/FB/Google+user")
	public void testGuestSignUprMoveToFavFromCart(String sUser, String device) throws Exception {
		try {
			loginPo.getEleLoginCloseBtn().click();
			loginPo.handleOkayBtn();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[4]);
			BaseLib.tapOnElement(.98, .99);
			loginPo.handleOkayBtn();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			browsePlpPdpPo.getEleProductImageLst().get(1).click();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Pdp");
			driver.findElement(By
					.xpath("//android.widget.LinearLayout[@resource-id='com.abof.android:id/sizeLayout']//android.widget.LinearLayout//android.widget.TextView[@index='2']"))
					.click();
			browsePlpPdpPo.getElePdpAddToBagBtn().click();
			BaseLib.elementStatus(browsePlpPdpPo.getElePdpViewBagLnk(), "SuccesFully added to bag View Bag",
					"displayed");
			browsePlpPdpPo.getElePdpViewBagLnk().click();
			String prodMoveToFav = shoppingBagPo.getEleBrandName().getText();
			shoppingBagPo.getEleMoveToFavouritesIcon().click();
			NXGReports.addStep(prodMoveToFav + " prodcut is moved to favourites", LogAs.INFO, null);
			Thread.sleep(2000);
			GenericLib.isVisible(GenericLib.path + "SignInToAddToFav.PNG");
			if (sUser.equalsIgnoreCase("ABOF")) {
				NXGReports.addStep("Guest user Move to Fav from Shopping bag Normal (ABOF) user SignUp", LogAs.INFO,
						null);
				loginPo.signUpUser("ABOF", "TC_SignUp_010");
			} else if (sUser.equalsIgnoreCase("FBLogged")) {
				NXGReports.addStep("Guest user Fav from PLP for FB user SignUp", LogAs.INFO, null);
				driver.startActivity("com.facebook.katana", "com.facebook.katana.LoginActivity");
				Thread.sleep(5000);
				loginPo.facebookLogout();
				Thread.sleep(5000);
				driver.startActivity("com.abof.android", "com.abof.android.login.views.LoginActivity");
				loginPo.loginApp("FB", "TC_SignUp_011");
				Thread.sleep(50000);
			} else {
				NXGReports.addStep("Guest userMove to Fav from Shopping bag for GMAIL user SignUp", LogAs.INFO, null);
				loginPo.addGmailAcc("TC_SignUp_012");
				Thread.sleep(25000);
			}
			if (BaseLib.verifyToastMessage(driver).equals("Cha-ching added  to your favorites")) {
				NXGReports.addStep("Cha-ching added  to your favorites", LogAs.PASSED, null);
			} else {
				NXGReports.addStep("Cha-ching added  to your favorites toast meassge is not displayed", LogAs.INFO,
						new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			navigateBackToHomePage();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyFavouritesLnk().click();
			String prodMovedFromCart = driver
					.findElement(By.xpath("//android.widget.TextView[@text='" + prodMoveToFav + "']")).getText();
			Assert.assertTrue(prodMoveToFav.contains(prodMovedFromCart), "the value");
			NXGReports.addStep(prodMovedFromCart + " The Favorites product text is  displayed", LogAs.PASSED, null);
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description: Check the Google+ Guest Sign-up functionality while placing
	 * the order for Normal/FB/Google+ user
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Test(dataProvider = "getUsers", enabled = false, priority = 18, description = "Guest Sign-up functionality while placing the order for Normal/FB/Google+ user")
	public void testGuestSignUpPlaceOrder(String sUser, String device) throws Exception {
		try {
			loginPo.getEleLoginCloseBtn().click();
			loginPo.handleOkayBtn();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[4]);
			BaseLib.tapOnElement(.98, .99);
			loginPo.handleOkayBtn();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			browsePlpPdpPo.getEleProductImageLst().get(1).click();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Pdp");
			driver.findElement(By
					.xpath("//android.widget.LinearLayout[@resource-id='com.abof.android:id/sizeLayout']//android.widget.LinearLayout//android.widget.TextView[@index='0']"))
					.click();
			browsePlpPdpPo.getElePdpAddToBagBtn().click();
			int prevBadgeCount = Integer.parseInt(browsePlpPdpPo.getEleBadgeCount().getText());
			browsePlpPdpPo.getElePdpViewBagLnk().click();
			BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
			shoppingBagPo.getElePlaceOrderBtn().click();
			Thread.sleep(2000);
			if (sUser.equalsIgnoreCase("ABOF")) {
				NXGReports.addStep("Guest user while placing the order for Normal (ABOF) user SignUp", LogAs.INFO,
						null);
				loginPo.signUpUser("ABOF", "TC_SignUp_013");
			} else if (sUser.equalsIgnoreCase("FBLogged")) {
				NXGReports.addStep("Guest user Fav from PLP for FB user SignUp", LogAs.INFO, null);
				driver.startActivity("com.facebook.katana", "com.facebook.katana.LoginActivity");
				Thread.sleep(5000);
				loginPo.facebookLogout();
				Thread.sleep(5000);
				driver.startActivity("com.abof.android", "com.abof.android.login.views.LoginActivity");
				loginPo.loginApp("FB", "TC_SignUp_014");
				Thread.sleep(50000);
			} else {
				NXGReports.addStep("Guest user while placing the order for GMAIL user SignUp", LogAs.INFO, null);
				loginPo.addGmailAcc("TC_SignUp_015");
				Thread.sleep(25000);
			}
			BaseLib.waitForElement(shoppingBagPo.getEleDeliveryAddressTxt(), "Develery address text is displayed ",
					"Develery address text is not displayed");
			Assert.assertTrue(shoppingBagPo.getEleDeliveryAddressTxt().isDisplayed());
			driver.navigate().back();
			int currBadgeCount = Integer.parseInt(shoppingBagPo.getEleShoppingBagCount().getText());
			Assert.assertTrue(prevBadgeCount < currBadgeCount,
					"cart merge is not working fine/user doesn't have existing item");
			NXGReports.addStep("cart merge is working fine ", LogAs.PASSED, null);
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:Check the normal Guest Sign-up functionality while accessing
	 * the my abof option/signin option in hamburger menu for Normal/FB/Google+
	 * user
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Test(dataProvider = "getUsers", enabled = false, priority = 19, description = "Guest Sign-up for my abof option/signin option in hamburger menu for Normal/FB/Google+ user")
	public void testGuestSignUpForMyAbofProfile(String sUser, String device) throws Exception {
		try {
			loginPo.getEleLoginCloseBtn().click();
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			BaseLib.elementStatus(hamburgerMenuPo.getEleUserProfileNameTxt(), "Hi Guest ", "displayed");
			String prevUserName = hamburgerMenuPo.getEleUserProfileNameTxt().getText();
			hamburgerMenuPo.getEleUserProfileImg().click();
			Thread.sleep(2000);
			GenericLib.isVisible(GenericLib.path + "SignInToAccessProfile.PNG");
			if (sUser.equalsIgnoreCase("ABOF")) {
				NXGReports.addStep(
						"Guest user my abof option/signin option in hamburger menu for Normal (ABOF) user SignUp",
						LogAs.INFO, null);
				loginPo.signUpUser("ABOF", "TC_SignUp_016");
			} else if (sUser.equalsIgnoreCase("FBLogged")) {
				NXGReports.addStep("Guest user Fav from PLP for FB user SignUp", LogAs.INFO, null);
				driver.startActivity("com.facebook.katana", "com.facebook.katana.LoginActivity");
				Thread.sleep(5000);
				loginPo.facebookLogout();
				driver.startActivity("com.abof.android", "com.abof.android.login.views.LoginActivity");
				Thread.sleep(5000);
				loginPo.loginApp("FB", "TC_SignUp_0017");
				Thread.sleep(50000);
			} else {
				NXGReports.addStep("Guest usermy abof option/signin option in hamburger menu for GMAIL user SignUp",
						LogAs.INFO, null);
				loginPo.addGmailAcc("TC_SignUp_018");
				Thread.sleep(25000);
			}
			BaseLib.waitForElement(homePagePo.getEleHamburgerMenuIcon(), "Hamburger Menu icon is displayed",
					"Hamburger Menu icon not is displayed");
			homePagePo.getEleHamburgerMenuIcon().click();
			NXGReports.addStep(hamburgerMenuPo.getEleUserProfileNameTxt().getText(), LogAs.INFO, null);
			String currUserName = hamburgerMenuPo.getEleUserProfileNameTxt().getText();
			Assert.assertNotEquals(prevUserName, currUserName, "Guest User name is same after logging in");
			NXGReports.addStep("Guest Login is success", LogAs.PASSED, null);
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description: Check whether the cart merge is working fine when guest
	 * user joining as a new user through normal sign-up screen for
	 * Normal/FB/Google+ user
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Test(dataProvider = "getUsers", enabled = false, priority = 20, description = "Guest Signup Cart merge for Normal/FB/Google+ user")
	public void testGuestSignUpCartMerge(String sUser, String device) throws Exception {
		try {
			loginPo.getEleLoginCloseBtn().click();
			loginPo.handleOkayBtn();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_002");
			homePagePo.searchOption(sData[4]);
			BaseLib.tapOnElement(.98, .99);
			loginPo.handleOkayBtn();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			browsePlpPdpPo.getEleProductImageLst().get(1).click();
			browsePlpPdpPo.toVerifyPageContentsDisplay("Pdp");
			driver.findElement(By
					.xpath("//android.widget.LinearLayout[@resource-id='com.abof.android:id/sizeLayout']//android.widget.LinearLayout//android.widget.TextView[@index='0']"))
					.click();
			browsePlpPdpPo.getElePdpAddToBagBtn().click();
			int prevBadgeCount = Integer.parseInt(browsePlpPdpPo.getEleBadgeCount().getText());
			browsePlpPdpPo.getElePdpViewBagLnk().click();
			BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
			shoppingBagPo.getElePlaceOrderBtn().click();
			Thread.sleep(2000);
			GenericLib.isVisible(GenericLib.path + "SignInToProceedToCheckout.PNG");
			if (sUser.equalsIgnoreCase("ABOF")) {
				NXGReports.addStep("Guest user Cart merge for Normal (ABOF) user SignUp", LogAs.INFO, null);
				loginPo.signUpUser("ABOF", "TC_SignUp_019");
			} else if (sUser.equalsIgnoreCase("FBLogged")) {
				NXGReports.addStep("Guest user Fav from PLP for FB user SignUp", LogAs.INFO, null);
				driver.startActivity("com.facebook.katana", "com.facebook.katana.LoginActivity");
				Thread.sleep(5000);
				loginPo.facebookLogout();
				Thread.sleep(5000);
				driver.startActivity("com.abof.android", "com.abof.android.login.views.LoginActivity");
				loginPo.loginApp("FB", "TC_SignUp_020");
				Thread.sleep(50000);
			} else {
				NXGReports.addStep("Guest user Cart merge for GMAIL user SignUp", LogAs.INFO, null);
				loginPo.addGmailAcc("TC_SignUp_021");
				Thread.sleep(25000);
			}
			BaseLib.waitForElement(shoppingBagPo.getEleDeliveryAddressTxt(), "Develery address text is displayed ",
					"Develery address text is not displayed");
			Assert.assertTrue(shoppingBagPo.getEleDeliveryAddressTxt().isDisplayed());
			driver.navigate().back();
			int currBadgeCount = Integer.parseInt(shoppingBagPo.getEleShoppingBagCount().getText());
			Assert.assertTrue(prevBadgeCount < currBadgeCount,
					"cart merge is not working fine/user doesn't have existing item");
			NXGReports.addStep("cart merge is working fine ", LogAs.PASSED, null);
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:Verify FBLogin functionalitywith and without app
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Parameters("device")
	@Test(enabled = false, priority = 21, description = "Verify FB Sign up with app")
	public void testFbSignUp(String device) throws Exception {
		try {
			loginPo.signUpUser("FB", "");
		} catch (Exception e) {
			throw e;
		}
	}
	/*
	 * @Description:Verify FBLogin functionalitywith and without app
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Parameters("device")
	@Test(enabled = false, priority = 22, description = "Verify FB Sign up with app")
	public void testGmailSignUp(String device) throws Exception {
		try {
			loginPo.getEleSignInGoogleLnkTab().click();
			loginPo.getEleChooseAcc("Add account").click();
			loginPo.getEleChooseAccOKBtn().click();
			Thread.sleep(5000);
			BaseLib.tapOnElement(.30,.70);
			BaseLib.tapOnElement(.30,.70);
			loginPo.getEleCreateGmailAccountLnk().click();
			Thread.sleep(5000);
			loginPo.getEleCreateGmailFirstNameTxtBox().sendKeys("Srinivas");
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * @Description:Verify FBLogin functionality for Inavlid credentials
	 * 
	 * @Author: Srinivas Hippargi
	 */
	@Parameters("device")
	@Test(enabled = false, priority = 22, description = "Verify FB Login with Inavild Credentials")
	public void testFbInavidLoginValidation(String device) throws Exception {
		try {
			sData = GenericLib.toReadExcelData("Login", "TC_Login_006");
			Thread.sleep(5000);
			driver.startActivity("com.facebook.katana", "com.facebook.katana.LoginActivity");
			Thread.sleep(10000);
			loginPo.facebookLogout();
			loginPo.getWtEleWEmailorPhoneTxtBox().clear();
			loginPo.getWtEleWEmailorPhoneTxtBox().sendKeys(sData[2]);
			loginPo.getWtEleFbPasswordTxtBox().sendKeys(sData[3]);
			loginPo.getWtEleFbLoginBtn().click();
			Thread.sleep(5000);
			Assert.assertTrue(loginPo.getEleFBInvalidLoginAlertMsg().isDisplayed(),
					"Error message is not displayed for FB inavild login");
		} catch (Exception e) {
			throw e;
		}
	}

	@DataProvider
	public Object[][] getUsers() {
		Object[][] user = new Object[3][2];
		String sDevice = GenericLib.getCongigValue(BaseLib.sConfigFile, "DeviceName");
		user[0][0] = "ABOF";
		user[0][1] = sDevice;
		user[1][0] = "GMAIL";
		user[1][1] = sDevice;
		user[2][0] = "FBLogged";
		user[2][1] = sDevice;
		return user;
	}

}
