package com.abof.android.testscripts;
/*package com.abof.scripts;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.abof.library.BaseLib;
import com.abof.library.GenericLib;
import com.abof.pageobjects.BrowsePlpPdpPO;
import com.abof.pageobjects.CheckOutPagePO;
import com.abof.pageobjects.HamburgerMenuPO;
import com.abof.pageobjects.HomePagePO;
import com.abof.pageobjects.LoginPagePO;
import com.abof.pageobjects.MyFavouritesPagePO;
import com.abof.pageobjects.MyOrdersPagePO;
import com.abof.pageobjects.PaymentPagePO;
import com.abof.pageobjects.ShoppingBagPO;
import com.abof.pageobjects.WhatsHotLandingPO;

public class MyOrderScript extends BaseLib {
	LoginPagePO loginPo = null;
	HomePagePO homePagePo = null;
	HamburgerMenuPO hamburgerMenuPo = null;
	WhatsHotLandingPO whatsHotLandingPo = null;
	ShoppingBagPO shoppingBagPo = null;
	MyFavouritesPagePO myfavouritespagePo;
	BrowsePlpPdpPO browsePlpPdpPo = null;
	String productName = null;
	String[] sData = null;
	CheckOutPagePO checkOutPagePo = null;
	PaymentPagePO paymentPagePo = null;
	MyOrdersPagePO myOrdersPagePo = null;
	String OrderNoValue = null;
	int iPayableAmount=0;

	@BeforeMethod
	public void init() {
		loginPo = new LoginPagePO(driver);
		homePagePo = new HomePagePO(driver);
		hamburgerMenuPo = new HamburgerMenuPO(driver);
		whatsHotLandingPo = new WhatsHotLandingPO(driver);
		shoppingBagPo = new ShoppingBagPO(driver);
		myfavouritespagePo = new MyFavouritesPagePO(driver);
		browsePlpPdpPo = new BrowsePlpPdpPO(driver);
		checkOutPagePo = new CheckOutPagePO(driver);
		paymentPagePo = new PaymentPagePO(driver);
		myOrdersPagePo = new MyOrdersPagePO(driver);
	}

	@Parameters("device")
	@Test(enabled =true, description = "Placing an Order Via Debit card")
	public void PlaceOrderViaDebitCard(String device) throws Exception {
		System.out.println("PlaceOrderViaDebitCard");
		try {

			loginPo.loginApp("ABOF", "TC_Login_001");
			loginPo.handleOkayBtn();
			sData = GenericLib.toReadExcelData("Login", "TC_Search_001");
			homePagePo.searchOption(sData[2]);
			loginPo.handleOkayBtn();
			Thread.sleep(20000);
			browsePlpPdpPo.toVerifyPageContentsDisplay("Plp");
			browsePlpPdpPo.getEleProductImageLst().get(0).click();
			visibilityOfElementWait(browsePlpPdpPo.getElePdpSizeGuideLnk(), "sizeGuideLink");
			Thread.sleep(40000);
			browsePlpPdpPo.toVerifyPageContentsDisplay("Pdp");
			sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
			browsePlpPdpPo.pdpFlow(sData[7], sData[2], "", sData[6]);
			BaseLib.scrollToElement(4, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
			productName = browsePlpPdpPo.getElePdpProductName().getText();
			System.out.println(productName);
			browsePlpPdpPo.getElePdpAddToBagBtn().click();
			browsePlpPdpPo.verifyViewBagStaus();
			shoppingBagPo.getEleShoppingBagIcon().click();
			Thread.sleep(10000);
			shoppingBagPo.isProductAvailableInshoppingCart(productName);
			BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
			shoppingBagPo.getElePlaceOrderBtn().click();
			Thread.sleep(15000);
			
			System.out.println(checkOutPagePo.getEleExistingAddressRdBtnLst().size());
			Thread.sleep(5000);
			
			if(checkOutPagePo.getEleExistingAddressRdBtnLst().get(0).getAttribute("checked").equals("false"))
			{
				checkOutPagePo.getEleExistingAddressRdBtnLst().get(0).click();
				checkOutPagePo.getEleDeliverAddressBtn().click();
			
			}
			else
			{
				checkOutPagePo.getEleDeliverAddressBtn().click();
				
			}
			
			
			Thread.sleep(15000);
			if(paymentPagePo.getEleAbofBuckCheckbox().getAttribute("checked").equals("true"))
			{
				paymentPagePo.getEleAbofBuckCheckbox().click();
			}
			
			BaseLib.scrollToElement(4,"UP",.80,.20,paymentPagePo.getEleDebitCardOption());
			paymentPagePo.addpaymentdetailswithoutsSaveCard("Debit card","TC_PMT_001");
			Thread.sleep(40000);
			paymentPagePo.verifyContentPaymentThankYouMessage("Visa");
			Thread.sleep(40000);
			BaseLib.scrollToElement(4,"DOWN",.20,.80,myOrdersPagePo.getEleOrderNoTxt());
			OrderNoValue = myOrdersPagePo.getEleOrderNoValue().getText();
			System.out.println(OrderNoValue);
		} catch (Exception e) {
			throw e;
		}
	}

	
	@Parameters("device")
	@Test(enabled = false, description = "to verify Confirmation and Cancellation for Order Placed by debit card")
	public void confirmationAndCancellationForOrderPlacedByDC(String device ) throws Exception {
		System.out.println("depends on PlaceOrderViaDebitCard");
		try {
			
			loginPo.loginApp("ABOF", "TC_Login_001");
			Thread.sleep(5000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyOrdersLnk().click();
			Thread.sleep(25000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			
			Thread.sleep(45000);
			
			myOrdersPagePo.OrderStatusConfirmation(OrderNoValue,"ORDER PLACED");
			myOrdersPagePo.OrderCancellation(OrderNoValue);
			myOrdersPagePo.getEleContinueShoppingBtn().click();
			Thread.sleep(3000);
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyOrdersLnk().click();
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			myOrdersPagePo.OrderStatusConfirmation(OrderNoValue,"CANCELLED");
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Parameters("device")
	@Test(enabled=true,description=" placeOrderwithCOD")
	public void placeOrderwithCOD(String device) throws Exception
	{	System.out.println("placeOrderwithCOD");
		try
		{
			
	  
		loginPo.loginApp("ABOF", "TC_Login_001");
		Thread.sleep(5000);
		loginPo.handleOkayBtn();
		sData = GenericLib.toReadExcelData("Login", "TC_Search_001");
		homePagePo.searchOption(sData[2]);
		loginPo.handleOkayBtn();
		Thread.sleep(40000);
		browsePlpPdpPo.getElePlpProductNameLst().get(0).click();
		Thread.sleep(40000);
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpSizeGuideLnk(), " sizeGuideLink is not displayed");
		browsePlpPdpPo.pdpFlow(sData[7], sData[2], "", sData[6]);
		BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		browsePlpPdpPo.getEleBackBtn().click();
		Thread.sleep(40000);
		System.out.println("sleep over");
		System.out.println(browsePlpPdpPo.getElePlpProductNameLst().get(1).getText());
		browsePlpPdpPo.getElePlpProductNameLst().get(1).click();
		Thread.sleep(40000);
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpSizeGuideLnk(), " sizeGuideLink is not displayed");
		browsePlpPdpPo.pdpFlow(sData[7], sData[2], "", sData[6]);
		BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpViewBagStatusMsg(),
				" View Bag Status Msg is not displayed");
		browsePlpPdpPo.verifyViewBagStaus();
		shoppingBagPo.getEleShoppingBagIcon().click();
		Thread.sleep(30000);
		BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
		shoppingBagPo.getElePlaceOrderBtn().click();
		Thread.sleep(20000);
		if(checkOutPagePo.getEleExistingAddressRdBtnLst().get(0).getAttribute("checked").equals("false"))
		{
			checkOutPagePo.getEleExistingAddressRdBtnLst().get(0).click();
			checkOutPagePo.getEleDeliverAddressBtn().click();
		
		}
		else
		{
			checkOutPagePo.getEleDeliverAddressBtn().click();
			
		}
		
		Thread.sleep(15000);
		System.out.println(paymentPagePo.getEleAbofBuckCheckbox().getAttribute("checked"));
		
		if(paymentPagePo.getEleAbofBuckCheckbox().getAttribute("checked").equals("true"))
		{
			paymentPagePo.getEleAbofBuckCheckbox().click();
		}
		BaseLib.scrollToElement(4,"UP",.80,.20,paymentPagePo.getEleCashOnDeliveryOption());
		paymentPagePo.getEleCashOnDeliveryOption().click();
		Thread.sleep(5000);
		paymentPagePo.cashOnDeliveryScreenVerification();
		Thread.sleep(5000);
		paymentPagePo.getEleConfirmOrderBtn().click();
		Thread.sleep(40000);
		paymentPagePo.verifyContentPaymentThankYouMessage("COD");
		Thread.sleep(40000);
		BaseLib.scrollToElement(4,"DOWN",.20,.80,myOrdersPagePo.getEleOrderNoTxt());
		OrderNoValue = myOrdersPagePo.getEleOrderNoValue().getText();
		System.out.println(OrderNoValue);
		
		
		
	} catch (Exception e) {
		throw e;
	}

		
	}
	@Parameters("device")
	@Test(enabled = true,description = "to verify Confirmation and Cancellation for Order Placed by COD")
	public void confirmationAndCancellationForOrderPlacedByCOD(String device) throws Exception {
		try {
			System.out.println("depends on placeOrderwithCOD");
			loginPo.loginApp("ABOF", "TC_Login_001");
			Thread.sleep(5000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyOrdersLnk().click();
			Thread.sleep(45000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			
			Thread.sleep(10000);
			myOrdersPagePo.OrderStatusConfirmation(OrderNoValue,"ORDER PLACED");
			myOrdersPagePo.OrderCancellation(OrderNoValue);
			myOrdersPagePo.getEleContinueShoppingBtn().click();
			Thread.sleep(3000);
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyOrdersLnk().click();
			Thread.sleep(40000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			Thread.sleep(10000);
			myOrdersPagePo.OrderStatusConfirmation(OrderNoValue,"CANCELLED");
			driver.findElement(By.xpath(
					"android.widget.TextView[contains(@text,'" + OrderNoValue + "')]/../../android.widget.ImageView"))
					.click();
			myOrdersPagePo.getEleViewDetailsOpt().click();
			
		} catch (Exception e) {
			throw e;
		}
	}
	@Parameters("device")
	@Test(enabled=true,description="placeOrderWithAbofBucks")
	public void placeOrderWithAbofBucks(String device) throws Exception
	{	System.out.println("placeOrderWithAbofBucks");
		try
		{
		loginPo.loginApp("ABOF", "TC_Login_001");
		Thread.sleep(5000);
		loginPo.handleOkayBtn();
		shoppingBagPo.removeItemInShoppingBag();
		Thread.sleep(10000);
		sData = GenericLib.toReadExcelData("Login", "TC_Search_001");
		homePagePo.searchOption(sData[2]);
		Thread.sleep(20000);
		loginPo.handleOkayBtn();
		Thread.sleep(40000);
		browsePlpPdpPo.getElePlpProductNameLst().get(0).click();
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpSizeGuideLnk(), " sizeGuideLink is not displayed");
		browsePlpPdpPo.pdpFlow(sData[7], sData[2], "", sData[6]);
		BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		Thread.sleep(20000);
		browsePlpPdpPo.getEleBackBtn().click();
		browsePlpPdpPo.getElePlpProductNameLst().get(1).click();
		Thread.sleep(40000);
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpSizeGuideLnk(), " sizeGuideLink is not displayed");
		browsePlpPdpPo.pdpFlow(sData[7], sData[2], "", sData[6]);
		BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpViewBagStatusMsg(),
				" View Bag Status Msg is not displayed");
		browsePlpPdpPo.verifyViewBagStaus();
		shoppingBagPo.getEleShoppingBagIcon().click();
		
		Thread.sleep(30000);
		BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
		String sPayableAmount=shoppingBagPo.getEleShoppingBagPayableAmount().getText();
		
		int iPayableAmount=browsePlpPdpPo.convertPriceToIntValue(sPayableAmount);
		shoppingBagPo.getElePlaceOrderBtn().click();
		Thread.sleep(25000);
		if(!(checkOutPagePo.getEleExistingAddressRdBtnLst().get(0).getAttribute("checked").equals("false")))
		{
			checkOutPagePo.getEleExistingAddressRdBtnLst().get(0).click();
			checkOutPagePo.getEleDeliverAddressBtn().click();
		
		}
		else
		{
			checkOutPagePo.getEleDeliverAddressBtn().click();
			
		}
		Thread.sleep(25000);
		paymentPagePo.abofBucksSegmentVerification(iPayableAmount);
		paymentPagePo.getEleConfirmOrderBtn().click();
		Thread.sleep(40000);
		paymentPagePo.verifyContentPaymentThankYouMessage("abofBucks");
		Thread.sleep(40000);
		BaseLib.scrollToElement(4,"DOWN",.20,.80,myOrdersPagePo.getEleOrderNoTxt());
		OrderNoValue = myOrdersPagePo.getEleOrderNoValue().getText();
		System.out.println(OrderNoValue);
		
		
	} catch (Exception e) {
		throw e;
	}

		
	}
	@Parameters("device")
	@Test(enabled = true,description = "to verify Confirmation and Cancellation for Order Placed by COD")
	public void confirmationAndCancellationForOrderPlacedByAbofBucks(String device) throws Exception {
		System.out.println("depends onplaceOrderWithAbofBucks");
		try {
			
			loginPo.loginApp("ABOF", "TC_Login_001");
			Thread.sleep(5000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyOrdersLnk().click();
			Thread.sleep(45000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			
			Thread.sleep(10000);
			myOrdersPagePo.OrderStatusConfirmation(OrderNoValue,"ORDER PLACED");
			myOrdersPagePo.OrderCancellation(OrderNoValue);
			myOrdersPagePo.getEleContinueShoppingBtn().click();
			Thread.sleep(3000);
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyOrdersLnk().click();
			Thread.sleep(40000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			Thread.sleep(10000);
			myOrdersPagePo.OrderStatusConfirmation(OrderNoValue,"CANCELLED");
			driver.findElement(By.xpath(
					"android.widget.TextView[contains(@text,'" + OrderNoValue + "')]/../../android.widget.ImageView"))
					.click();
			myOrdersPagePo.getEleViewDetailsOpt().click();
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Parameters("device")
	@Test(enabled=true,description="placeOrderWithAbofBucksAndDebitCard")
	public void placeOrderWithAbofBucksAndDebitCard(String device) throws Exception
	{System.out.println("placeOrderWithAbofBucksAndDebitCard");
		try
		{
		loginPo.loginApp("ABOF", "TC_Login_001");
		Thread.sleep(5000);
		loginPo.handleOkayBtn();
		sData = GenericLib.toReadExcelData("Login", "TC_Search_001");
		homePagePo.searchOption(sData[2]);
		Thread.sleep(20000);
		loginPo.handleOkayBtn();
		Thread.sleep(40000);
		browsePlpPdpPo.getElePlpProductNameLst().get(0).click();
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpSizeGuideLnk(), " sizeGuideLink is not displayed");
		browsePlpPdpPo.pdpFlow(sData[7], sData[2], "", sData[6]);
		BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		browsePlpPdpPo.getEleBackBtn().click();
		Thread.sleep(20000);
		browsePlpPdpPo.getElePlpProductNameLst().get(1).click();
		Thread.sleep(40000);
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpSizeGuideLnk(), " sizeGuideLink is not displayed");
		browsePlpPdpPo.pdpFlow(sData[7], sData[2], "", sData[6]);
		BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpViewBagStatusMsg(),
				" View Bag Status Msg is not displayed");
		browsePlpPdpPo.verifyViewBagStaus();
		Thread.sleep(20000);
		browsePlpPdpPo.getEleCartIcon().click();
		
		Thread.sleep(10000);
		BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
		String sPayableAmount=shoppingBagPo.getEleShoppingBagPayableAmount().getText();
		
		 iPayableAmount=browsePlpPdpPo.convertPriceToIntValue(sPayableAmount);
		shoppingBagPo.getElePlaceOrderBtn().click();
		Thread.sleep(5000);
		if(!(checkOutPagePo.getEleExistingAddressRdBtnLst().get(0).getAttribute("checked").equals("false")))
		{
			checkOutPagePo.getEleExistingAddressRdBtnLst().get(0).click();
			checkOutPagePo.getEleDeliverAddressBtn().click();
		
		}
		else
		{
			checkOutPagePo.getEleDeliverAddressBtn().click();
			
		}
		Thread.sleep(25000);
		paymentPagePo.abofBucksSegmentVerification(iPayableAmount);
		BaseLib.scrollToElement(4,"UP",.80,.20,paymentPagePo.getEleDebitCardOption());
		
		paymentPagePo.addpaymentdetailswithoutsSaveCard("DebitCard","TC_PMT_001");
		Thread.sleep(40000);
		paymentPagePo.verifyContentPaymentThankYouMessage("Visa");
		Thread.sleep(40000);
		BaseLib.scrollToElement(4,"DOWN",.20,.80,myOrdersPagePo.getEleOrderNoTxt());
		OrderNoValue = myOrdersPagePo.getEleOrderNoValue().getText();
		System.out.println(OrderNoValue);
		
	} catch (Exception e) {
		throw e;
	}

		
	}
	@Parameters("device")
	@Test(enabled = true,description = "to verify Confirmation and Cancellation for Order Placed by COD")
	public void confirmationAndCancellationForOrderPlacedByAbofbucksAndDebitcard(String device) throws Exception {
		System.out.println("depends on placeOrderWithAbofBucksAndDebitCard");
		try {
			
			loginPo.loginApp("ABOF", "TC_Login_001");
			Thread.sleep(5000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyOrdersLnk().click();
			Thread.sleep(45000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			
			Thread.sleep(10000);
			myOrdersPagePo.OrderStatusConfirmation(OrderNoValue,"ORDER PLACED");
			myOrdersPagePo.OrderCancellation(OrderNoValue);
			myOrdersPagePo.getEleContinueShoppingBtn().click();
			Thread.sleep(3000);
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyOrdersLnk().click();
			Thread.sleep(40000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			Thread.sleep(10000);
			myOrdersPagePo.OrderStatusConfirmation(OrderNoValue,"CANCELLED");
			driver.findElement(By.xpath(
					"android.widget.TextView[contains(@text,'" + OrderNoValue + "')]/../../android.widget.ImageView"))
					.click();
			myOrdersPagePo.getEleViewDetailsOpt().click();
			
		} catch (Exception e) {
			throw e;
		}
	}
	@Parameters("device")
	@Test(enabled=true,description="placeOrderWithCreditCard")
	public void placeOrderWithCreditCard(String device) throws Exception
	{System.out.println("placeOrderWithCreditCard");
		try
		{
		loginPo.loginApp("ABOF", "TC_Login_001");
		Thread.sleep(5000);
		loginPo.handleOkayBtn();
		sData = GenericLib.toReadExcelData("Login", "TC_Search_001");
		homePagePo.searchOption(sData[2]);
		Thread.sleep(20000);
		loginPo.handleOkayBtn();
		Thread.sleep(20000);
		browsePlpPdpPo.getElePlpProductNameLst().get(0).click();
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpSizeGuideLnk(), " sizeGuideLink is not displayed");
		browsePlpPdpPo.pdpFlow(sData[7], sData[2], "", sData[6]);
		BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		Thread.sleep(30000);
		browsePlpPdpPo.getEleBackBtn().click();
		browsePlpPdpPo.getElePlpProductNameLst().get(1).click();
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpSizeGuideLnk(), " sizeGuideLink is not displayed");
		browsePlpPdpPo.pdpFlow(sData[7], sData[2], "", sData[6]);
		BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		Thread.sleep(20000);
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpViewBagStatusMsg(),
				" View Bag Status Msg is not displayed");
		browsePlpPdpPo.verifyViewBagStaus();
		browsePlpPdpPo.getEleCartIcon().click();
		Thread.sleep(30000);
		BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
		shoppingBagPo.getElePlaceOrderBtn().click();
		Thread.sleep(25000);
		if(!(checkOutPagePo.getEleExistingAddressRdBtnLst().get(0).getAttribute("checked").equals("false")))
		{
			checkOutPagePo.getEleExistingAddressRdBtnLst().get(0).click();
			checkOutPagePo.getEleDeliverAddressBtn().click();
		
		}
		else
		{
			checkOutPagePo.getEleDeliverAddressBtn().click();
			
		}
		Thread.sleep(25000);
		if(paymentPagePo.getEleAbofBuckCheckbox().getAttribute("checked").equals("true"))
		{
			paymentPagePo.getEleAbofBuckCheckbox().click();
		}
		BaseLib.scrollToElement(4,"UP",.80,.20,paymentPagePo.getEleCreditCardOption());
		Thread.sleep(5000);
		paymentPagePo.getEleCreditCardOption().click();
		Thread.sleep(5000);
		paymentPagePo.addpaymentdetailswithoutsSaveCard("Credit card","TC_PMT_003");
		Thread.sleep(40000);
		paymentPagePo.verifyContentPaymentThankYouMessage("Visa");
		Thread.sleep(40000);
		BaseLib.scrollToElement(4,"DOWN",.20,.80,myOrdersPagePo.getEleOrderNoTxt());
		OrderNoValue = myOrdersPagePo.getEleOrderNoValue().getText();
		System.out.println(OrderNoValue);
		
		
	} catch (Exception e) {
		throw e;
	}

		
	}
	
	@Parameters("device")
	@Test(enabled = true,description = "to verify Confirmation and Cancellation for Order Placed by COD")
	public void confirmationAndCancellationForOrderPlacedByCC(String device) throws Exception {
		System.out.println("depends onplaceOrderWithCreditCard");
		
		try {
			
			loginPo.loginApp("ABOF", "TC_Login_001");
			Thread.sleep(5000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyOrdersLnk().click();
			Thread.sleep(45000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			
			Thread.sleep(10000);
			myOrdersPagePo.OrderStatusConfirmation(OrderNoValue,"ORDER PLACED");
			myOrdersPagePo.OrderCancellation(OrderNoValue);
			myOrdersPagePo.getEleContinueShoppingBtn().click();
			Thread.sleep(3000);
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyOrdersLnk().click();
			Thread.sleep(40000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			Thread.sleep(10000);
			myOrdersPagePo.OrderStatusConfirmation(OrderNoValue,"CANCELLED");
			driver.findElement(By.xpath(
					"android.widget.TextView[contains(@text,'" + OrderNoValue + "')]/../../android.widget.ImageView"))
					.click();
			myOrdersPagePo.getEleViewDetailsOpt().click();
			
		} catch (Exception e) {
			throw e;
		}
	}
	@Parameters("device")
	@Test(enabled=true,description="placeOrderWithAbofBucksAndCreditCard")
	public void placeOrderWithAbofBucksAndCreditCard(String device) throws Exception
	{
		System.out.println("placeOrderWithAbofBucksAndCreditCard");
		try
		{
		loginPo.loginApp("ABOF", "TC_Login_001");
		Thread.sleep(5000);
		loginPo.handleOkayBtn();
		sData = GenericLib.toReadExcelData("Login", "TC_Search_001");
		homePagePo.searchOption(sData[2]);
		Thread.sleep(20000);
		loginPo.handleOkayBtn();
		Thread.sleep(40000);
		browsePlpPdpPo.getElePlpProductNameLst().get(0).click();
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpSizeGuideLnk(), " sizeGuideLink is not displayed");
		browsePlpPdpPo.pdpFlow(sData[7], sData[2], "", sData[6]);
		BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		browsePlpPdpPo.getEleBackBtn().click();
		Thread.sleep(40000);
		browsePlpPdpPo.getElePlpProductNameLst().get(1).click();
		Thread.sleep(40000);
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpSizeGuideLnk(), " sizeGuideLink is not displayed");
		browsePlpPdpPo.pdpFlow(sData[7], sData[2], "", sData[6]);
		BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpViewBagStatusMsg(),
				" View Bag Status Msg is not displayed");
		browsePlpPdpPo.verifyViewBagStaus();
		browsePlpPdpPo.getEleCartIcon().click();
		
		Thread.sleep(10000);
		BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
		String sPayableAmount=shoppingBagPo.getEleShoppingBagPayableAmount().getText();
		
		 iPayableAmount=browsePlpPdpPo.convertPriceToIntValue(sPayableAmount);
		shoppingBagPo.getElePlaceOrderBtn().click();
		Thread.sleep(5000);
		if(!(checkOutPagePo.getEleExistingAddressRdBtnLst().get(0).getAttribute("checked").equals("false")))
		{
			checkOutPagePo.getEleExistingAddressRdBtnLst().get(0).click();
			checkOutPagePo.getEleDeliverAddressBtn().click();
		
		}
		else
		{
			checkOutPagePo.getEleDeliverAddressBtn().click();
			
		}
		Thread.sleep(25000);
		paymentPagePo.abofBucksSegmentVerification(iPayableAmount);
		BaseLib.scrollToElement(4,"UP",.80,.20,paymentPagePo.getEleCreditCardOption());
		paymentPagePo.addpaymentdetailswithoutsSaveCard("Credit card","TC_PMT_003");
		Thread.sleep(40000);
		paymentPagePo.verifyContentPaymentThankYouMessage("Visa");
		Thread.sleep(40000);
		BaseLib.scrollToElement(4,"DOWN",.20,.80,myOrdersPagePo.getEleOrderNoTxt());
		OrderNoValue = myOrdersPagePo.getEleOrderNoValue().getText();
		System.out.println(OrderNoValue);
		
	} catch (Exception e) {
		throw e;
	}

		
	}
	@Parameters("device")
	@Test(enabled = true,description = "to verify Confirmation and Cancellation for Order Placed by COD" )
	public void confirmationAndCancellationForOrderPlacedByAbofbucksAndCreditcard(String device) throws Exception {
		
		System.out.println("depends on placeOrderWithAbofBucksAndCreditCard");
		try {
			
			loginPo.loginApp("ABOF", "TC_Login_001");
			Thread.sleep(5000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyOrdersLnk().click();
			Thread.sleep(45000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			
			Thread.sleep(10000);
			myOrdersPagePo.OrderStatusConfirmation(OrderNoValue,"ORDER PLACED");
			myOrdersPagePo.OrderCancellation(OrderNoValue);
			myOrdersPagePo.getEleContinueShoppingBtn().click();
			Thread.sleep(3000);
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyOrdersLnk().click();
			Thread.sleep(40000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			Thread.sleep(10000);
			myOrdersPagePo.OrderStatusConfirmation(OrderNoValue,"CANCELLED");
			driver.findElement(By.xpath(
					"android.widget.TextView[contains(@text,'" + OrderNoValue + "')]/../../android.widget.ImageView"))
					.click();
			myOrdersPagePo.getEleViewDetailsOpt().click();
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Parameters("device")
	@Test(enabled=true,description="placeOrderWithAbofBucksAndCOD")
	public void placeOrderWithAbofBucksAndCOD(String device) throws Exception
	{	System.out.println("placeOrderWithAbofBucksAndCOD");
		try
		{
		loginPo.loginApp("ABOF", "TC_Login_001");
		Thread.sleep(5000);
		loginPo.handleOkayBtn();
		sData = GenericLib.toReadExcelData("Login", "TC_Search_001");
		homePagePo.searchOption(sData[2]);
		Thread.sleep(20000);
		loginPo.handleOkayBtn();
		Thread.sleep(40000);
		
		browsePlpPdpPo.getElePlpProductNameLst().get(0).click();
		Thread.sleep(20000);
		
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpSizeGuideLnk(), " sizeGuideLink is not displayed");
		browsePlpPdpPo.pdpFlow(sData[7], sData[2], "", sData[6]);
		BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		browsePlpPdpPo.getEleBackBtn().click();
		Thread.sleep(20000);
		
		browsePlpPdpPo.getElePlpProductNameLst().get(1).click();
		Thread.sleep(40000);
		
		sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpSizeGuideLnk(), " sizeGuideLink is not displayed");
		browsePlpPdpPo.pdpFlow(sData[7], sData[2], "", sData[6]);
		BaseLib.scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
		browsePlpPdpPo.getElePdpAddToBagBtn().click();
		BaseLib.waitforElement(driver, browsePlpPdpPo.getElePdpViewBagStatusMsg(),
				" View Bag Status Msg is not displayed");
		browsePlpPdpPo.verifyViewBagStaus();
		browsePlpPdpPo.getEleCartIcon().click();
		
		Thread.sleep(10000);
		BaseLib.scrollToElement(5, "UP", .90, .50, shoppingBagPo.getElePlaceOrderBtn());
		String sPayableAmount=shoppingBagPo.getEleShoppingBagPayableAmount().getText();
		
		 iPayableAmount=browsePlpPdpPo.convertPriceToIntValue(sPayableAmount);
		shoppingBagPo.getElePlaceOrderBtn().click();
		Thread.sleep(5000);
		if(!(checkOutPagePo.getEleExistingAddressRdBtnLst().get(0).getAttribute("checked").equals("false")))
		{
			checkOutPagePo.getEleExistingAddressRdBtnLst().get(0).click();
			checkOutPagePo.getEleDeliverAddressBtn().click();
		
		}
		else
		{
			checkOutPagePo.getEleDeliverAddressBtn().click();
			
		}
		Thread.sleep(25000);
		paymentPagePo.abofBucksSegmentVerification(iPayableAmount);
		BaseLib.scrollToElement(4,"UP",.80,.20,paymentPagePo.getEleCashOnDeliveryOption());
		paymentPagePo.getEleCashOnDeliveryOption().click();
		Thread.sleep(5000);
		paymentPagePo.cashOnDeliveryScreenVerification();
		Thread.sleep(5000);
		paymentPagePo.getEleConfirmOrderBtn().click();
		Thread.sleep(40000);
		paymentPagePo.verifyContentPaymentThankYouMessage("Visa");
		Thread.sleep(40000);
		BaseLib.scrollToElement(4,"DOWN",.20,.80,myOrdersPagePo.getEleOrderNoTxt());
		OrderNoValue = myOrdersPagePo.getEleOrderNoValue().getText();
		System.out.println(OrderNoValue);
		
	} catch (Exception e) {
		throw e;
	}
	}
	@Parameters("device")
	@Test(enabled = true,description = "to verify Confirmation and Cancellation for Order Placed by COD")
	public void confirmationAndCancellationForOrderPlacedByAbofbucksAndCOD(String device) throws Exception {
		
		System.out.println("depends on placeOrderWithAbofBucksAndCOD");
		try {
			
			loginPo.loginApp("ABOF", "TC_Login_001");
			Thread.sleep(5000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyOrdersLnk().click();
			Thread.sleep(45000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			
			Thread.sleep(10000);
			myOrdersPagePo.OrderStatusConfirmation(OrderNoValue,"ORDER PLACED");
			myOrdersPagePo.OrderCancellation(OrderNoValue);
			myOrdersPagePo.getEleContinueShoppingBtn().click();
			Thread.sleep(3000);
			homePagePo.getEleHamburgerMenuIcon().click();
			hamburgerMenuPo.getEleMyOrdersLnk().click();
			Thread.sleep(40000);
			BaseLib.waitForElement(loginPo.getEleOkayBtn(), "Okay btn is displayed", "Okay btn is not displayed");
			loginPo.handleOkayBtn();
			Thread.sleep(10000);
			myOrdersPagePo.OrderStatusConfirmation(OrderNoValue,"CANCELLED");
			driver.findElement(By.xpath(
					"android.widget.TextView[contains(@text,'" + OrderNoValue + "')]/../../android.widget.ImageView"))
					.click();
			myOrdersPagePo.getEleViewDetailsOpt().click();
			
		} catch (Exception e) {
			throw e;
		}
	}
}
*/