/***********************************************************************
* @author 			:		Prerana Bhatt

* @description		: 		Test scripts of Skult
* @module			:		Payment
* @testmethod		:	   	testVerifySkult(),veirfySkultLookDetailOutOfStockProduct(),skultLookDetailEndToEndFlow()
*/
package com.abof.android.testscripts;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.abof.android.pageobjects.BrowsePlpPdpPO;
import com.abof.android.pageobjects.HamburgerMenuPO;
import com.abof.android.pageobjects.HomePagePO;
import com.abof.android.pageobjects.LoginPagePO;
import com.abof.android.pageobjects.MyFavouritesPagePO;
import com.abof.android.pageobjects.ShoppingBagPO;
import com.abof.android.pageobjects.SkultPO;
import com.abof.android.pageobjects.WhatsHotLandingPO;
import com.abof.library.BaseLib;
import com.abof.library.GenericLib;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;

public class SkultScript extends BaseLib {

	LoginPagePO loginPo = null;
	HomePagePO homePagePo = null;
	HamburgerMenuPO hamburgerMenuPo = null;
	WhatsHotLandingPO whatsHotLandingPo = null;
	ShoppingBagPO shoppingBagPo = null;
	MyFavouritesPagePO myfavouritespagePo;
	BrowsePlpPdpPO browsePlpPdpPo = null;
	SkultPO skultpo=null;
	String productSaveForLater = null;
	String removedProductFromFav = null;
	String[] sData = null;
	String size = null;
	String pin = null;
	String[] ss = null;
	String text = null;
	String PDP = null;
	String productName = null;
	String ProductfoundTxt=null;
	int count=1;
	int productFound=0;

	@BeforeMethod
	public void init() {
		loginPo = new LoginPagePO(driver);
		homePagePo = new HomePagePO(driver);
		hamburgerMenuPo = new HamburgerMenuPO(driver);
		whatsHotLandingPo = new WhatsHotLandingPO(driver);
		shoppingBagPo = new ShoppingBagPO(driver);
		myfavouritespagePo = new MyFavouritesPagePO(driver);
		skultpo = new SkultPO(driver);
		browsePlpPdpPo=new BrowsePlpPdpPO(driver);
	}
	
	
	/* @Description:This method is to verify ,skult favorite(selected/remove),and share functionality 
	   * @Author: Prerana Bhatt*/
	 @Parameters("device")
	 @Test(enabled =true, priority = 1, description = "Login as abof user,Check the Product in Favourites and share functionality")
	 public void toVerifySkult(String device) throws Exception {
			try {
				
				loginPo.loginApp("ABOF", "TC_Login_001");
				BaseLib.waitForElement( loginPo.getEleOkayBtn(),"OkayBtn is displayed","OkayBtn is not displayed");
				loginPo.handleOkayBtn();
				shoppingBagPo.removeItemInShopping();
				BaseLib.waitForElement( whatsHotLandingPo.getEleMenModule(),"MenModule is displayed","MenModule is not displayed");
				whatsHotLandingPo.getEleMenModule().click();

				scrollToElement(3, "UP", .70, .20, whatsHotLandingPo.getEleViewAllBtn());
				BaseLib.waitForElement( whatsHotLandingPo.getEleViewAllBtn(),"ViewAllBtn is displayed", "ViewAllBtn is not displayed");
				
				whatsHotLandingPo.getEleViewAllBtn().click();
				BaseLib.waitForElement( whatsHotLandingPo.getEleViewAllSearchTxtBx(),"View All Search TextBx is displayed",
						"View All Search TextBx is not displayed");
				whatsHotLandingPo.getEleViewAllSearchTxtBx().click();
				whatsHotLandingPo.getEleViewAllSearchTxtBx().sendKeys("skult");
				Thread.sleep(15000);
				whatsHotLandingPo.getEleShopBrandLst().get(1).click();
			
				BaseLib.waitForElement(browsePlpPdpPo.getElePlpFavIcon(),"fav icon is not displayed","fav icon is displayed");
				browsePlpPdpPo.getElePlpFavIcon().click();
				BaseLib.waitForElement( browsePlpPdpPo.getElePlpProductName()," PLP first Product Name is displayed",
						" PLP first Product Name is not displayed");
				productSaveForLater = browsePlpPdpPo.getElePlpProductName().getText();
				
				browsePlpPdpPo.getEleProductImageLst().get(1).click();
				loginPo.handleOkayBtn();
				sData = GenericLib.toReadExcelData("Login", "TC_PLPPDP_001");
				BaseLib.waitForElement( browsePlpPdpPo.getElePdpSizeGuideLnk(),"sizeGuideLink is displayed"," sizeGuideLink is not displayed");
				browsePlpPdpPo.pdpFlow(sData[2],"","0");// not selecting size and pincode 
				 BaseLib.waitForElement(browsePlpPdpPo.getElePdpShareLnk(),"PdpShareLink is displayed"," PdpShareLink is not displayed");
				 browsePlpPdpPo.getElePdpShareLnk().click(); 
				 scrollToElement(2,"UP", .80, .20, browsePlpPdpPo.getEleShareFacebookLnk());
				 BaseLib.waitForElement(browsePlpPdpPo.getEleShareFacebookLnk(),"PdpShareFacebookLink is displayed"," PdpShareFacebookLink is not displayed"); 
				 browsePlpPdpPo.getEleShareFacebookLnk().click();
				 BaseLib.waitForElement(browsePlpPdpPo.getEleSharedProductInfo(),"SharedProductInfo isdisplayed"," SharedProductInfo is not displayed");
				 BaseLib.waitForElement(browsePlpPdpPo.getEleFacebookPostBtn(),"FacebookPostBtn is displayed"," FacebookPostBtn is not displayed");
				 browsePlpPdpPo.getEleFacebookPostBtn().click();
				/* String postMsg = verifyToastMessage(driver); 
				 if(postMsg.contains("You sharedthis post")) {
				 NXGReports.addStep(" product is shared in facebook",LogAs.PASSED, null); 
				 }
*/				BaseLib.waitForElement( browsePlpPdpPo.getElePdpSizeGuideLnk(),"sizeGuideLink is displayed","sizeGuideLink is not displayed");
				browsePlpPdpPo.pdpFlow(sData[6], "", sData[5]);
				NXGReports.addStep(" size and pincode is selected in pdp ", LogAs.PASSED, null);
				browsePlpPdpPo.getElePdpSaveForLaterLnk().click();
				scrollToElement(2, "DOWN", .20, .80, browsePlpPdpPo.getEleCartIcon());
				productName=browsePlpPdpPo.getElePdpProductName().getText();
				BaseLib.waitForElement( browsePlpPdpPo.getElePdpAddToBagBtn()," Add To Bag btn is displayed"," Add To Bag btn is not displayed");
				browsePlpPdpPo.getElePdpAddToBagBtn().click();
				NXGReports.addStep(" add to bag btn is selected in pdp ", LogAs.PASSED, null);
				BaseLib.waitForElement( browsePlpPdpPo.getElePdpViewBagStatusMsg()," View Bag Status Msg is displayed",
						" View Bag Status Msg is not displayed");
				browsePlpPdpPo.verifyViewBagStaus();
				BaseLib.waitForElement( shoppingBagPo.getEleShoppingBagIcon()," ShoppingBagIcon is displayed" ," ShoppingBagIcon is not displayed");
				shoppingBagPo.getEleShoppingBagIcon().click();
				shoppingBagPo.isProductAvailableInshoppingCart(productName);
				removedProductFromFav=productName;
				
				driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+productName+"')]/../../..//android.widget.RelativeLayout[contains(@resource-id,'com.abof.android:id/add_favorite_view')]")).click();
				scrollToElement(2, "UP", .80, .20, shoppingBagPo.getElePlaceOrderBtn());
				
				shoppingBagPo.getEleContinueShoppingLink().click();
				
				Thread.sleep(30000);
				BaseLib.scrollToElement(5,"DOWN",.40,.80,homePagePo.getEleHamburgerMenuIcon());
				Thread.sleep(10000);
				BaseLib.waitForElement( homePagePo.getEleHamburgerMenuIcon()," Hamburger Menu Icon is not displayed" ," Hamburger Menu Icon is not displayed");
				homePagePo.getEleHamburgerMenuIcon().click();
				BaseLib.waitForElement( hamburgerMenuPo.getEleMyFavouritesLnk(),"My Favourites Lnk isdisplayed" ,"My Favourites Lnk is not displayed");
				hamburgerMenuPo.getEleMyFavouritesLnk().click();
				BaseLib.waitForElement( hamburgerMenuPo.getEleFavouritesTxt(),"Favourites title text isdisplayed" ,"Favourites title text is not displayed");
				hamburgerMenuPo.isProductAvailableInFavList(productSaveForLater);// to check whether given product is in list
				hamburgerMenuPo.isProductUnavailableInFavList(removedProductFromFav);// to check whether given product is not in list
				myfavouritespagePo.getEleFavBackBtn().click();
				
				
			
			} catch (Exception e) {
				throw e;
			}

		}
			
	/* @Description:Looks should be scroll able from left to right and right to left,without selecting size,without selecting product error message should be displayed
	  * @Author: Raghu Kiran*/
	@Parameters("device")
	@Test(enabled =true, priority = 2, description = "Looks should be scroll able from left to right and right to left,without selecting size,without selecting product error message should be displayed")
	public void testskultViewAllLooks(String device) throws Exception {
		loginPo.loginApp("ABOF", "TC_Login_001");
		loginPo.handleOkayBtn();
		myfavouritespagePo.handleFavourite();
		browsePlpPdpPo.getEleBackBtn().click();
		homePagePo.getEleHamburgerMenuIcon().click();
		BaseLib.waitForElement( hamburgerMenuPo.getEleShopMenLnk(),"Men Option is displayed in Whats Hot page",
				" Men Option is  not displayed in Whats Hot page");
		hamburgerMenuPo.getEleShopMenLnk().click();
		BaseLib.scrollToElement(4, "UP", .70, .40, whatsHotLandingPo.getEleViewAllBtn());
		whatsHotLandingPo.getEleShopBrandGridViewImageLst().get(1).click();
		Thread.sleep(3000);
		BaseLib.scrollToElement(7, "UP", .80, .10, skultpo.getEleSkultViewAllLooksBtn());
		driver.findElement(By.id("com.abof.android:id/trending_header_image")).click();
		driver.findElement(By.id("com.abof.android:id/look_detail_add_to_cart_stickybutton")).click();
		GenericLib.isVisible(GenericLib.path + "PleaseSelectSize.PNG");
		whatsHotLandingPo.handlecheckbox();
		BaseLib.swipeBottomToTop(.22,.90);
		driver.findElement(By.id("com.abof.android:id/look_detail_add_to_cart_stickybutton")).click();
		GenericLib.isVisible(GenericLib.path + "PleaseSelectProduct.PNG");
		driver.findElement(By.id("com.abof.android:id/closeview")).click();
		skultpo.getEleSkultViewAllLooksBtn().click();
		BaseLib.elementStatus(skultpo.getEleSkultLooksTxt(), "Skult Looks text", "displayed");
		skultpo.getEleBackBtn().click();
		BaseLib.scrollToElement(7, "UP", .80, .10, skultpo.getEleSkultContentTxt());
		for (int i = 0; i <= 4; i++) {
			Thread.sleep(4000);
			driver.swipe(405, 847, 30, 859, 4000);
			Assert.assertTrue(skultpo.getEleSkultContentTxt().isDisplayed(),
					skultpo.getEleSkultContentTxt().getText() + " is dispalyed in the skult page");
			NXGReports.addStep(skultpo.getEleSkultContentTxt().getText() + "is dispalyed in the skult page",
					LogAs.PASSED, null);
		}
		for (int i = 0; i <= 4; i++) {
			Thread.sleep(4000);
			driver.swipe(30, 859, 405, 847, 4000);
			Assert.assertTrue(skultpo.getEleSkultContentTxt().isDisplayed(),
					skultpo.getEleSkultContentTxt().getText() + " is dispalyed in the skult page");
			NXGReports.addStep(skultpo.getEleSkultContentTxt().getText() + "is dispalyed in the skult page",
					LogAs.PASSED, null);
		}

	}
	
	
	

	 /* @Description: To verify whether more like it is available for OOS product
	  * @Author:Prerana Bhatt*/
	
	@Parameters("device")
	@Test(enabled =true,priority=3,description="To verify whether more like it is available for OOS product")
	public void veirfySkultLookDetailOutOfStockProduct(String device) throws InterruptedException, IOException
	{
		loginPo.getEleLoginCloseBtn().click();
		BaseLib.waitForElement( loginPo.getEleOkayBtn()," Okay Btn is displayed"," Okay Btn is not displayed");
		loginPo.handleOkayBtn();
		BaseLib.waitForElement(whatsHotLandingPo.getEleMenModule()," ","MenModule is not displayed");
		whatsHotLandingPo.getEleMenModule().click();
		BaseLib.scrollToElement(5, "UP", .80, .20, whatsHotLandingPo.getEleViewAllBtn());

		whatsHotLandingPo.getEleShopBrandGridViewImageLst().get(1).click();
		BaseLib.waitForElement(skultpo.getEleSkultTxt(),"Skult title Txt is displayed","Skult title Txt is not displayed");
		BaseLib.scrollToElement(7, "UP", .80, .10, skultpo.getEleSkultViewAllLooksBtn());
    	skultpo.getEleSkultShopLooksCarousalLst().get(0).click();
		BaseLib.scrollToElement(4, "UP", .50, .20, skultpo.getEleTotalPriceTxt());
		/* below code is to verify whether all check box is by default selected*/
		for (int i = 0; i <= skultpo.getEleSkultLookDetailCheckboxLst().size() - 1; i++) {
			skultpo.getEleSkultLookDetailCheckboxLst().get(i).isSelected();
		}
		/*selecting size*/
	
		skultpo.getElelookDetailsizeDrpdwnLst().get(0).click();

		for (int k = 0; k <= browsePlpPdpPo.getEleSizeNameLst().size() - 1; k++) {
			if (browsePlpPdpPo.getEleSizeNameLst().get(k).getText().equals("L")) {
				browsePlpPdpPo.getEleSizeNameLst().get(k).click();
				break;
			}
		}
		/*selecting add to bag btn ,need to verify accordians*/
		driver.findElement(By.xpath("//android.widget.Button[contains(@text,'ADD TO BAG')]")).click();
        /*selecting close bnt*/ 
		driver.findElement(By.id("com.abof.android:id/lookdetail_closeLayout")).click();
      	homePagePo.getEleHomeCartIcon().click();
		
		shoppingBagPo.updateQuantityAtCart("2");
		/*looking for oos product in shopping cart*/
		BaseLib.elementStatus(skultpo.getEleOutofStockTxt(),"Out Of Stock","displayed");
		driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Out-of-stock')]/../../../../android.widget.ImageView")).click();
		browsePlpPdpPo.toVerifyPageContentsDisplay("Pdp");
		/*scroll until, more like it text displayed for oos product*/
		BaseLib.scrollToElement(5,"UP",.80,.20 ,browsePlpPdpPo.getEleMoreLikeItTxt());
		/* to verify more like it product list and its content*/
		browsePlpPdpPo.toVerifyPageContentsDisplay(browsePlpPdpPo.getEleMoreLikeItTxt().getText());

	
	}

	/*
	 * @Description:To verify whether user is able to add look detail product to
	 * cart or not .
	 * 
	 * here,checking for different variation for negative scenario like ,without
	 * selecting size,without selecting product, and checking for positive flow
	 * : select size,product and add it to cart
	 * 
	 * @Author:Prerana Bhatt
	 */
	@Parameters("device")
	@Test(enabled=true,priority=4,description="adding product to cart from skult LookDetail page with positive and negative scenario")
	public void skultLookDetailEndToEndFlow(String device) throws InterruptedException, IOException
	{
		loginPo.getEleLoginCloseBtn().click();
		BaseLib.waitForElement( loginPo.getEleOkayBtn()," Okay Btn is displayed "," Okay Btn is not displayed");
		loginPo.handleOkayBtn();
		BaseLib.waitForElement( homePagePo.getEleHamburgerMenuIcon(),"Hamburger Menu Icon is displayed","Hamburger Menu Icon is not displayed");
		homePagePo.getEleHamburgerMenuIcon().click();
		BaseLib.waitForElement( hamburgerMenuPo.getEleUserProfileImg(),"UserProfileImg isdisplayed ","UserProfileImg is not displayed");
		hamburgerMenuPo.getEleUserProfileImg().click();
		loginPo.getEleLoginCloseBtn().click();
		BaseLib.waitForElement( whatsHotLandingPo.getEleMenModule(),"MenModule is displayed","MenModule is not displayed");
		whatsHotLandingPo.getEleMenModule().click();

		BaseLib.scrollToElement(7, "UP", .80, .20, whatsHotLandingPo.getEleViewAllBtn());

		whatsHotLandingPo.getEleShopBrandGridViewImageLst().get(1).click();
		BaseLib.elementStatus(skultpo.getEleSkultTxt(), "Skult title", "displayed");

		BaseLib.scrollToElement(7, "UP", .80, .10, skultpo.getEleSkultViewAllLooksBtn());

		skultpo.getEleSkultShopLooksCarousalLst().get(1).click();
		BaseLib.waitForElement(skultpo.getEleLookDetailAddToCartBtn(),"LookDetailAddToCartBtn is displayed", "LookDetailAddToCartBtn is displayed");
		skultpo.getEleLookDetailAddToCartBtn().isDisplayed();
		skultpo.getEleLookDetailFavIcon().isDisplayed();

		BaseLib.waitForElement(skultpo.getEleLookDetailFavIcon(), "LookDetailFavIcon is displayed","LookDetailFavIcon is not displayed");
		skultpo.getEleLookDetailFavIcon().click();
		BaseLib.elementStatus(loginPo.getEleSigninRegister(), "signinRegister", "displayed");
		
		
		loginPo.loginApp("ABOF","TC_Login_001");
		Thread.sleep(500);

		BaseLib.scrollToElement(6, "UP", .80, .20, skultpo.getEleTotalPriceTxt());

		
		/* * in this peice of code , verifying whether all check box should be
		 * by-default selected selecting add to cart btn,without selecting size
		 * and product*/
		 
        NXGReports.addStep("verifying whether all check boxes are selectedand will try to add to card", LogAs.INFO,null);
		for (int i = 0; i <=skultpo.getEleSkultLookDetailCheckboxLst().size() - 1; i++) {
			skultpo.getEleSkultLookDetailCheckboxLst().get(i).getAttribute("checked").equals("true");
		}
		skultpo.getEleLookDetailAddToCartBtn().click();
		Thread.sleep(1000);

		/* * in this piece of code , de-select all check box selecting size and
		 * adding to cart without selecting any product
		 * 
		 **/
		 
		 NXGReports.addStep("deselect all checkboxes", LogAs.INFO,null);
		 System.out.println(skultpo.getEleSkultLookDetailCheckboxLst().size());
		 for (int i = 1; i <=skultpo.getEleSkultLookDetailCheckboxLst().size()-1; i++) {
			if(	skultpo.getEleSkultLookDetailCheckboxLst().get(i).getAttribute("checked").equals("true"))
			{
				
				skultpo.getEleSkultLookDetailCheckboxLst().get(i).click();
			}
			}

		 NXGReports.addStep("select size for the product", LogAs.INFO,null);
       for(int j=0;j<=skultpo.getElelookDetailsizeDrpdwnLst().size()-1;j++)
       {
    	   skultpo.getElelookDetailsizeDrpdwnLst().get(j).click();
		for (int k = 0; k <= browsePlpPdpPo.getEleSizeNameLst().size() - 1; k++) {
			if (browsePlpPdpPo.getEleSizeNameLst().get(k).getText().equals("XL") ||browsePlpPdpPo.getEleSizeNameLst().get(k).getText().equals("34")) {
				browsePlpPdpPo.getEleSizeNameLst().get(k).click();
				break;
			}
		}
       }
		skultpo.getEleLookDetailAddToCartBtn().click();
		Thread.sleep(1000);

		
		/* * in this piece of code , selecting size ,check product and add to cart
		 * 
		 * verifying for successful message
		 */
		 NXGReports.addStep("select all checkboxes and size for the given product", LogAs.INFO,null);
		for (int i = 0; i <= skultpo.getEleSkultLookDetailCheckboxLst().size() - 1; i++) {
			if(	skultpo.getEleSkultLookDetailCheckboxLst().get(i).getAttribute("checked").equals("false"))
			{
				skultpo.getEleSkultLookDetailCheckboxLst().get(i).click();
			}
		}

		
		
		try
		{
		 NXGReports.addStep("Share post in facebook", LogAs.INFO,null);
		BaseLib.waitForElement( skultpo.getLookDetailShareLnk(),"LookDetailShareLink is displayed", " LookDetailShareLink is not displayed");
		skultpo.getLookDetailShareLnk().click();
		 BaseLib.scrollToElement(2, "UP", .80, .20,
				 browsePlpPdpPo.getEleShareFacebookLnk());
		 BaseLib.waitForElement(browsePlpPdpPo.getEleShareFacebookLnk(),"PdpShareFacebookLink is displayed","PdpShareFacebookLink is not displayed");
		 browsePlpPdpPo.getEleShareFacebookLnk().click();
		 BaseLib.waitForElement(browsePlpPdpPo.getEleSharedProductInfo(),"SharedProductInfo is displayed"," SharedProductInfo is not displayed");
		
		 BaseLib.waitForElement(browsePlpPdpPo.getEleFacebookPostBtn(),"FacebookPostBtn is displayed","FacebookPostBtn is not displayed");
		 browsePlpPdpPo.getEleFacebookPostBtn().click();
		}
		catch(Exception e)
		{
			NXGReports.addStep(" not able to post pproduct in facebook", LogAs.FAILED,new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
		 
		 BaseLib.scrollToElement(2,"UP",.80,.20, skultpo.getEleLookDetailAddToCartBtn());
		// BaseLib.waitForElement( skultpo.getEleLookDetailAddToCartBtn(),"LookDetailAddToCartBtn is displayed","LookDetailAddToCartBtn is  not displayed");
		 skultpo.getEleLookDetailAddToCartBtn().click();
	
		driver.findElement(By.id("com.abof.android:id/lookdetail_closeLayout")).click();

		homePagePo.getEleHomeCartIcon().click();
		
		
			
	}
	
		/* description: this method is to verify skult video functionality,adding and removing same item from fav list
	 * validating filter option for skult and to verify product displayed equals to item count
	 * in skult  look detail page.
	 * 
	 *  @author:Prerana Bhatt
	 * 
	 * */
	@Parameters("device")
	@Test(enabled=true,priority=5,description="To check the skult video banner")
	public void testSkultVideoBanner(String device) throws InterruptedException, IOException
	{		
		int i=0;
		loginPo.loginApp("ABOF","TC_Login_001");
		BaseLib.waitForElement(loginPo.getEleOkayBtn(), "handleOkayBtn is displayed","handleOkayBtn is notdisplayed");
		loginPo.handleOkayBtn();
		BaseLib.waitForElement(whatsHotLandingPo.getEleMenModule(), "MenModule is displayed","MenModule is notdisplayed");
		whatsHotLandingPo.getEleMenModule().click();
		BaseLib.scrollToElement(5, "UP", .80, .20, whatsHotLandingPo.getEleViewAllBtn());
		whatsHotLandingPo.getEleShopBrandGridViewImageLst().get(1).click();
		try
		{
		Thread.sleep(10000);
		skultpo.getEleSkultVideoLayout().click();
		Thread.sleep(5000);
		skultpo.getEleSkultVideoPlayBtn().click();
		Thread.sleep(1000);
		}
		catch(Exception e)
		{
			NXGReports.addStep("not able to synchronize with video", LogAs.INFO,null);
		}
		
		BaseLib.swipeBottomToTop(.80, .10);
		
		/*below, code for list  is hard coded,need to do maintenance once unique id is provided by abofdev*/
		
		List<WebElement> lst=driver.findElements(By.xpath("//android.widget.ImageView"));
		lst.get(3).click();
		
		
		browsePlpPdpPo.getElePlpFavIcon().click();
//		String saveForLater=BaseLib.verifyToastMessage(driver);
//     	System.out.println(saveForLater);
//		
//		if(saveForLater.contains("Added to favorite"))
//		{
//			NXGReports.addStep("product added to favorite list", LogAs.PASSED,null);
//		}
		
		browsePlpPdpPo.getElePlpFavIcon().click();
		//String removeFromFav=BaseLib.verifyToastMessage(driver);
		//System.out.println(removeFromFav);
//		if(saveForLater.contains(""))
//		{
//			NXGReports.addStep("product removed to favorite list", LogAs.PASSED,null);
//		}
		BaseLib.waitForElement(browsePlpPdpPo.getElePlpFilterIcon(), "PlpFilterIcon is displayed","PlpFilterIcon is not displayed");
		browsePlpPdpPo.getElePlpFilterIcon().click();
		sData=GenericLib.toReadExcelData("Login","TC_PLPFilter_003");
		browsePlpPdpPo.toValidateFilterOptions(sData[5],sData[6]);
		browsePlpPdpPo.toValidateFilterOptions(sData[9],sData[10]);
		BaseLib.waitForElement(browsePlpPdpPo.getEleFilterTickBtn(), "FilterTickBtn is displayed","FilterTickBtn is not displayed");
		browsePlpPdpPo.getEleFilterTickBtn().click();
		BaseLib.waitForElement(browsePlpPdpPo.getElePlpProductFoundTxt(), "ProductFoundTxt is displayed","ProductFoundTxt is not displayed");
		ProductfoundTxt=browsePlpPdpPo.getElePlpProductFoundTxt().getText();
		productFound=browsePlpPdpPo.convertPriceToIntValue(ProductfoundTxt);
		
		while(count<=6)  //productFound
		{
			
			    browsePlpPdpPo.getEleProductNameLst().get(i).click();	
				if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+sData[10]+"')]")).isDisplayed())
				{
					NXGReports.addStep("given size  is  available", LogAs.PASSED,null);
				}
				browsePlpPdpPo.getEleBackBtn().click(); 
		
				//check for 'Price-High to Low
				
				
				    int firstOfferPriceHighestValue=browsePlpPdpPo.convertPriceToIntValue(browsePlpPdpPo.getElePlpOfferPriceLst().get(0).getText());
				
					int secondOfferPriceLowestValue=browsePlpPdpPo.convertPriceToIntValue(browsePlpPdpPo.getElePlpOfferPriceLst().get(i).getText());
					browsePlpPdpPo.checkPriceFilter( sData[6],firstOfferPriceHighestValue,secondOfferPriceLowestValue);
							
				    	
	    	count++;
	    	
				if(i%2==1)
				{
					
					swipeBottomToTop(.80,.40);
					
					loginPo.handleOkayBtn();
					
					 if(i==3)
						{
							i=-1;
						}
				}
								
			i++;
		}
		
		BaseLib.scrollToElement(5, "DOWN", .20, .80, browsePlpPdpPo.getEleBackBtn());
	//	BaseLib.waitForElement(browsePlpPdpPo.getEleBackBtn(),"BackBtn is displayed","BackBtn is  not displayed");
		Thread.sleep(2000);
		BaseLib.elementStatus(browsePlpPdpPo.getEleBackBtn(), "Backbutton","displayed");
		browsePlpPdpPo.getEleBackBtn().click();
		
		BaseLib.scrollToElement(5, "UP", .80, .20, skultpo.getEleSkultViewAllLooksBtn());
		skultpo.getEleSkultShopLooksCarousalLst().get(0).click();
		String itemCountTxt=skultpo.getEleItemCountInLookDetail().getText();
		String [] icount=itemCountTxt.split(" ");
		String itemCount=icount[0];
		BaseLib.scrollToElement(5, "UP", .80, .20, skultpo.getEleTotalPriceTxt());
		int ImageListSize=skultpo.getEleLookDetailProductImageLst().size();
		
		if(ImageListSize==Integer.parseInt(itemCount))
		{
			NXGReports.addStep("Item count equals to product image displayed", LogAs.PASSED,null);
		}
		
		
	}

	

}
