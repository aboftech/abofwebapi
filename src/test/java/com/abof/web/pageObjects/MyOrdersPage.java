package com.abof.web.pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.abof.library.BaseLib;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;

import io.appium.java_client.android.AndroidDriver;

public class MyOrdersPage {

	String status = null;

	AndroidDriver driver;

	public MyOrdersPage(AndroidDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/*
	 * retrieved webElements from payment summary screen once order is
	 * placed,orderid ,date,expected delivery and payment mode is displayedd
	 */

	@FindBy(id = "com.abof.android:id/txtOrderDetails")
	private WebElement eleOrderDetailsTxt;

	public WebElement getEleOrderDetailsTxt() {
		return eleOrderDetailsTxt;
	}

	@FindBy(id = "com.abof.android:id/txtOrderNo")
	private WebElement eleOrderNoTxt;

	public WebElement getEleOrderNoTxt() {
		return eleOrderNoTxt;
	}

	@FindBy(id = "com.abof.android:id/txtOrderNoValue")
	private WebElement eleOrderNoValue;

	public WebElement getEleOrderNoValue() {
		return eleOrderNoValue;
	}

	@FindBy(id = "com.abof.android:id/txtOrderDate")
	private WebElement eleOrderPlacedOnTxt;

	public WebElement getEleOrderPlacedOnTxt() {
		return eleOrderPlacedOnTxt;
	}

	@FindBy(id = "com.abof.android:id/txtOrderDateValue")
	private WebElement eleOrderDateValue;

	public WebElement getEleOrderDateValue() {
		return eleOrderDateValue;
	}

	@FindBy(id = "com.abof.android:id/txtDeliveryDate")
	private WebElement eleExpectedDeliveryTxt;

	public WebElement getEleExpectedDeliveryTxt() {
		return eleExpectedDeliveryTxt;
	}

	@FindBy(id = "com.abof.android:id/txtDeliveryDateValue")
	private WebElement eleDeliveryDateValue;

	public WebElement getEleDeliveryDateValue() {
		return eleDeliveryDateValue;
	}

	@FindBy(id = "com.abof.android:id/txtPaymentMode")
	private WebElement elePaymentModeTxt;

	public WebElement getElePaymentModeTxt() {
		return elePaymentModeTxt;
	}

	@FindBy(id = "com.abof.android:id/txtPaymentModeValue")
	private WebElement elePaymentModeValue;

	public WebElement getElePaymentModeValue() {
		return elePaymentModeValue;
	}

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Enjoying abof! Rate our app on the App Store, thanks!')]")
	private WebElement eleRateAppAlert;

	public WebElement getEleRateAppAlert() {
		return eleRateAppAlert;
	}

	@FindBy(id = "com.abof.android:id/rate_it_now")
	private WebElement eleRateItNowTxt;

	public WebElement getEleRateItNowTxt() {
		return eleRateItNowTxt;
	}

	@FindBy(id = "com.abof.android:id/do_later")
	private WebElement eleDoLaterTxt;

	public WebElement getEleDoLaterTxt() {
		return eleDoLaterTxt;
	}

	/* webElement form My order Screen */

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'My Orders')]")
	private WebElement eleMyOrdersTxt;

	public WebElement getEleMyOrdersTxt() {
		return eleMyOrdersTxt;
	}

	@FindBy(id = "com.abof.android:id/order_view_more")
	private WebElement eleViewMoreIcon;

	public WebElement getEleViewMoreIcon() {
		return eleViewMoreIcon;
	}

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'View Details')]")
	private WebElement eleViewDetailsOpt;

	public WebElement getEleViewDetailsOpt() {
		return eleViewDetailsOpt;
	}

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Can')]")
	private WebElement eleCancelOpt;

	public WebElement getEleCancelOpt() {
		return eleCancelOpt;
	}

	@FindBy(id = "com.abof.android:id/order_id_count")
	private WebElement eleOrderIdCountsTxt;

	public WebElement getEleOrderIdCountsTxt() {
		return eleOrderIdCountsTxt;
	}

	@FindBy(id = "com.abof.android:id/order_placed_date")
	private WebElement eleOrderPlacedDateTxt;

	public WebElement getEleOrderPlacedDateTxt() {
		return eleOrderPlacedDateTxt;
	}

	@FindBy(id = "com.abof.android:id/order_delivery_status")
	private WebElement eleOrderDeliveryStatusTxt;

	public WebElement getEleOrderDeliveryStatusTxt() {
		return eleOrderDeliveryStatusTxt;
	}

	@FindBy(id = "com.abof.android:id/order_track_shipment")
	private WebElement eleTrackShipmentBtn;

	public WebElement getEleTrackShipmentBtn() {
		return eleTrackShipmentBtn;
	}

	@FindBy(id = "com.abof.android:id/order_cancel_txt")
	private WebElement eleAlertCancelLnk;

	public WebElement getEleAlertCancelLnk() {
		return eleAlertCancelLnk;
	}

	@FindBy(id = "com.abof.android:id/cancel_items_count_txt")
	private WebElement eleCancelledItemCountTxt;

	public WebElement getEleCancelledItemCountTxt() {
		return eleCancelledItemCountTxt;
	}

	@FindBy(id = "com.abof.android:id/cancel_cancel_close_btn")
	private WebElement eleCancelCloseBtn;

	public WebElement getCancelCloseBtn() {
		return eleCancelCloseBtn;
	}

	@FindBy(id = "com.abof.android:id/select_reason_text_view")
	private WebElement eleSelectReasonTxt;

	public WebElement getSelectReasonTxt() {
		return eleSelectReasonTxt;
	}

	@FindBy(xpath = "//android.widget.Button[contains(@text,'Done')]")
	private WebElement eleDoneBtn;

	public WebElement getEleDoneBtn() {
		return eleDoneBtn;
	}

	@FindBy(id = "com.abof.android:id/shipment_text")
	private WebElement eleAlertCancellationCount;

	public WebElement getEleAlertCancellationCount() {
		return eleAlertCancellationCount;
	}

	@FindBy(id = "com.abof.android:id/item_cancelled")
	private WebElement eleAlertItemSelectedTxt;

	public WebElement getEleAlertItemSelectedTxt() {
		return eleAlertItemSelectedTxt;
	}

	@FindBy(id = "com.abof.android:id/cancel_proceed_bt")
	private WebElement eleProceedForCancellationBtn;

	public WebElement getEleProceedForCancellationBtn() {
		return eleProceedForCancellationBtn;
	}

	@FindBy(id = "com.abof.android:id/cancel_confirm_text")
	private WebElement eleConfirmCancellationTxt;

	public WebElement getEleConfirmCancellationTxt() {
		return eleConfirmCancellationTxt;
	}

	@FindBy(id = "com.abof.android:id/confirm_cancel_bt")
	private WebElement eleConfirmCancellationBtn;

	public WebElement getEleConfirmCancellationBtn() {
		return eleConfirmCancellationBtn;
	}

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'item(s) cancelled successfully')]")
	private WebElement eleCancellationSuccesfulMsg;

	public WebElement getEleCancellationSuccesfulMsg() {
		return eleCancellationSuccesfulMsg;
	}

	@FindBy(id = "com.abof.android:id/continue_shop_bt")
	private WebElement eleContinueshoppingBtn;

	public WebElement getEleContinueShoppingBtn() {
		return eleContinueshoppingBtn;
	}

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Total  :')]")
	private WebElement eleTotalTxt;

	public WebElement getEleTotalTxt() {
		return eleTotalTxt;
	}

	@FindBy(id = "com.abof.android:id/cancelled_total")
	private WebElement eleCancelledTotalValue;

	public WebElement getEleCancelledTotalValue() {
		return eleCancelledTotalValue;
	}

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Total Refund Due')]")
	private WebElement eleTotalRefundDue;

	public WebElement getEleTotalRefundDue() {
		return eleTotalRefundDue;
	}

	@FindBy(id = "com.abof.android:id/total_refund")
	private WebElement eleTotalRefundDueValue;

	public WebElement getEleTotalRefundDueValue() {
		return eleTotalRefundDueValue;
	}

	@FindBy(id = "com.abof.android:id/business_days_txt")
	private WebElement eleBusinessDaysTxt;

	public WebElement getEleBusinessDaysTxt() {
		return eleBusinessDaysTxt;
	}

	@FindBy(id = "com.abof.android:id/ordered_product_name")
	private WebElement eleCancelledProductName;

	public WebElement getEleCancelledProductName() {
		return eleCancelledProductName;
	}

	@FindBy(id = "com.abof.android:id/shipment_img")
	private WebElement eleShipmentProductImg;

	public WebElement getEleShipmentProductImg() {
		return eleShipmentProductImg;
	}

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Size :')]")
	private WebElement eleCancelledProductSizeTxt;

	public WebElement getEleCancelledProductSizeTxt() {
		return eleCancelledProductSizeTxt;
	}

	@FindBy(id = "com.abof.android:id/ordered_size")
	private WebElement eleCancelledProductSizeValue;

	public WebElement getEleCancelledProductSizeValue() {
		return eleCancelledProductSizeValue;
	}

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'  / Qty :')]")
	private WebElement eleCancelledProductQtyTxt;

	public WebElement getEleCancelledProductQtyTxt() {
		return eleCancelledProductQtyTxt;
	}

	@FindBy(id = "com.abof.android:id/ordered_quantity")
	private WebElement eleCancelledProductQtyValue;

	public WebElement getEleCancelledProductQtyValue() {
		return eleCancelledProductQtyValue;
	}

	@FindBy(id = "com.abof.android:id/icn_cancel_help")
	private WebElement eleCancelHelpIcn;

	public WebElement getEleCancelHelpIcn() {
		return eleCancelHelpIcn;
	}

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'VIEW MORE')]")
	private WebElement eleViewMoreBtn;

	public WebElement getEleViewMoreBtn() {
		return eleViewMoreBtn;
	}

	@FindBys({ @FindBy(id = "com.abof.android:id/ordered_nt_canceled_text") })
	private List<WebElement> eleAlreadyCancelledLst;

	public List<WebElement> getEleAlreadyCancelledLst() {
		return eleAlreadyCancelledLst;
	}

	@FindBy(id = "com.abof.android:id/ordered_nt_canceled_text")
	private WebElement eleItemAlreadyCancelledTxt;

	public WebElement getEleItemAlreadyCancelledTxt() {
		return eleItemAlreadyCancelledTxt;
	}

	@FindBy(id = "com.abof.android:id/order_total_amount")
	private WebElement eleViewDetailOrderTotalAmt;

	public WebElement getEleViewDetailOrderTotalAmt() {
		return eleViewDetailOrderTotalAmt;
	}

	/* webElement list of my orders */
	@FindBys({ @FindBy(id = "com.abof.android:id/order_id_count") })
	private List<WebElement> eleOrderNoLst;

	public List<WebElement> getEleOrderNoLst() {
		return eleOrderNoLst;
	}

	@FindBys({ @FindBy(id = "com.abof.android:id/ordered_product_name") })
	private List<WebElement> eleCancelAlertProductNameLst;

	public List<WebElement> getEleCancelAlertProductNameLst() {
		return eleCancelAlertProductNameLst;
	}

	@FindBys({ @FindBy(id = "com.abof.android:id/order_cancel_txt") })
	private List<WebElement> eleCancelAlertButtonLst;

	public List<WebElement> getEleCancelAlertButtonLst() {
		return eleCancelAlertButtonLst;
	}

	public void OrderCancellation(String OrderId) throws InterruptedException {
		System.out.println(" cancellation");
		try {

			driver.findElement(By.xpath(
					"//android.widget.TextView[contains(@text,'" + OrderId + "')]/../../android.widget.ImageView"))
					.click();
			getEleCancelOpt().click();
			Thread.sleep(25000);
			cancelFirstProduct();
			Thread.sleep(35000);
			driver.findElement(By
					.xpath("//android.widget.TextView[contains(@text,'Not Interested Any More')]/..//android.widget.RadioButton"))
					.click();
			getEleDoneBtn().click();
			BaseLib.elementStatus(getEleAlertCancellationCount(), "Alert Cancellation Count Txt", "displayedd");
			BaseLib.elementStatus(getEleAlertItemSelectedTxt(), "Alert Item Selected Txt", "displayedd");
			getEleProceedForCancellationBtn().click();
			BaseLib.elementStatus(getEleConfirmCancellationTxt(), "Confirm CancellationTxt", "displayedd");
			BaseLib.elementStatus(getEleConfirmCancellationBtn(), "Confirm CancellationBtn", "displayedd");
			getEleConfirmCancellationBtn().click();
			Thread.sleep(25000);

			IscontentOfCancellationScreendisplayedd();

		} catch (Exception e) {
			throw e;
		}

	}

	public void OrderStatusConfirmation(String OrderId, String status) throws InterruptedException {

		if (status.contains("ORDER PLACED")) {

			for (int i = 0; i <= getEleOrderNoLst().size() - 1; i++) {

				System.out.println("loop");
				if (getEleOrderNoLst().get(i).getText().contains(OrderId)) {

					System.out.println(getEleOrderNoLst().get(i).getText());
					status = driver
							.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + OrderId
									+ "')]/../../../../../android.widget.LinearLayout/android.widget.TextView[2]"))
							.getText();

					if (status.equals("ORDER PLACED") || status.equals("In Process")) {
						NXGReports.addStep("Confirm : OrderIS Placed properly with ORderNo " + OrderId, LogAs.PASSED,
								null);
						break;
					}
				} else {
					BaseLib.swipeBottomToTop(.80, .10);
					i = 0;
				}

			}

		} else {

			for (int i = 0; i <= getEleOrderNoLst().size() - 1; i++) {

				if (getEleOrderNoLst().get(i).getText().contains(OrderId)) {

					status = driver
							.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + OrderId
									+ "')]/../../../../../android.widget.LinearLayout/android.widget.TextView[contains(@resource-id,'com.abof.android:id/order_shipment_status')]"))
							.getText();

					if (status.equals("CANCELLED")) {
						NXGReports.addStep("Confirm : OrderIS Cancelled properly with ORderNo " + OrderId, LogAs.PASSED,
								null);
						break;
					}
				} else {
					BaseLib.swipeBottomToTop(.80, .10);
					i = 0;
				}

			}

		}

	}

	public void IscontentOfCancellationScreendisplayedd() {
		BaseLib.elementStatus(getEleConfirmCancellationTxt(), "Confirmation", "displayedd");
		BaseLib.elementStatus(getEleCancellationSuccesfulMsg(), "Cancellation SuccesfulMsg", "displayedd");
		BaseLib.elementStatus(getEleTotalTxt(), "TotalTxt", "displayedd");
		BaseLib.elementStatus(getEleCancelledTotalValue(), "CancelledTotalValue ", "displayedd");
		BaseLib.elementStatus(getEleTotalRefundDue(), "TotalRefundDue ", "displayedd");
		BaseLib.elementStatus(getEleTotalRefundDueValue(), "TotalRefundDueValue ", "displayedd");
		BaseLib.elementStatus(getEleBusinessDaysTxt(), "BusinessDaysTxt", "displayedd");
		// BaseLib.elementStatus(getEleCancelHelpIcn(),"CancelHelpIcon","displayedd");
		BaseLib.elementStatus(getEleShipmentProductImg(), "ShipmentProductImg ", "displayedd");
		BaseLib.elementStatus(getEleCancelledProductName(), "CancelledProductName", "displayedd");
		BaseLib.elementStatus(getEleCancelledProductSizeTxt(), "CancelledProductSizeTxt ", "displayedd");
		BaseLib.elementStatus(getEleCancelledProductSizeValue(), "CancelledProductSizeValue ", "displayedd");
		BaseLib.elementStatus(getEleCancelledProductQtyTxt(), "CancelledProductQtyTxt ", "displayedd");
		BaseLib.elementStatus(getEleCancelledProductQtyValue(), "CancelledProductQtyValue", "displayedd");

	}

	/*
	 * @Description:This method will cancel very first product for which cancel
	 * link is enabled.
	 * 
	 * @Author: Prerana Bhatt
	 */
	public void cancelFirstProduct() throws InterruptedException {
		while (!(getEleAlreadyCancelledLst().isEmpty())) {

			if (getEleItemAlreadyCancelledTxt().isDisplayed()) {
				System.out.println("disp" + getEleItemAlreadyCancelledTxt().isDisplayed());
				BaseLib.swipeRightToLeft(.90, .10);

			}

		}
		if (!(getEleCancelAlertButtonLst().isEmpty())) {
			getEleCancelAlertButtonLst().get(0).click();
		}

	}
}
