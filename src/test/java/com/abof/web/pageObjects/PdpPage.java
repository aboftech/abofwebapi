/***********************************************************************
* @author 			:		Shireesh Bandela
* @description		: 		Page objects and re-usbale methods for BrowsePLPPDP screen
* @module			:		BrowsePLPPDP
* @reusable methods : 		verifyPlpContentDisplayed(),verifyPdpContentDisplayed(),checkPagination()
* 							handleSizePopUp(),viewBagStatus(),validatePinCodeStatus(),validateSizeGuide(),selectSize()
*/
package com.abof.web.pageObjects;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.abof.web.testscripts.BaseTest;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;


public class PdpPage extends BaseTest {


	WebDriver driver;

	public PdpPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/*
	 * These webElements are similar for PLP and PDP. Captured web element of
	 * Action Bar .
	 */

	@FindBy(xpath = "//ul[@class='product-detail__size-boxes--wrapper']/li/a[@class='size-select']")
	private static List<WebElement> eleAvailableSize;

	public static List<WebElement> getEleAvailableSize() {
		return eleAvailableSize;
	}

	@FindBy(xpath = "//a[contains(text(), 'Free Size')]")
	private WebElement eleFreeSize;
	public WebElement getEleFreeSize() {
		return eleFreeSize;
	}
	@FindBy(xpath = "//a[contains(text(), 'Add to Bag')]")
	private static WebElement eleAddtobagbtn;
	public static WebElement getEleAddToBagbtn() {
		return eleAddtobagbtn;
	}
	
	/*
	 * Author: Shireesh Bandela
	 * Description: verify search option based on
	 * valid and invalid input
	 */
	public void clickOnAvailableSize() throws Exception{
		List<WebElement> availableSizes = null;
		String size="";
		try{
			availableSizes = getEleAvailableSize();
			if(!availableSizes.isEmpty()){
				WebElement ele = availableSizes.get(0);
				size=ele.getText();
				ele.click();
				waitTillPageLoad();
				waitForElementToBeClickable(getEleAddToBagbtn(),"Wait for AddToBag button to be clickable","AddToBag button is clickable");
				Assert.assertTrue(getEleAddToBagbtn().isDisplayed());
				NXGReports.addStep("Click on first available size",size, "Size "+size+" should be clicked","Size "+size+" Clicked Successfully", LogAs.PASSED, null);
			}
			else{
				NXGReports.addStep("Click on first available size","Free Size", "Size should be clicked","'Free Size' Clicked Successfully", LogAs.PASSED, null);
			}
		}
		catch(Exception e){
			NXGReports.addStep("Click on first available size",size, "Size "+size+" should be clicked","Size "+size+" Click Failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}
	public void clickOnAddToBagbtn() throws Exception{
		WebElement addToBag = null;
		try{
			addToBag = getEleAddToBagbtn();
			addToBag.click();
			waitTillPageLoad();
			NXGReports.addStep("Click on 'Add To Bag' button","Add to Bag", "Add to Bag button should be clicked","Add to Bag button Clicked Successfully", LogAs.PASSED, null);
			waitForElementToBeClickable(HomePage.getEleViewBagbtn(),"Wait for ViewBag button to be appear","View Bag button is displayed");
			Assert.assertTrue(HomePage.getEleViewBagbtn().isDisplayed());
			
		}
		catch(Exception e){
			NXGReports.addStep("Click on 'Add To Bag' button","Add to Bag", "Add to Bag button should be clicked","Add to Bag button Click Failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}
	
	
}
