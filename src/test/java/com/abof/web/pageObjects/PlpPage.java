/***********************************************************************
* @author 			:		Shireesh Bandela
* @description		: 		Page objects and re-usbale methods for BrowsePLPPDP screen
* @module			:		BrowsePLPPDP
* @reusable methods : 		verifyPlpContentDisplayed(),verifyPdpContentDisplayed(),checkPagination()
* 							handleSizePopUp(),viewBagStatus(),validatePinCodeStatus(),validateSizeGuide(),selectSize()
*/
package com.abof.web.pageObjects;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.abof.library.BaseLib;
import com.abof.web.testscripts.BaseTest;
import com.abof.web.utils.ExcelUtility;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;


//this page class will contain products listing related info and PLP verification
public class PlpPage extends BaseTest{

	WebDriver driver;
	public PlpPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
	@FindBy(xpath = "//*[contains(text(), 'Show')]")
	private WebElement showFilter;
	public WebElement getEleShowFilter() {
		return showFilter;
	}
	
	@FindBy(xpath="//*[@class='breadcrumbs__list']")
	private WebElement eleBreadScrumbpath;
	public WebElement getEleBreadScrumbPath(){
		return eleBreadScrumbpath;
	}
	@FindBy(xpath = "//*[@class='product-grid-header__count']")
	private WebElement productHeaderCount;
	public WebElement getEleProductHeaderCount() {
		return productHeaderCount;
	}

	
	/* to get the list of gender filter options */
	@FindBy(xpath = "//div[@class='filter filter--gender']//div[@class='filter__options']/div//span[@class='filter-option__title']")
	private List<WebElement> eleFilterGenderOptionsTitles;
	public List<WebElement> getEleFilterGenderOptionsTitles() {
		return eleFilterGenderOptionsTitles;
	}
	
	@FindBy(xpath = "//div[@class='filter filter--gender']//div[@class='filter__options']/div//span[@class='filter-option__count']")
	private List<WebElement> eleFilterGenderOptionsCounts;
	public List<WebElement> getEleFilterGenderOptionsCounts() {
		return eleFilterGenderOptionsCounts;
	}
	
	/* to get the list of product filter options */
	@FindBy(xpath = "//div[@class='filter filter--product']//div[@class='filter__options']/div//span[@class='filter-option__title']")
	private List<WebElement> eleFilterProductOptionsTitles;
	public List<WebElement> getEleFilterProductOptionsTitles() {
		return eleFilterProductOptionsTitles;
	}

	@FindBy(xpath = "//div[@class='filter filter--product']//div[@class='filter__options']/div//span[@class='filter-option__count']")
	private List<WebElement> eleFilterProductOptionsCounts;
	public List<WebElement> getEleFilterProductOptionsCounts() {
		return eleFilterProductOptionsCounts;
	}
	
	/* to get the list of price filter options */
	@FindBy(xpath = "//div[@class='filter filter--price']//div[@class='filter__options']/div//span[@class='filter-option__title']")
	private List<WebElement> eleFilterPriceOptionsTitles;
	public List<WebElement> getEleFilterPriceOptionsTitles() {
		return eleFilterPriceOptionsTitles;
	}
	@FindBy(xpath = "//div[@class='filter filter--price']//div[@class='filter__options']/div//span[@class='filter-option__title']/preceding-sibling::span/div[@class='checkbox checkbox--checked']")
	private List<WebElement> eleFilterPriceOptionSeleted;
	public List<WebElement> getEleFilterPriceOptionSelected() {
		return eleFilterPriceOptionSeleted;
	}
	@FindBy(xpath = "//div[@class='filter filter--price']//div[@class='filter__options']/div//span[@class='filter-option__count']")
	private List<WebElement> eleFilterPriceOptionsCounts;
	public List<WebElement> getEleFilterPriceOptionsCounts() {
		return eleFilterPriceOptionsCounts;
	}

	/* to get the list of size filter options */
	@FindBy(xpath = "//div[@class='filter filter--size']//div[@class='filter__options']/div//span[@class='filter-option__title']")
	private List<WebElement> eleFilterSizeOptionsTitles;
	public List<WebElement> getEleFilterSizeOptionsTitles() {
		return eleFilterSizeOptionsTitles;
	}
	@FindBy(xpath = "//div[@class='filter filter--size']//div[@class='filter__options']/div//span[@class='filter-option__title']/preceding-sibling::span/div[@class='checkbox checkbox--checked']")
	private List<WebElement> eleFilterSizeOptionSelected;
	public List<WebElement> getEleFilterSizeOptionSelected() {
		return eleFilterSizeOptionSelected;
	}
	@FindBy(xpath = "//div[@class='filter filter--size']//div[@class='filter__options']/div//span[@class='filter-option__count']")
	private List<WebElement> eleFilterSizeOptionsCounts;
	public List<WebElement> getEleFilterSizeOptionsCounts() {
		return eleFilterSizeOptionsCounts;
	}
		
	/* to get the list of brand filter options */
	@FindBy(xpath = "//div[@class='filter filter--brand']//div[@class='filter__options']/div//span[@class='filter-option__title']")
	private List<WebElement> eleFilterBrandOptionsTitles;
	public List<WebElement> getEleFilterBrandOptionsTitles() {
		return eleFilterBrandOptionsTitles;
	}
	@FindBy(xpath = "//div[@class='filter filter--brand']//div[@class='filter__options']/div//span[@class='filter-option__title']/preceding-sibling::span/div[@class='checkbox checkbox--checked']")
	private List<WebElement> eleFilterBrandOptionSelected;
	public List<WebElement> getEleFilterBrandOptionSelected() {
		return eleFilterBrandOptionSelected;
	}
	@FindBy(xpath = "//div[@class='filter filter--brand']//div[@class='filter__options']/div//span[@class='filter-option__count']")
	private List<WebElement> eleFilterBrandOptionsCounts;
	public List<WebElement> getEleFilterBrandOptionsCounts() {
		return eleFilterBrandOptionsCounts;
	}
	@FindBy(xpath = "//*[@class='filter__search-input']")
	private WebElement eleBrandFilterSearchTextBox;
	public WebElement getEleBrandFilterSearchTextBox() {
		return eleBrandFilterSearchTextBox;
	}
	@FindBy(xpath = "//div[@class='filter-option filter-option--color']/a/span/span")
	private List<WebElement> eleColorFilterOptions;
	public List<WebElement> getEleColorFilterOptions() {
		return eleColorFilterOptions;
	}
	@FindBy(xpath = "//div[@class='filter-option filter-option--color']/a/span/span/ancestor::div/div[@class='filter-option filter-option--selected filter-option--color']")
	private List<WebElement> eleColorFilterOptionSelected;
	public List<WebElement> getEleColorFilterOptionSelected() {
		return eleColorFilterOptionSelected;
	}
	@FindBy(xpath = "//*[@class='filter-tag__title' and contains(text(),'Clear All')]")
	private WebElement eleClearAllBtn;
	public  WebElement getEleClearAllButton() {
		return eleClearAllBtn;
	}
	@FindBy(xpath = "//*[contains(text(),'Done')]")
	private List<WebElement> eleDoneBtn;
	public  List<WebElement> getEleDoneButton() {
		return eleDoneBtn;
	}
	@FindBy(xpath = "//div[@class='filter-group filter-group--open filter-group--secondary']//div[contains(@class,'filter--secondary')]")
	private List<WebElement> eleSecondaryFilters;
	public List<WebElement> getEleAdditionalFilters() {
		return eleSecondaryFilters;
	}
	@FindBy(xpath = "//div[@class='filter-tags']/span[@class='filter-tag']/span")
	private List<WebElement> eleFilterTags;
	public List<WebElement> getEleFilterTags() {
		return eleFilterTags;
	}
	@FindBy(xpath = "//div[@class='filter-tags']/span[@class='filter-tag']/span[@class='swatch swatch--small']")
	private List<WebElement> eleFilterColorTags;
	public List<WebElement> getEleFilterColorTags() {
		return eleFilterColorTags;
	}
	@FindBy(xpath = "//*[contains(text(), 'Sort By')]")
	private WebElement sortBy;
	public WebElement getEleSortBy() {
		return sortBy;
	}
	@FindBy(xpath = "//*[@class='dropdown__contents']/span")
	private List<WebElement> sortOptions;
	public List<WebElement> getEleSortOptions() {
		return sortOptions;
	}
	@FindBy(xpath = "//div[@class='product']/a[@title='Save for later']")
	private List<WebElement> eleFavoriteImages;
	public List<WebElement> getEleFavoriteImages() {
		return eleFavoriteImages;
	}
	
	@FindBy(xpath = "//*[contains(@class,'product')][@itemprop='itemListElement']")
	private static List<WebElement> eleProductsImages;
	public static List<WebElement> getEleProductsImages() {
		return eleProductsImages;
	}
	@FindBy(xpath = "//*[@class='product']//*[@class='product__title']")
	private List<WebElement> eleProductsTitles;
	public List<WebElement> getEleProductsTitle() {
		return eleProductsTitles;
	}
	@FindBy(xpath = "//*[@class='product']//*[@class='product__pricing']/div/div[contains(@class,'selling')]")
	private List<WebElement> eleProductsSellingPrice;
	public List<WebElement> getEleProductsSellingPrice() {
		return eleProductsSellingPrice;
	}
	@FindBy(xpath = "//div[@id='metail-container']/div[@id='metail-tab']")
	private WebElement eleMetalTabHead;
	public WebElement getEleMetalTab(){
		return eleMetalTabHead;
	}
	
	@FindBy(xpath = ".//*[@class='product-grid-header__title']/span[1]")
	private WebElement eleSerachedText;
	public WebElement geteleSerachedText(){
		return eleSerachedText;
	}
	
	/*
	 * Author: Shireesh Bandela
	 * Description: verify search option based on
	 * valid and invalid input
	 */
	public void addProductToWishList() throws Exception{
		List<WebElement> favouriteIconList = null;
		try{
			favouriteIconList = getEleFavoriteImages();
			WebElement ele = favouriteIconList.get(0);
			ele.click();
			Thread.sleep(2000);
			waitTillPageLoad();
			elementStatus(LoginPage.getEleEmailAddressTxtBox(), "Abof user EmailAddress element", "displayed");
			Assert.assertTrue(LoginPage.getEleEmailAddressTxtBox().isDisplayed());
			NXGReports.addStep("Click on add to favorite for Product ","", "Product should be added to favorites","Product added to favorites Successfully", LogAs.PASSED, null);
		}
		catch(Exception e){
			NXGReports.addStep("Click on add to favorite for product ","", "Product should be added to favorites","Add to favorite Failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}
	public void closeMetalTab(){
		try{
				if(getEleMetalTab().isEnabled()){
					BaseLib.waitForStaleElement(driver, getEleMetalTab(), "Wait for Metel window", "");
					getEleMetalTab().click();
					NXGReports.addStep("Close Metal tab window","", "Metaltab should be closed","Metal tab closed Successfully", LogAs.PASSED, null);
				}	
		}catch(Exception e){
				throw e;
		}
	}

	public void clickOnProductOnPLPPage() {
		List<WebElement> productsImages = null;
		try{
			waitTillPageLoad();
			productsImages = getEleProductsImages();
			WebElement ele = productsImages.get(0);
			BaseLib.waitForStaleElement(driver, ele, "Wait for PLP Page loading", "Waited for PLP Page load is over");
			ele.click();
			waitTillPageLoad();
			Assert.assertTrue(PdpPage.getEleAvailableSize().size()>0 || PdpPage.getEleAddToBagbtn().isDisplayed());
			NXGReports.addStep("Click on product ","", "Product should be clicked","Product Clicked Successfully", LogAs.PASSED, null);
		}
		catch(Exception e){
			NXGReports.addStep("Click on product ","", "Product should be clicked","Product Click Failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}
	public void clickOnShowFilter(){
		try{
			logger.info("Click on Show Filters");
			WebElement showFilter = getEleShowFilter();
			waitForElementToBeClickable(showFilter, "Show Filter should be ready for click","Show Filter is ready for Click");
			showFilter.click();
			NXGReports.addStep("Click on 'Show Filter' ","", "'Show Filter' element should be clicked","'Show Filter' Clicked Successfully", LogAs.PASSED, null);
		}
		catch(Exception e){
			NXGReports.addStep("Click on 'Show Filter' ","", "'Show Filter' element should be clicked","'Show Filter' Click Failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}
	public void saveFilterOptionsQtyInExcel(String pathToExcel, String fileName) throws InterruptedException, IOException {
			
		try {
			WebElement productHeaderCount = getEleProductHeaderCount();
			List<WebElement> genderOptions = getEleFilterGenderOptionsTitles();
			List<WebElement> genderOptionsQty = getEleFilterGenderOptionsCounts();
			List<WebElement> productOptions  = getEleFilterProductOptionsTitles();
			List<WebElement> productOptionsQty = getEleFilterProductOptionsCounts();
			List<WebElement> priceOptions = getEleFilterPriceOptionsTitles();
			List<WebElement> priceOptionsQty = getEleFilterPriceOptionsCounts();
			List<WebElement> sizeOptions = getEleFilterSizeOptionsTitles();
			List<WebElement> sizeOptionsQty = getEleFilterSizeOptionsCounts();
			List<WebElement> brandOptions = getEleFilterBrandOptionsTitles();
			List<WebElement> brandOptionsQty = getEleFilterBrandOptionsCounts();
			
			ExcelUtility.saveFilterOptionsQtyInExcel(driver, productHeaderCount, genderOptions, genderOptionsQty, productOptions, productOptionsQty,
					priceOptions, priceOptionsQty, sizeOptions, sizeOptionsQty, brandOptions, brandOptionsQty, pathToExcel, fileName);
		} catch (Exception e) {
			throw e;
		}

	}
	@SuppressWarnings("unused")
	public void verifyFilterOptionsThresholdCount(String url, String filename) throws InterruptedException, IOException{
		String message="1";
		try{
			ExcelUtility.moveFilterOptions("C:\\Opkey\\MonitoringThresholdQuantity\\Men", filename, "Gender");
			ExcelUtility.moveFilterOptions("C:\\Opkey\\MonitoringThresholdQuantity\\Men", filename, "Product");
			ExcelUtility.moveFilterOptions("C:\\Opkey\\MonitoringThresholdQuantity\\Men", filename, "Price");
			ExcelUtility.moveFilterOptions("C:\\Opkey\\MonitoringThresholdQuantity\\Men", filename, "Size");
			ExcelUtility.moveFilterOptions("C:\\Opkey\\MonitoringThresholdQuantity\\Men", filename, "Brand");
			saveFilterOptionsQtyInExcel("C:\\Opkey\\MonitoringThresholdQuantity\\Men",filename);
			message = message+ExcelUtility.droppedFilterOptions("C:\\Opkey\\MonitoringThresholdQuantity\\Men",filename, "Gender");
			message = message+ExcelUtility.droppedFilterOptions("C:\\Opkey\\MonitoringThresholdQuantity\\Men",filename, "Product");
			message = message+ExcelUtility.droppedFilterOptions("C:\\Opkey\\MonitoringThresholdQuantity\\Men",filename, "Price");
			message = message+ExcelUtility.droppedFilterOptions("C:\\Opkey\\MonitoringThresholdQuantity\\Men",filename, "Size");
			message = message+ExcelUtility.droppedFilterOptions("C:\\Opkey\\MonitoringThresholdQuantity\\Men",filename, "Brand");
			message = message+ExcelUtility.newAddedFilterOptions("C:\\Opkey\\MonitoringThresholdQuantity\\Men",filename, "Gender");
			message = message+ExcelUtility.newAddedFilterOptions("C:\\Opkey\\MonitoringThresholdQuantity\\Men",filename, "Product");
			message = message+ExcelUtility.newAddedFilterOptions("C:\\Opkey\\MonitoringThresholdQuantity\\Men",filename, "Price");
			message = message+ExcelUtility.newAddedFilterOptions("C:\\Opkey\\MonitoringThresholdQuantity\\Men",filename, "Size");
			message = message+ExcelUtility.newAddedFilterOptions("C:\\Opkey\\MonitoringThresholdQuantity\\Men",filename, "Brand");
			if(message.equalsIgnoreCase("1")){
				NXGReports.addStep("Verify Threshold count @URL: "+url,"", "Verify threshold count change not morethan 15%","Verified threshold count change is not morethan 15%", LogAs.PASSED, null);
			}
			else{
				NXGReports.addStep("Verify Threshold count @URL: "+url,"", "Verify threshold count change not morethan 15%","@URL: "+url+"\n"+message, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		}
		catch(Exception e){
			throw e;
		}
	}
	
	public void validateTheSectionsofProductListingpage(){
		try{
			BaseLib.waitForStaleElement(driver, getEleBreadScrumbPath(), "Wait for element sttached on webpage", "Element found as stale");
			getEleDoneButton().get(0).click();
		}
		catch(Exception e){
			throw e;
		}
	}
	public void validateBrandFilter() throws Exception{
		try{
			boolean isSorted = findFilterOptionsSortOrder(getEleFilterBrandOptionsTitles());
			Sassert.assertTrue(isSorted, "Brand filter options are not in Sorted order");
			NXGReports.addStep("Verify brand filter options are in alphabetical sort order","", "Brand filter options should be in sort order","Brand filter options are in sort order", LogAs.PASSED, null);
			elementStatus(getEleBrandFilterSearchTextBox(), "Brand filter Search Textbox", "displayed");
			
			Sassert.assertTrue(getEleBrandFilterSearchTextBox().isDisplayed(), "Brand filter search textbox not displayed");
			getEleBrandFilterSearchTextBox().click();
			String option = getEleFilterBrandOptionsTitles().get(getEleFilterBrandOptionsTitles().size()-1).getText();
			waitAndEnterText(getEleBrandFilterSearchTextBox(), "Brand filter Search Textbox",option,250, 120 );
			String brandOption = getEleFilterBrandOptionsTitles().get(0).getText();
			Sassert.assertTrue(brandOption.equalsIgnoreCase(option), "Brand option not match with typed text");
			NXGReports.addStep("Verify brand filter option change with search text",option, "Brand filter search should give abof option only","Brand filter search option modified with serch text: abof", LogAs.PASSED, null);
			String count = getEleFilterBrandOptionsCounts().get(0).getText();
			getEleFilterBrandOptionsTitles().get(0).click();
			Thread.sleep(2000);
			waitTillPageLoad();
			Sassert.assertTrue(getEleDoneButton().get(0).isDisplayed(), "Done button is not displayed");
			NXGReports.addStep("Verify Done button is displayed","", "Done button should be present on filter section","Done button is there in filter section", LogAs.PASSED, null);
			getEleDoneButton().get(0).click();
			waitTillPageLoad();
			Thread.sleep(2000);
			Sassert.assertTrue(getEleShowFilter().isDisplayed(), "On clicking DONE button filter section not collapsd");
			NXGReports.addStep("Verify Done button functionality to close filters section","", "Click on Done button should close Filter options","Click on Done button closed Filter options", LogAs.PASSED, null);
			System.out.println(count+" , "+getEleProductHeaderCount().getText());
			Sassert.assertTrue(count.equals(getEleProductHeaderCount().getText()));
			NXGReports.addStep("Verify products update as brand filter selected","", "Products should be updated as per selected brand filter option count","Products updated as brand filter option selection", LogAs.PASSED, null);
			BaseLib.waitForStaleElement(driver, getEleShowFilter(), "Show Filer should be attached to webpage", "Stale Show Filter Element");
		}
		catch(Exception e){
			NXGReports.addStep("Validate Brand Filter","","Brand filter validation failed due to: "+e.getMessage(),  LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void validatePriceFilter() throws Exception{
		try{
			boolean isSorted = findPriceFilterOptionsSortedOrder(getEleFilterPriceOptionsTitles());
			Sassert.assertTrue(isSorted,"Price fiter options are not in sorted order");
			NXGReports.addStep("Verify price filter options are in sorted order","", "Price filter options should be in sorted order","Price filter options are in sorted order", LogAs.PASSED, null);
			String count = getEleFilterPriceOptionsCounts().get(0).getText();
			getEleFilterPriceOptionsTitles().get(0).click();
			Thread.sleep(2000);waitTillPageLoad();
			Sassert.assertTrue(getEleDoneButton().get(0).isDisplayed(),"Done button is not displayed");
			NXGReports.addStep("Verify Done button is displayed","", "Done button should be present on filter section","Done button is there in filter section", LogAs.PASSED, null);
			getEleDoneButton().get(0).click();
			Thread.sleep(2000);waitTillPageLoad();
			Sassert.assertTrue(getEleShowFilter().isDisplayed(), "Show Filter is not displayed");
			NXGReports.addStep("Verify Done button functionality to close filters section","", "Click on Done button should close Filter options","Click on Done button closed Filter options", LogAs.PASSED, null);
			System.out.println(count+" , "+getEleProductHeaderCount().getText());
			Sassert.assertTrue(count.equals(getEleProductHeaderCount().getText()), "Product count not matched");
			NXGReports.addStep("Verify products update as price filter selected","", "Products should be updated as per selected price filter option count","Products updated as price filter option selection", LogAs.PASSED, null);
			BaseLib.waitForStaleElement(driver, getEleShowFilter(), "Show Filer should be attached to webpage", "Stale Show Filter Element");

		}
		catch(Exception e){
			NXGReports.addStep("Validate Price filter","","Price filter validation failed due to: "+e.getMessage(),  LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void validateColorFilter() throws Exception{
		String style = "";
		try{
			Sassert.assertTrue(getEleColorFilterOptions().size()>0, "Color options are not displayed");
			NXGReports.addStep("Verify Color filter options are displayed","", "Color filter option should be present on filter section","Color filter option is there in filter section", LogAs.PASSED, null);
			style = getEleColorFilterOptions().get(0).getAttribute("style");
			getEleColorFilterOptions().get(0).click();
			Thread.sleep(2000);waitTillPageLoad();
			getEleDoneButton().get(0).click();
			Thread.sleep(2000);waitTillPageLoad();
			Sassert.assertTrue(getEleFilterColorTags().get(0).getAttribute("style").equals(style), "Color filter not applied");
			NXGReports.addStep("Verify products update as color filter selected","", "Products should be updated as per selected color filter option count","Products updated as color filter option selection", LogAs.PASSED, null);
			BaseLib.waitForStaleElement(driver, getEleShowFilter(), "Show Filer should be attached to webpage", "Stale Show Filter Element");

		}
		catch(Exception e){
			NXGReports.addStep("Validate color filter","","Color filter validation failed due to: "+e.getMessage(),  LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void validateSizeFilter() throws Exception{
		try{
			Sassert.assertTrue(getEleFilterSizeOptionsTitles().size()>0);
			NXGReports.addStep("Verify Size filter options are displayed","", "Size filter option should be present on filter section","Size filter option is there in filter section", LogAs.PASSED, null);
			boolean flag = findSizeFilterOptionsSortedOrder(getEleFilterSizeOptionsTitles());
			Sassert.assertTrue(flag, "Size options are not in sort order");
			NXGReports.addStep("Verify Size filter options are in sorted order","", "Size filter options should be in sorted order","Size filter options are in sorted order", LogAs.PASSED, null);
			
			String count = getEleFilterSizeOptionsCounts().get(0).getText();
			getEleFilterSizeOptionsTitles().get(0).click();
			NXGReports.addStep("Verify Size filter options selected","", "Size filter options should be selected","Size filter option is selected", LogAs.PASSED, null);
			Thread.sleep(2000);waitTillPageLoad();
			Sassert.assertTrue(getEleDoneButton().get(0).isDisplayed(), "Done buton not displayed");
			NXGReports.addStep("Verify Done button is displayed","", "Done button should be present on filter section","Done button is there in filter section", LogAs.PASSED, null);
			getEleDoneButton().get(0).click();
			Thread.sleep(2000);waitTillPageLoad();
			Sassert.assertTrue(getEleShowFilter().isDisplayed(), "Filter section not collapsed after clicking DONE");
			NXGReports.addStep("Verify Done button functionality to close filters section","", "Click on Done button should close Filter options","Click on Done button closed Filter options", LogAs.PASSED, null);
			
			Sassert.assertTrue(count.equalsIgnoreCase( getEleProductHeaderCount().getText()), "Products count not matched for Size option");
		}
		catch(Exception e){
			NXGReports.addStep("Validate size filter","","Size filter validation failed due to: "+e.getMessage(),  LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void validateAdditionalFilterSection() throws Exception{
		try{
			getEleFilterProductOptionsTitles().get(0).click();
			Thread.sleep(2000);waitTillPageLoad();
			Sassert.assertTrue(getEleAdditionalFilters().size()>0,"Additional filters are present");
			NXGReports.addStep("Verify Additional filter options are available for single Product Filter options","", "Additional filter options should be available","Additional filter options are available", LogAs.PASSED, null);
			Thread.sleep(2000);waitTillPageLoad();
			getEleFilterProductOptionsTitles().get(1).click();	
			Thread.sleep(2000);waitTillPageLoad();
			Sassert.assertTrue(getEleAdditionalFilters().isEmpty(),"Additional filters are present");
			NXGReports.addStep("Verify Additional filter options are not available for 2 or more Product Filter options","", "Additional filter options should be disappear","Additional filter options are disappeared", LogAs.PASSED, null);
			getEleDoneButton().get(0).click();
		}
		catch(Exception e){
			NXGReports.addStep("Validate Additional filters","","Additional filters validation failed due to: "+e.getMessage(),  LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void validateCategoryFilter(){
		try{
			Assert.assertTrue(getEleFilterGenderOptionsTitles().size()>0);
			NXGReports.addStep("Verify Category filter options are displayed","", "Category filter option should be present on filter section","Category filter option is there in filter section", LogAs.PASSED, null);
			boolean flag = findFilterOptionsSortOrder(getEleFilterGenderOptionsTitles());
			Assert.assertTrue(flag);
			NXGReports.addStep("Verify Category filter options are in sorted order","", "Category filter options should be in sorted order","Category filter options are in sorted order", LogAs.PASSED, null);
			
		}
		catch(Exception e){
			NXGReports.addStep("Validate Category Filter","","Category filter validation failed due to: "+e.getMessage(),  LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void validateProductFilter() throws Exception{
		try{
			Sassert.assertTrue(getEleFilterProductOptionsTitles().size()>0, "Product options are not displayed");
			NXGReports.addStep("Verify Product filter options are displayed","", "Product filter option should be present on filter section","Product filter option is there in filter section", LogAs.PASSED, null);
			boolean flag = findFilterOptionsSortOrder(getEleFilterProductOptionsTitles());
			Sassert.assertTrue(flag, "Product options are not in sorted order");
			NXGReports.addStep("Verify Product filter options are in sorted order","", "Product filter options should be in sorted order","Product filter options are in sorted order", LogAs.PASSED, null);
			Sassert.assertTrue(getEleDoneButton().get(0).isDisplayed(),"Done button not displayed");
			NXGReports.addStep("Verify Done button is displayed","", "Done button should be present on filter section","Done button is there in filter section", LogAs.PASSED, null);
			getEleDoneButton().get(0).click();
			Thread.sleep(2000);
			waitTillPageLoad();
			Sassert.assertTrue(getEleShowFilter().isDisplayed(),"Filter options are not collapsed");
			NXGReports.addStep("Verify Done button functionality to close filters section","", "Click on Done button should close Filter options","Click on Done button closed Filter options", LogAs.PASSED, null);
			
		}
		catch(Exception e){
			NXGReports.addStep("validate Product Filter","","Product Filter validation failed due to: "+e.getMessage(),  LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void validateAvailableFiltersSelection() throws Exception{
		try{
			BaseLib.waitForStaleElement(driver, getEleFilterPriceOptionsTitles().get(0), "Wait for Stale option "+getEleFilterPriceOptionsTitles().get(0).getText(), getEleFilterPriceOptionsTitles().get(0).getText()+" option not attached to page");
			getEleFilterPriceOptionsTitles().get(0).click();
			Thread.sleep(2000);
			waitTillPageLoad();
			if((getEleFilterPriceOptionsTitles().get(0).findElements(By.xpath("preceding-sibling::span/div[@class='checkbox checkbox--checked']"))).size()>0)
				NXGReports.addStep("Verify Price filter option clicked","", "Price filter option should be selected","Price filter option clicked", LogAs.PASSED, null);
			else
				NXGReports.addStep("Verify Price filter option clicked","", "Price filter option should be selected","Price filter option NOT clicked", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			BaseLib.waitForStaleElement(driver, getEleFilterSizeOptionsTitles().get(0), "Wait for Stale option "+getEleFilterSizeOptionsTitles().get(0).getText(), getEleFilterSizeOptionsTitles().get(0).getText()+" option not attached to page");
			getEleFilterSizeOptionsTitles().get(0).click();
			Thread.sleep(3000);
			waitTillPageLoad();
			if((getEleFilterSizeOptionsTitles().get(0).findElements(By.xpath("preceding-sibling::span/div[@class='checkbox checkbox--checked']"))).size()>0)
				NXGReports.addStep("Verify Size filter option clicked","", "Size filter option should be selected","Size filter option clicked", LogAs.PASSED, null);
			else
				NXGReports.addStep("Verify Size filter option clicked","", "Size filter option should be selected","Size filter option NOT clicked", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			BaseLib.waitForStaleElement(driver, getEleFilterBrandOptionsTitles().get(0), "Wait for Stale option "+getEleFilterBrandOptionsTitles().get(0).getText(), getEleFilterBrandOptionsTitles().get(0).getText()+" option not attached to page");
			getEleFilterBrandOptionsTitles().get(0).click();
			Thread.sleep(3000);
			waitTillPageLoad();
			if((getEleFilterBrandOptionsTitles().get(0).findElements(By.xpath("preceding-sibling::span/div[@class='checkbox checkbox--checked']"))).size()>0)
				NXGReports.addStep("Verify Brand filter option clicked","", "Brand filter option should be selected","Brand filter option clicked", LogAs.PASSED, null);
			else
				NXGReports.addStep("Verify Brand filter option clicked","", "Brand filter option should be selected","Brand filter option NOT clicked", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			
			BaseLib.waitForStaleElement(driver, getEleColorFilterOptions().get(0), "Wait for Stale option color", "Color option not attached to page");
			getEleColorFilterOptions().get(0).click();
			Thread.sleep(2000);
			waitTillPageLoad();
			if((getEleColorFilterOptions().get(0).findElements(By.xpath("ancestor::div/div[@class='filter-option filter-option--selected filter-option--color']"))).size()>0)
				NXGReports.addStep("Verify Color filter option clicked","", "Color filter option should be selected","Color filter option clicked", LogAs.PASSED, null);
			else
				NXGReports.addStep("Verify Color filter option clicked","", "Color filter option should be selected","Color filter option NOT clicked", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			
			Sassert.assertTrue(getEleDoneButton().get(0).isDisplayed(),"Done button not displayed");
			NXGReports.addStep("Verify Done button is displayed","", "Done button should be present on filter section","Done button is there in filter section", LogAs.PASSED, null);
			getEleDoneButton().get(0).click();
			Thread.sleep(2000);waitTillPageLoad();
			Sassert.assertTrue(getEleShowFilter().isDisplayed(),"Filter options are not collapsed");
			NXGReports.addStep("Verify Done button functionality to close filters section","", "Click on Done button should close Filter options","Click on Done button closed Filter options", LogAs.PASSED, null);
		
		}
		catch(Exception e){
			NXGReports.addStep("validate Available Filters Selection","","Filters Selection validation failed due to:"+e.getMessage(),  LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void checkInventoryStatus(PdpPage pdp) throws Exception{
		try{
			
			clickOnProductOnPLPPage();
			Thread.sleep(2000);
			waitTillPageLoad();
			pdp.clickOnAvailableSize();
			driver.navigate().back();
			waitTillPageLoad();
			
		}
		catch(Exception e){
			throw e;
		}
	}
	public void validateSelectedFiltersInRefineSection(){
		try{
			
		}
		catch(Exception e){
			throw e;
		}
	}
	public void verifyFilterDeselection() throws Exception{
			try{
				
				BaseLib.waitForStaleElement(driver, getEleColorFilterOptions().get(0), "Wait for Stale option color", "Color option not attached to page");
				getEleColorFilterOptionSelected().get(0).click();
				Thread.sleep(2000);
				waitTillPageLoad();
				if(getEleColorFilterOptionSelected().size()==0)
					NXGReports.addStep("Verify Color filter option clicked","", "Color filter option should be selected","Color filter option clicked", LogAs.PASSED, null);
				else
					NXGReports.addStep("Verify Color filter option clicked","", "Color filter option should be selected","Color filter option NOT clicked", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				BaseLib.waitForStaleElement(driver, getEleFilterBrandOptionsTitles().get(0), "Wait for Stale option "+getEleFilterBrandOptionsTitles().get(0).getText(), getEleFilterBrandOptionsTitles().get(0).getText()+" option not attached to page");
				getEleFilterBrandOptionSelected().get(0).click();
				Thread.sleep(3000);
				waitTillPageLoad();
				if(getEleFilterBrandOptionSelected().size()==0)
					NXGReports.addStep("Verify Brand filter option unselected","", "Brand filter option should be unselected","Brand filter option unselected", LogAs.PASSED, null);
				else
					NXGReports.addStep("Verify Brand filter option unselected","", "Brand filter option should be unselected","Brand filter option NOT unselected", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				BaseLib.waitForStaleElement(driver, getEleFilterSizeOptionsTitles().get(0), "Wait for Stale option "+getEleFilterSizeOptionsTitles().get(0).getText(), getEleFilterSizeOptionsTitles().get(0).getText()+" option not attached to page");
				getEleFilterSizeOptionSelected().get(0).click();
				waitTillPageLoad();
				Thread.sleep(3000);
				if(getEleFilterSizeOptionSelected().size()==0)
					NXGReports.addStep("Verify Size filter option unselected","", "Size filter option should be unselected","Size filter option unselected", LogAs.PASSED, null);
				else
					NXGReports.addStep("Verify Size filter option unselected","", "Size filter option should be unselected","Size filter option NOT unselected", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				BaseLib.waitForStaleElement(driver, getEleFilterPriceOptionsTitles().get(0), "Wait for Stale option "+getEleFilterPriceOptionsTitles().get(0).getText(), getEleFilterPriceOptionsTitles().get(0).getText()+" option not attached to page");
				getEleFilterPriceOptionSelected().get(0).click();
				Thread.sleep(2000);
				waitTillPageLoad();
				if(getEleFilterPriceOptionSelected().size()==0){
					NXGReports.addStep("Verify Price filter option unselected","", "Price filter option should be unselected","Price filter option unselected", LogAs.PASSED, null);
				} else{
					NXGReports.addStep("Verify Price filter option unselected","", "Price filter option should be unselected","Price filter option NOT unselected", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
					throw new Exception();
				}
				
		}
		catch(Exception e){
			throw e;
		}
	}
	public void verifyClearAllButtonFunctionality() throws Exception{
		try{
			getEleClearAllButton().click();
			Thread.sleep(2000);
			waitTillPageLoad();
			if(getEleFilterTags().size()==0){
				NXGReports.addStep("Click on <Clear All> button","", "Clear All button should be clicked","Clear All button working", LogAs.PASSED, null);
			}else{
				NXGReports.addStep("Click on <Clear All> button","", "Clear All button should be clicked","Clear All button NOT working", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			
		}
		catch(Exception e){
			throw e;
		}
	}
	public void verifyRomovalOfFilterOptionFromRefineSection() throws Exception{
		String count="";
		try{
			count = getEleProductHeaderCount().getText();
			getEleFilterTags().get(0).findElement(By.xpath("following-sibling::a")).click();
			Thread.sleep(2000);
			waitTillPageLoad();
			if(!getEleProductHeaderCount().getText().equalsIgnoreCase(count)){
				NXGReports.addStep("Verify Products list gets updated based on the removed filter value","", "Filter tags should be removed","Removing filter tags working", LogAs.PASSED, null);
			} else{
				NXGReports.addStep("Verify Products list gets updated based on the removed filter value","", "Filter tags should be removed","Removing filter tags NOT working", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		}
		catch(Exception e){
			throw e;
		}
	}
	public void selectProductFilterOption() throws Exception{
		String option = "", title="";
		int count=0;
		try{
			option = getEleFilterProductOptionsTitles().get(0).getText();
			if(option.equalsIgnoreCase("3/4th")){
				option = "Three-quarter";
			}
			getEleFilterProductOptionsTitles().get(0).click();
			Thread.sleep(2000);
			waitTillPageLoad();

			for(int i=0; i<getEleProductsTitle().size();i++){
				title = getEleProductsTitle().get(i).getText();
				if(title.contains(option)){
					count = count+1;
				}
			}
			if(count>0){
				NXGReports.addStep("Selected L3 category should be displayed in search result set","", "Selected L3 category should be displayed in search result set",option+" found in search result set", LogAs.PASSED, null);
			}else{
				NXGReports.addStep("Selected L3 category should be displayed in search result set","", "Selected L3 category should be displayed in search result set",option+" NOT found in search result set", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		}
		catch(Exception e){
			throw e;
		}
	}
	public void validateAdditionalFilterSectionDisplayed(){
		try{
			Sassert.assertTrue(getEleAdditionalFilters().size()>0,"Additional filters are NOT present");
			NXGReports.addStep("Verify Additional filter options are available for single Product Filter options","", "Additional filter options should be available","Additional filter options are available", LogAs.PASSED, null);
			/*boolean isSorted = findFilterOptionsSortOrder(getEleAdditionalFilters());
			if(isSorted){
				NXGReports.addStep("Verify Additional filter options are available sorted order","", "Additional filter options should be sorted order","Additional filter options are sorted", LogAs.PASSED, null);
			} else{
				NXGReports.addStep("Verify Additional filter options are available sorted order","", "Additional filter options should be sorted order","Additional filter options are not in sorted order", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				Sassert.assertTrue(isSorted,"Additional filter options are not in sort order");
			}*/
		}
		catch(Exception e){
			throw e;
		}
	}
	public void validateAdditionalFilterSectionForMoreThanOneproductFilterOptionSelection() throws Exception{
		try{
			if(getEleFilterProductOptionsTitles().size()>1){
			getEleFilterProductOptionsTitles().get(getEleFilterProductOptionsTitles().size()-1).click();
			Thread.sleep(2000);
			waitTillPageLoad();
			Sassert.assertTrue(!getEleAdditionalFilters().isEmpty(),"Additional filters are NOT present");
			NXGReports.addStep("Verify Additional filter options are not available for 2 or more Product Filter options","", "Additional filter options should be disappear","Additional filter options are disappeared", LogAs.PASSED, null);	
			}
		}
		catch(Exception e){
			NXGReports.addStep("Verify Additional filter options are not available for 2 or more Product Filter options","", "Additional filter options should be disappear","Additional filter options are NOT disappeared", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}
	public void validateProductTab(){
		String message = "";
		try{
			for(int i=0; i<getEleProductsImages().size(); i++){
				if(getEleProductsTitle().get(i).getText().equals(""))
					message = message+"Product @ position"+i+1+". Title is missing <br>";
				if(!getEleProductsSellingPrice().get(i).getText().contains("₹"))
					message = message+ "Product @ position"+i+1+" price missing <br>";
			}
			int imagesCount = getEleProductsImages().size(), titleCount = getEleProductsTitle().size(), priceCount=getEleProductsSellingPrice().size() ;	
			if((imagesCount==titleCount&&titleCount==priceCount)&&message.equals("")){
				NXGReports.addStep("Verify product image, title and price displayed","", "Product image, title and price should be displayed","Product image, title and price are verified", LogAs.PASSED, null);
			}
			else{
				Sassert.assertTrue(1>2, message);
				NXGReports.addStep("Verify product image, title and price displayed","", "Product image, title and price should be displayed",message, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		}
		catch(Exception e){
			throw e;
		}
	}
	public void verifyProductLoadByScrollingDownpage() throws Exception {
		List<WebElement> productsList = null;
		try{
			productsList = getEleProductsImages();
			int oldCount = productsList.size();
			logger.info("Products count before scroll: "+oldCount);
			 ((JavascriptExecutor)driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
			 Thread.sleep(3000);
			 waitTillPageLoad();
				logger.info("Products count after scroll: "+getEleProductsImages().size());
			 Assert.assertTrue(oldCount<getEleProductsImages().size());
			 NXGReports.addStep("Verify The products should be loaded based on scroll request.","", "The products should be loaded based on scroll request.","Products loaded as user scrolldown the PLP page", LogAs.PASSED, null);
		}
		catch(Exception e){
			NXGReports.addStep("Verify The products should be loaded based on scroll request.","", "The products should be loaded based on scroll request.","Products NOT loaded on scroll request.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}
	
	public void clickOnSortBy() throws Exception {
		try{
			getEleSortBy().click();
			Thread.sleep(1000);
			waitTillPageLoad();
			NXGReports.addStep("Verify Click on Sort By","", "Sort options should be populate.","Clicked on Sort By", LogAs.PASSED, null);
			waitForElement(getEleSortOptions().get(0), "Sort Options dropdown toggle should appear", "Sort Options dropdown toggle is present");
			Sassert.assertTrue(getEleSortOptions().size()>0,"Sort Options dropdown toggle not present");
		 }catch(Exception e){
				NXGReports.addStep("Verify Click on Sort By","", "Sort options should be populate.","Click on Sort By failed due to "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				throw e;
		}
	}
	
	public void verifySortByOptionPopularity() {
		try{
			for(WebElement ele:getEleSortOptions()){
				if(ele.getText().equalsIgnoreCase("Popularity")){
					Sassert.assertTrue(getEleSortOptions().get(0).getText().toLowerCase().equalsIgnoreCase("popularity"), "Popularity  is not first option");
					ele.click();
					NXGReports.addStep("Verify Click on Popularity Option","", "Click on Sort By should be refine products","Click on popularity is success ", LogAs.PASSED, null);
					break;
				}
			}
		}catch(Exception e){
			NXGReports.addStep("Verify Click on Popularity Option","", "Click on Sort By should be refine products","Click on popularity failed due to "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
	}
	}
	public void verifySortByJustIn() throws Exception {
		try{
			String firstProdTitle = getEleProductsTitle().get(0).getText();
			for(WebElement ele:getEleSortOptions()){
				if(ele.getText().equalsIgnoreCase("Just In")){
					ele.click();
					Thread.sleep(3000);
					waitTillPageLoad();
					Assert.assertTrue(driver.findElements(By.xpath("//*[contains(text(), 'Sort By')]/following-sibling::*[contains(text(), 'Just In')]")).size()>0, "Not sorted by Just In");
					Assert.assertFalse(firstProdTitle.equalsIgnoreCase(getEleProductsTitle().get(0).getText()));
					NXGReports.addStep("Verify Click on 'Just In' Sort Option","", "Click on Sort By 'Just In' should be refine products","Click on 'Just In' Sort Option is success ", LogAs.PASSED, null);
					break;
				}
			}
		} catch (Exception e){
			NXGReports.addStep("Verify Click on 'Just In' Sort Option","", "Click on Sort By 'Just In' should be refine products","Click on 'Just In' failed.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}
	public void verifySortByDiscountHighToLow() throws Exception{
		try{
			String firstProdTitle = getEleProductsTitle().get(0).getText();
			for(WebElement ele:getEleSortOptions()){
				if(ele.getText().equalsIgnoreCase("Discount - High to Low")){
					ele.click();
					Thread.sleep(3000);
					waitTillPageLoad();
					Assert.assertTrue(driver.findElements(By.xpath("//*[contains(text(), 'Sort By')]/following-sibling::*[contains(text(), 'Discount - High to Low')]")).size()>0, "Not sorted by Discount - High to Low");
					Assert.assertFalse(firstProdTitle.equalsIgnoreCase(getEleProductsTitle().get(0).getText()));
					NXGReports.addStep("Verify Click on 'Discount - High to Low' Sort Option","", "Click on Sort By 'Discount - High to Low' should be refine products","Click on 'Discount - High to Low' Sort Option is success ", LogAs.PASSED, null);
					break;
				}
			}
		} catch (Exception e){
			NXGReports.addStep("Verify Click on 'Discount - High to Low' Sort Option","", "Click on Sort By 'Discount - High to Low' should be refine products","Click on 'Discount - High to Low' failed.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}
	public void verifySortByPriceLowToHigh() throws Exception{
		List<String> amt = new ArrayList<>();
		try{
			for(WebElement ele:getEleSortOptions()){
				if(ele.getText().equalsIgnoreCase("Price – Low to High")){
					ele.click();
					Thread.sleep(3000);
					waitTillPageLoad();
					for(WebElement price:getEleProductsSellingPrice()){
						String value = price.getText().replace("₹", "").replace(",", "");
						amt.add(value.trim());
					}
					Assert.assertTrue(isAscending(amt), "Not sorted by Price – High to Low");
					NXGReports.addStep("Verify Click on 'Price – Low to High' Sort Option","", "Click on Sort By 'Price – Low to High' should be refine products","Click on 'Price – Low to High' Sort Option is success ", LogAs.PASSED, null);
					break;
				}
			}
		} catch (Exception e){
			NXGReports.addStep("Verify Click on 'Price – Low to High' Sort Option","", "Click on Sort By 'Price – Low to High' should be refine products","Click on 'Price – Low to High' failed.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}
	public void verifySortByPriceHighToLow() throws Exception{
		List<String> amt = new ArrayList<>();
		try{
			for(WebElement ele:getEleSortOptions()){
				if(ele.getText().equalsIgnoreCase("Price – High to Low")){
					ele.click();
					Thread.sleep(3000);
					waitTillPageLoad();
					for(WebElement price:getEleProductsSellingPrice()){
						String value = price.getText().replace("₹", "").replace(",", "");
						amt.add(value.trim());
					}
					Assert.assertTrue(isDescending(amt), "Not sorted by Price – High to Low");
					NXGReports.addStep("Verify Click on 'Price – High to Low' Sort Option","", "Click on Sort By 'Price – High to Low' should be refine products","Click on 'Price – High to Low' Sort Option is success ", LogAs.PASSED, null);
					break;
				}
			}
		} catch (Exception e){
			NXGReports.addStep("Verify Click on 'Price – High to Low' Sort Option","", "Click on Sort By 'Price – High to Low' should be refine products","Click on 'Price – High to Low' failed.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}
	
	public String verfiySearchHeaderInPLP() throws InterruptedException, IOException {
		
		String actualText = geteleSerachedText().getText();
		System.out.println("Searched text is: " +actualText );
	
		return actualText;
		

		
	}
	

}
