/***********************************************************************
* @author 			:		RaghuKiran MR
* @description		: 		Page objects and re-usbale methods for CheckOut screen
* @module			:		SavedCard
* @reusable methods : 		fillCheckOutDetails()
*/
package com.abof.web.pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.abof.library.BaseLib;
import com.abof.web.testscripts.BaseTest;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;


public class CheckOutPage extends BaseTest{
	WebDriver driver;

	public CheckOutPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver,this);
	}

	@FindBys({ @FindBy(xpath = "//*[text()='Remove']") })
	private List<WebElement> eleRemoveTxt;
	public List<WebElement> getEleRemoveLinks() {
		return eleRemoveTxt;
	}
	@FindBy(xpath = "//*[contains(text(), 'Place Order')]")
	private WebElement elePlaceOrderBtn;
	public WebElement getElePlaceOrderBtn() {
		return elePlaceOrderBtn;
	}
	@FindBy(xpath = "//*[contains(text(), 'View Bag')]")
	private WebElement eleViewBagLink;
	public WebElement getEleViewBagLink() {
		return eleViewBagLink;
	}
	@FindBy(xpath = "//*[contains(text(), 'Sign in')]")
	private WebElement eleSignInForCouponLink;
	public WebElement getEleSignInForCouponLink() {
		return eleSignInForCouponLink;
	}
	/*
	*@author: Shireesh
	 *Description: Method for deleting products from bag page
	 */

	public void removeProductsFromBag(){
		List<WebElement> removeLinks = null;
		int size ;
		try{
			removeLinks = getEleRemoveLinks();
			size = getEleRemoveLinks().size();
			if(removeLinks.isEmpty()){
				NXGReports.addStep("Remove all products from bag.", "","Products from bag should be removed", "Shopping Bag is empty.", LogAs.PASSED, null);
			}
			else{
				for(WebElement ele:removeLinks){
					waitForElementToBeClickable(ele, "Remove should be ready for click","Remove product is ready for Click");
					ele.click();
					waitTillPageLoad();

				}
			}
			NXGReports.addStep("Remove all products from bag.", "","Products from bag should be removed", "Products from bag are removed", LogAs.PASSED, null);
		} catch (Exception e) {
			NXGReports.addStep("Remove all products from bag.", "","Products from bag should be removed", "Products from bag are not removed due to Exception: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
		
	}
	public void clickOnPLACEORDER_Btn() throws Exception{
		try{
			BaseLib.waitForStaleElement(driver, getElePlaceOrderBtn(), "Place Order button should be displayed.", "Place Order button is displayed.");
			if(!getElePlaceOrderBtn().isDisplayed()){
				NXGReports.addStep("Verify Place Order button is displayed.", "","Place Order button should be displayed.", "Place Order button is not displayed.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			else{
					waitForElementToBeClickable(getElePlaceOrderBtn(), "Place Order button should be ready for click","Place Order button is ready for Click");
					getElePlaceOrderBtn().click();
					waitTillPageLoad();
					}
			NXGReports.addStep("Verify Place Order button click.", "","Place Order button should be clicked.", "Place Order button is clicked.", LogAs.PASSED, null);
			BaseLib.waitForStaleElement(driver, LoginPage.getEleGuestUserBtn(), "Wait for Login Page loading", "Waited for Login Page load is over");		
		} catch (Exception e) {
			NXGReports.addStep("Verify Place Order button click.", "","Place Order button should be clicked.", "Place Order button clicked failed due to Exception: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}
	public void clickOnViewBagLink_ShippingAddress(){
		BaseLib.waitForStaleElement(driver, getEleViewBagLink(), "View bag link should be displayed.", "View bag link is displayed.");
		getEleViewBagLink().click();
		waitTillPageLoad();
		NXGReports.addStep("Verify click on View bag link.", "View bag link should be clicked.","View bag link is clicked.",LogAs.PASSED,null);
		BaseLib.waitForStaleElement(driver, getElePlaceOrderBtn(), "Place Order button should be displayed.", "Place Order button is displayed.");
		Assert.assertTrue(getElePlaceOrderBtn().isDisplayed());
	}
	
	public void clickOnSignInToApplyCoupon(){
		BaseLib.waitForStaleElement(driver, getEleSignInForCouponLink(), "Sign in link should be displayed.", "Sign in link is displayed.");
		getEleSignInForCouponLink().click();
		waitTillPageLoad();
		NXGReports.addStep("Verify click on Sign in link.", "Sign in link should be clicked.","Sign in link is clicked.",LogAs.PASSED,null);
		BaseLib.waitForStaleElement(driver, LoginPage.getEleEmailAddressTxtBox(), "Email textfiled should be displayed.", "Email textfiled is displayed.");
		LoginPage.getEleAlreadyRegistered().click();
		waitTillPageLoad();
		Assert.assertTrue(LoginPage.getEleSignInBtn().isDisplayed());
		NXGReports.addStep("Verify Sign in form displayed.", "Sign in form should be displayed.","Sign in form is displayed.",LogAs.PASSED,null);
	}
	
}
