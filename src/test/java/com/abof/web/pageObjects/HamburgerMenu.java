/***********************************************************************
* @author 			:		Srinivas Hippargi 
* @description		: 		Page objects and re-usbale methods for Hamburger Menu screen
* @module			:		Hamburger Menu
* @reusable methods : 		verifyHamburgerMenuOptions()
*/
package com.abof.web.pageObjects;

import java.util.List;

import javax.swing.text.html.HTML.Tag;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.abof.library.BaseLib;
import com.abof.web.testscripts.BaseTest;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;


public class HamburgerMenu extends BaseTest{
	static WebDriver driver = BaseTest.driver;
	LoginPage loginPo = null;	
	public static SoftAssert Sassert;

	public HamburgerMenu(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		loginPo = new LoginPage(driver);
	}
	@FindBy(xpath = "//*[@id='abofApp']/div/div[1]/div/div[2]/div/div[1]/div/a/*")
	private static WebElement eleToggleLnk;
	public static WebElement getEleToggleLnk() {
		return eleToggleLnk;
	}
	@FindBy(xpath = "//*[contains(text(), "+'"'+"What's Hot"+'"'+")]")
	private WebElement eleWhatshotLnk;
	public WebElement getElewhatshotLnk() {
		return eleWhatshotLnk;
	}

	//@FindBy(xpath = "//span[starts-with(.,'men')][@itemprop='name']")
	@FindBy(xpath = "//*[contains(text(), 'Shop Men')]")
	private static  WebElement eleShopMenLnk;
	public static  WebElement getEleShopMenLnk() {
		return eleShopMenLnk;
	}

	@FindBy(xpath = "//*[contains(text(), 'Shop Women')]")
	private static WebElement eleShopWomenLnk;
	public static WebElement getEleShopWomenLnk() {
		return eleShopWomenLnk;
	}

	@FindBy(xpath = "//ul/li/a/*[contains(text(), 'My Orders')]")
	private WebElement eleMyOrdersLnk;

	public WebElement getEleMyOrdersLnk() {
		return eleMyOrdersLnk;
	}

	@FindBy(xpath = "//ul/li/a/*[contains(text(), 'My Favorites')]")
	private WebElement eleMyfavouritesLnk;

	public WebElement getEleMyFavouritesLnk() {
		return eleMyfavouritesLnk;
	}

	@FindBy(xpath = "//ul/li/a/*[contains(text(), 'Refer a friend')]")
	private WebElement eleReferaFriendLnk;

	public WebElement getEleReferaFriendLnk() {
		return eleReferaFriendLnk;
	}

	@FindBy(xpath = "//ul/li/a/*[contains(text(), 'Return & Exchange')]")
	private WebElement eleReturnAndExchangeLnk;

	public WebElement getEleReturnAndExchangeLnk() {
		return eleReturnAndExchangeLnk;
	}

	@FindBy(xpath = "//ul/li/a/*[contains(text(), 'abof bucks & gift cards')]")
	private WebElement eleAbofBucksAndGiftCardLnk;

	public WebElement getEleAbofBucksAndGiftCardLnk() {
		return eleAbofBucksAndGiftCardLnk;
	}
	
	@FindBy(id = "com.abof.android:id/gift_code_pin")
	private WebElement eleGvCodePinTxtBox;

	public WebElement getEleGvCodePinTxtBox() {
		return eleGvCodePinTxtBox;
	}
	//com.abof.android:id/gift_code_pin
	
	@FindBy(id = "com.abof.android:id/txtTotalStoreCredits")
	private WebElement eleAbofBucksCreditsTxt;

	public WebElement getEleAbofBucksCreditsTxt() {
		return eleAbofBucksCreditsTxt;
	}
	//com.abof.android:id/txtTotalStoreCredits

	@FindBy(xpath = "//ul/li/a/*[contains(text(), 'abof Support')]")
	private WebElement eleAbofSupportLnk;

	public WebElement getEleAbofSupportLnk() {
		return eleAbofSupportLnk;
	}

	@FindBy(xpath = "//ul/li/a/*[contains(text(), 'About abof')]")
	private WebElement eleAboutAbofLnk;

	public WebElement getEleAboutAbofLnk() {
		return eleAboutAbofLnk;
	}

	@FindBy(xpath = "//ul/li/a/*[contains(text(), 'Terms & Conditions')]")
	private WebElement eleTermsAndConditionsLnk;

	public WebElement getEleTermsAndConditionsLnk() {
		return eleTermsAndConditionsLnk;
	}

	@FindBy(xpath = "//ul/li/a/*[contains(text(), 'Privacy policy')]")
	private WebElement elePrivacyPolicyLnk;

	public WebElement getElePrivacyPolicyLnk() {
		return elePrivacyPolicyLnk;
	}

	@FindBy(id = "com.abof.android:id/style_text")
	private WebElement eleStylePreferenceLnk;

	public WebElement getEleStylePreferenceLnk() {
		return eleStylePreferenceLnk;
	}

	@FindBy(id = "com.abof.android:id/bucks_text")
	private WebElement eleAbofBucksAndGiftCardsLnk;

	public WebElement getEleAbofBucksAndGiftCardsLnk() {
		return eleAbofBucksAndGiftCardsLnk;
	}

	@FindBys({ @FindBy(id = "com.abof.android:id/txtProductName") })
	private List<WebElement> eleMyFavoriteProductLst;

	public List<WebElement> getEleMyFavoriteProductLst() {
		return eleMyFavoriteProductLst;
	}

	@FindBy(id = "com.abof.android:id/user_profile")
	private WebElement eleUserProfileImg;

	public WebElement getEleUserProfileImg() {
		return eleUserProfileImg;
	}
	@FindBy(id = "com.abof.android:id/txtMyabof")
	private WebElement eleUserProfileNameTxt;

	public WebElement getEleUserProfileNameTxt() {
		return eleUserProfileNameTxt;
	}
	
	@FindBy(xpath = "//android.view.View/android.widget.ImageButton")
	private WebElement eleHambergurMenuBackBtn;

	public WebElement getEleHambergurMenuBackBtn() {
		return eleHambergurMenuBackBtn;
	}
	
	@FindBy(xpath = "//android.widget.LinearLayout[@index='1']//android.widget.LinearLayout[@index='2']//android.widget.RelativeLayout[@index='0']//android.widget.TextView")
	private List<WebElement> eleSavedCardInfoTxt;

	public List<WebElement> getEleSavedCardInfoTxt() {
		return eleSavedCardInfoTxt;
	}
	
	@FindBy(id ="com.abof.android:id/txtSavedCard")
	private WebElement eleSaveCardText;

	public WebElement getEleSaveCardText() {
		return eleSaveCardText;
	}
	
	@FindBy(id ="com.abof.android:id/gift_code")
	private WebElement eleGiftCodeTxtBox;

	public WebElement getEleGiftCodeTxtBox() {
		return eleGiftCodeTxtBox;
	}
	
	@FindBy(id ="com.abof.android:id/message")
	private WebElement eleGitVoucherSuccessFullyTxt;

	public WebElement getEleGitVoucherSuccessFullyTxt() {
		return eleGitVoucherSuccessFullyTxt;
	}
	
	@FindBy(id ="com.abof.android:id/rdbProSavedCard")
	private WebElement eleProSavedCardIcon;

	public WebElement getEleProSavedCardIcon() {
		return eleProSavedCardIcon;
	}
	
	@FindBy(id ="com.abof.android:id/btnDeleteCard")
	private WebElement eleDeleteCardBtn;

	public WebElement getEleDeleteCardBtn() {
		return eleDeleteCardBtn;
	}
	
	@FindBy(id ="com.abof.android:id/confirm_btn")
	private WebElement eleDeleteConfirmBtn;

	public WebElement getEleDeleteConfirmBtn() {
		return eleDeleteConfirmBtn;
	}
	//
	
	// For validating hamburger menu
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Refer a friend')]")
	private WebElement eleReferaFriendTitleTxt;

	public WebElement getEleReferaFriendTitleTxt() {
		return eleReferaFriendTitleTxt;
	}
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Returns/Exchanges')]")
	private WebElement eleReturnAndExchangeTitleTxt;

	public WebElement getEleReturnAndExchangeTitleTxt() {
		return eleReturnAndExchangeTitleTxt;
	}
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'abof Bucks & abof Gift Card')]")
	private WebElement eleAbofBucksandGiftCardTitleTxt;

	public WebElement getEleAbofBucksandGiftCardTitleTxt() {
		return eleAbofBucksandGiftCardTitleTxt;
	}
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'abof Support')]")
	private WebElement eleAbofSupportTitleTxt;

	public WebElement getEleAbofSupportTitleTxt() {
		return eleAbofSupportTitleTxt;
	}
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'About abof')]")
	private WebElement eleAboutAbofTitleTxt;

	public WebElement getEleAboutAbofTitleTxt() {
		return eleAboutAbofTitleTxt;
	}
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Terms & Conditions')]")
	private WebElement eleTermsAndConditionsTitleTxt;

	public WebElement getEleTermsAndConditionsTitleTxt() {
		return eleTermsAndConditionsTitleTxt;
	}
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Privacy Policy')]")
	private WebElement elePrivacyPolicyTitleTxt;

	public WebElement getElePrivacyPolicyTitleTxt() {
		return elePrivacyPolicyTitleTxt;
	}
	
	@FindBy(xpath = "//ul/li/a/*[contains(text(), 'Download App')]")
	private WebElement eleDownloadAppLnk;

	public WebElement getEleDownloadAppLnk() {
		return eleDownloadAppLnk;
	}
	
	@FindBy(xpath = "//	android.widget.TextView[contains(@text,'Favourites')]")
	private WebElement eleFavouritesTxt;

	public WebElement getEleFavouritesTxt() {
		return eleFavouritesTxt;
	}
	
	
	@FindBy(id="com.abof.android:id/ABOF_logout_btn")
	private WebElement eleLogoutBtn;

	public WebElement getEleLogoutBtn() {
		return eleLogoutBtn;
	}

	/*
	 * @author: Srinivas Hippargi Description: to check, after choosing an item
	 * as fav,whether it is available in favorite list
	 * 
	 */
	public static void selectMen(){
		try{
			WebElement element = getEleShopMenLnk();
			System.out.println("Click Toggle menu");
			getEleToggleLnk().click();
			Thread.sleep(2000);
			System.out.println("Found links : "+element.getText());
			//waitForElementToBeClickable(element, "Toggle Menu should opened with ShopMen Link", "Shop Men link not available on toggle menu");
			//Sassert.assertTrue(element.isEnabled(),"Toggle Menu not opened");
			NXGReports.addStep("Click on Toggle menu", LogAs.PASSED, null);
			element.click();
			Thread.sleep(2000);
			waitTillPageLoad();
			//logger.info(driver.getCurrentUrl().endsWith("/men"));
			//Sassert.assertTrue(driver.getCurrentUrl().endsWith("/men"),"Men landing page not loaded");
			NXGReports.addStep("Click on Shop Men Link", "User Should navigate to Men Landing page","Men Landing page loaded",LogAs.PASSED, null);
		}
		catch(Exception e){
			NXGReports.addStep("Men Landing page not loaded due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public static void selectWomen(){
		try{
			getEleToggleLnk().click();
			Thread.sleep(2000);
			waitForElementToBeClickable(getEleShopWomenLnk(), "Toggle Menu should opened with ShopWomen Link", "Shop Women link not available on toggle menu");
			Sassert.assertTrue(getEleShopWomenLnk().isEnabled(),"Toggle Menu not opened");
			NXGReports.addStep("Click on Toggle menu", LogAs.PASSED, null);
			getEleShopWomenLnk().click();
			Thread.sleep(2000);
			waitTillPageLoad();
			Sassert.assertTrue(driver.getCurrentUrl().endsWith("/women"));
			NXGReports.addStep("Click on Shop Women Link", "User Should navigate to Women Landing page","Women Landing page loaded",LogAs.PASSED, null);
		}
		catch(Exception e){
			NXGReports.addStep("Women Landing page not loaded due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void isProductAvailableInFavList(String product) {
		
		try
		{
		for (int i = 0; i <= getEleMyFavoriteProductLst().size() - 1; i++) {
			if (getEleMyFavoriteProductLst().get(i).getText().equals(product)) {
				NXGReports.addStep("Selected item is succesfully added favorite list", LogAs.PASSED, null);
				break;
			} else {
				BaseLib.swipeBottomToTop(.80, .20);
			}
		}

		}catch(Exception e)
		{
			NXGReports.addStep("Selected item is not in favorite list", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}
	
	/*
	 * @author: PreranaBhatt
	 *  Description: to check, when item is removed from fav (PLP,PdP,Cart),it should not  available in favorite list.
	 * 
	 */
	
public void isProductUnavailableInFavList(String product) {
	
		int flag=0;
		for (int i = 0; i <= getEleMyFavoriteProductLst().size() - 1; i++) {
			if (getEleMyFavoriteProductLst().get(i).getText().equals(product)) {
				flag=1;
				break;
			} else {
				BaseLib.swipeBottomToTop(.80, .20);
			}
		}
		if(flag==1)
		{
			NXGReports.addStep("Product stil in favorite list instead of getting removed", LogAs.PASSED, null);
		}
		else
		{
			NXGReports.addStep("Selected item is succesfully removed from favorite list", LogAs.PASSED, null);
		}
	}

	/*
	 * @author: Srinivas Hippargi 
	 * Description: Method for verifying each option
	 * is displayed in Hamburger menu
	 */
	public void clickHamburgerMenu() throws  Exception {
		try{
			getEleToggleLnk().click();
			Thread.sleep(2000);
			if(getEleDownloadAppLnk().isDisplayed()){
				NXGReports.addStep("Click on Hamburger is Success", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			else {
				NXGReports.addStep("Hamburger Click failed.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		} catch(Exception e){
			NXGReports.addStep("Hamburger Click failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void verfyHamberMenuOptions() throws InterruptedException {
		BaseLib.elementStatus(getElewhatshotLnk(), "Whats Hot Option", "displayed");
		BaseLib.elementStatus(getEleShopMenLnk(), "Shop Men Option", "displayed");
		BaseLib.elementStatus(getEleShopWomenLnk(), "Shop WoMen Option", "displayed");
		BaseLib.elementStatus(getEleMyOrdersLnk(), "My Oders option", "displayed");
		BaseLib.elementStatus(getEleMyFavouritesLnk(), "My Oders option", "displayed");
		BaseLib.elementStatus(getEleReferaFriendLnk(), "Refer a friend option", "displayed");
		BaseLib.elementStatus(getEleReturnAndExchangeLnk(), "Return and Exchange option", "displayed");
		BaseLib.elementStatus(getEleAbofBucksAndGiftCardLnk(), "abof bucks & gift cards option", "displayed");
		BaseLib.elementStatus(getEleAbofSupportLnk(), "abof support option", "displayed");
		BaseLib.elementStatus(getEleAboutAbofLnk(), "About abof option ", "displayed");
		Thread.sleep(3000);
		//BaseLib.swipeBottomToTop(.90, .70);
		BaseLib.elementStatus(getEleTermsAndConditionsLnk(), "Terms & Condition option ", "displayed");
		BaseLib.elementStatus(getElePrivacyPolicyLnk(), "Privacy Policy option", "displayed");

	}

	public void validateHamburgerMenuOptions(WebElement expElement, WebElement actElement) throws InterruptedException {
		Thread.sleep(2000);
		BaseLib.elementStatus(expElement, expElement.getText(), "displayed");
		expElement.click();
		Thread.sleep(2000);
		loginPo.handleOkayBtn();
		Thread.sleep(2000);
		Assert.assertTrue(actElement.isDisplayed(), actElement.getText() + "is not displayed");
		NXGReports.addStep(actElement.getText() + " Header Text is displayed", LogAs.PASSED, null);

	}
	
	public void validateWhatsHotLink() throws Exception {
		String tabName = "";
		try{
			getElewhatshotLnk().click();
			Thread.sleep(2000);
			waitTillPageLoad();
			tabName = driver.findElement(By.xpath("//*[@class='story-site-nav__item story-site-nav__item--active']//a")).getText();
			if(tabName.equalsIgnoreCase("what's hot")){
				NXGReports.addStep("What's Hot link is validated", LogAs.PASSED, null);
			} 
			else{
				NXGReports.addStep("What's Hot link is validation failed","","Expected What's Hot but, found: "+tabName, LogAs.PASSED, null);
			}
		}
		catch(Exception e){
			NXGReports.addStep("Click on What's Hot link failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void validateMenLink() throws Exception {
		String tabName = "";
		try{
			getEleShopMenLnk().click();
			Thread.sleep(2000);
			waitTillPageLoad();
			tabName = driver.findElement(By.xpath("//*[@class='story-site-nav__item story-site-nav__item--active']//a")).getText();
			if(tabName.equalsIgnoreCase("Men")){
				NXGReports.addStep("Shop Men link is validated", LogAs.PASSED, null);
			}
			else{
				NXGReports.addStep("Shop Men link is validation failed","","Expected Men but, found: "+tabName, LogAs.PASSED, null);
			}
		}
		catch(Exception e){
			NXGReports.addStep("Click on Shop Men link failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void validateWomenLink() throws Exception {
		String tabName = "";
		try{
			getEleShopWomenLnk().click();
			Thread.sleep(2000);
			waitTillPageLoad();
			tabName = driver.findElement(By.xpath("//*[@class='story-site-nav__item story-site-nav__item--active']//a")).getText();
			if(tabName.equalsIgnoreCase("Women")){
				NXGReports.addStep("Shop Women link is validated", LogAs.PASSED, null);
			} else{
				NXGReports.addStep("Shop Women link is validation failed","","Expected Shop Women but, found: "+tabName, LogAs.PASSED, null);
			}
		}
		catch(Exception e){
			NXGReports.addStep("Click on Shop Women link failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void validateMyOrdersLink() throws Exception {
		try{
			getEleMyOrdersLnk().click();
			Thread.sleep(2000);
			waitTillPageLoad();
			if(LoginPage.getEleEmailAddressTxtBox().isEnabled()){
				NXGReports.addStep("My Orders link is validated", LogAs.PASSED, null);
			} 
			else{
				NXGReports.addStep("My Orders link is validation failed","","My Orders Link not clicked.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			driver.navigate().back();
		}
		catch(Exception e){
			NXGReports.addStep("Click on My Orders link failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void validateMyFavoritesLink() throws Exception {
		try{
			getEleMyFavouritesLnk().click();
			Thread.sleep(2000);
			waitTillPageLoad();
			if(LoginPage.getEleEmailAddressTxtBox().isEnabled()){
				NXGReports.addStep("My Favorites link is validated", LogAs.PASSED, null);
			} 
			else{
				NXGReports.addStep("My Favorites link is validation failed","","My Favorites Link not clicked.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			driver.navigate().back();
		}
		catch(Exception e){
			NXGReports.addStep("Click on My Favorites link failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void validateRefer_A_FriendLink() throws Exception {
		try{
			getEleReferaFriendLnk().click();
			Thread.sleep(2000);
			waitTillPageLoad();
			if(LoginPage.getEleEmailAddressTxtBox().isEnabled()){
				NXGReports.addStep("Refer a Friend link is validated", LogAs.PASSED, null);
			} 
			else{
				NXGReports.addStep("Refer a Friend link is validation failed","","Refer a Friend Link not clicked.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		}
		catch(Exception e){
			NXGReports.addStep("Click on Refer a Friend link failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void verifyReturnExchangeLink() throws Exception {
		try{
			getEleReturnAndExchangeLnk().click();
			Thread.sleep(2000);
			waitTillPageLoad();
			if(LoginPage.getEleEmailAddressTxtBox().isEnabled()){
				NXGReports.addStep("Return & Exchange link is validated", LogAs.PASSED, null);
			} 
			else{
				NXGReports.addStep("Return & Exchange link is validation failed","","Return & Exchange Link not clicked.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		}
		catch(Exception e){
			NXGReports.addStep("Click on Return & Exchange link failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void verifyAbofBuckGiftCardsLink() throws Exception {
		try{
			getEleAbofBucksAndGiftCardLnk().click();
			Thread.sleep(2000);
			waitTillPageLoad();
			if(LoginPage.getEleEmailAddressTxtBox().isEnabled()){
				NXGReports.addStep("Abof bucks & gift cards link is validated", LogAs.PASSED, null);
			} 
			else{
				NXGReports.addStep("Abof bucks & gift cards link is validation failed","","Abof bucks & gift cards Link not clicked.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		}
		catch(Exception e){
			NXGReports.addStep("Click on Abof bucks & gift cards link failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void verifyAbofSupportLink() throws Exception {
		try{
			getEleAbofSupportLnk().click();
			Thread.sleep(2000);
			waitTillPageLoad();
			if(driver.findElements(By.xpath("//*[contains(text(), 'Support Home Page')]")).size()>0){
				NXGReports.addStep("My Orders link is validated", LogAs.PASSED, null);
			} 
			else{
				NXGReports.addStep("My Orders link is validation failed","","My Orders Link not clicked.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		}
		catch(Exception e){
			NXGReports.addStep("Click on My Orders link failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void verifyAboutAbofLink() throws Exception {
		try{
			getEleAboutAbofLnk().click();
			Thread.sleep(2000);
			waitTillPageLoad();
			if(driver.findElement(By.xpath("//h5[contains(@class, 'seo-content__heading')]")).getText().contains("ABOUT abof")){
				NXGReports.addStep("ABOUT abof link is validated", LogAs.PASSED, null);
			} 
			else{
				NXGReports.addStep("ABOUT abof link is validation failed","","ABOUT abof Link not clicked.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		}
		catch(Exception e){
			NXGReports.addStep("Click on ABOUT abof link failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void verifyTermsConditionsLink() throws Exception {
		try{
			getEleTermsAndConditionsLnk().click();
			Thread.sleep(2000);
			waitTillPageLoad();
			if(driver.findElement(By.xpath("//h5[contains(@class, 'seo-content__heading')]")).getText().contains("TERMS AND CONDITIONS OF USE")){
				NXGReports.addStep("Terms&Conditions link is validated", LogAs.PASSED, null);
			} 
			else{
				NXGReports.addStep("Terms&Conditions link is validation failed","","Terms&Conditions Link not clicked.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		}
		catch(Exception e){
			NXGReports.addStep("Click on Terms&Conditions link failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public void verifyPrivacyPolicyLink() throws Exception {
		try{
			getElePrivacyPolicyLnk().click();
			Thread.sleep(2000);
			waitTillPageLoad();
			if(driver.findElement(By.xpath("//h5[contains(@class, 'seo-content__heading')]")).getText().equalsIgnoreCase("PRIVACY POLICY")){
				NXGReports.addStep("Privacy policy link is validated", LogAs.PASSED, null);
			} 
			else{
				NXGReports.addStep("Privacy policy link is validation failed","","Privacy policy Link not clicked.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		}
		catch(Exception e){
			NXGReports.addStep("Click on Privacy policy link failed due to: "+e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
}
