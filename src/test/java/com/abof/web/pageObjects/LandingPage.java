/***********************************************************************
* @author 			:		Shireesh Kumar Bandela 
* @description		: 		Page objects and re-usbale methods for Mega Menu 
* @module			:		Homepage, Mega Menu
* @reusable methods : 		
*/
package com.abof.web.pageObjects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.openqa.selenium.By;

import com.abof.web.pageObjects.PdpPage;
import com.abof.web.pageObjects.PlpPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.StringUtils;
import org.testng.Assert;

import com.abof.library.BaseLib;
import com.abof.web.testscripts.BaseTest;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;

public class LandingPage extends BaseTest {

	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	WebDriver driver;

	public LandingPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(xpath = "//img[@alt='OBMenHeroBanner' or @alt='OBWomen-Herobanner']")
	private WebElement eleAbofBannerImag;

	public WebElement getEleAbofBannerImag() {
		return eleAbofBannerImag;
	}

	@FindBy(xpath = "//a[contains(text(), 'Shop Collection')]")
	private WebElement eleShopCollection;

	public WebElement getEleShopCollection() {
		return eleShopCollection;
	}

	@FindBys({
			@FindBy(xpath = "//div[@class='brand-collection-right' or @class='brand-collection-right brand-collection-left--level2']") })
	private List<WebElement> eleProducts;

	public List<WebElement> getEleCollections4Products() {
		return eleProducts;
	}

	@FindBy(xpath = "//*[contains(text(), 'SHOP LOOKS')]")
	private WebElement eleShopLooks;

	public WebElement getEleShopLooks() {
		return eleShopLooks;
	}

	@FindBy(xpath = "//div[@class='product-slider-wrapper']/div/div/div/div")
	private WebElement eleLooksWidget;

	public WebElement getEleLooksWidget() {
		return eleLooksWidget;
	}

	@FindBy(xpath = "//div[@class='product-slider-wrapper']/div/div/div/div/div")
	private List<WebElement> eleLooksProducts;

	public List<WebElement> getEleLooksProducts() {
		return eleLooksProducts;
	}

	@FindBy(xpath = "//div[@class='product-slider-wrapper']/div/div/button[@class='slick-arrow slick-next']")
	private WebElement eleLooksSlickNext;

	public WebElement getEleLooksSlickNextBtn() {
		return eleLooksSlickNext;
	}

	@FindBy(xpath = "//div[@class='product-slider-wrapper']/div/div/button[@class='slick-arrow slick-prev']")
	private WebElement eleLooksSlickPrev;

	public WebElement getEleLooksSlickPrevBtn() {
		return eleLooksSlickPrev;
	}

	@FindBy(xpath = "//a[contains(text(), 'View all Looks') or contains(text(), 'View All Looks')]")
	private WebElement eleViewallLooks;

	public WebElement getEleViewallLooks() {
		return eleViewallLooks;
	}

	@FindBy(xpath = "//*[contains(text(), 'SHOP PRODUCTS') or contains(text(), 'Shop Products')]")
	private WebElement eleShopProducts;

	public WebElement getEleShopProducts() {
		return eleShopProducts;
	}

	@FindBy(xpath = "//div[contains(@class,'brand-products__wrapper')]/div/div/div/a/img")
	private List<WebElement> eleShopProductCategories;

	public List<WebElement> getEleShopProductCategories() {
		return eleShopProductCategories;
	}

	@FindBy(xpath = "//a[contains(text(), 'View all products')]")
	private WebElement eleViewallProducts;

	public WebElement getEleViewallProducts() {
		return eleViewallProducts;
	}

	@FindBy(xpath = "//*[contains(text(), 'More on abof')]")
	private WebElement eleAbofStoriesTitle;

	public WebElement eleAbofStoriesTitle() {
		return eleAbofStoriesTitle;
	}

	@FindBy(xpath = "//div[@class='story-slider-content']//div/a/div/img")
	private List<WebElement> eleStories;

	public List<WebElement> getEleStories() {
		return eleStories;
	}

	@FindBy(xpath = "//div[@class='story-slider-wrapper']//div[@class='slick-slide slick-active']/a/div/img")
	private WebElement eleStorycards;

	public WebElement getEleActiveStorycards() {
		return eleStorycards;
	}

	@FindBy(xpath = "//div[@class='slick-slide slick-active']//h4[@class='story-card__title']")
	private WebElement eleStorytitle;

	public WebElement getEleActiveStoryTitle() {
		return eleStorytitle;
	}

	@FindBy(xpath = "//*[@class='story-card__date']")
	private List<WebElement> eleStorycardDates;

	public List<WebElement> getEleStorycardDates() {
		return eleStorycardDates;
	}

	@FindBy(xpath = "//div[@class='story-slider-wrapper']/div/div/button[@class='slick-arrow slick-next']")
	private WebElement eleStorySlickNext;

	public WebElement getEleStorySlickNextBtn() {
		return eleStorySlickNext;
	}

	@FindBy(xpath = "//div[@class='story-slider-wrapper']/div/div/button[@class='slick-arrow slick-prev']")
	private WebElement eleStorySlickPrev;

	public WebElement getEleStorySlickPrevBtn() {
		return eleStorySlickPrev;
	}

	@FindBy(xpath = "//div[@class='site-header-wrapper']")
	private WebElement eleHeader;

	public WebElement getEleHeader() {
		return eleHeader;
	}

	@FindBy(xpath = "//div[@class='footer-wrapper-content']")
	private WebElement eleFooter;

	public WebElement getEleFooter() {
		return eleFooter;
	}

	@FindBy(xpath = ".//*[@class='espot espot--MenRow4_Slot9']/div/a/img")
	private WebElement eleMenHeroBanner;

	public WebElement geteleMenHeroBanner() {
		return eleMenHeroBanner;
	}

	@FindBy(xpath = ".//*[@class='espot espot--MenRow4_Slot9']/div/a")
	private WebElement eleMenHeroBannerPosition;

	public WebElement geteleMenHeroBannerPosition() {
		return eleMenHeroBannerPosition;
	}

	@FindBy(xpath = ".//*[@class='espot espot--WomenRow4_Slot9']/div/a/img")
	private WebElement eleWomenHeroBanner;

	public WebElement geteleWomenHeroBanner() {
		return eleMenHeroBanner;
	}

	@FindBy(xpath = ".//*[@class='espot espot--WomenRow4_Slot9']/div/a")
	private WebElement eleWomenHeroBannerPosition;

	public WebElement geteleWomenHeroBannerPosition() {
		return eleWomenHeroBannerPosition;
	}

	@FindBy(xpath = ".//div[@class='espot espot--Men Logos']")
	private List<WebElement> eleTotalBrandsInMenPage;

	public List<WebElement> geteleTotalBrandsInMenPage() {
		return eleTotalBrandsInMenPage;
	}

	@FindBy(xpath = ".//div[@class='espot espot--Men Logos']/div/a")
	private List<WebElement> elegetBrandsNameInMenPage;

	public List<WebElement> getelegetBrandsNameInMenPage() {
		return elegetBrandsNameInMenPage;
	}

	@FindBy(xpath = ".//div[@class='espot espot--Women Logos']/div/a")
	private List<WebElement> eleBrandsNameInwomenPage;

	public List<WebElement> geteleBrandsNameInwomenPage() {
		return eleBrandsNameInwomenPage;
	}

	@FindBy(xpath = ".//div[@class='espot espot--Women Logos']")
	private WebElement eleTotalBrandsInWomenPage;

	public WebElement geteleTotalBrandsInWomenPage() {
		return eleTotalBrandsInWomenPage;
	}

	@FindBy(xpath = ".//*[@class='espot espot--OBMenHeroBanner']/div/a/img")
	private WebElement eleAbofHeroBanner;

	public WebElement geteleAbofHeroBanner() {
		return eleAbofHeroBanner;
	}

	@FindBy(xpath = ".//*[@class='skultTobanner']/video/source")
	private WebElement eleSkultHeroBanner;

	public WebElement geteleSkultHeroBanner() {
		return eleSkultHeroBanner;
	}

	@FindBy(xpath = "//button[contains(text(), 'View All')]")
	private WebElement eleViewallButton;

	public WebElement geteleViewallButton() {
		return eleViewallButton;
	}

	@FindBy(xpath = ".//*[@class='seo-content footer-trending']/div/div/h1")
	private WebElement elefotterText;

	public WebElement getelefotterText() {
		return elefotterText;
	}

	@FindBy(xpath = ".//*[@class='seo-content footer-trending']/div/div/p[1]")
	private WebElement elefotterTextFirstParaGraph;

	public WebElement getelefotterTextFirstParaGraph() {
		return elefotterTextFirstParaGraph;
	}

	@FindBy(xpath = ".//*[@class='category__seocontent--txtunderneath category__seocontent--heading']")
	private WebElement eleStoriesHeader;

	public WebElement geteleStoriesHeader() {
		return eleStoriesHeader;
	}

	@FindBy(xpath = ".//*[@class='slick-track']/div/a/div[2]/h4")
	private List<WebElement> eleStoriesList;

	public List<WebElement> geteleStoriesList() {
		return eleStoriesList;
	}

	@FindBy(xpath = ".//button[@class='slick-arrow slick-next']")
	private WebElement eleToClickNextStory;

	public WebElement geteleToClickNextStory() {
		return eleToClickNextStory;
	}

	@FindBy(xpath = ".//button[@class='slick-arrow slick-prev']")
	private WebElement eleToClickPreviousStory;

	public WebElement geteleToClickPreviousStory() {
		return eleToClickPreviousStory;
	}

	@FindBy(xpath = ".//*[@class='uppercase newFontsize']/following-sibling::h1")
	private WebElement eleStoryTitleInStoryPage;

	public WebElement geteleStoryTitleInStoryPage() {
		return eleStoryTitleInStoryPage;
	}

	@FindBy(xpath = ".//*[@class='shop-action no-margin shop-look-sm uppercase']")
	private WebElement eleShopSimilarProducts;

	public WebElement geteleShopSimilarProducts() {
		return eleShopSimilarProducts;
	}

	@FindBy(xpath = ".//*[@class='storeCardNew']/h3")
	private WebElement eleCollectionName;

	public WebElement geteleCollectionName() {
		return eleCollectionName;
	}

	@FindBy(xpath = ".//*[@class='bottomBTN']/div/a")
	private WebElement eleShopCollectionButton;

	public WebElement geteleShopCollectionButton() {
		return eleShopCollectionButton;
	}

	@FindBy(xpath = ".//*[@class='backtowcs']")
	private WebElement eleBackToWhatsHotButton;

	public WebElement geteleBackToWhatsHotButton() {
		return eleBackToWhatsHotButton;
	}

	@FindBy(xpath = ".//*[@class='category__landing-row']/li/div/div/a")
	private List<WebElement> eleEspotList;

	public List<WebElement> geteleEspotList() {
		return eleEspotList;
	}

	public void verifyPageElementsDisplayed() {
		BaseTest.waitForElement(getEleShopCollection(), "ABOF Landing page has SHOP COLLECTION Displayed",
				"ABOF Landing page should consist of SHOP COLLECTIONS");
		BaseTest.waitForElement(getEleViewallLooks(), "ABOF Landing page has View All Looks Displayed",
				"ABOF Landing page should consist of View All Looks");
		BaseTest.waitForElement(getEleViewallProducts(), "ABOF Landing page has View All Products Displayed",
				"ABOF Landing page should consist of View All Products");
		BaseTest.waitForElement(getEleActiveStorycards(), "ABOF Landing page has Story Cards Displayed",
				"ABOF Landing page should consist of Story Cards");
	}

	public void verifyShopCollectionsDisplayed() {
		BaseTest.waitForElement(getEleShopCollection(), "ABOF Landing page has SHOP COLLECTION Displayed",
				"ABOF Landing page should consist of SHOP COLLECTIONS");
	}

	public void verify4CollectionProductsDisplayed() {
		List<WebElement> productTiles = null;
		label: try {
			productTiles = getEleCollections4Products();
			for (int i = 1; i <= productTiles.size(); i++) {
				if (driver.findElements(By
						.xpath("(//div[@class='brand-collection-right' or @class='brand-collection-right brand-collection-left--level2'])["
								+ i + "]/div"))
						.size() != 4)
					break label;
			}
			NXGReports.addStep("Verify collections has 4 products displayed.", "",
					"Shop collections should consist 4 products.",
					"Verified Shop collections should consist 4 products.", LogAs.PASSED, null);
		} catch (Exception e) {
			NXGReports.addStep("Verify collections has 4 products displayed.", "",
					"Shop collections should consist 4 products.",
					"Verification Failed for Shop collections 4 products due to: " + e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}

	public void verifyClickOnShopCollections() throws Exception {
		try {
			WebElement shopCollections = getEleShopCollection();
			shopCollections.click();
			waitTillPageLoad();
			Sassert.assertTrue(PlpPage.getEleProductsImages().size() > 0, "Products in grid should be displayed");
			NXGReports.addStep("Verify Shop collections header link to PLP page.", "ABOF Landing page",
					"Shop  Collections header should navigte to PLP page",
					"Click on Shop Collections header is successful.", LogAs.PASSED, null);
		} catch (Exception e) {
			NXGReports.addStep("Verify Shop collections header link to PLP page.", "ABOF Landing page",
					"Shop  Collections header should navigte to PLP page",
					"Failed to click Shop Collection due to : " + e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}

	public void clickOnViewAllProducts() throws Exception {
		WebElement viewAllProducts = getEleViewallProducts();
		try {
			mouseOver(driver, viewAllProducts);
			viewAllProducts.click();
			waitTillPageLoad();
			Sassert.assertTrue(PlpPage.getEleProductsImages().size() > 0, "Products in grid should be displayed");
			NXGReports.addStep("Verify Click on View All Products button.", "ABOF Landing page",
					"Clicking on View All products button should navigate the user to abof listing page",
					"Verified Clicking on View All products button should navigate the user to abof listing page",
					LogAs.PASSED, null);
		} catch (Exception e) {
			NXGReports.addStep("Verify Click on View All Products button.", "ABOF Landing page",
					"Clicking on View All products button should navigate the user to abof listing page",
					"Clicking on View All products failed due to: " + e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}

	public void verifyShopLooksDisplayed() {
		try {
			WebElement shopLooks = getEleShopLooks();
			if (!shopLooks.isDisplayed()) {
				throw new Exception();
			}
			NXGReports.addStep("Verify Shop looks header displayed.", "",
					"Landing page should consist Shop looks header.", "Verified Shop looks header is displayed",
					LogAs.PASSED, null);
		} catch (Exception e) {
			NXGReports.addStep("Verify Shop looks header displayed.", "",
					"Landing page should consist Shop looks header.",
					"Verification Failed for Shop looks header due to: " + e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}

	public void verifyLooksWidgetBlock() {
		try {

			BaseLib.elementStatus(getEleLooksWidget(), "Looks Widget", "displayed");
			if (getEleLooksProducts().size() > 3) {
				BaseLib.elementStatus(getEleLooksSlickNextBtn(), "Looks Next Button", "displayed");
				getEleLooksSlickNextBtn().click();
				waitTillPageLoad();
				BaseLib.elementStatus(getEleLooksSlickPrevBtn(), "Looks Prev Button", "displayed");
				Sassert.assertTrue(getEleLooksSlickPrevBtn().isDisplayed(), "Looks Prev Button should be displayed");
			}
			NXGReports.addStep("Verify Shop looks widget displayed.", "",
					"Landing page should consist Shop looks widget.", "Verified Shop looks widget is displayed",
					LogAs.PASSED, null);
		} catch (Exception e) {
			NXGReports.addStep("Verify Shop looks widget displayed.", "",
					"Landing page should consist Shop looks widget.",
					"Verification Failed for Shop looks widget due to: " + e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}

	public void clickOnLooksAtPosition(int position) {
		try {
			if (position > 3)
				position = 2;
			driver.findElements(By
					.xpath("//div[@class='product-slider-wrapper']/div/div/div/div/div[@class='slick-slide slick-active']"))
					.get(position).click();
			waitTillPageLoad();
			Sassert.assertTrue(PdpPage.getEleAddToBagbtn().isDisplayed(),
					"Look page should be loaded with Add To Bag button");
			NXGReports.addStep("Verify Shop looks product clicked.", "", "Looks widget product should be clicked.",
					"Verified Shop looks widget product is clicked", LogAs.PASSED, null);

		} catch (Exception e) {
			NXGReports.addStep("Verify Shop looks product clicked.", "", "Looks widget product should be clicked.",
					"Verification Failed for Shop looks product click due to: " + e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}

	public void verifyShopProductsDisplayed() {
		try {
			BaseLib.elementStatus(getEleShopProducts(), "Shop Products", "displayed");
		} catch (Exception e) {
			NXGReports.addStep("Verify Shop products header displayed.", "",
					"Landing page should consist Shop products header.",
					"Verification Failed for Shop products header due to: " + e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}

	public void verifyClickonProductCategory() {
		try {
			getEleShopProductCategories().get(0).click();
			Sassert.assertTrue(PlpPage.getEleProductsImages().size() > 0, "Products in grid should be displayed");
			NXGReports.addStep("Verify Shop products category click.", "", "Product Category should be clicked.",
					"Successfully Clicked on Product Category.", LogAs.PASSED, null);
		} catch (Exception e) {
			NXGReports.addStep("Verify Shop products category click.", "", "Product Category should be clicked.",
					"Verification Failed due to: " + e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}

	public void verifyAbofStoriesDisplayed() {
		Date date1, date2;
		boolean flag = false;
		List<String> dates = new ArrayList<String>();
		try {
			BaseLib.elementStatus(eleAbofStoriesTitle(), "Abof Stories Title", "displayed");
			List<WebElement> list = getEleStorycardDates();
			for (WebElement date : list) {
				dates.add(date.getText());
			}
			for (int i = 0; i < dates.size() - 2; i++) {
				logger.info("Iteration: " + (i + 1));
				date1 = new Date(dates.get(i));
				logger.info("date = " + date1);
				date2 = new Date(dates.get(i + 1));
				if (date1.compareTo(date2) >= 0) {
					flag = true;
				} else {
					logger.info("Date is not in descending order");
					flag = false;
					break;
				}
			}
			if (flag == true) {
				NXGReports.addStep("Verify Abof stories are order by date.", "",
						"Landing page should consist Abof Stories in sorted by date.", "Verified as Dates are in order",
						LogAs.PASSED, null);
			} else {
				NXGReports.addStep("Verify Abof stories are order by date.", "",
						"Landing page should consist Abof Stories in sorted by date.",
						"Verification Failed for due to: Dates are NOT in order", LogAs.FAILED,
						new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			if (getEleStories().size() > 3) {
				BaseLib.elementStatus(getEleStorySlickNextBtn(), "Stories Next Button", "displayed");
				getEleStorySlickNextBtn().click();
				WebDriverWait wait = new WebDriverWait(driver, 30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"//div[@class='story-slider-wrapper']/div/div/button[@class='slick-arrow slick-prev']")));
				waitTillPageLoad();
				BaseLib.elementStatus(getEleStorySlickPrevBtn(), "Stories Prev Button", "displayed");
				Sassert.assertTrue(getEleStorySlickPrevBtn().isDisplayed(), "Stories Prev Button should be active");
			}
		} catch (Exception e) {
			NXGReports.addStep("Verify Abof stories section displayed.", "",
					"Landing page should consist Abof Stories in sorted by date.",
					"Verification Failed for due to: " + e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}

	public void verifyClickOnStoryCard() {
		try {
			String title = getEleActiveStoryTitle().getText();
			getEleActiveStorycards().click();
			waitTillPageLoad();
			Sassert.assertEquals(title.toLowerCase(),
					driver.findElement(By.xpath("//div[@class='title-hotnot']//h1")).getText().toLowerCase());
			NXGReports.addStep("Verify click on Abof Story cards.", "",
					"Click on Abof Story card should navigate to story page.",
					"Click on Abof Story card is navigate to story page.", LogAs.PASSED, null);
		} catch (Exception e) {
			NXGReports.addStep("Verify click on Abof Story cards.", "",
					"Click on Abof Story card should navigate to story page.", "Failed due to : " + e.getMessage(),
					LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}

	public void verifyHeaderAndFooterDisplayed() {
		try {
			BaseLib.elementStatus(getEleHeader(), "ABOF Header", "displayed");
			BaseLib.elementStatus(getEleFooter(), "ABOF Footer", "displayed");
			NXGReports.addStep("Verify Abof header and footer displayed.", "",
					"Abof header and footer should be displayed.", "Abof header and footer are displayed.",
					LogAs.PASSED, null);
		} catch (Exception e) {
			NXGReports.addStep("Verify Abof header and footer displayed.", "",
					"Abof header and footer should be displayed.", "Failed due to : " + e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}

	/*
	 * This method verifies men landing page test cases ,Author : Shreekant R S
	 *  
	 **/

	public void verifyMenLandingPage(String expectedPageTitle) throws Exception {

		String expectedLink = prop.getProperty("expectedMenLandingPageLink").trim();

		String actualLink = driver.getCurrentUrl();

		Sassert.assertEquals(actualLink, expectedLink, "On clicking Men Tab, It is not redirecting men page");

		if (actualLink.equals(expectedLink)) {

			NXGReports.addStep("Verify Men Page URL", "It should be : " + expectedLink, "It is :" + actualLink,
					LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

		}

		else {

			NXGReports.addStep("Verify Men Page URL", "It should be : " + expectedLink,
					"It is not matching :" + actualLink, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		boolean pageTitleValue = verifyPageTitle(expectedPageTitle);

		Sassert.assertEquals(actualPageTitle, expectedPageTitle, "Men page title is not as expected");

		if (pageTitleValue) {
			NXGReports.addStep("Verify Men Page Title", "It should be : " + expectedPageTitle,
					"It is :" + actualPageTitle, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		else {

			NXGReports.addStep("Verify Men Page Title", "It should be : " + expectedPageTitle,
					"It is not matching :" + actualPageTitle, LogAs.PASSED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		boolean imageDispalyed = CheckImage(geteleMenHeroBanner());

		if (imageDispalyed) {
			Sassert.assertTrue(imageDispalyed, "It is not dispalying under men mega menu ");
			NXGReports.addStep("Verify men landing page hero baner", "Image should dispaly", "Image is dispalying ",
					LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		else {
			NXGReports.addStep("Verify men landing page hero baner", "Image should dispaly", "Image is not dispalying ",
					LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		String actualImageName = utl.fetchImageName(geteleMenHeroBanner());

		if (utl.validateImage(actualImageName)) {

			Sassert.assertTrue(true, "Image name is ending with .JPG " + actualImageName);
			NXGReports.addStep("Verfiy Image Name Ending with .jpg", "Image Name Should End With .jpg",
					"Image Name Is Ending With .jpg " + actualImageName, LogAs.PASSED, null);
		}

		else {

			Sassert.assertTrue(false, "Image name is not ending with .JPG " + actualImageName);

			String actualImageExtName = utl.verifyImageExtension(actualImageName);

			logger.info("Actual Image ExtensionName Is: " + actualImageExtName);

			NXGReports.addStep("Verfiy Image Name Ending with .jpg", "Image Name Should End With .jpg",
					"Image Name Is Not Ending With .jpg , It is ending with ." + actualImageExtName, LogAs.FAILED,
					null);
		}

		String logoFileName;
		logoFileName = geteleMenHeroBannerPosition().getAttribute("data-banner-name");
		logoFileName = utl.splitStringByPosition(logoFileName, '-', 1).trim();
		
		logger.info("Hero Baner Name is" + logoFileName);

		waitAndClick(geteleMenHeroBannerPosition(), "Hero Baner", 256, 10);

		waitTillPageLoad();

		PlpPage plp = new PlpPage(driver);

		String actualText = plp.verfiySearchHeaderInPLP();

		String count = driver.findElement(By.xpath(".//*[@class='product-grid-header__count']")).getText();
		count = count.replace("(", "");
		count = count.replace(")", "");
		logger.info("Total Count is : " + count);

		boolean countValue = Integer.parseInt(count) > 0;

		Sassert.assertTrue(countValue,
				"On cliking " + logoFileName + " , In PLP page , product count is not greater than zero ");

		if (countValue) {
			NXGReports.addStep("Verify Total Product Count", "Product Count should not be zero",
					"Product count is : " + count, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		} else {
			NXGReports.addStep("Verify Total Product Count", "Product Count should not be zero",
					"Product count is : " + count, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		waitAndClick(geteleViewallButton(), "View All", 256, 10);

		waitTillPageLoad();

		plp = new PlpPage(driver);

		actualText = plp.verfiySearchHeaderInPLP();

		Sassert.assertTrue(actualText.contains("Trending Brands"),
				"Searched text is not matching with user entered text");

		if (actualText.contains("Trending Brands")) {
			NXGReports.addStep("Verify Search Header ", "It should contain : Trending Brands ",
					" Search header in UI is : " + actualText, LogAs.PASSED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		} else {
			NXGReports.addStep("Verify Search Header ", "It should contain : Trending Brands  ",
					"Search header is not containing Trending Brands ." + " Search header in UI is : " + actualText,
					LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		String pagetitle = driver.getTitle();
		Sassert.assertTrue(pagetitle.contains("Trending Brands"),
				"On clicking view all button in Men Landing Page,it is not redirecting to respective brand page");

		if (pagetitle.contains("Trending Brands")) {
			NXGReports.addStep("Verify Page Title ", "It should contian : Trending Brands ",
					"It contians : Trending Brands" + " Page Title in UI is  : " + pagetitle, LogAs.PASSED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		} else {
			NXGReports.addStep("Verify Page Title ", "It should contian : Trending Brands ",
					"It does not contain : Trending Brands" + " Page Title in UI is  : " + pagetitle + pagetitle,
					LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		driver.navigate().back();
		waitTillPageLoad();

		mousescrollTillEle(geteleStoriesHeader());

		String actualFooterText = getelefotterText().getText();

		Sassert.assertEquals(actualFooterText, "The Latest Fashion Trends For Men",
				"Footer header in men landing page is not as expected");

		if (actualFooterText.equals("The Latest Fashion Trends For Men")) {
			NXGReports.addStep("Verify Footer Header of Men Landing Page",
					"It should be : The Latest Fashion Trends For Men ", "Footer text in UI is : " + actualFooterText,
					LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		} else {
			NXGReports.addStep("Verify Footer Header of Men Landing Page",
					"It should be : The Latest Fashion Trends For Men ", "Footer text in UI is : " + actualFooterText,
					LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		String actualFooterFirstParaGraphText;

		try {
			actualFooterFirstParaGraphText = getelefotterTextFirstParaGraph().getText();
			logger.info("Text is: " + actualFooterFirstParaGraphText);

			if (!(StringUtils.isEmpty(actualFooterFirstParaGraphText))) {
				logger.info("String is not empty");

				Sassert.assertTrue(true);
				NXGReports.addStep("Verify Footer Text of Men Landing Page", "It should not be empty",
						"It is not empty", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

			}

			else {
				logger.info("String is empty");

				Sassert.assertTrue(false, "Men Landing Page Footer Is Not having any Text");
				NXGReports.addStep("Verify Footer Text of Men Landing Page", "It should not be empty", "It is empty",
						LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

			}
		} catch (Exception e) {

			Sassert.assertTrue(false, "Not able to fetch men landing page footer text");

		}

	}

	/*
	 * This method verify brands in Men Landning Page
	 * 
	 * Author : Shreekant R S
	 */

	public void verifyBrandsInMenLandingPage() throws Exception {

		String logoFileName;

		int size = getelegetBrandsNameInMenPage().size();

		Sassert.assertEquals(size, 6, "Total brnads count in men page is not as expected");

		if (size == 6) {

			NXGReports.addStep("Verify Total Brands", "UI should show 6 brands", "UI is showing " + size + " brands",
					LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		else {
			NXGReports.addStep("Verify Total Brands", "UI should show 6 brands", "UI is showing " + size + " brands",
					LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

		}

		List<WebElement> eleTotalBrandsInMenPage = driver
				.findElements(By.xpath(".//div[@class='espot espot--Men Logos']"));

		for (int i = 1; i <= size; i++) {

			logoFileName = elegetBrandsNameInMenPage.get(i - 1).getAttribute("data-banner-name");
			logoFileName = utl.splitStringByPosition(logoFileName, '-', 2).trim();
			logger.info("Brand name is " + logoFileName);

			logger.info("Updated Size is: " + geteleTotalBrandsInMenPage().size());

			waitAndClick(elegetBrandsNameInMenPage.get(i - 1), logoFileName + " brand ", 256, 10);
			waitTillPageLoad();

			String pagetitle = driver.getTitle();

			if (logoFileName.equalsIgnoreCase("abof")) {

				Sassert.assertEquals(pagetitle, prop.getProperty("abofLandingPageTitle"), "On clicking " + logoFileName
						+ "logo in Men Landing Page,it is not redirecting to respective brand page");
				if (pagetitle.equals(prop.getProperty("abofLandingPageTitle"))
						|| pagetitle.equals(prop.getProperty("skultLandingPageTitle"))) {
					NXGReports.addStep("Verify Page Title of " + logoFileName + " brnad",
							"It should be : " + prop.getProperty("abofLandingPageTitle"), "It is :" + pagetitle,
							LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				} else {
					NXGReports.addStep("Verify Page Title of " + logoFileName + " brnad",
							"It should be : " + prop.getProperty("abofLandingPageTitle"), "It is :" + pagetitle,
							LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

				boolean imageDispalyed = CheckImage(getEleAbofBannerImag());

				if (imageDispalyed) {
					Sassert.assertTrue(imageDispalyed, "It is not dispalying abof hero banner image");
					NXGReports.addStep("Verify abof landing page hero baner", "Image should dispaly",
							"Image is dispalying ", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

				else {
					NXGReports.addStep("Verify abof landing page hero baner", "Image should dispaly",
							"Image is not dispalying ", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

			}

			else if (logoFileName.equalsIgnoreCase("skult")) {
				Sassert.assertEquals(pagetitle, prop.getProperty("skultLandingPageTitle"), "On clicking " + logoFileName
						+ "logo in Men Landing Page,it is not redirecting to respective brand page");

				if (pagetitle.equals(prop.getProperty("skultLandingPageTitle"))) {
					NXGReports.addStep("Verify Page Title of " + logoFileName + " brnad",
							"It should be : " + logoFileName.equalsIgnoreCase("skult"), "It is :" + pagetitle,
							LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				} else {
					NXGReports.addStep("Verify Page Title of " + logoFileName + " brnad",
							"It should be : " + logoFileName.equalsIgnoreCase("skult"), "It is :" + pagetitle,
							LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

				

				waitTillPageLoad();
				boolean videoEnabled = geteleSkultHeroBanner().isEnabled();
				logger.info("Video enabled" + videoEnabled);

				Sassert.assertTrue(videoEnabled, "It is not dispalying skult hero banner video");
				if (videoEnabled) {

					NXGReports.addStep("Verify skult landing page hero baner video", "Video should dispaly",
							"Video is dispalying ", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

				else {
					NXGReports.addStep("Verify abof landing page hero baner", "Video should dispaly",
							"Video is not dispalying ", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

			}

			else {

				PlpPage plp = new PlpPage(driver);

				String actualText = plp.verfiySearchHeaderInPLP();
				String convertedActual = actualText.toLowerCase();

		

				Sassert.assertTrue(convertedActual.contains(logoFileName.toLowerCase()),
						"Searched text is not matching with user entered text");

				if (convertedActual.contains(logoFileName.toLowerCase())) {
					NXGReports.addStep("Verify Search Header ", "It should contain : " + logoFileName.toLowerCase(),
							"Search header contains " + logoFileName + " Search header in UI is : " + convertedActual,
							LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				} else {
					NXGReports.addStep("Verify Search Header ",
							"It should contain : " + logoFileName.toLowerCase(), "Search header is not containing"
									+ logoFileName + " Search header in UI is : " + convertedActual,
							LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

				Sassert.assertTrue(pagetitle.toLowerCase().contains(logoFileName.toLowerCase()), "On clicking "
						+ logoFileName + " logo in Men Landing Page,it is not redirecting to respective brand page");

				if (pagetitle.toLowerCase().contains(logoFileName.toLowerCase())) {
					NXGReports.addStep("Verify Page Title ", "It should contian : " + logoFileName.toLowerCase(),
							"It contians :" + logoFileName.toLowerCase() + " Page Title is : " + pagetitle,
							LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				} else {
					NXGReports.addStep("Verify Page Title ", "It should contian : " + logoFileName.toLowerCase(),
							"It does not contian:" + logoFileName.toLowerCase() + " Page Title is : " + pagetitle,
							LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

				actualText = plp.verfiySearchHeaderInPLP();

				String count = driver.findElement(By.xpath(".//*[@class='product-grid-header__count']")).getText();
				count = count.replace("(", "");
				count = count.replace(")", "");

				logger.info("Total Count is : " + count);

				boolean countValue = Integer.parseInt(count) > 0;

				Sassert.assertTrue(countValue,
						"On cliking " + logoFileName + " , In PLP page , product count is not greater than zero ");

				if (countValue) {
					NXGReports.addStep("Verify Total Product Count", "Product Count should not be zero",
							"Product count is : " + count, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				} else {
					NXGReports.addStep("Verify Total Product Count", "Product Count should not be zero",
							"Product count is : " + count, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

			}

			driver.navigate().back();
			waitTillPageLoad();

		}

	}

	/*
	 * This method verify all brands in Women Landning Page
	 * 
	 * Author : Shreekant R S
	 */

	public void verifyBrandsInWomenLandingPage() throws Exception {

		String logoFileName;

		int size = geteleBrandsNameInwomenPage().size();

		Sassert.assertEquals(size, 6, "Total brnads count in women page is not as expected");

		if (size == 6) {

			NXGReports.addStep("Verify Total Brands", "UI should show 6 brands", "UI is showing " + size + " brands",
					LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		else {
			NXGReports.addStep("Verify Total Brands", "UI should show 6 brands", "UI is showing " + size + " brands",
					LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

		}

		List<WebElement> eleTotalBrandsInWomenPage = driver
				.findElements(By.xpath(".//div[@class='espot espot--Women Logos']"));

		for (int i = 1; i <= size; i++) {

			logoFileName = eleBrandsNameInwomenPage.get(i - 1).getAttribute("data-banner-name");
			logoFileName = utl.splitStringByPosition(logoFileName, '-', 2);

			logger.info("Brand name is " + logoFileName);

			waitAndClick(eleBrandsNameInwomenPage.get(i - 1), logoFileName + " brand ", 256, 10);
			waitTillPageLoad();

			String pagetitle = driver.getTitle();

			Sassert.assertEquals(pagetitle, prop.getProperty("womenAbofLandingPageTitle"), "On clicking " + logoFileName
					+ "logo in women Landing Page,it is not redirecting to respective brand page");

			if (logoFileName.equalsIgnoreCase("abof")) {

				if (pagetitle.equals(prop.getProperty("womenAbofLandingPageTitle"))
						|| pagetitle.equals(prop.getProperty("skultLandingPageTitle"))) {
					NXGReports.addStep("Verify Page Title of " + logoFileName + " brnad",
							"It should be : " + prop.getProperty("abofLandingPageTitle"), "It is :" + pagetitle,
							LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				} else {
					NXGReports.addStep("Verify Page Title of " + logoFileName + " brnad",
							"It should be : " + prop.getProperty("abofLandingPageTitle"), "It is :" + pagetitle,
							LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

				boolean imageDispalyed = CheckImage(getEleAbofBannerImag());
				Sassert.assertTrue(imageDispalyed, "It is not dispalying abof hero banner image");
				if (imageDispalyed) {

					NXGReports.addStep("Verify abof landing page hero baner", "Image should dispaly",
							"Image is dispalying ", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

				else {
					NXGReports.addStep("Verify abof landing page hero baner", "Image should dispaly",
							"Image is not dispalying ", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

			}

			else {

				PlpPage plp = new PlpPage(driver);

				String actualText = plp.verfiySearchHeaderInPLP();
				String convertedActual = actualText.toLowerCase();

				Sassert.assertTrue(convertedActual.contains(logoFileName.toLowerCase()),
						"Searched text is not matching with user entered text");

				if (convertedActual.contains(logoFileName.toLowerCase())) {
					NXGReports.addStep("Verify Search Header ", "It should contain : " + logoFileName.toLowerCase(),
							"Search header contains " + logoFileName + " Search header in UI is : " + convertedActual,
							LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				} else {
					NXGReports.addStep("Verify Search Header ",
							"It should contain : " + logoFileName.toLowerCase(), "Search header is not containing"
									+ logoFileName + " Search header in UI is : " + convertedActual,
							LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

				Sassert.assertTrue(pagetitle.toLowerCase().contains(logoFileName.toLowerCase()), "On clicking "
						+ logoFileName + " logo in women Landing Page,it is not redirecting to respective brand page");

				if (pagetitle.toLowerCase().contains(logoFileName.toLowerCase())) {
					NXGReports.addStep("Verify Page Title ", "It should contian : " + logoFileName.toLowerCase(),
							"It contians :" + logoFileName.toLowerCase() + " Page Title is : " + pagetitle,
							LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				} else {
					NXGReports.addStep("Verify Page Title ", "It should contian : " + logoFileName.toLowerCase(),
							"It does not contian:" + logoFileName.toLowerCase() + " Page Title is : " + pagetitle,
							LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

			}

			driver.navigate().back();
			waitTillPageLoad();

		}

	}

	/*
	 * This method verifies women landing page Author : Shreekant R S
	 **/

	public void verifyWomenLandingPage(String expectedPageTitle) throws Exception {

		String expectedWomenLink = prop.getProperty("expectedWomenLandingPageLink").trim();

		String actualLink = driver.getCurrentUrl();

		Sassert.assertEquals(actualLink, expectedWomenLink, "Women page title is not as expected");

		if (actualLink.equals(expectedWomenLink)) {

			NXGReports.addStep("Verify Women Page URL", "It should be : " + expectedWomenLink, "It is :" + actualLink,
					LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

		}

		else {

			NXGReports.addStep("Verify Women Page URL", "It should be : " + expectedWomenLink,
					"It is not matching :" + actualLink, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		/* verify women page title */

		boolean pageTitleValue = verifyPageTitle(expectedPageTitle);

		Sassert.assertEquals(actualPageTitle, expectedPageTitle, "Women page title is not as expected");

		if (pageTitleValue) {
			NXGReports.addStep("Verify Women Page Title", "It should be : " + expectedPageTitle,
					"It is :" + actualPageTitle, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		else {

			NXGReports.addStep("Verify Women Page Title", "It should be : " + expectedPageTitle,
					"It is not matching :" + actualPageTitle, LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		WebElement womenBanerEle = driver
				.findElement(By.xpath(".//*[@class='espot espot--WomenRow4_Slot9']/div/a/img"));
		boolean imageDispalyed = CheckImage(womenBanerEle);

		if (imageDispalyed) {
			Sassert.assertTrue(imageDispalyed, "It is not dispalying under women mega menu ");
			NXGReports.addStep("Verify women landing page hero baner", "Image should dispaly", "Image is dispalying ",
					LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		else {
			NXGReports.addStep("Verify women landing page hero baner", "Image should dispaly",
					"Image is not dispalying ", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		String actualImageName = utl.fetchImageName(womenBanerEle);

		if (utl.validateImage(actualImageName)) {

			Sassert.assertTrue(true, "Image name is ending with .JPG " + actualImageName);
			NXGReports.addStep("Verfiy Image Name Ending with .jpg", "Image Name Should End With .jpg",
					"Image Name Is Ending With .jpg " + actualImageName, LogAs.PASSED, null);
		}

		else {

			Sassert.assertTrue(false, "Image name is not ending with .JPG " + actualImageName);

			String actualImageExtName = utl.verifyImageExtension(actualImageName);

			logger.info("Actual Image ExtensionName Is: " + actualImageExtName);

			NXGReports.addStep("Verfiy Image Name Ending with .jpg", "Image Name Should End With .jpg",
					"Image Name Is Not Ending With .jpg , It is ending with ." + actualImageExtName, LogAs.FAILED,
					null);
		}

		waitAndClick(geteleViewallButton(), "View All", 256, 10);

		waitTillPageLoad();
		PlpPage plp = new PlpPage(driver);

		String actualText = plp.verfiySearchHeaderInPLP();

		Sassert.assertTrue(actualText.contains("Trending Brands"),
				"Searched text is not matching with user entered text");

		if (actualText.contains("Trending Brands")) {
			NXGReports.addStep("Verify Search Header ", "It should contain : Trending Brands ",
					" Search header in UI is : " + actualText, LogAs.PASSED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		} else {
			NXGReports.addStep("Verify Search Header ", "It should contain : Trending Brands  ",
					"Search header is not containing Trending Brands ." + " Search header in UI is : " + actualText,
					LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		String pagetitle = driver.getTitle();
		Sassert.assertTrue(pagetitle.contains("Trending Brands"),
				"On clicking view all button in Men Landing Page,it is not redirecting to respective brand page");

		if (pagetitle.contains("Trending Brands")) {
			NXGReports.addStep("Verify Page Title ", "It should contian : Trending Brands ",
					"It contians : Trending Brands" + " Page Title in UI is  : " + pagetitle, LogAs.PASSED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		} else {
			NXGReports.addStep("Verify Page Title ", "It should contian : Trending Brands ",
					"It does not contain : Trending Brands" + " Page Title in UI is  : " + pagetitle + pagetitle,
					LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		driver.navigate().back();
		waitTillPageLoad();

		mousescrollTillEle(geteleStoriesHeader());

		String actualFooterText = getelefotterText().getText();

		Sassert.assertEquals(actualFooterText, "The Latest Fashion Trends For Men",
				"Footer header in womne landing page is not as expected");

		if (actualFooterText.equals("The Latest Fashion Trends For Men")) {
			NXGReports.addStep("Verify Footer Header of Women Landing Page",
					"It should be : The Latest Fashion Trends For Men ", "Footer text in UI is : " + actualFooterText,
					LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		} else {
			NXGReports.addStep("Verify Footer Header of Women Landing Page",
					"It should be : The Latest Fashion Trends For Men ", "Footer text in UI is : " + actualFooterText,
					LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		String actualFooterFirstParaGraphText;

		try {
			actualFooterFirstParaGraphText = getelefotterTextFirstParaGraph().getText();

			logger.info("Text is: " + actualFooterFirstParaGraphText);

			if (!(StringUtils.isEmpty(actualFooterFirstParaGraphText))) {

				logger.info("String is not empty");

				Sassert.assertTrue(true);
				NXGReports.addStep("Verify Footer Text of Women Landing Page", "It should not be empty",
						"It is not empty", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

			}

			else {
				logger.info("String is empty");

				Sassert.assertTrue(false, "Men Landing Page Footer Is Not having any Text");
				NXGReports.addStep("Verify Footer Text of Women Landing Page", "It should not be empty", "It is empty",
						LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

			}
		} catch (Exception e) {

			Sassert.assertTrue(false, "Not able to fetch Women Landing Page Footer Text");

		}

	}

	/*
	 * This method verify stories in Women Landning Page
	 * 
	 * Author : Shreekant R S
	 */

	public void verifyEspotsInMenLandingPage() throws Exception {

		waitTillPageLoad();
		String logoFileName;

		int totalEspots = geteleEspotList().size();

		boolean value = totalEspots > 0;

		logger.info("value is : " + value);

		Sassert.assertTrue(value, "Total Espots count in women page is less than 1");

	

		if (totalEspots > 0) {

			NXGReports.addStep("Verify Total Espots", "UI should show atleast one E spot",
					"UI is showing " + totalEspots + " Espots", LogAs.PASSED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		else {
			NXGReports.addStep("Verify Total Espots", "UI should show atleast one E spot",
					"UI is showing " + totalEspots + " Espots", LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

		}

		String storyNameName;

		logger.info("Size is : " + totalEspots);

		for (int i = 1; i < totalEspots; i++) {

			waitTillPageLoad();

			Thread.sleep(4000);

			logoFileName = geteleEspotList().get(i - 1).getAttribute("data-banner-name");
			logoFileName = utl.splitStringByPosition(logoFileName, '-', 1).trim();

			logger.info("Espot Name is" + logoFileName);

			waitAndClick(geteleEspotList().get(i - 1), logoFileName + " E-spot ", 256, 10);
			waitTillPageLoad();

			String shopAlProductsExpURL;
			String expSerchTag;

			String shopAlProductsActURL = driver.getCurrentUrl();

			PlpPage plp = new PlpPage(driver);

			String actualText = plp.verfiySearchHeaderInPLP();

			String count = driver.findElement(By.xpath(".//*[@class='product-grid-header__count']")).getText();
			count = count.replace("(", "");
			count = count.replace(")", "");

			boolean countValue = Integer.parseInt(count) > 0;

			Sassert.assertTrue(countValue,
					"On cliking " + logoFileName + " , In PLP page , product count is not greater than zero ");

			if (countValue) {
				NXGReports.addStep("Verify Total Product Count", "Product Count should not be zero",
						"Product count is : " + count, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			} else {
				NXGReports.addStep("Verify Total Product Count", "Product Count should not be zero",
						"Product count is : " + count, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}

			driver.navigate().back();
			waitTillPageLoad();
		}

	}

	/*
	 * This method verify stories in Landning Page. This verifies stories
	 * irrespective of Men and Women Landing Page
	 * 
	 * Author : Shreekant R S
	 */

	public void verifyStoriesInLandingPage() throws Exception {

		waitTillPageLoad();

		String storyNameName;

		int totalStories = geteleStoriesList().size();

		logger.info("Size is : " + totalStories);

		for (int i = 0; i < totalStories; i++) {

			if ((i >= 3)) {

				waitTillPageLoad();

				boolean value = geteleStoriesList().get(i).isDisplayed();

				Thread.sleep(4000);
				int j = 1;
				while (!(geteleStoriesList().get(i).isDisplayed())) {

					logger.info("Clicking on next story button");

					if (j == 1) {
						waitAndClick(geteleToClickNextStory(), "Stories Next Button", 256, 10);
						waitTillPageLoad();
						j++;
					} else {
						geteleToClickNextStory().click();
						waitTillPageLoad();
					}

					Thread.sleep(4000);
					boolean value3 = geteleStoriesList().get(i).isDisplayed();

				}
			}

			boolean value1 = geteleStoriesList().get(i).isDisplayed();

			waitTillPageLoad();
			Thread.sleep(4000);
			storyNameName = geteleStoriesList().get(i).getText();

			logger.info("Story Title is " + storyNameName);

			waitAndClick(geteleStoriesList().get(i), "Story " + storyNameName, 256, 20);
			waitTillPageLoad();

			String pagetitle = driver.getTitle();

			logger.info("Page Title is " + pagetitle);

			String pageValue = pagetitle.toLowerCase();
			logger.info("Page Title lower case " + pageValue);

			String storyValue = storyNameName.toLowerCase();
			logger.info("story Name lower case " + storyValue);

			boolean value = pagetitle.toLowerCase().contains(storyNameName.toLowerCase().trim());

			String storyName = geteleStoryTitleInStoryPage().getText();

			Sassert.assertTrue(storyName.toUpperCase().equals(storyNameName),
					"Stroy Name is not as expected . Story name in Landing Page and Story Page are different"
							+ "Story Name In Landing Page : " + storyNameName + "Story Name In Story Page : "
							+ storyName);

			if (storyName.toUpperCase().equals(storyNameName)) {

				NXGReports.addStep("Verify Story Name", "Story Name In Landing Page : " + storyNameName,
						"Story Name In Story Page : " + storyName, LogAs.PASSED,
						new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

			}

			else {

				NXGReports.addStep("Verify Story Name", "Story Name In Landing Page : " + storyNameName,
						"Story Name In Story Page : " + storyName, LogAs.FAILED,
						new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

			}

			String shopAlProductsExpURL;
			String expSerchTag;

			if (utl.fetchTextFromWebElement(geteleCollectionName(), "COLLECTION").equals("COLLECTION")) {

				shopAlProductsExpURL = geteleShopCollectionButton().getAttribute("href");

				expSerchTag = utl.splitStringByPosition(shopAlProductsExpURL, '/', 1);

				logger.info("expSerchTag tag is " + expSerchTag);

				waitAndClick(geteleShopCollectionButton(), " SHOP COLLECTION", 256, 5);
			}

			else {

				shopAlProductsExpURL = geteleShopSimilarProducts().getAttribute("href");

				expSerchTag = utl.splitStringByPosition(shopAlProductsExpURL, '/', 1);

				logger.info("expSerchTag tag is " + expSerchTag);

				waitAndClick(geteleShopSimilarProducts(), " SHOP SIMILAR PRODUCTS", 256, 5);
			}

			String shopAlProductsActURL = driver.getCurrentUrl();

			if (!(expSerchTag.contains("%"))) {

				Sassert.assertTrue(shopAlProductsExpURL.equals(shopAlProductsActURL),
						"On clicking shop similar products button , it is redirecting wrong page" + "Expected URL: "
								+ shopAlProductsExpURL + "Actual URL: " + shopAlProductsActURL);

				if (shopAlProductsExpURL.equals(shopAlProductsActURL)) {

					NXGReports.addStep("Verify Serach URL", "Expected URL: " + shopAlProductsExpURL,
							"Actual URL: " + shopAlProductsActURL, LogAs.PASSED,
							new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

				}

				else {

					NXGReports.addStep("Verify Serach URL", "Expected URL: " + shopAlProductsExpURL,
							"Actual URL: " + shopAlProductsActURL, LogAs.FAILED,
							new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

				}

				PlpPage plp = new PlpPage(driver);

				String actualText = plp.verfiySearchHeaderInPLP();

				Sassert.assertTrue(expSerchTag.equals(actualText),
						"Searched text is not matching with user entered text");

				if (expSerchTag.equals(actualText)) {
					NXGReports.addStep("Verify Search Header ", "It should be : " + expSerchTag,
							" Search header in UI is : " + actualText, LogAs.PASSED,
							new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				} else {
					NXGReports.addStep("Verify Search Header ", "It should be : " + expSerchTag,
							" Search header in UI is : " + actualText, LogAs.FAILED,
							new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

			}

			PlpPage plp = new PlpPage(driver);
			String count = driver.findElement(By.xpath(".//*[@class='product-grid-header__count']")).getText();
			count = count.replace("(", "");
			count = count.replace(")", "");

			String actualText = plp.verfiySearchHeaderInPLP();

			boolean countValue = Integer.parseInt(count) > 0;

			Sassert.assertTrue(countValue,
					"On cliking View All Button , In PLP page , product count is not greater than zero ");

			if (countValue) {
				NXGReports.addStep("Verify Total Product Count", "Product Count should not be zero",
						"Product count is : " + count, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			} else {
				NXGReports.addStep("Verify Total Product Count", "Product Count should not be zero",
						"Product count is : " + count, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}

			driver.navigate().back();
			waitTillPageLoad();

			driver.navigate().back();
			waitTillPageLoad();

		}

	}
}
