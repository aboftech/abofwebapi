/***********************************************************************
* @author 			:		Shireesh Bandela
* @description		: 		Page objects and re-usbale methods for BrowsePLPPDP screen
* @module			:		BrowsePLPPDP
* @reusable methods : 		verifyPlpContentDisplayed(),verifyPdpContentDisplayed(),checkPagination()
* 							handleSizePopUp(),viewBagStatus(),validatePinCodeStatus(),validateSizeGuide(),selectSize()
*/
package com.abof.web.pageObjects;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.abof.web.utils.ExcelUtility;

import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;


//this page class will contain product related info and PLP and PDP verification
public class LookDetailPage {

	WebDriver driver;
	


	public LookDetailPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/*
	 * These webElements are similar for PLP and PDP. Captured web element of
	 * Action Bar .
	 */

	@FindBy(xpath = "//div[@class='checkbox checkbox--dark checkbox--checked look-detail--component__selected']")
	private List<WebElement> eleLookProducts;

	public List<WebElement> getEleLookProducts() {
		return eleLookProducts;
	}


	
	
	
	
	
}
