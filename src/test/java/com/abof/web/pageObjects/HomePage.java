/***********************************************************************
* @author 			:		Shireesh Kumar Bandela 
* @description		: 		Page objects and re-usbale methods for Mega Menu 
* @module			:		Homepage, Mega Menu
* @reusable methods : 		
*/
package com.abof.web.pageObjects;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;
import com.abof.library.GenericLib;
import com.abof.web.testscripts.BaseTest;

public class HomePage extends BaseTest {

	WebDriver driver = BaseTest.driver;
	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//a[@title='Men']")
	private WebElement men;

	public WebElement getMenElement() {
		return men;
	}

	@FindBy(xpath = "//a[@title='Women']")
	private WebElement women;

	public WebElement getWomenElement() {
		return women;
	}

	@FindBy(xpath = ".//*[@class='espot__content']/div/img']")
	private WebElement menImage;

	public WebElement getMenImage() {
		return menImage;
	}

	@FindBy(xpath = ".//*[@class='espot__content']/div/img']")
	private WebElement womenImage;

	public WebElement getWomenImage() {
		return womenImage;
	}

	@FindBy(xpath = "//*[@class='quick-search-input']/input")
	private WebElement eleSearchTextbox;

	public WebElement getEleSearchTextbox() {
		return eleSearchTextbox;
	}

	public WebElement getCategoryElement(String category, String subcategory) {
		String xpath;
		if (GenericLib.getCongigValue(System.getProperty("user.dir") + "/Config.Properties", "PlatformName")
				.equalsIgnoreCase("android")) {
			if (!subcategory.equals(""))
				xpath = "//div[@class='tile active']/div[@class='tile__content']/ul/li/a[contains(text(),'"
						+ subcategory + "')]";
			else
				xpath = "//div[@class='tile__header']/div/div/a/img[contains(@alt,'" + category + "')]";
			WebElement ele = driver.findElement(By.xpath(xpath));
			return ele;
		} else {
			if (!subcategory.equals(""))
				xpath = "//div[@class='dropdown-panel__nav']/ul/li/a[contains(text(),'" + category
						+ "')]/following-sibling::ul/li/a[@title='" + subcategory + "']";
			else
				xpath = "//div[@class='dropdown-panel__nav']/ul/li/a[contains(text(),'" + category + "')]";
			WebElement ele = driver.findElement(By.xpath(xpath));
			return ele;
		}
	}

	@FindBy(xpath = "//div[contains(@class,'dropdown-panel')]//ul/li/a")
	private List<WebElement> webMegaMenuCategories;

	public List<WebElement> getwebMegaMenuCategories() {
		return webMegaMenuCategories;
	}

	@FindBy(xpath = "//*[text()= 'Bag']")
	private WebElement bagIcon;

	public WebElement getEleBagIcon() {
		return bagIcon;
	}

	@FindBy(xpath = "//a[contains(text(), 'View Bag')]")
	private static WebElement viewBag;

	public static WebElement getEleViewBagbtn() {
		return viewBag;
	}

	@FindBy(xpath = "//*[contains(@class,'caret') or @title='Sign In/Register']")
	private static WebElement eleUserProfile;

	public static WebElement getEleUserProfile() {
		return eleUserProfile;
	}

	@FindBy(xpath = "//*[contains(text(),'Sign Out')]")
	private static WebElement eleSignOut;

	public static WebElement getEleSignOut() {
		return eleSignOut;
	}

	public void selectGender(String gender) throws Exception {
		try {
			if (gender.equalsIgnoreCase("men")) {
				mouseHoverOnMen();
			} else {
				mouseHoverOnWomen();
			}
		} catch (Exception e) {
			throw e;
		}
	}

	public void mouseHoverOnMen() throws Exception {
		if (GenericLib.getCongigValue(System.getProperty("user.dir") + "/Config.Properties", "PlatformName")
				.equalsIgnoreCase("android")) {
			HamburgerMenu.selectMen();
		} else {
			try {
				// LOGGER.info("Get men element");
				WebElement ele = getMenElement();
				waitForElement(ele, "Wait for Men Tab", "");
				mouseOver(driver, ele);
				// LOGGER.info("Mouse hovered");
				NXGReports.addStep("Mouse hover on Men.", "Men", "Mega menu should populate", "Mega menu is populated",
						LogAs.PASSED, null);
			} catch (Exception e) {
				NXGReports.addStep("Mouse hover on Men.", "Men", "Mega menu should populate",
						"Mega menu is NOT populated", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				throw e;
			}

		}
	}

	public void mouseHoverOnWomen() throws Exception {
		if (GenericLib.getCongigValue(System.getProperty("user.dir") + "/Config.Properties", "PlatformName")
				.equalsIgnoreCase("android")) {
			HamburgerMenu.selectWomen();
		} else {
			try {
				WebElement ele = getWomenElement();
				waitForElement(ele, "Wait for Women tab", "");
				mouseOver(driver, ele);
				NXGReports.addStep("Mouse hover on Women.", "Women", "Mega menu should populate",
						"Mega menu is populated", LogAs.PASSED, null);
			} catch (Exception e) {
				NXGReports.addStep("Mouse hover on Women.", "Women", "Mega menu should populate",
						"Mega menu is NOT populated", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				throw e;
			}
		}
	}

	public void clickCategory_subcategory(String category, String subcategory) throws Exception {
		WebElement ele = null;
		try {
			if (GenericLib.getCongigValue(System.getProperty("user.dir") + "/Config.Properties", "PlatformName")
					.equalsIgnoreCase("android")) {
				getCategoryElement(category, "").click();
				if (subcategory.equals(""))
					getCategoryElement(category, "View All").click();
				else
					getCategoryElement(category, subcategory).click();
			} else {
				ele = getCategoryElement(category, subcategory);
				waitForElement(ele, "Wait for Sub-Category link in megamenu", "");
				ele.click();
				waitTillPageLoad();
				NXGReports.addStep("Click on " + category + ", " + subcategory + ".", category,
						category + ", " + subcategory + " should be clicked",
						category + ", " + subcategory + " is clicked", LogAs.PASSED, null);
			}
		} catch (Exception e) {
			NXGReports.addStep("Click on " + category + ", " + subcategory + ".", category,
					category + ", " + subcategory + " should be clicked",
					category + ", " + subcategory + " is NOT clicked", LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}

	public void enterTextInSearchBox(String text) throws Exception {
		WebElement ele = null;
		try {
			ele = getEleSearchTextbox();
			waitForElement(ele, "Wait for search box in header", "");
			ele.click();
			waitTillPageLoad();
			waitAndEnterText(ele, "Search box", text, 250, 45);
			ele.sendKeys(Keys.RETURN);
			Thread.sleep(3000);
			waitTillPageLoad();
			NXGReports.addStep("Type " + text + " in search box", text, text + " should be typed in search box",
					text + " is typed in search box", LogAs.PASSED, null);
		} catch (Exception e) {
			NXGReports.addStep("Type " + text + " in search box", text, text + " should be typed in search box",
					text + " is NOT typed in search box", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			throw e;
		}
	}

	// after performing invalid search My fav item is displayed or not
	@SuppressWarnings("resource")
	public void saveCategoriesIntoExcel(String filePath, String fileName, String gender) {
		List<String> categories = new ArrayList<String>();
		List<String> hrefList = new ArrayList<String>();
		List<WebElement> links = null;
		String linkText = "", href = "";
		Row row;
		try {
			logger.info("Fetch categories links by xpath");

			links = getwebMegaMenuCategories();
			for (WebElement link : links) {
				linkText = link.getText();
				categories.add(linkText);
				href = link.getAttribute("href");
				hrefList.add(href);

			}
			logger.info("Categories count today: " + categories.size());
			File file = new File(filePath + "\\" + fileName + ".xlsx");
			FileInputStream inputStream = new FileInputStream(file);
			// logger.info("Workbook connection");
			Workbook workbook = null;
			workbook = new XSSFWorkbook(inputStream);
			// logger.info("Work book with given name: "+fileName);
			Sheet sheet1 = workbook.getSheet("Sheet1");
			// logger.info("Sheet1 found");
			logger.info("Now set data into excel");
			for (int i = 1; i <= categories.size(); i++) {
				if (gender.toLowerCase().equals("men")) {
					logger.info(i + ". " + categories.get(i - 1));
					row = sheet1.getRow(i);
					Cell todayCategory = row.createCell(1);
					todayCategory.setCellValue(categories.get(i - 1));
					Cell categoryhref = row.createCell(2);
					categoryhref.setCellValue(hrefList.get(i - 1));
				} else {
					logger.info(i + ". " + categories.get(i - 1));
					row = sheet1.getRow(i);
					Cell todayCategory = row.createCell(5);
					todayCategory.setCellValue(categories.get(i - 1));
					Cell categoryhref = row.createCell(6);
					categoryhref.setCellValue(hrefList.get(i - 1));
				}
			}
			logger.info("Save count based on gender");
			if (gender.toLowerCase().equals("men")) {
				Row row1 = sheet1.getRow(100);
				Cell men_count = row1.createCell(1);
				men_count.setCellValue(categories.size());
			} else {
				Row row2 = sheet1.getRow(100);
				Cell women_count = row2.createCell(5);
				women_count.setCellValue(categories.size());
			}
			// Close input stream
			inputStream.close();
			// Create an object of FileOutputStream class to create write data
			// in excel file
			FileOutputStream outputStream = new FileOutputStream(file);

			// write data in the excel file
			workbook.write(outputStream);

			// close output stream
			outputStream.close();

			NXGReports.addStep("Save categories under '" + gender + "'  into excel.", gender,
					"It should save categories in Categories.xlsx", "Saving categories in Categories.xlsx Successful.",
					LogAs.PASSED, null);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			NXGReports.addStep("Save categories under '" + gender + "'  into excel.", gender,
					"It should save categories in Categories.xlsx", "Saving categories in Categories.xlsx failed",
					LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}

	public void goToBag() {
		WebElement bagicon = null;
		WebElement viewBag = null;
		try {
			bagicon = getEleBagIcon();
			viewBag = getEleViewBagbtn();
			mouseOver(driver, bagicon);
			waitTillPageLoad();
			waitForElementToBeClickable(viewBag, "View Bag should be ready for click", "View bag is ready for Click");
			viewBag.click();
			waitTillPageLoad();
			NXGReports.addStep("Click on View Bag button.", "View Bag", "View Bag button should be clicked",
					"View Bag button is clicked", LogAs.PASSED, null);
		} catch (Exception e) {
			NXGReports.addStep("Click on View Bag button.", "View Bag", "View Bag button should be clicked",
					"View Bag button is Not clicked due to Exception: " + e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}

	public void logout() {
		try {
			mouseOver(driver, getEleUserProfile());
			waitTillPageLoad();
			getEleSignOut().click();
			NXGReports.addStep("Logout from user session.",
					"Mouse hover on user profile and Sign Out link should be clicked",
					"Mouse hover on user profile and Clicked on Sign Out link", LogAs.PASSED, null);
		} catch (Exception e) {
			NXGReports.addStep("Logout from user session.",
					"Mouse hover on user profile and Sign Out link should be clicked",
					"Failed due to Exception: " + e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}

	public void MouseHoverOnMegaMenu(WebElement megaMenuTab) throws InterruptedException {

		NXGReports.addStep("Mouse hover on Men Menu", LogAs.INFO, null);

		WebElement megaMenuElement = megaMenuTab;

		String expectedL1Category = megaMenuElement.getAttribute("title");

		mouseOver(driver, getMenElement());

		NXGReports.addStep("Count total sub menu under Men Menu", LogAs.INFO, null);

		waitTillPageLoad();

		int size = driver
				.findElements(By
						.xpath(".//*[@class='dropdown-panel-nav dropdown-panel-nav--l1 dropdown-panel-nav--compact']/li"))
				.size();

		logger.info("Size is : " + size);

		NXGReports.addStep("There are total " + size + " sub menus", LogAs.INFO,
				new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

		for (int i = 1; i <= 1; i++) {

			mouseOver(driver, getMenElement());

			waitTillPageLoad();

			WebElement subMenu = driver.findElement(
					By.xpath(".//*[@class='dropdown-panel-nav dropdown-panel-nav--l1 dropdown-panel-nav--compact']/li["
							+ i + "]/a"));

			String submenuValue = subMenu.getAttribute("aria-label");

			logger.info("Sub Menu is " + submenuValue);

			String submenuLinkValue = subMenu.getAttribute("href");

			logger.info("Sub Menu Link is " + submenuLinkValue);

			String expectedURL = submenuLinkValue;

			NXGReports.addStep("Mouse hover on " + submenuValue, LogAs.INFO, null);

			NXGReports.addStep("Click on " + submenuValue + " link", LogAs.INFO, null);

			logger.info("Click on " + submenuValue);

			subMenu.click();

			waitTillPageLoad();

			String actualL1Category = driver.findElement(By.xpath(".//*[@class='breadcrumbs__list']/li[1]/a/span"))
					.getText();

			String actualL2Category = driver.findElement(By.xpath(".//*[@class='breadcrumbs__list']/li[2]/a/span"))
					.getText();

			String actualmainURL = driver.getCurrentUrl();

			logger.info("actualL2Category is : " + actualL2Category);

			WebElement parentPageBreadCrumb = driver.findElement(By.xpath(".//*[@class='breadcrumbs__list']/li[2]/a"));

			String actualParentPageBreadCrumbHref = parentPageBreadCrumb.getAttribute("href");

			actualParentPageBreadCrumbHref = actualParentPageBreadCrumbHref.replaceAll("http://www.abof.com/", "");

			logger.info("actualParentPageBreadCrumbHref is " + actualParentPageBreadCrumbHref);

			String expectedParentPageBreadCrumbHref = actualmainURL.replaceAll("http://www.abof.com/", "");

			logger.info("expectedParentPageBreadCrumbHref is " + expectedParentPageBreadCrumbHref);

			logger.info(submenuValue + " URL is " + actualmainURL);

			Sassert.assertEquals(actualParentPageBreadCrumbHref, expectedParentPageBreadCrumbHref,
					"\n On clicking " + submenuValue + " Under Mega Men Menu , Bread crumb is wrong ");

			Sassert.assertEquals(actualL1Category, expectedL1Category,
					" L1 Category is not matching with Bread crumb L1 category");

			if (actualL1Category.equals(expectedL1Category)) {
				NXGReports.addStep("Verify L1 category Name in Parent Page", expectedL1Category, actualL1Category,
						LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

			}

			else {
				NXGReports.addStep("Verify L1 category Name in Parent Page", expectedL1Category, actualL1Category,
						LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}

			Sassert.assertEquals(actualL2Category, submenuValue,
					" L2 Category is not matching with Bread crumb L2 category");

			if (actualL2Category.equals(submenuValue)) {
				NXGReports.addStep("Verify L2 category Name in Parent Page", submenuValue, actualL2Category,
						LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

			}

			else {
				NXGReports.addStep("Verify L2 category Name in Parent Page", submenuValue, actualL2Category,
						LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}

			Sassert.assertEquals(actualmainURL, expectedURL,
					"On clicking " + submenuValue + " Under Mega Men Menu , it is not redirecting to " + expectedURL);

			if (actualmainURL.equals(expectedURL)) {
				NXGReports.addStep("On clicking " + submenuValue + " It is redirecting to " + expectedURL, expectedURL,
						actualmainURL, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}

			else {
				NXGReports.addStep("On clicking " + submenuValue + " It is not redirecting to " + expectedURL,
						expectedURL, actualmainURL, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

			}

			String expectedParentBreadCrumbText = expectedL1Category + "'s " + actualL2Category;

			logger.info("expectedParentBreadCrumbText is : " + expectedParentBreadCrumbText);

			if (actualL2Category.equals("Trending Brands") || actualL2Category.equals(("Shop By Trend"))) {

				expectedParentBreadCrumbText = actualL2Category;

				logger.info("expectedParentBreadCrumbText for Trending and shop by trend is : "
						+ expectedParentBreadCrumbText);
			}

			String actualParentBreadCrumbText = driver
					.findElement(By.xpath(".//*[@class='product-grid-header__title']/span[1]")).getText();

			Sassert.assertEquals(actualParentBreadCrumbText, expectedParentBreadCrumbText,
					"Parent Bread Crumb Text is wrong");

			if (actualParentBreadCrumbText.equals(expectedParentBreadCrumbText)) {
				NXGReports.addStep("Verify Parent Bread Crumb Text", expectedParentBreadCrumbText,
						actualParentBreadCrumbText, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}

			else {
				NXGReports.addStep("Verify Parent Bread Crumb Text", expectedParentBreadCrumbText,
						actualParentBreadCrumbText, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

			}
			// assertEquals(actualmainURL, expectedURL);

			mouseOver(driver, getMenElement());

			waitTillPageLoad();

			NXGReports.addStep("Count total sub menu under " + submenuValue, LogAs.INFO, null);

			mouseOver(driver,
					driver.findElement(By
							.xpath(".//*[@class='dropdown-panel-nav dropdown-panel-nav--l1 dropdown-panel-nav--compact']/li["
									+ i + "]/a")));

			int linkSize = driver.findElements(
					By.xpath(".//*[@class='dropdown-panel-nav dropdown-panel-nav--l1 dropdown-panel-nav--compact']/li["
							+ i + "]/ul/li"))
					.size();

			logger.info("Sub Menu link count " + linkSize);

			NXGReports.addStep("There are total " + linkSize + " Sub menus links under " + " submenuValue ", LogAs.INFO,
					null);

			for (int j = 1; j <= 1; j++) {
				mouseOver(driver, getMenElement());

				mouseOver(driver, subMenu);

				WebElement subMenuLinks = driver.findElement(By
						.xpath(".//*[@class='dropdown-panel-nav dropdown-panel-nav--l1 dropdown-panel-nav--compact']/li["
								+ i + "]/ul/li[" + j + "]/a"));

				String subMenuLinkName = subMenuLinks.getAttribute("aria-label");

				logger.info("Link under " + submenuValue + " is " + subMenuLinkName);

				String LinkValue = subMenuLinks.getAttribute("href");

				logger.info("Link under " + submenuValue + " link value " + LinkValue);

				boolean isItAbofBrand = LinkValue.contains("trending-brands/abof");

				if (isItAbofBrand) {

					LinkValue = LinkValue.replaceAll("trending-brands/", "");
				}

				boolean isItSkultBrand = LinkValue.contains("trending-brands/skult");

				if (isItSkultBrand) {

					LinkValue = LinkValue.replaceAll("men/trending-brands/", "");
				}
				String expectedSubMenuLinkURL = LinkValue;

				logger.info("Expected Sub Menu Link value is " + expectedSubMenuLinkURL);

				NXGReports.addStep("Mouse hover on " + subMenuLinkName + " link", LogAs.INFO, null);

				mouseOver(driver,
						driver.findElement(By
								.xpath(".//*[@class='dropdown-panel-nav dropdown-panel-nav--l1 dropdown-panel-nav--compact']/li["
										+ i + "]/ul/li[" + j + "]/a")));

				NXGReports.addStep("Click on " + subMenuLinkName + " link", LogAs.INFO, null);

				driver.findElement(By
						.xpath(".//*[@class='dropdown-panel-nav dropdown-panel-nav--l1 dropdown-panel-nav--compact']/li["
								+ i + "]/ul/li[" + j + "]/a"))
						.click();

				waitTillPageLoad();

				String actualMainSubMenuURL = driver.getCurrentUrl();

				// String actualMainSubMenuURL = "";

				logger.info("Actual sub menu url is : " + actualMainSubMenuURL);

				String childPageActualL1Category = driver
						.findElement(By.xpath(".//*[@class='breadcrumbs__list']/li[1]/a/span")).getText();

				String childPageActualL2Category = driver
						.findElement(By.xpath(".//*[@class='breadcrumbs__list']/li[2]/a/span")).getText();

				String childPageActualL3Category = driver
						.findElement(By.xpath(".//*[@class='breadcrumbs__list']/li[3]/a/span")).getText();

				WebElement childPageElement = driver.findElement(By.xpath(".//*[@class='breadcrumbs__list']/li[3]/a"));

				String actualChildPageBreadCrumb = childPageElement.getAttribute("href");

				actualChildPageBreadCrumb = actualChildPageBreadCrumb.replaceAll("http://www.abof.com/", "");

				logger.info("actualChildPageBreadCrumb is :" + actualChildPageBreadCrumb);

				String expectedChildPageBreadCrumb = expectedL1Category + "/" + actualL2Category + "/"
						+ subMenuLinkName;

				Sassert.assertEquals(childPageActualL1Category, actualL1Category,
						"On clicking " + subMenuLinkName + " \n , L1 name is wrong  ");

				if (childPageActualL1Category.equals(actualL1Category)) {
					NXGReports.addStep("Verify L1 Category Name in Child Page", actualL1Category,
							childPageActualL1Category, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

				}

				else {
					NXGReports.addStep("Verify L1 Category Name in Child Page", actualL1Category,
							childPageActualL1Category, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

				Sassert.assertEquals(childPageActualL2Category, actualL2Category,
						"On clicking " + subMenuLinkName + " \n , L2 name is wrong  ");

				if (childPageActualL2Category.equals(actualL2Category)) {
					NXGReports.addStep("Verify L2 Category Name in Child Page", actualL2Category,
							childPageActualL2Category, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

				}

				else {
					NXGReports.addStep("Verify L2 Category Name in Child Page", actualL2Category,
							childPageActualL2Category, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

				Sassert.assertEquals(childPageActualL3Category, subMenuLinkName,
						"On clicking " + subMenuLinkName + " \n , L2 name is wrong  ");

				if (childPageActualL3Category.equals(subMenuLinkName)) {
					NXGReports.addStep("Verify L3 Category Name in Child Page", subMenuLinkName,
							childPageActualL3Category, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

				}

				else {
					NXGReports.addStep("Verify L3 Category Name in Child Page", subMenuLinkName,
							childPageActualL3Category, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

				Sassert.assertEquals(actualMainSubMenuURL, expectedSubMenuLinkURL,
						"On clicking " + subMenuLinkName + " , It is not redirecting to " + expectedSubMenuLinkURL);

				if (actualMainSubMenuURL.equals(expectedSubMenuLinkURL)) {
					NXGReports.addStep(
							"On clicking " + subMenuLinkName + " , It is  redirecting to " + expectedSubMenuLinkURL,
							expectedSubMenuLinkURL, actualMainSubMenuURL, LogAs.PASSED,
							new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

				else {
					NXGReports.addStep(
							"On clicking " + subMenuLinkName + " , It is not redirecting to " + expectedSubMenuLinkURL,
							expectedSubMenuLinkURL, actualMainSubMenuURL, LogAs.FAILED,
							new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

				}

				String actualChildBreadCrumbText = driver
						.findElement(By.xpath(".//*[@class='product-grid-header__title']/span[1]")).getText();

				String expectedChildBreadCrumbText = subMenuLinkName + " for " + expectedL1Category;

				logger.info("expectedParentBreadCrumbText is : " + expectedChildBreadCrumbText);

				if (actualL2Category.equals("Trending Brands") || actualL2Category.equals("Shop By Trend")) {

					expectedChildBreadCrumbText = subMenuLinkName;

					logger.info("expectedParentBreadCrumbText for Trending and shop by trend is : "
							+ expectedChildBreadCrumbText);
				}

				if (actualL2Category.equals("New In")) {

					expectedChildBreadCrumbText = expectedL1Category + "'s Latest " + subMenuLinkName;

					logger.info("expectedParentBreadCrumbText for Trending and shop by trend is : "
							+ expectedChildBreadCrumbText);
				}

				if (isItAbofBrand || isItSkultBrand) {
					break;

				}

				Sassert.assertEquals(actualChildBreadCrumbText, expectedChildBreadCrumbText,
						"Parent Bread Crumb Text is wrong");

				if (actualChildBreadCrumbText.equals(expectedChildBreadCrumbText)) {
					NXGReports.addStep("Verify Parent Bread Crumb Text", expectedChildBreadCrumbText,
							actualChildBreadCrumbText, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				}

				else {
					NXGReports.addStep("Verify Parent Bread Crumb Text", expectedChildBreadCrumbText,
							actualChildBreadCrumbText, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

				}

			}

		}

	}

	/*
	 * @author : Shreekant R S
	 * 
	 * @description : Verify Images Under Mega Menu
	 * 
	 * @module : Browse
	 * 
	 * @reusable methods :
	 */

	public void verifyImageUnderMenMegaMenu() throws Exception {

		WebElement srcImage = driver.findElement(By.xpath(".//*[@class='espot__content']/div/img"));

		boolean imageDispalyed = CheckImage(srcImage);

		String srcURL = srcImage.getAttribute("src");

		char splitchar = '/';

		String actualImageName = utl.splitString(srcURL, splitchar);

		String expectedImageName = BaseTest.prop.getProperty("expectedImageNameUnderMegaMenuName");

		logger.info("Expected name is : " + expectedImageName);

		if (imageDispalyed) {

			Sassert.assertTrue(imageDispalyed, "It is not dispalying under men mega menu ");
			NXGReports.addStep("Verified iamge under men mega menu", "Image should dispaly", "Image is dispalying ",
					LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		else {
			NXGReports.addStep("Verified iamge under men mega menu", "Image should dispaly", "Image is not dispalying ",
					LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		if (actualImageName.contains(expectedImageName)) {

			Sassert.assertTrue(true, "Image name under men mega menu does not contain " + expectedImageName);
			NXGReports.addStep("Verify image name in men mega menu", "Image Name Should Contain : " + expectedImageName,
					"Actaul Image name is : " + actualImageName, LogAs.PASSED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		else {
			Sassert.assertTrue(false, "Image name under men mega menu does not contain " + expectedImageName);
			NXGReports.addStep("Verify image name in men mega menu", "Image Name Should contain : " + expectedImageName,
					"Actaul Image name is : " + actualImageName, LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		if (utl.validateImage(actualImageName)) {

			Sassert.assertTrue(true, "Image name is ending with .JPG " + actualImageName);
			NXGReports.addStep("Verfiy Image Name Ending with .jpg", "Image Name Should End With .jpg",
					"Image Name Is Ending With .jpg " + actualImageName, LogAs.PASSED, null);
		}

		else {

			Sassert.assertTrue(false, "Image name is not ending with .JPG " + actualImageName);

			String actualImageExtName = utl.verifyImageExtension(actualImageName);

			logger.info("Actual Image ExtensionName Is: " + actualImageExtName);

			NXGReports.addStep("Verfiy Image Name Ending with .jpg", "Image Name Should End With .jpg",
					"Image Name Is Not Ending With .jpg , It is ending with ." + actualImageExtName, LogAs.FAILED,
					null);
		}
	}

	public void verifyImageUnderWomenMegaMenu() throws Exception {

		WebElement srcImage = driver.findElement(By.xpath(".//*[@class='espot__content']/div/img"));

		boolean imageDispalyed = CheckImage(srcImage);

		String srcURL = srcImage.getAttribute("src");

		char splitchar = '/';

		String actualImageName = utl.splitString(srcURL, splitchar);

		String expectedImageName = BaseTest.prop.getProperty("expectedImageNameUnderMegaMenuName");

		System.out.println("Expected name is : " + expectedImageName);

		if (imageDispalyed) {

			Sassert.assertTrue(imageDispalyed, "It is not dispalying under women mega menu ");
			NXGReports.addStep("Verify iamge under women mega menu", "Image should dispaly", "Image is dispalying ",
					LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		else {
			NXGReports.addStep("Verify iamge under women mega menu", "Image should dispaly", "Image is not dispalying ",
					LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}

		if (actualImageName.contains(expectedImageName)) {

			Sassert.assertTrue(true, "Image name under men mega menu does not contain " + expectedImageName);
			NXGReports.addStep("Verified image name in men mega menu",
					"Image Name Should Contain : " + expectedImageName, "Actaul Image name is : " + actualImageName,
					LogAs.PASSED, null);
		}

		else {
			NXGReports.addStep("Verify image name in men mega menu", "Image Name Should contain : " + expectedImageName,
					"Actaul Image name is : " + actualImageName, LogAs.FAILED, null);
		}

		Sassert.assertTrue(utl.validateImage(actualImageName), "Image name is ending with .JPG " + actualImageName);

		if (utl.validateImage(actualImageName)) {

			NXGReports.addStep("Verfiy Image Name Ending with .jpg", "Image Name Should End With .jpg",
					"Image Name Is Ending With .jpg " + actualImageName, LogAs.PASSED, null);
		}

		else {

			String actualImageExtName = utl.verifyImageExtension(actualImageName);

			System.out.println("Actual Image ExtensionName Is: " + actualImageExtName);

			NXGReports.addStep("Verfiy Image Name Ending with .jpg", "Image Name Should End With .jpg",
					"Image Name Is Not Ending With .jpg , It is ending with ." + actualImageExtName, LogAs.FAILED,
					null);
		}

	}

}
