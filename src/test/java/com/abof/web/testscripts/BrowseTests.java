/***********************************************************************
* @author 			:	    Shreekant R S 
* @description		: 		Browse Test Scripts
* @module			:		Browse

*/
package com.abof.web.testscripts;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.abof.android.pageobjects.BrowsePlpPdpPO;
import com.abof.android.pageobjects.HamburgerMenuPO;

import com.abof.android.pageobjects.LoginPagePO;
import com.abof.android.pageobjects.MyFavouritesPagePO;
import com.abof.android.pageobjects.MyOrdersPagePO;
import com.abof.android.pageobjects.ProfilePagePO;
import com.abof.android.pageobjects.WhatsHotLandingPO;
import com.abof.library.BaseLib;
import com.abof.library.GenericLib;
import com.abof.web.pageObjects.HomePage;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;

public class BrowseTests extends BaseTest{


	HomePage homePagePo= null;

	String sData[]=null;
	@BeforeMethod
	public void init() {
	
		
		
		homePagePo = new HomePage(driver);

	}
	 /* @Description:Login as FB user in FB app user is not logged in and validate search options for valid and invalid inputs
	  * @Author: Srinivas Hippargi*/


	@Test(enabled = true, priority = 1, description = "Verify All The Links Under Men Menu")
	public void verifyMenMenuLinks() throws Exception {
	
		
		NXGReports.addStep("Men tab is dispalying", LogAs.INFO, null);
		BaseTest.waitForElement(homePagePo.getMenElement(), "Men tab is dispalying", "Men tab is not displayed");
		
		homePagePo.MouseHoverOnMegaMenu(homePagePo.getMenElement());
		Sassert.assertAll();
			
		}
	

	@Test(enabled = true, priority = 2, description = "Verify All The Links Under Women Menu")
	public void verifyWomenMenuLinks() throws Exception {
	
		
		NXGReports.addStep("Men tab is dispalying", LogAs.INFO, null);
		BaseTest.waitForElement(homePagePo.getWomenElement(), "Women tab is dispalying", "Women tab is not displayed");
		
		homePagePo.MouseHoverOnMegaMenu(homePagePo.getWomenElement());
		Sassert.assertAll();
			
		}
	
	@Test(enabled = true, priority = 3, description = "Verify Men Mega Menu Image")
	public void menMegaMenuImage() throws Exception {
	
	
		NXGReports.addStep("Men tab is dispalying", LogAs.INFO, null);
		BaseTest.waitForElement(homePagePo.getWomenElement(), "Men tab is dispalying", "Men tab is not displayed");
		
		homePagePo.mouseHoverOnMen();
		
		homePagePo.verifyImageUnderMenMegaMenu();
		
		Sassert.assertAll();
					
		}
	
	@Test(enabled = true, priority = 4, description = "Verify Women Mega Menu Image")
	public void womenMegaMenuImage() throws Exception {
	
	
		NXGReports.addStep("Verify Women tab", LogAs.INFO, null);
		
		BaseTest.waitForElement(homePagePo.getWomenElement(), "Women tab is dispalying", "Women tab is not displayed");
		
		homePagePo.mouseHoverOnWomen();
		
		homePagePo.verifyImageUnderWomenMegaMenu();
		
		Sassert.assertAll();
					
		}
	
	
	
	

		
}
