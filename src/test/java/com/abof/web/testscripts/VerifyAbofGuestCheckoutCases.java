package com.abof.web.testscripts;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.abof.web.pageObjects.LandingPage;
import com.abof.web.pageObjects.CheckOutPage;
import com.abof.web.pageObjects.HomePage;
import com.abof.web.pageObjects.LoginPage;
import com.abof.web.pageObjects.LookDetailsPage;
import com.abof.web.pageObjects.PdpPage;
import com.abof.web.pageObjects.PlpPage;

public class VerifyAbofGuestCheckoutCases  extends BaseTest{

	public static String abof_url= "http://www.abof.com";
	HomePage homepage = null;
	LandingPage abofpage = null;
	PlpPage plp = null;
	PdpPage pdp = null;
	CheckOutPage bag = null;
	LoginPage login = null;
	LookDetailsPage lookDetails = null;
	
	@BeforeClass
	public void init() {
		homepage = new HomePage(driver);
		abofpage = new LandingPage(driver);
		plp = new PlpPage(driver);
		pdp = new PdpPage(driver);
		bag = new CheckOutPage(driver);
		login = new LoginPage(driver);
		lookDetails = new LookDetailsPage(driver);
	}
	@Test(enabled = false, priority = 1)
	public void  verifyCouponApplicationRestrictedForGuestUser() throws Exception{
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","Jeans");
		plp.clickOnProductOnPLPPage();
		pdp.clickOnAvailableSize();
		pdp.clickOnAddToBagbtn();
		homepage.goToBag();
		bag.clickOnPLACEORDER_Btn();
		waitTillPageLoad();
		login.checkoutAsGuestUser();
		bag.clickOnViewBagLink_ShippingAddress();
		bag.clickOnSignInToApplyCoupon();
		
	}
	@Test(enabled = false, priority = 2)
	public void  verifyUserIsAbleToContinueAsGuest() throws Exception{
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","Jeans");
		plp.clickOnProductOnPLPPage();
		pdp.clickOnAvailableSize();
		pdp.clickOnAddToBagbtn();
		homepage.goToBag();
		bag.clickOnPLACEORDER_Btn();
		waitTillPageLoad();
		login.checkoutAsGuestUser();
	}
	@Test(enabled = true, priority = 3)
	public void  verifyGuestUserIsAbleToSubmitEmail() throws Exception{
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","Jeans");
		plp.clickOnProductOnPLPPage();
		pdp.clickOnAvailableSize();
		pdp.clickOnAddToBagbtn();
		homepage.goToBag();
		bag.clickOnPLACEORDER_Btn();
		waitTillPageLoad();
		login.checkoutAsGuestUser();
		login.verifyUnRegisteredGuestEmailID();
	}
	@Test(dataProvider = "getGenders",enabled = false, priority = 4)
	public void  verifyUserIsAbleToAddFromAbofCollections() throws Exception{
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","Jeans");
		plp.clickOnProductOnPLPPage();
		pdp.clickOnAvailableSize();
		pdp.clickOnAddToBagbtn();
		homepage.goToBag();
		bag.clickOnPLACEORDER_Btn();
		waitTillPageLoad();
		login.checkoutAsGuestUser();
		login.verifyUnRegisteredGuestEmailID();	
	}
	@Test(enabled = false, priority = 5)
	public void  VerifySignInStageSkippedForGuestUser() throws Exception{
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","Jeans");
		plp.clickOnProductOnPLPPage();
		pdp.clickOnAvailableSize();
		pdp.clickOnAddToBagbtn();
		homepage.goToBag();
		bag.clickOnPLACEORDER_Btn();
		waitTillPageLoad();
		login.checkoutAsGuestUser();
		login.verifyUnRegisteredGuestEmailID();
	}
	@Test(enabled = false, priority = 6)
	public void  verifyGenderOfGuestuserPickedBasedOnGarmentsInBag() throws Exception{
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","Jeans");
		plp.clickOnProductOnPLPPage();
		pdp.clickOnAvailableSize();
		pdp.clickOnAddToBagbtn();
		homepage.goToBag();
		bag.clickOnPLACEORDER_Btn();
		waitTillPageLoad();
		login.checkoutAsGuestUser();
		login.verifyUnRegisteredGuestEmailID();
	}

	@Test(enabled = false, priority = 7)
	public void  verifyPaymentSectionForGuestUser() throws Exception{
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","Jeans");
		plp.clickOnProductOnPLPPage();
		pdp.clickOnAvailableSize();
		pdp.clickOnAddToBagbtn();
		homepage.goToBag();
		bag.clickOnPLACEORDER_Btn();
		waitTillPageLoad();
		login.checkoutAsGuestUser();
		login.verifyUnRegisteredGuestEmailID();
	}
		
	@Test(enabled = false, priority = 8)
	public void  verifyGuestuserisAbleToPlace_COD_Order() throws Exception{
		
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","Jeans");
		plp.clickOnProductOnPLPPage();
		pdp.clickOnAvailableSize();
		pdp.clickOnAddToBagbtn();
		homepage.goToBag();
		bag.clickOnPLACEORDER_Btn();
		waitTillPageLoad();
		login.checkoutAsGuestUser();
		login.verifyUnRegisteredGuestEmailID();
		
	}
	@Test(enabled = false, priority = 9)
	public void  verifyGuestuserisAbleToPlace_Prepaid_Order() throws Exception{
		
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","Jeans");
		plp.clickOnProductOnPLPPage();
		pdp.clickOnAvailableSize();
		pdp.clickOnAddToBagbtn();
		homepage.goToBag();
		bag.clickOnPLACEORDER_Btn();
		waitTillPageLoad();
		login.checkoutAsGuestUser();
		login.verifyUnRegisteredGuestEmailID();
		
	}
	
	@Test(enabled = false, priority = 10)
	public void  verifyGuestuserProfileUpdateAtOrderConfirmation() throws Exception{
		
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","Jeans");
		plp.clickOnProductOnPLPPage();
		pdp.clickOnAvailableSize();
		pdp.clickOnAddToBagbtn();
		homepage.goToBag();
		bag.clickOnPLACEORDER_Btn();
		waitTillPageLoad();
		login.checkoutAsGuestUser();
		login.verifyUnRegisteredGuestEmailID();
		verifyGuestUserNavigateToThankYouPageOnSubmittingProfile();
	}
	@Test(enabled = false, priority = 11)
	public void  verifyGuestUserNavigateToThankYouPageOnSubmittingProfile() throws Exception{
		login.checkoutAsGuestUser();
		login.verifyUnRegisteredGuestEmailID();
		
	}
	@Test(enabled = false, priority = 12)
	public void  verifyGuestUserNavigateToHomePageIfInactiveOn_OrderConfirmation() throws Exception{
		login.checkoutAsGuestUser();
		login.verifyUnRegisteredGuestEmailID();
		
	}
	@Test(enabled = false, priority = 13)
	public void  verifyGuestUserReceiveWelcomeEmail_OnOrderConfirmation() throws Exception{
		login.checkoutAsGuestUser();
		login.verifyUnRegisteredGuestEmailID();
		
	}
	
	
}
