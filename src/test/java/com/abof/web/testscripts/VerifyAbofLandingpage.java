package com.abof.web.testscripts;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.abof.web.pageObjects.LandingPage;
import com.abof.web.pageObjects.CheckOutPage;
import com.abof.web.pageObjects.HomePage;
import com.abof.web.pageObjects.LoginPage;

import com.abof.web.pageObjects.LookDetailsPage;

import com.abof.web.pageObjects.PdpPage;
import com.abof.web.pageObjects.PlpPage;

public class VerifyAbofLandingpage  extends BaseTest{

	public static String abof_url= "http://www.abof.com";
	HomePage homepage = null;
	LandingPage abofpage = null;
	PlpPage plp = null;
	PdpPage pdp = null;
	CheckOutPage bag = null;
	LoginPage login = null;

	LookDetailsPage lookDetails = null;

	
	@BeforeClass
	public void init() {
		homepage = new HomePage(driver);
		abofpage = new LandingPage(driver);
		plp = new PlpPage(driver);
		pdp = new PdpPage(driver);
		bag = new CheckOutPage(driver);
		login = new LoginPage(driver);

		lookDetails = new LookDetailsPage(driver);

	}
	@Test(dataProvider = "getGenders",enabled = false, priority = 1)
	public void  verifyAbofLandingpageAccessFromMegamenu(String num, String gender) throws Exception{
		driver.get(abof_url);
		homepage.selectGender(gender);
		homepage.clickCategory_subcategory("Trending Brands","abof");
		abofpage.verifyPageElementsDisplayed();
		Sassert.assertAll();
	}
	@Test(dataProvider = "getGenders",enabled = false, priority = 3)
	public void  verifyUserIsAbleToAddAbofProductsToBag(String num, String gender) throws Exception{
		driver.get(abof_url);
		homepage.selectGender(gender);
		homepage.clickCategory_subcategory("Trending Brands","abof");
		abofpage.clickOnViewAllProducts();
		plp.addProductToWishList();
		login.loginWeb(prop.getProperty("username"), prop.getProperty("password"));
		waitTillPageLoad();
		plp.clickOnProductOnPLPPage();
		pdp.clickOnAvailableSize();
		pdp.clickOnAddToBagbtn();
		homepage.goToBag();
		waitTillPageLoad();
		bag.removeProductsFromBag();
		waitTillPageLoad();
		driver.navigate().to(abof_url);
		homepage.logout();
		Sassert.assertAll();
	}
	@Test(dataProvider = "getGenders",enabled = false, priority = 4)
	public void  verifyUserIsAbleToAddFromAbofCollections(String num, String gender) throws Exception{
		driver.get(abof_url);
		homepage.selectGender(gender);
		homepage.clickCategory_subcategory("Trending Brands","abof");
		abofpage.verifyShopCollectionsDisplayed();
		abofpage.verify4CollectionProductsDisplayed();
		abofpage.verifyClickOnShopCollections();
		plp.clickOnProductOnPLPPage();
		pdp.clickOnAvailableSize();
		pdp.clickOnAddToBagbtn();
		homepage.goToBag();
		waitTillPageLoad();
		bag.removeProductsFromBag();
		waitTillPageLoad();	
		Sassert.assertAll();
	}
	@Test(dataProvider = "getGenders",enabled = false, priority = 5)
	public void  verifyUserIsAbleToAddFromAbofLooks(String num, String gender) throws Exception{
		driver.get(abof_url);
		homepage.selectGender(gender);
		homepage.clickCategory_subcategory("Trending Brands","abof");
		abofpage.verifyShopLooksDisplayed();
		abofpage.verifyLooksWidgetBlock();
		abofpage.clickOnLooksAtPosition(1);
		lookDetails.selectAvailableSizeOnLookDetailsPage();
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, 0)");
		pdp.clickOnAddToBagbtn();
		homepage.goToBag();
		waitTillPageLoad();
		bag.removeProductsFromBag();
		waitTillPageLoad();
		Sassert.assertAll();
	}
	@Test(dataProvider = "getGenders",enabled = false, priority = 6)
	public void  verifyUserIsAbleToAddFromAbofCategory(String num, String gender) throws Exception{
		driver.get(abof_url);
		homepage.selectGender(gender);
		homepage.clickCategory_subcategory("Trending Brands","abof");
		abofpage.verifyShopLooksDisplayed();
		abofpage.verifyLooksWidgetBlock();
		abofpage.clickOnLooksAtPosition(1);
		lookDetails.selectAvailableSizeOnLookDetailsPage();
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, 0)");
		pdp.clickOnAddToBagbtn();
		homepage.goToBag();
		waitTillPageLoad();
		bag.removeProductsFromBag();
		waitTillPageLoad();
		Sassert.assertAll();
	}

	@Test(dataProvider = "getGenders",enabled = false, priority = 7)
	public void  verifyUserIsAbleToAddFromAbofStories(String num, String gender) throws Exception{
		driver.get(abof_url);
		homepage.selectGender(gender);
		homepage.clickCategory_subcategory("Trending Brands","abof");
		abofpage.verifyAbofStoriesDisplayed();
		abofpage.verifyClickOnStoryCard();	
		Sassert.assertAll();
	}
		
	@Test(dataProvider = "getGenders", enabled = false, priority = 8)
	public void  verifyAbofLandingpageHeaderAndFooter(String num, String gender) throws Exception{
		
		driver.get(abof_url);
		homepage.selectGender(gender);
		homepage.clickCategory_subcategory("Trending Brands","abof");
		abofpage.verifyHeaderAndFooterDisplayed();
		abofpage.verifyHeaderAndFooterDisplayed();
		Sassert.assertAll();		
	}
	@DataProvider
	public Object[][] getGenders() {
		Object[][] gender = new Object[2][2];
		gender[0][0] = "1";
		gender[0][1] = "men";
		gender[1][0] = "2";
		gender[1][1] = "women";	
		return gender;
	}
}
