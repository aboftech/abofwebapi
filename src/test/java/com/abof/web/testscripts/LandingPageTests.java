/***********************************************************************
* @author 			:	    Shreekant R S 
* @description		: 		Browse Test Scripts
* @module			:		Browse

*/
package com.abof.web.testscripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.abof.library.BaseLib;
import com.abof.library.GenericLib;
import com.abof.web.pageObjects.HomePage;
import com.abof.web.pageObjects.LandingPage;
import com.abof.web.pageObjects.PlpPage;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;

public class LandingPageTests extends BaseTest {

	HomePage homePagePo = null;

	LandingPage landingPage = null;

	PlpPage Plp = null;

	String sData[] = null;

	@BeforeMethod
	public void init() {

		homePagePo = new HomePage(driver);
		landingPage = new LandingPage(driver);
		Plp = new PlpPage(driver);

	}


	@Test(enabled = false, priority = 1, description = "Verify Men Landing Page")
	public void verifyMenLandingPage() throws Exception {

		NXGReports.addStep("Go to www.abof.com ", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

		BaseTest.waitAndClick(homePagePo.getMenElement(), "Men Tab", 256, 10);

		landingPage.verifyMenLandingPage(prop.getProperty("menLandingPageTitle").trim());
		Sassert.assertAll();

	}

	@Test(enabled = false, priority = 2, description = "Verify Brands In Men Landing Page")
	public void verifyBrandsInMenLandingPage() throws Exception {

		NXGReports.addStep("Go to www.abof.com ", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

		BaseTest.waitAndClick(homePagePo.getMenElement(), "Men Tab", 256, 10);

		landingPage.verifyBrandsInMenLandingPage();
		Sassert.assertAll();

	}
	
	
	@Test(enabled = false, priority = 5, description = "Verify E spots In Men Landing Page")
	public void verifyEspotsInmenLandingPage() throws Exception {

		NXGReports.addStep("Go to www.abof.com ", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

		BaseTest.waitAndClick(homePagePo.getMenElement(), "Men Tab", 256, 10);

		
		landingPage.verifyEspotsInMenLandingPage();
		 
		Sassert.assertAll();

	}
	
	
	@Test(enabled = false, priority = 5, description = "Verify Stories In Men Landing Page")
	public void verifyStoriesInmenLandingPage() throws Exception {

		NXGReports.addStep("Go to www.abof.com ", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

		BaseTest.waitAndClick(homePagePo.getMenElement(), "Men Tab", 256, 10);

		
		landingPage.verifyStoriesInLandingPage();
		 
		Sassert.assertAll();

	}

	@Test(enabled = false, priority = 3, description = "Verify Women Landing Page ")
	public void verifyWomenLandingPage() throws Exception {

		NXGReports.addStep("Go to www.abof.com ", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

		BaseTest.waitAndClick(homePagePo.getWomenElement(), "Women Tab", 256, 10);

//		landingPage.verifyWomenLandingPage(prop.getProperty("WomenLandingPageTitle").trim());
		Sassert.assertAll();

	}

	@Test(enabled = false, priority = 4, description = "Verify Brands In Women Landing Page")
	public void verifyBrandsInwomenLandingPage() throws Exception {

		NXGReports.addStep("Go to www.abof.com ", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

		BaseTest.waitAndClick(homePagePo.getWomenElement(), "Women Tab", 256, 10);

		landingPage.verifyBrandsInWomenLandingPage();
		Sassert.assertAll();

	}
	
	@Test(enabled = false, priority = 6, description = "Verify E spots In Men Landing Page")
	public void verifyEspotsInWomenLandingPage() throws Exception {

		NXGReports.addStep("Go to www.abof.com ", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

		BaseTest.waitAndClick(homePagePo.getWomenElement(), "Women Tab", 256, 10);

		
		landingPage.verifyEspotsInMenLandingPage();
		 
		Sassert.assertAll();

	}
	
	@Test(enabled = true, priority = 5, description = "Verify Stories In Women Landing Page")
	public void verifyStoriesInwomenLandingPage() throws Exception {

		NXGReports.addStep("Go to www.abof.com ", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

		BaseTest.waitAndClick(homePagePo.getWomenElement(), "Women Tab", 256, 10);

		
		landingPage.verifyStoriesInLandingPage();
		 
		Sassert.assertAll();

	}

}
