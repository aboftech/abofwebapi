package com.abof.web.testscripts;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.abof.web.pageObjects.HamburgerMenu;
import com.abof.web.pageObjects.HomePage;
import com.abof.web.pageObjects.LoginPage;
import com.abof.web.pageObjects.PdpPage;
import com.abof.web.pageObjects.PlpPage;

public class PlpTests extends BaseTest{
	public static String abof_url= "http://www.abof.com";
	HomePage homepage = null;
	PlpPage plp = null;
	PdpPage pdp = null;
	LoginPage login = null;
	HamburgerMenu toggleMenu = null;
	@BeforeClass
	public void init() throws Exception {
		homepage = new HomePage(driver);
		plp = new PlpPage(driver);
		pdp = new PdpPage(driver);	
		login = new LoginPage(driver);
		toggleMenu = new HamburgerMenu(driver);
	}
	@Test(enabled = true, priority = 1, description="To verify the sections of Product Listing page")
	public void  tc1_verifyTheSectionsOfProductListingPage() throws Exception{
		//driver.get(abof_url);
		waitTillPageLoad();
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","");	
		plp.clickOnShowFilter();
		plp.validateTheSectionsofProductListingpage();
		Sassert.assertAll();
	}
	
	@Test(enabled = true, priority = 2, description="To verify the different sections of filters displayed in Refine section")
	public void tc2_validateDifferentFiltersSelections()  throws Exception{
		
		//driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","Jeans");	
		plp.clickOnShowFilter();
		plp.validateBrandFilter();
		plp.clickOnShowFilter();
		plp.validatePriceFilter();
		plp.clickOnShowFilter();
		plp.validateColorFilter();
		plp.clickOnShowFilter();
		plp.validateSizeFilter();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 3, description="To verify the additional filters section displayed in Refine section")
	public void tc3_validateCategoryProductFiltersSelections() throws Exception {
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","");	
		plp.clickOnShowFilter();
		plp.validateAdditionalFilterSection();
		homepage.enterTextInSearchBox("shirt");	
		waitTillPageLoad();
		plp.clickOnShowFilter();
		plp.validateCategoryFilter();
		plp.validateProductFilter();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 4, description="To verify that product list refreshes for each filter selection")
	public void tc4_validateAvailableFiltersSelectionAndInventoryStatus() throws Exception {
		
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","");	
		plp.clickOnShowFilter();
		plp.validateAvailableFiltersSelection();
		plp.checkInventoryStatus(pdp);
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 5,description="To verify that product list refreshes, when customer clicks on a selected value to deselect a certain filter")
	public void tc5_verifyFilterDeselection() throws Exception{
		
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","");	
		plp.clickOnShowFilter();
		plp.validateAvailableFiltersSelection();	
		plp.clickOnShowFilter();
		plp.verifyFilterDeselection();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 6, description="To verify that all the filters are removed and the original Product listing page is presented to the customer on clicking <Clear All>")
	public void tc6_verifyClearAllButtonFunctionality() throws Exception {
		
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","");	
		plp.clickOnShowFilter();
		plp.validateAvailableFiltersSelection();
		plp.verifyClearAllButtonFunctionality();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 7, description="To verify that Products list gets updated based on the removed filter value")
	public void tc7_verifyRomovalOfFilterOptionFromRefineSection() throws Exception{
		
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","");	
		plp.clickOnShowFilter();
		plp.validateAvailableFiltersSelection();
		plp.verifyRomovalOfFilterOptionFromRefineSection();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 8, description="To verify that Refine further section is expanded and shown, when only 1 L3 category [Product filter] is present in the search result set")
	public void tc8_validateAdditionalFilterSectionDisplayed() throws Exception {
		
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","");	
		plp.clickOnShowFilter();
		plp.selectProductFilterOption();
		plp.validateAdditionalFilterSectionDisplayed();
		plp.verifyClearAllButtonFunctionality();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 9, description="To verify that Refine further section is hidden , when more than one L3 category is present in the search result set")
	public void tc9_validateAdditionalFilterSectionHidden() throws Exception {
		
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","");	
		plp.clickOnShowFilter();
		plp.selectProductFilterOption();
		plp.validateAdditionalFilterSectionForMoreThanOneproductFilterOptionSelection();
		plp.verifyClearAllButtonFunctionality();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 11, description="To verify the contents displayed in each product tab")
	public void tc11_validateProductTab() {
		
		plp.validateProductTab();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 12, description="To verify the products are displayed in unobtrusive manner without any pagination in Products listing page")
	public void tc12_verifyProductLoadByScrollingDownpage() throws Exception {
		
		plp.verifyProductLoadByScrollingDownpage();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 12, description="To verify the functionality of each Sort options available in the PLP")
	public void tc15_verifySortOptions() throws Exception {
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","");	
		plp.clickOnSortBy();
		plp.verifySortByOptionPopularity();	
		plp.clickOnSortBy();
		plp.verifySortByJustIn();
		plp.clickOnSortBy();
		plp.verifySortByDiscountHighToLow();
		plp.clickOnSortBy();
		plp.verifySortByPriceLowToHigh();
		plp.clickOnSortBy();
		plp.verifySortByPriceHighToLow();
		Thread.sleep(2000);
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 13, description="Verify that the customer is able to successfully add a product to Favorites from Product Details  page.")
	public void tc16_verifyAddToFavourites() throws Exception{
		driver.get(abof_url);
		homepage.selectGender("Men");
		homepage.clickCategory_subcategory("Clothing","");
		plp.addProductToWishList();
		login.loginWeb(prop.getProperty("username"), prop.getProperty("password"));
		waitTillPageLoad();
		login.webLogout();
		Sassert.assertAll(); 
	}
	
}
