package com.abof.web.testscripts;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.abof.web.pageObjects.HamburgerMenu;
import com.abof.web.pageObjects.HomePage;
import com.abof.web.pageObjects.LoginPage;
import com.abof.web.pageObjects.PdpPage;
import com.abof.web.pageObjects.PlpPage;

public class HamburgerTest extends BaseTest{
	public static String abof_url= "http://www.abof.com";
	HomePage homepage = null;
	PlpPage plp = null;
	PdpPage pdp = null;
	LoginPage login = null;
	HamburgerMenu toggleMenu = null;
	@BeforeClass
	public void init() throws Exception {
		homepage = new HomePage(driver);
		plp = new PlpPage(driver);
		pdp = new PdpPage(driver);	
		login = new LoginPage(driver);
		toggleMenu = new HamburgerMenu(driver);
	}
	@Test(enabled = true, priority = 1, description="To verify Hambuger menu options Displayed or Not?")
	public void  tc1_verfyHamberMenuOptions() throws Exception{
		driver.get(abof_url);
		waitTillPageLoad();
		toggleMenu.clickHamburgerMenu();
		toggleMenu.verfyHamberMenuOptions();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 2, description="To verify Men Landing page link in Hambuger menu")
	public void  tc2_verifyMenLink() throws Exception{
		driver.get(abof_url);
		waitTillPageLoad();
		toggleMenu.clickHamburgerMenu();
		toggleMenu.validateMenLink();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 3, description="To verify Women Landing page link in Hambuger menu")
	public void  tc3_verifyWomenLink() throws Exception{
		//driver.get(abof_url);
		waitTillPageLoad();
		toggleMenu.clickHamburgerMenu();
		toggleMenu.validateWomenLink();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 4, description="To verify WhatsHot Landing page link in Hambuger menu")
	public void  tc4_verifyWhatHotLink() throws Exception{
		waitTillPageLoad();
		toggleMenu.clickHamburgerMenu();
		toggleMenu.validateWhatsHotLink();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 5, description="To verify My Orders link in Hambuger menu")
	public void  tc5_verifyMyOrdersLink() throws Exception{
		waitTillPageLoad();
		toggleMenu.clickHamburgerMenu();
		toggleMenu.validateMyOrdersLink();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 6, description="To verify My Favorites link in Hambuger menu")
	public void  tc6_verifyMyFavoritesLink() throws Exception{
		waitTillPageLoad();
		toggleMenu.clickHamburgerMenu();
		toggleMenu.validateMyFavoritesLink();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 7, description="To verify Refer a friend link in Hambuger menu")
	public void  tc7_verifyRefer_A_FriendLink() throws Exception{
		waitTillPageLoad();
		toggleMenu.clickHamburgerMenu();
		toggleMenu.validateRefer_A_FriendLink();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 8, description="To verify Return & Exchange link in Hambuger menu")
	public void  tc8_verifyReturnExchangeLink() throws Exception{
		waitTillPageLoad();
		toggleMenu.clickHamburgerMenu();
		toggleMenu.verifyReturnExchangeLink();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 9, description="To verify Abof bucks & Gift Cards link in Hambuger menu")
	public void  tc9_verifyAbofBuckGiftCardsLink() throws Exception{
		waitTillPageLoad();
		toggleMenu.clickHamburgerMenu();
		toggleMenu.verifyAbofBuckGiftCardsLink();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 10, description="To verify Abof Support link in Hambuger menu")
	public void  tc10_verifyAbofSupportLink() throws Exception{
		waitTillPageLoad();
		toggleMenu.clickHamburgerMenu();
		toggleMenu.verifyAbofSupportLink();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 11, description="To verify About Abof link in Hambuger menu")
	public void  tc11_verifyAboutAbofLink() throws Exception{
		waitTillPageLoad();
		toggleMenu.clickHamburgerMenu();
		toggleMenu.verifyAboutAbofLink();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 12, description="To verify Terms & Conditions link in Hambuger menu")
	public void  tc12_verifyTermsConditionsLink() throws Exception{
		waitTillPageLoad();
		toggleMenu.clickHamburgerMenu();
		toggleMenu.verifyTermsConditionsLink();
		Sassert.assertAll();
	}
	@Test(enabled = true, priority = 13, description="To verify Privacy policy Link in Hambuger menu")
	public void  tc13_verifyPriivacyPolicyLink() throws Exception{
		waitTillPageLoad();
		toggleMenu.clickHamburgerMenu();
		toggleMenu.verifyPrivacyPolicyLink();
		Sassert.assertAll();
	}
	
	
	
}
