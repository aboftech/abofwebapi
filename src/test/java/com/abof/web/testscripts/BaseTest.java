package com.abof.web.testscripts;

import static com.jayway.restassured.RestAssured.given;
import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.By;

import org.openqa.selenium.interactions.Actions;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.StringUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;


import com.google.common.base.Function;

import com.abof.library.GenericLib;
import com.abof.web.utils.utility;
import com.google.common.collect.Ordering;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;

import atu.testrecorder.ATUTestRecorder;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.testng.annotations.BeforeMethod;
import org.testng.asserts.SoftAssert;

public class BaseTest {

	public static WebDriver driver;
	static public String sDirPath = System.getProperty("user.dir");
	public static String sConfigFile = sDirPath + "/Config.Properties";
	DesiredCapabilities capabilities;
	static WebElement wElement;
	static Dimension dSize;
	static int sStatusCnt = 0;
	public static Properties prop = new Properties();
	public String videoPath;
	public String videoPath1;
	public DateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH-mm-ss");
	public Date date = new Date();

	public ATUTestRecorder recorder;
	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	public static SoftAssert Sassert;
	public static String browser = System.getProperty("browser");
	public static String env = System.getProperty("env");
	public static Map<String, String> emailMessageBody = new HashMap<String, String>();
	public static String className;
	private Pattern pattern;
	private Matcher matcher;

	public String actualPageTitle;
	
	protected utility utl = new utility();

	private static final String IMAGE_PATTERN = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";

	static {
		logger.info("Inside static class");
		if (StringUtils.isEmpty(browser) || StringUtils.isEmpty(env)) {
			browser = "Chrome";
			env = "prod";
			System.setProperty("browser", browser);
			System.setProperty("env", env);
			logger.info("Static class Setting browser to :" + browser);
			logger.info("Static class Setting environment to :" + env);

		}

		else {
			logger.info("Static class using browser to :" + browser);
			logger.info("Static class Setting environment to :" + env);

		}
	}

	@BeforeClass
	public void setUp() throws IOException {

		className = this.getClass().getName();
		logger.info("Control Came into beforeClass");
		File fileVideo = new File("TestVideos\\" + "Test_video" + dateFormat.format(date));
		fileVideo.mkdir();
		videoPath = fileVideo.getAbsolutePath();
		logger.info("Before class Video path is : " + videoPath);

		InputStream input = null;
		input = new FileInputStream(env + ".properties");

		// load a properties file
		prop.load(input);

		setDriver();

	}



	private void setDriver() throws MalformedURLException {
		logger.info("Setting drivers for " + browser);

		switch (browser) {
		case "Chrome":
			initChromeDriver();
			break;
		case "Firefox":
			initFirefoxDriver();
			break;
		default:
			logger.info("browser : " + browser + " is invalid, Launching Firefox as browser of choice..");
			initFirefoxDriver();
		}
	}

	private void initFirefoxDriver() {
		logger.info("Setting firefox");
		driver = new FirefoxDriver();
		logger.info("Setting firefox done");
		NXGReports.setWebDriver(driver);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(prop.getProperty("WCS_URL"));
		driver.manage().window().maximize();

	}

	@SuppressWarnings("rawtypes")
	private void initChromeDriver() throws MalformedURLException {
		if (System.getProperty("os.name").toLowerCase().contains("windows")) {
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		} else {
			System.setProperty("webdriver.chrome.driver", "chromedriver");
		}
		if(GenericLib.getCongigValue(sConfigFile, "PlatformName").equalsIgnoreCase("android")){
			DesiredCapabilities capabilities=DesiredCapabilities.android();
			capabilities.setCapability(MobileCapabilityType.BROWSER_NAME,GenericLib.getCongigValue(sConfigFile, "Browser"));
			capabilities.setCapability(MobileCapabilityType.PLATFORM,GenericLib.getCongigValue(sConfigFile, "PlatformName"));
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,"my phone");
			capabilities.setCapability(MobileCapabilityType.VERSION,"5.0.1");
			capabilities.setCapability("appPackage", GenericLib.getCongigValue(sConfigFile, "ChromePackage"));
			capabilities.setCapability("appActivity", GenericLib.getCongigValue(sConfigFile, "ChromeActivity"));
			URL url= new URL("http://127.0.0.1:4723/wd/hub");
			driver = new AndroidDriver(url, capabilities);
			NXGReports.setWebDriver(driver);
			driver.get(prop.getProperty("WCS_URL"));
			logger.info("Android driver for chrome is setup and running");
		}
		else{
			ChromeOptions options = new ChromeOptions();
			// options.addArguments("--start-fullscreen");
			// options.addExtensions (new File(“/path/to/extension.crx”));
			DesiredCapabilities caps = DesiredCapabilities.chrome();
			caps.setCapability(ChromeOptions.CAPABILITY, options);

			LoggingPreferences logPrefs = new LoggingPreferences();
			logPrefs.enable(LogType.BROWSER, Level.ALL);
			logPrefs.enable(LogType.DRIVER, Level.ALL);
			logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
			caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
			driver = new ChromeDriver(caps);
			NXGReports.setWebDriver(driver);
			driver.get(prop.getProperty("WCS_URL"));
			driver.manage().window().maximize();
		}
	}

	@BeforeMethod
	public void beforeMethod(Method caller) throws Exception {

		logger.info("Method name is : " + caller.getName());
		logger.info("Video path in Testbase is : " + videoPath1);
		recorder = new ATUTestRecorder(videoPath, caller.getName(), false);

		recorder.start();
		logger.info("Call Before Method");

		Sassert = new SoftAssert();
		// ExcelUtils.setExcelFile("SalesOrder");

	}

	@AfterMethod
	public void afterMethod(Method caller) throws Exception {

		recorder.stop();
		logger.info("Call After Method");
		// ExcelUtils.setExcelFile("SalesOrder");

	}

	public static Response postRequest(String url, String body, String WCToken, String WCTrustedToken, String userId) {
		Response PostResponse = given().contentType(ContentType.JSON).header(new Header("WCToken", WCToken))
				.header(new Header("WCTrustedToken", WCTrustedToken)).header(new Header("userId", userId)).body(body)
				.log().everything().expect().log().ifError().when().post(url);
		return PostResponse;
	}

	public static Response getRequest(String url, String WCToken, String WCTrustedToken, String userId) {
		Response getResponse = given().contentType(ContentType.JSON).header(new Header("WCToken", WCToken))
				.header(new Header("WCTrustedToken", WCTrustedToken)).header(new Header("userId", userId)).log()
				.everything().expect().log().ifError().when().get(url);
		return getResponse;
	}

	public static Response postRequestNew(String url, String Body) {
		Response getResponse = given()
				.contentType(ContentType.JSON)
				.header(new Header("Authorization", "Basic NjlDREI0QUE5NkZGNDM3QjhGQUJBMUJGQUQ4RkQxQzg="))
				.header(new Header("Content-Type", "application/x-www-form-urlencoded"))
				.log()
				.everything()
				.expect()
				.log()
				.ifError()
				.when()
				.get(url);
		return getResponse;
	}

	public static void elementStatus(WebElement element, String elementName, String checkType)

	{
		switch (checkType) {
		case "displayed":
			try {
				element.isDisplayed();
				NXGReports.addStep(elementName + " is displayed", LogAs.PASSED, null);
			} catch (Exception e) {
				sStatusCnt++;
				NXGReports.addStep(elementName + " is not displayed", LogAs.FAILED,
						new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			break;
		case "enabled":
			try {
				element.isEnabled();
				NXGReports.addStep(elementName + " is enabled", LogAs.PASSED, null);
			} catch (Exception e) {
				sStatusCnt++;
				NXGReports.addStep(elementName + " is not enabled", LogAs.FAILED,
						new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			break;
		case "selected":
			try {
				element.isSelected();
				NXGReports.addStep(elementName + " is selected", LogAs.PASSED, null);
			} catch (Exception e) {
				sStatusCnt++;
				NXGReports.addStep(elementName + " is not selected", LogAs.FAILED,
						new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			break;
		}
	}

	
	/*
	 * This method verify video is dispalyed Author : Shreekant R S
	 */

	public boolean verifyVideo(WebElement video) throws Exception {

		// WebElement ImageFile =
		// driver.findElement(By.xpath("//img[contains(@id,'Test Image')]"));

		Boolean ImagePresent = (Boolean) ((JavascriptExecutor) driver).executeScript("return arguments[0].play();",
				video);
		if (!ImagePresent) {
			System.out.println("Video not displayed.");
		} else {
			System.out.println("Video displayed.");
		}
		return ImagePresent;
	}
	
	
	public void visibilityOfElementWait(WebElement webElement, String elementName) {
		try {

			WebDriverWait	wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOf(webElement));

		} catch (Exception e) {
			NXGReports.addStep(elementName + " is not Visible", LogAs.FAILED, null);
		}
	}

	/*
	 * @author:Prerana Bhatt Description:check the list of webelemtns.
	 */
	@SuppressWarnings("rawtypes")
	public static void IsListDisplayed(List lst, String lstName) {
		try {
			if (lst.size() > 0) {
				NXGReports.addStep(lstName + " List is displayed", LogAs.PASSED, null);
			}
		} catch (Exception e) {
			NXGReports.addStep(lstName + " List is not displayed", LogAs.FAILED, null);
		}

	}

	public static void handleNavigation(WebElement ele) throws InterruptedException {
		while (!ele.isDisplayed()) {
			driver.navigate().back();
		}
	}

	/*
	 * @author:Shreekant R S Description:Explicit wait for element to load
	 */
	public static void waitForElement(WebElement ele, String expResult, String actResult) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(50, TimeUnit.SECONDS)
				.pollingEvery(250, TimeUnit.MICROSECONDS).ignoring(NoSuchElementException.class);
		Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(ele)) != null, actResult);
		NXGReports.addStep("", expResult, actResult, LogAs.INFO, null);

	}
	public static void waitForElementDisabled(WebElement ele, String expResult, String actResult) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(50, TimeUnit.SECONDS)
				.pollingEvery(250, TimeUnit.MICROSECONDS).ignoring(NoSuchElementException.class);
		Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(ele)) == null, actResult);
		NXGReports.addStep(expResult, LogAs.INFO, null);

	}
	public static void waitForElementToBeClickable(WebElement ele, String expResult, String actResult)
	{
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(50, TimeUnit.SECONDS)
				.pollingEvery(250, TimeUnit.MICROSECONDS).ignoring(NoSuchElementException.class);
		Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(ele)) != null, actResult);
		NXGReports.addStep(expResult, LogAs.INFO, null);

	}

	public static void mouseOver(WebDriver driver, WebElement toElemet) {

		try
		{
			Actions action1=new Actions(driver) ;
			action1.moveToElement(toElemet).build().perform();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}




	/*
	 * @author:Shreekant R S Description:wait till element display and click it
	 */

	public static void isElementDisplayed(WebElement ele, String elementName, int pollingMiliSecs, int timoutSec,
			String assertionType) {

		boolean eleDispalyed = false;

		try {

			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(timoutSec, TimeUnit.SECONDS)
					.pollingEvery(pollingMiliSecs, TimeUnit.MICROSECONDS).ignoring(NoSuchElementException.class);

			// Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(ele))
			// != null, actResult);

			wait.until(ExpectedConditions.visibilityOf(ele));

			ele.isDisplayed();
			eleDispalyed = true;

		}

		catch (Exception e) {

			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);

			switch (assertionType) {
			case "HardAssertion":
				NXGReports.addStep("Verify element" + elementName, "User should able to see in UI ",
						"User is able to see in UI ", LogAs.PASSED, null);
				Assert.assertTrue(eleDispalyed, elementName + "is not dispalying " + sw.toString());
				break;
			case "SoftAssertion":
				NXGReports.addStep("Verify element" + elementName, "User should able to see in UI ",
						"User is not able to see in UI ", LogAs.PASSED, null);
				Sassert.assertTrue(eleDispalyed, elementName + "is not dispalying " + sw.toString());
				break;

			}

		}

	}

	/*
	 * @author:Shreekant R S Description:wait till element display and click it
	 */

	public static void isElementEnabled(WebElement ele, String elementName, int pollingMiliSecs, int timoutSec,
			String assertionType) {

		boolean eleDispalyed = false;
		try {
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
			wait.pollingEvery(250, TimeUnit.MILLISECONDS);
			wait.withTimeout(2, TimeUnit.SECONDS);
			wait.ignoring(NoSuchElementException.class); // We need to ignore
															// this
															// exception.

			Function<WebDriver, WebElement> function = new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver arg0) {
					System.out.println("Checking for the object!!");
					boolean element = ele.isEnabled();
					if (!(element)) {
						System.out.println("A new dynamic object is found.");
					}
					return ele;
				}
			};

			wait.until(function);

			eleDispalyed = ele.isEnabled();
		}

		catch (Exception e) {

			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);

			switch (assertionType) {
			case "HardAssertion":
				NXGReports.addStep("Verify element " + elementName, "User should able to see in UI ",
						"User is able to see in UI ", LogAs.PASSED, null);
				Assert.assertTrue(eleDispalyed, elementName + "is not dispalying " + sw.toString());
				break;
			case "SoftAssertion":
				NXGReports.addStep("Verify element " + elementName, "User should able to see in UI ",
						"User is not able to see in UI ", LogAs.FAILED, null);
				Sassert.assertTrue(eleDispalyed, elementName + "is not dispalying " + sw.toString());
				break;

			}

		

		}
	}

	
	/*
	 * This method verify image is displayed Author : Shreekant R S
	 */

	public boolean CheckImage(WebElement ImageFile) throws Exception {

		Boolean ImagePresent = (Boolean) ((JavascriptExecutor) driver).executeScript(
				"return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0",
				ImageFile);
		if (!ImagePresent) {
			System.out.println("Image not displayed.");
		} else {
			System.out.println("Image displayed.");
		}
		return ImagePresent;
	}

	
	
	/*
	 * @author:Shreekant R S Description:wait till element display and click it
	 */

	public static void waitAndClick(WebElement ele, String buttonName, int pollingMiliSecs, int timoutSec) {

		try {

			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(timoutSec, TimeUnit.SECONDS)
					.pollingEvery(pollingMiliSecs, TimeUnit.MICROSECONDS).ignoring(NoSuchElementException.class);

			// Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(ele))
			// != null, actResult);

			Sassert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(ele)) != null,
					"User is able to click on " + buttonName);

			ele.click();
			NXGReports.addStep("Click on " + buttonName, "User should able to click on " + buttonName,
					"User is able to click on " + buttonName, LogAs.PASSED, null);
		}

		catch (Exception e) {

			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);

			NXGReports.addStep("Click on " + buttonName, "User should able to click on " + buttonName,
					"User is not able to click on " + buttonName, LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			Assert.assertFalse(true, "User is not able to click on " + buttonName + sw.toString());

		}

	}

	/*
	 * @author:Shreekant R S Description:Enter text in text box
	 */

	public static void waitAndEnterText(WebElement ele, String textBox, String textToEnter, int pollingMiliSecs,
			int timoutSec) {

		try {

			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(timoutSec, TimeUnit.SECONDS)
					.pollingEvery(pollingMiliSecs, TimeUnit.MICROSECONDS).ignoring(NoSuchElementException.class);

			// Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(ele))
			// != null, actResult);

			Sassert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(ele)) != null,
					"Not able to find  " + textBox);

			ele.click();
			NXGReports.addStep("Enter text into " + textBox, "User should able to enter text " + textToEnter,
					"User is able to enter text " + textToEnter, LogAs.PASSED, null);
		}

		catch (Exception e) {

			NXGReports.addStep("Enter text into " + textBox, "User should able to enter text " + textToEnter,
					"User is not able to enter text " + textToEnter, LogAs.FAILED, null);

		}

	}

	/*
	 * @author:Shreekant R S Description:wait till java script gives ready
	 * status
	 */

	public static void waitTillPageLoad()  {

		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

			public Boolean apply(WebDriver driver) {
				logger.info("Waiting for page load conditon : waitTillPageLoad");
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 240);
		wait.until(pageLoadCondition);
	}










	/*
	 * This method verify page title Author : Shreekant R S
	 */
	public boolean verifyPageTitle(String expectedPageTitle) {

		actualPageTitle = driver.getTitle();
		System.out.println("Expected Page title is:" + expectedPageTitle);
		;
		System.out.println("Actual Page title is:" + actualPageTitle);
		boolean pageTitelValue = actualPageTitle.equals(expectedPageTitle);
		System.out.println("Page title value:" + pageTitelValue);
		return actualPageTitle.equals(expectedPageTitle);

	}



	/*
	 * This method scroll till web element  Author : Shreekant R S
	 */

	
	public void mousescrollTillEle(WebElement element) {
		JavascriptExecutor js = ((JavascriptExecutor) driver);

		js.executeScript("arguments[0].scrollIntoView(true);", element);
	}


	public boolean findFilterOptionsSortOrder(List<WebElement> options) {
		boolean isSorted;
		try
		{
			ArrayList<String> list=new ArrayList<String>();
			for(int i=0;i<options.size();i++)
			{
				list.add(options.get(i).getText().toUpperCase());
				//logger.info("Options : "+options.get(i).getText().toUpperCase());
			}

			isSorted = Ordering.natural().isOrdered(list);
			//logger.info("IsSorted : "+isSorted);
			return isSorted;
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	public boolean findPriceFilterOptionsSortedOrder(List<WebElement> options){
		boolean flag=true;
		try{
			int i, interval;
			for(i=0;i<options.size();i++){
				String[] str= options.get(i).getText().split("-");
				if(!str[0].contains("More"))
				{
					//logger.info(str[1]+ "-" +str[0]);
					interval=(Integer.parseInt(str[1])+1)-Integer.parseInt(str[0]);
					System.out.println(interval);
					if(interval!=500)
					{
						if(interval!=1000)
						{
							flag=false;
							//logger.info("1000 : "+flag);
							break;
						}
					}
				}
				else
				{
					//logger.info("More than 4000");
					break;
				}
			}
			return flag;
		}
		catch(Exception e){
			throw e;
		}
	}

	public boolean findSizeFilterOptionsSortedOrder(List<WebElement> options){
		boolean isSorted=true;
		List<String> list=new ArrayList<String>();
		for(int i=0;i<options.size();i++)
		{
			if(options.get(i).getText().equals("XS"))
				list.add("0a");
			else if(options.get(i).getText().equals("S"))
				list.add("0b");
			else if(options.get(i).getText().equals("M"))
				list.add("0c");
			else if(options.get(i).getText().equals("L"))
				list.add("0d");
			else if(options.get(i).getText().equals("XL"))
				list.add("0e");
			else if(options.get(i).getText().equals("XXL"))
				list.add("0f");
			else if(options.get(i).getText().equals("XXXL"))
				list.add("0g");
			else if(options.get(i).getText().equals("SM"))
				list.add("0h");
			else if(options.get(i).getText().equals("ML"))
				list.add("0i");
			else if(options.get(i).getText().equals("LXL"))
				list.add("0j");
			else if(options.get(i).getText().equals("Free Size"))
				list.add("0k");
			else if(options.get(i).getText().equals("No Size"))
				list.add("0l");
			else 
				list.add(options.get(i).getText());
		}
		try{
			List<String> sortedListOfStrings = list.stream()
					.sorted()
					.collect(Collectors.toList());
			for (int i = 0; i < list.size()-1; i++) {
				if(list.get(i).equals(sortedListOfStrings.get(i))&&isSorted==true){
					isSorted=true;
				}else{
					isSorted=false;
				}
			}

			return isSorted;
		}catch(Exception e){
			throw e;
		}
	}
	public boolean isAscending(List<String> list)
	{
		boolean sorted = false;
		try
		{
			sorted = Ordering.natural().isOrdered(list);
			if(sorted)
				return true;
			else
				return false;
		}
		catch(Exception e)
		{
			return false;
		}		
	}

	public boolean isDescending(List<String> list)
	{
		boolean sorted = false;
		try
		{					
			for (int i = 0; i < list.size()-1; i++) 
			{
				if (Integer.parseInt(list.get(i)) >= Integer.parseInt(list.get(i+1))) 
					sorted=true;
				else
					sorted=false;
			}
			return sorted;
		}
		catch(Exception e)
		{
			return false;
		}		
	}


	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
