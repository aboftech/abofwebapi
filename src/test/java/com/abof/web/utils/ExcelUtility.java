package com.abof.web.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.abof.api.tests.ThresholdMonitoringAPI;
import com.abof.api.tests.ThresholdMonitoringAPI.entryCount4Facet;
import com.abof.web.testscripts.BaseTest;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;

public class ExcelUtility {
	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	public static void createDataFile(File sourceFile, File destFile) throws IOException {
	     if(!destFile.exists()) {
	      destFile.createNewFile();
	     }

	     FileChannel source = null;
	     FileChannel destination = null;
	     try {
	      source = new RandomAccessFile(sourceFile,"rw").getChannel();
	      destination = new RandomAccessFile(destFile,"rw").getChannel();

	      long position = 0;
	      long count    = source.size();

	      source.transferTo(position, count, destination);
	     }
	     finally {
	      if(source != null) {
	       source.close();
	      }
	      if(destination != null) {
	       destination.close();
	      }
	    }
	 }
	public static void moveYesterdayCategoryURLs(String filePath, String fileName){
		String  todayCategory="", yDayCount="", oldCount="";
		int big;
		DataFormatter formatter = new DataFormatter();
		try{
						// Create an object of File class to open xlsx file
						File file = new File(filePath + "\\" + fileName + ".xlsx");
						// Create an object of FileInputStream class to read excel file
						FileInputStream inputStream = new FileInputStream(file);
						Workbook workbook = null;
						// If it is xlsx file then create object of XSSFWorkbook class
						workbook = new XSSFWorkbook(inputStream);
						// Read sheet inside the workbook by its name
						Sheet sheet = workbook.getSheet("Sheet2");
						
						Row count = sheet.getRow(100);
							yDayCount = formatter.formatCellValue(count.getCell(2));
							//logger.info("Yesterday count: "+yDayCount);
							oldCount = formatter.formatCellValue(count.getCell(1));
							//logger.info("Old count: "+oldCount);
							Cell oldDataCont = count.createCell(1);
							oldDataCont.setCellValue(yDayCount);
						if(Integer.parseInt(oldCount)>Integer.parseInt(yDayCount)){
							big=Integer.parseInt(oldCount);
							//logger.info(big+" is Bigger value");
						}
						else{
							big=Integer.parseInt(yDayCount);
							//logger.info(big+" is Bigger value");
						}
						//logger.info("Move old data: yDayCategories");
						for(int i=1; i<=big; i++){
							Row category_Row = sheet.getRow(i);
							if(i<=Integer.parseInt(yDayCount)){
								todayCategory = category_Row.getCell(2).getStringCellValue().toString();	
								//Row row1 = sheet.getRow(i);
								Cell oldData = category_Row.createCell(1);
								oldData.setCellValue(todayCategory);
							} else{
								Cell oldData = category_Row.createCell(1);
								oldData.setCellValue("  ");
							}
							
						}	 
						    inputStream.close();
	
						    //Create an object of FileOutputStream class to create write data in excel file
						    FileOutputStream outputStream = new FileOutputStream(file);
	
						    //write data in the excel file
	
						    workbook.write(outputStream);
	
						    //close output stream
	
						    outputStream.close();
						    NXGReports.addStep("Move categories under in Categories.xlsx.","", "It should move old data in Categories.xlsx","It moved old data in Categories.xlsx successfully", LogAs.PASSED, null);
						    
						}	    
						catch(Exception exp){
							logger.info("Exception occured: due to null cell. "+exp.getMessage());
							exp.printStackTrace();	
							NXGReports.addStep("Move categories under in Categories.xlsx.","", "It should move old data in Categories.xlsx","Moving old data in Categories.xlsx failed due to exception: "+exp.getMessage(), LogAs.FAILED, null);
						}
	}
	@SuppressWarnings("resource")
	public static void saveCategoriesFromAPI(String filePath, String fileName, List<String> categories) {
		String yDayCount="" ;
		int big;
		DataFormatter formatter = new DataFormatter();
		try{
			// Create an object of File class to open xlsx file
			File file = new File(filePath + "\\" + fileName + ".xlsx");
			// Create an object of FileInputStream class to read excel file
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
			// If it is xlsx file then create object of XSSFWorkbook class
			workbook = new XSSFWorkbook(inputStream);
			// Read sheet inside the workbook by its name
			Sheet sheet = workbook.getSheet("Sheet2");
			Row row100 = sheet.getRow(100);
			yDayCount = formatter.formatCellValue(row100.getCell(2));
			if(Integer.parseInt(yDayCount)<categories.size()){
				big = categories.size();
				//logger.info(big+ " is big.");
 			} else{
 				big = Integer.parseInt(yDayCount);
				//logger.info(big+ " is big.");
			}
			for(int i=1; i<=big; i++){
				Row category_Row = sheet.getRow(i);
				Cell newData = category_Row.createCell(2);
				if(i<=categories.size()){
					newData.setCellValue(categories.get(i-1));
				} else{
					newData.setCellValue("  ");					
				}
			}
			Cell newData = row100.createCell(2);
			newData.setCellValue(categories.size()+"");
			inputStream.close();
			
		    //Create an object of FileOutputStream class to create write data in excel file
		    FileOutputStream outputStream = new FileOutputStream(file);

		    //write data in the excel file

		    workbook.write(outputStream);

		    //close output stream

		    outputStream.close();
			NXGReports.addStep("Save today categories in Categories.xlsx.","", "It should save today categories in Categories.xlsx","It saved today categories in Categories.xlsx successfully", LogAs.PASSED, null);
	    }
		catch(Exception exp){
			//logger.info("Exception occured: due to null cell. "+exp.getMessage());
			exp.printStackTrace();	
			NXGReports.addStep("Save today categories under in Categories.xlsx.","", "It should save today categories in Categories.xlsx","Saving today categories in Categories.xlsx failed due to exception: "+exp.getMessage(), LogAs.FAILED, null);
		}
	}
	
	@SuppressWarnings("resource")
	public static void verifyDroppedCategories(String filePath, String fileName){
		String msg="", yDayCount="", todayCount="", value="";
		List<String> oldList = new ArrayList<String>();
		List<String> newList = new ArrayList<String>();
		List<String> notPresent = new ArrayList<String>();
		DataFormatter formatter = new DataFormatter();
		try{
						// Create an object of File class to open xlsx file
						File file = new File(filePath + "\\" + fileName + ".xlsx");
						// Create an object of FileInputStream class to read excel file
						FileInputStream inputStream = new FileInputStream(file);
						Workbook workbook = null;
						// If it is xlsx file then create object of XSSFWorkbook class
						workbook = new XSSFWorkbook(inputStream);
						// Read sheet inside the workbook by its name
						Sheet sheet = workbook.getSheet("Sheet2");
						Row row100 = sheet.getRow(100);
						yDayCount = formatter.formatCellValue(row100.getCell(1));
						todayCount = formatter.formatCellValue(row100.getCell(2));
						for(int o=1; o<=Integer.parseInt(yDayCount); o++){
				        	Row dataRange = sheet.getRow(o);
				        	value =formatter.formatCellValue(dataRange.getCell(1));
				        	oldList.add(value);
				        }
				        for(int n=1; n<=Integer.parseInt(todayCount); n++){
				        	Row dataRange = sheet.getRow(n);
				        	value =formatter.formatCellValue(dataRange.getCell(2));
				        	newList.add(value);
				        }
						notPresent = new ArrayList<String>(oldList);
						notPresent.removeAll(newList);
						
						
							//Close input stream
						    inputStream.close();
						    //Create an object of FileOutputStream class to create write data in excel file
						    FileOutputStream outputStream = new FileOutputStream(file);

						    //write data in the excel file
						    workbook.write(outputStream);

						    //close output stream
						    outputStream.close();
			if(notPresent.isEmpty()){
				NXGReports.addStep("Verify dropped categories in Categories.xlsx.","", "It should verify any dropped categories today in Categories.xlsx","No Catgories Dropped today.", LogAs.PASSED, null);
			} else{
				for(int i=1; i<=notPresent.size(); i++){
					msg = msg+ i+") "+notPresent.get(i-1)+"\n";
				}
				//logger.info("Dropped categories: "+msg);
				BaseTest.emailMessageBody.put("Dropped categories: ", msg);
		    	NXGReports.addStep("Verify dropped categories in Categories.xlsx.","", "It should verify any dropped categories today in Categories.xlsx","Dropped categories: "+msg, LogAs.FAILED, null);
			}
	    }
		catch(Exception exp){
			exp.printStackTrace();	
			NXGReports.addStep("Verify dropped categories under in Categories.xlsx.","", "It should verify any dropped categories today in Categories.xlsx","Saving today categories in Categories.xlsx failed due to exception: "+exp.getMessage(), LogAs.FAILED, null);
		}
	}

	@SuppressWarnings("resource")
	public static void verifyNewAddedCategories(String filePath, String fileName){
		String msg="", yDayCount="", todayCount="", value="";
		List<String> oldList = new ArrayList<String>();
		List<String> newList = new ArrayList<String>();
		List<String> notPresent = new ArrayList<String>();
		DataFormatter formatter = new DataFormatter();
		try{
						// Create an object of File class to open xlsx file
						File file = new File(filePath + "\\" + fileName + ".xlsx");
						// Create an object of FileInputStream class to read excel file
						FileInputStream inputStream = new FileInputStream(file);
						Workbook workbook = null;
						// If it is xlsx file then create object of XSSFWorkbook class
						workbook = new XSSFWorkbook(inputStream);
						// Read sheet inside the workbook by its name
						Sheet sheet = workbook.getSheet("Sheet2");
						Row row100 = sheet.getRow(100);
						yDayCount = formatter.formatCellValue(row100.getCell(1));
						todayCount = formatter.formatCellValue(row100.getCell(2));
						for(int o=1; o<=Integer.parseInt(yDayCount); o++){
				        	Row dataRange = sheet.getRow(o);
				        	value =formatter.formatCellValue(dataRange.getCell(1));
				        	oldList.add(value);
				        }
				        for(int n=1; n<=Integer.parseInt(todayCount); n++){
				        	Row dataRange = sheet.getRow(n);
				        	value =formatter.formatCellValue(dataRange.getCell(2));
				        	newList.add(value);
				        }
						notPresent = new ArrayList<String>(newList);
						notPresent.removeAll(oldList);
												
							//Close input stream
						    inputStream.close();
						    //Create an object of FileOutputStream class to create write data in excel file
						    FileOutputStream outputStream = new FileOutputStream(file);

						    //write data in the excel file
						    workbook.write(outputStream);

						    //close output stream
						    outputStream.close();
			if(notPresent.isEmpty()){
				NXGReports.addStep("Verify new added categories in Categories.xlsx.","", "It should verify any new added categories today in Categories.xlsx","No Catgories added today.", LogAs.PASSED, null);
			} else{
				for(int i=1; i<=notPresent.size(); i++){
					String filename = notPresent.get(i-1).toString().replace("/", "");
					String[] result = (notPresent.get(i-1).toString()).split("/");
					
					File source=new File("C:\\Opkey\\MonitoringThresholdQuantity\\"+result[1]+"\\"+result[1]+"new-in.xlsx");
					File destination=new File("C:\\Opkey\\MonitoringThresholdQuantity\\"+result[1]+"\\"+filename+".xlsx");
					createDataFile(source,destination);
					msg = msg+ i+") "+notPresent.get(i-1)+"\n";
				}
				//logger.info("New added categories: "+msg);
				BaseTest.emailMessageBody.put("New Added categories: ", msg);
		    	NXGReports.addStep("Verify new added categories in Categories.xlsx.","", "It should verify any new added categories today in Categories.xlsx","New Added categories: "+msg, LogAs.FAILED, null);
			}
	    }
		catch(Exception exp){
			//logger.info("Exception occured: due to null cell. "+exp.getMessage());
			exp.printStackTrace();	
			NXGReports.addStep("Verify dropped categories under in Categories.xlsx.","", "It should verify any dropped categories today in Categories.xlsx","Saving today categories in Categories.xlsx failed due to exception: "+exp.getMessage(), LogAs.FAILED, null);
		}
	}
	
	@SuppressWarnings("resource")
	public static List<String> commonCategories(String filePath, String fileName){
		DataFormatter formatter = new DataFormatter();
		List<String> oldList = new ArrayList<String>();
		List<String> newList = new ArrayList<String>();
		List<String> common = new ArrayList<String>();
		Row row100, dataRange;
		String oldCount="", newCount="", value="";
		try{
			fileName = fileName.replace("/", "");
			// Create an object of File class to open xlsx file
			File file = new File(filePath + "\\" + fileName + ".xlsx");
				// Create an object of FileInputStream class to read excel file
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
				// If it is xlsx file then create object of XSSFWorkbook class
			workbook = new XSSFWorkbook(inputStream);
				// Read sheet inside the workbook by its name
			Sheet sheet = workbook.getSheet("Sheet2");
			row100 = sheet.getRow(100);
	        Cell oldCountCell = row100.getCell(1);
	        oldCount = formatter.formatCellValue(oldCountCell);
	        Cell newCountCell = row100.getCell(2);
	        newCount = formatter.formatCellValue(newCountCell);
	        //logger.info("Get count of "+sheet+" options");
			for(int o=1; o<=Integer.parseInt(oldCount); o++){
	        	dataRange = sheet.getRow(o);
	        	value =formatter.formatCellValue(dataRange.getCell(1));
	        	if(!value.equalsIgnoreCase("/men/sale")&&!value.equalsIgnoreCase("/women/sale"))
	        	oldList.add(value);
	        }
	        for(int n=1; n<=Integer.parseInt(newCount); n++){
	        	dataRange = sheet.getRow(n);
	        	value =formatter.formatCellValue(dataRange.getCell(2));
	        	if(!value.equalsIgnoreCase("/men/sale")&&!value.equalsIgnoreCase("/women/sale"))
	        	newList.add(value);
	        }
			common = new ArrayList<String>(oldList);
			common.retainAll(newList);
			return common;
		}	catch(Exception es){
			return common;
		}
	}
	@SuppressWarnings("resource")
	public static String moveFilterOptionsData(String filePath, String fileName, String sheet){
		String  oldCount="", yDayCount="", yDayOptionName="", yDayOptionQty="";
		int big;
		DataFormatter formatter = new DataFormatter();
		try{
			fileName = fileName.replace("/", "");
				// Create an object of File class to open xlsx file
			File file = new File(filePath + "\\" + fileName + ".xlsx");
				// Create an object of FileInputStream class to read excel file
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
				// If it is xlsx file then create object of XSSFWorkbook class
			workbook = new XSSFWorkbook(inputStream);
				// Read sheet inside the workbook by its name
			Sheet sheet1 = workbook.getSheet(sheet);
			Row countRow = sheet1.getRow(100);
			oldCount = formatter.formatCellValue(countRow.getCell(1));
			yDayCount = formatter.formatCellValue(countRow.getCell(3));
			//logger.info("Yesterday count: "+yDayCount);
	        if(Integer.parseInt(oldCount)>Integer.parseInt(yDayCount)){
	        	big=Integer.parseInt(oldCount);
	        	for(int c=0; c<big; c++){
		        	Row option_Row = sheet1.getRow(c+1);
						yDayOptionName = formatter.formatCellValue(option_Row.getCell(3));
						yDayOptionQty = formatter.formatCellValue(option_Row.getCell(4));
						if(c<Integer.parseInt(yDayCount)){
							Cell oldOptionData = option_Row.createCell(1);
							oldOptionData.setCellValue(yDayOptionName);
							Cell oldOptionQtyData = option_Row.createCell(2);
							oldOptionQtyData.setCellValue(yDayOptionQty);
						}else{
							Cell oldOptionData = option_Row.createCell(1);
							oldOptionData.setCellValue(" ");
							Cell oldOptionQtyData = option_Row.createCell(2);
							oldOptionQtyData.setCellValue(" ");
						}
		        }
	        }
	        else{
	        	big=Integer.parseInt(yDayCount);
	        	for(int c=0; c<big; c++){
		        	Row option_Row = sheet1.getRow(c+1);
						yDayOptionName = formatter.formatCellValue(option_Row.getCell(3));
						yDayOptionQty = formatter.formatCellValue(option_Row.getCell(4));
						Cell oldOptionData = option_Row.createCell(1);
						oldOptionData.setCellValue(yDayOptionName);
						Cell oldOptionQtyData = option_Row.createCell(2);
						oldOptionQtyData.setCellValue(yDayOptionQty);
						
		        }
	        }

			Cell oldData = countRow.createCell(1);
	        oldData.setCellValue(yDayCount);
			inputStream.close();
			    //Create an object of FileOutputStream class to create write data in excel file
			FileOutputStream outputStream = new FileOutputStream(file);
				//write data in the excel file
		    workbook.write(outputStream);
			    //close output stream
		    outputStream.close();
		    //NXGReports.addStep("Move filter options under "+sheet+".", sheet, "It should move filter options under"+sheet+".","Moving of filter options under "+sheet+" is successful.", LogAs.PASSED, null);
		    return "";
		}	    
		catch(Exception exp){
			//NXGReports.addStep("Move filter options under "+sheet+".", sheet, "It should move filter options under"+sheet+".","Moving of filter options under "+sheet+" is failed, due to: "+exp.getMessage(), LogAs.FAILED, null);
			return "Moving of filter options under <b>"+sheet+"</b> is failed, due to: "+exp.getMessage()+"\n";
		}
	}

	@SuppressWarnings({ "resource" })
	public static String saveFilterOptionsQtyDataInExcel(String filePath, String fileName, List<entryCount4Facet> genderData, List<entryCount4Facet> typeData, List<entryCount4Facet> priceData, List<entryCount4Facet> sizeData, List<entryCount4Facet> manufacturerNameData){
		String count="", url="";
		int big;
		DataFormatter formatter = new DataFormatter();
		try{
			fileName = fileName.replace("/", "");
			url = "\\"+fileName;
			File file = new File(filePath + "\\" + fileName + ".xlsx");
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
				workbook = new XSSFWorkbook(inputStream);
				Sheet genderSheet = workbook.getSheet("Gender");
				Row row100 = genderSheet.getRow(100);
				count = formatter.formatCellValue(row100.getCell(3));
				if(Integer.parseInt(count)>genderData.size()){
					big = Integer.parseInt(count);
				} else{
					big = genderData.size();
				}
			for(int i=0; i<big; i++){
					Row row = genderSheet.getRow(i+1);
					if(i<genderData.size()){
						Cell name = row.createCell(3);
				        name.setCellValue(genderData.get(i).getLabel());
				        //logger.info("Gender option : "+genderData.get(i).getLabel());
				        Cell qty = row.createCell(4);
				        qty.setCellValue(genderData.get(i).getCount()+"");
				        //logger.info("Gender Quantity: "+genderData.get(i).getCount()+"");
					} else{
						Cell name = row.createCell(3);
			        	//logger.info("Gender option : "+genderOptions.get(i).getText());
				        name.setCellValue("  ");
				        Cell qty = row.createCell(4);
				        qty.setCellValue("  ");
				        //logger.info("Gender option: "+genderOptions.get(i).getText()+" "+genderOptionsQty.get(i).getText());
					}
			        if(i==genderData.size()-1){
			        	Row countRow = genderSheet.getRow(100);
			        	Cell countCell = countRow.createCell(3);
			        	countCell.setCellValue(""+genderData.size());
			        }
			        
			}
			Sheet productSheet = workbook.getSheet("Product");
			Row prow100 = productSheet.getRow(100);
			count = formatter.formatCellValue(prow100.getCell(3));
			if(Integer.parseInt(count)>typeData.size()){
				big = Integer.parseInt(count);
			} else{
				big = typeData.size();
			}
			for(int i=0; i<big; i++){
				Row row = productSheet.getRow(i+1);
				if(i<typeData.size()){
			        Cell name = row.createCell(3);
			        name.setCellValue(typeData.get(i).getLabel());
			        //logger.info("Product option : "+typeData.get(i).getLabel());
			        Cell qty = row.createCell(4);
			        qty.setCellValue(typeData.get(i).getCount()+"");
			        //logger.info("Product option : "+typeData.get(i).getCount()+"");
				} else{
					Cell name = row.createCell(3);
			        name.setCellValue("  ");
			        Cell qty = row.createCell(4);
			        qty.setCellValue("  ");
				}
			        if(i==typeData.size()-1){
		        	Row countRow = productSheet.getRow(100);
		        	Cell countCell = countRow.createCell(3);
		        	countCell.setCellValue(""+typeData.size());
		        }
			}
			Sheet priceSheet = workbook.getSheet("Price");
			Row prrow100 = priceSheet.getRow(100);
			count = formatter.formatCellValue(prrow100.getCell(3));
			if(Integer.parseInt(count)>priceData.size()){
				big = Integer.parseInt(count);
			} else{
				big = priceData.size();
			}
			for(int i=0; i<big; i++){
				Row row = priceSheet.getRow(i+1);
				if(i<priceData.size()){
			        Cell name = row.createCell(3);
			        String value = priceData.get(i).getLabel();
			        value = value.replace("({*", "0 to");
			        value = value.replace("*})", "or more");
			        value = value.replace("({", "");
			        value = value.replace(")", "");
			        value = value.replace("}", "");
			        name.setCellValue(value);
			        Cell qty = row.createCell(4);
			        qty.setCellValue(priceData.get(i).getCount());
			         
			        
				} else{
					Cell name = row.createCell(3);
			        name.setCellValue("  ");
			        Cell qty = row.createCell(4);
			        qty.setCellValue("  ");
				}
			        if(i==priceData.size()-1){
		        	Row countRow = priceSheet.getRow(100);
		        	Cell countCell = countRow.createCell(3);
		        	countCell.setCellValue(""+priceData.size());
		        }
			}

			Sheet sizeSheet = workbook.getSheet("Size");
			Row srow100 = sizeSheet.getRow(100);
			count = formatter.formatCellValue(srow100.getCell(3));
			if(Integer.parseInt(count)>sizeData.size()){
				big = Integer.parseInt(count);
			} else{
				big = sizeData.size();
			}
			for(int i=0; i<big; i++){
				Row row = sizeSheet.getRow(i+1);
				if(i<sizeData.size()){
			        Cell name = row.createCell(3);
			        name.setCellValue(sizeData.get(i).getLabel());
			        Cell qty = row.createCell(4);
			        qty.setCellValue(sizeData.get(i).getCount()+"");
				} else{
					Cell name = row.createCell(3);
			        name.setCellValue("  ");
			        Cell qty = row.createCell(4);
			        qty.setCellValue("  ");
				}
		        if(i==sizeData.size()-1){
		        	Row countRow = sizeSheet.getRow(100);
		        	Cell countCell = countRow.createCell(3);
		        	countCell.setCellValue(""+sizeData.size());
		        }
			}

			Sheet brandSheet = workbook.getSheet("Brand");
			Row brow100 = brandSheet.getRow(100);
			count = formatter.formatCellValue(brow100.getCell(3));
			if(Integer.parseInt(count)>manufacturerNameData.size()){
				big = Integer.parseInt(count);
			} else{
				big = manufacturerNameData.size();
			}
			for(int i=0; i<big; i++){
				Row row = brandSheet.getRow(i+1);
				if(i<manufacturerNameData.size()){
					Cell name = row.createCell(3);
			        name.setCellValue(manufacturerNameData.get(i).getLabel());
			        Cell qty = row.createCell(4);
			        qty.setCellValue(manufacturerNameData.get(i).getCount()+"");
				} else{
					Cell name = row.createCell(3);
			        name.setCellValue("  ");
			        Cell qty = row.createCell(4);
			        qty.setCellValue("  ");
				}
		        if(i==manufacturerNameData.size()-1){
		        	Row countRow = brandSheet.getRow(100);
		        	Cell countCell = countRow.createCell(3);
		        	countCell.setCellValue(""+manufacturerNameData.size());
		        }
			}
			//Close input stream
		    inputStream.close();
		    //Create an object of FileOutputStream class to create write data in excel file
		    FileOutputStream outputStream = new FileOutputStream(file);

		    //write data in the excel file
		    workbook.write(outputStream);

		    //close output stream
		    outputStream.close();
		    //NXGReports.addStep("Save filter options in "+ filePath+url+".xlsx"+".", filePath+url+".xlsx","It should save filter options in "+ filePath+url+".xlsx"+".","Filter options Saved successfully in "+ filePath+url+".xlsx", LogAs.PASSED, null);
		    return "";
		} catch (FileNotFoundException e) {
            e.printStackTrace();
            //NXGReports.addStep("Save filter options in "+ filePath+url+".xlsx"+".",  filePath+url+".xlsx", "It should save filter options in "+ filePath+url+".xlsx"+".", filePath+url+".xlsx"+" File Not Found Exception occured. "+e.getMessage(), LogAs.FAILED, null);
            return filePath+url+".xlsx"+" File Not Found Exception occured. "+e.getMessage();
        } catch( Exception exp){
			exp.printStackTrace();
			//NXGReports.addStep("Save filter options in "+ filePath+url+".xlsx"+".", filePath+url+".xlsx","It should save filter options in "+ filePath+url+".xlsx"+".","Exception occured. "+exp.getMessage(), LogAs.FAILED, null);
			return "Exception occured. "+exp.getMessage()+"\n";
		}
	}
	@SuppressWarnings("resource")
	public static String droppedFilterOptionsData(String filePath, String fileName, String sheet) {
		String msg="", yDayCount="", todayCount="", value="", url="";
		List<String> oldList = new ArrayList<String>();
		List<String> newList = new ArrayList<String>();
		List<String> notPresent = new ArrayList<String>();
		DataFormatter formatter = new DataFormatter();
		try{
			fileName = fileName.replace("/", "");
			url = "\\"+fileName;
			// Create an object of File class to open xlsx file
			File file = new File(filePath + "\\" + fileName + ".xlsx");
			// Create an object of FileInputStream class to read excel file
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
			// If it is xlsx file then create object of XSSFWorkbook class
			workbook = new XSSFWorkbook(inputStream);
			// Read sheet inside the workbook by its name
			Sheet sheet1 = workbook.getSheet(sheet);
						
						Row row100 = sheet1.getRow(100);
						yDayCount = formatter.formatCellValue(row100.getCell(1));
						todayCount = formatter.formatCellValue(row100.getCell(3));
						for(int o=1; o<=Integer.parseInt(yDayCount); o++){
				        	Row dataRange = sheet1.getRow(o);
				        	value =formatter.formatCellValue(dataRange.getCell(1));
				        	oldList.add(value);
				        }
				        for(int n=1; n<=Integer.parseInt(todayCount); n++){
				        	Row dataRange = sheet1.getRow(n);
				        	value =formatter.formatCellValue(dataRange.getCell(3));
				        	newList.add(value);
				        }
						notPresent = new ArrayList<String>(oldList);
						notPresent.removeAll(newList);

						
						
						//Close input stream
					    inputStream.close();
					    //Create an object of FileOutputStream class to create write data in excel file
					    FileOutputStream outputStream = new FileOutputStream(file);

					    //write data in the excel file
					    workbook.write(outputStream);

					    //close output stream
					    outputStream.close();
					    if(notPresent.isEmpty()){
					    	//NXGReports.addStep("Verify dropped filter options in "+ filePath+url+".xlsx"+".", filePath+url+".xlsx","It should verify dropped filter options in "+ filePath+url+".xlsx"+".","No Filter options dropped in "+sheet, LogAs.PASSED, null);
					    	return "";
					    } else{
					    	for(int i=0; i<notPresent.size(); i++){
					    		msg = msg+(i+1)+") "+notPresent.get(i)+"\n";
					    	}
					    	//NXGReports.addStep("Verify dropped filter options in "+ filePath+url+".xlsx"+".", filePath+url+".xlsx","It should verify dropped filter options in "+ filePath+url+".xlsx"+".","Dropper Filter options dropped in "+sheet+" :"+msg, LogAs.FAILED, null);
					    	return "<b>"+sheet+"</b> :"+msg+"<b>Dropped</b> "+"\n";
					    }
					} catch (FileNotFoundException e) {
			            e.printStackTrace();
			            //NXGReports.addStep("Verify dropped filter options in "+ filePath+url+".xlsx"+".",  filePath+url+".xlsx", "It should verify dropped filter options in "+ filePath+url+".xlsx"+".", filePath+url+".xlsx"+" File Not Found Exception occured. "+e.getMessage(), LogAs.FAILED, null);
			            return filePath+url+".xlsx"+" File Not Found Exception occured. "+e.getMessage()+"\n";
			        } catch( Exception exp){
						exp.printStackTrace();
						//NXGReports.addStep("Verify dropped filter options in "+ filePath+url+".xlsx"+".", filePath+url+".xlsx","It should verify dropped filter options in "+ filePath+url+".xlsx"+".","Exception occured. "+exp.getMessage(), LogAs.FAILED, null);
						return "Exception occured. "+exp.getMessage()+"\n";
			        }
	}
	
	@SuppressWarnings("resource")
	public static String newAddedFilterOptionsData(String filePath, String fileName, String sheet) {
		String msg="", yDayCount="", todayCount="", value="", url="";
		List<String> oldList = new ArrayList<String>();
		List<String> newList = new ArrayList<String>();
		List<String> notPresent = new ArrayList<String>();
		DataFormatter formatter = new DataFormatter();
		try{
			fileName = fileName.replace("/", "");
			url = "\\"+fileName;
			// Create an object of File class to open xlsx file
			File file = new File(filePath + "\\" + fileName + ".xlsx");
			// Create an object of FileInputStream class to read excel file
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
			// If it is xlsx file then create object of XSSFWorkbook class
			workbook = new XSSFWorkbook(inputStream);
			// Read sheet inside the workbook by its name
			Sheet sheet1 = workbook.getSheet(sheet);
						
						Row row100 = sheet1.getRow(100);
						yDayCount = formatter.formatCellValue(row100.getCell(1));
						todayCount = formatter.formatCellValue(row100.getCell(3));
						for(int o=1; o<=Integer.parseInt(yDayCount); o++){
				        	Row dataRange = sheet1.getRow(o);
				        	value =formatter.formatCellValue(dataRange.getCell(1));
				        	oldList.add(value);
				        }
				        for(int n=1; n<=Integer.parseInt(todayCount); n++){
				        	Row dataRange = sheet1.getRow(n);
				        	value =formatter.formatCellValue(dataRange.getCell(3));
				        	newList.add(value);
				        }
						notPresent = new ArrayList<String>(newList);
						notPresent.removeAll(oldList);

						
						
						//Close input stream
					    inputStream.close();
					    //Create an object of FileOutputStream class to create write data in excel file
					    FileOutputStream outputStream = new FileOutputStream(file);

					    //write data in the excel file
					    workbook.write(outputStream);

					    //close output stream
					    outputStream.close();
					    if(notPresent.isEmpty()){
					    	//NXGReports.addStep("Verify new added filter options in "+ filePath+url+".xlsx"+".", filePath+url+".xlsx","It should verify new added filter options in "+ filePath+url+".xlsx"+".","No Filter options added today in "+sheet, LogAs.PASSED, null);
					    	return "";
					    } else{
					    	for(int i=0; i<notPresent.size(); i++){
					    		msg = msg+(i+1)+") "+notPresent.get(i)+"\n";
					    	}
					    	//NXGReports.addStep("Verify new added filter options in "+ filePath+url+".xlsx"+".", filePath+url+".xlsx","It should verify new added filter options in "+ filePath+url+".xlsx"+".","New added Filter options in "+sheet+" :"+msg, LogAs.FAILED, null);
					    	return "@"+sheet+":"+msg+" <b>New Added</b>"+"\n";
					    }
					} catch (FileNotFoundException e) {
			            e.printStackTrace();
			            //NXGReports.addStep("Verify new added filter options in "+ filePath+url+".xlsx"+".",  filePath+url+".xlsx", "It should verify new added filter options in "+ filePath+url+".xlsx"+".", filePath+url+".xlsx"+" File Not Found Exception occured. "+e.getMessage(), LogAs.FAILED, null);
			            return filePath+url+".xlsx"+" File Not Found Exception occured. "+e.getMessage()+"\n";
			        } catch( Exception exp){
						exp.printStackTrace();
						 //NXGReports.addStep("Verify new added filter options in "+ filePath+url+".xlsx"+".", filePath+url+".xlsx","It should verify new added filter options in "+ filePath+url+".xlsx"+".","Exception occured. "+exp.getMessage(), LogAs.FAILED, null);
						return " Exception occured. "+exp.getMessage()+"\n";
			        }
		
	}
	public static String thresholdMonitoring(String pathToExcel, String fileName, String url, String gender, List<entryCount4Facet> genderData, List<entryCount4Facet> typeData, List<entryCount4Facet> priceData, List<entryCount4Facet> sizeData, List<entryCount4Facet> manufacturerNameData) throws Exception{
		String message = " ";
		try{
			message = message+ ExcelUtility.moveFilterOptionsData("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Gender");
			message = message+ ExcelUtility.moveFilterOptionsData("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Product");
			message = message+ ExcelUtility.moveFilterOptionsData("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Price");
			message = message+ ExcelUtility.moveFilterOptionsData("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Size");
			message = message+ ExcelUtility.moveFilterOptionsData("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Brand");
		    
			message = message+ ExcelUtility.saveFilterOptionsQtyDataInExcel("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, genderData, typeData, priceData, sizeData, manufacturerNameData);
			message = message+ ExcelUtility.droppedFilterOptionsData("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Gender");
			message = message+ ExcelUtility.newAddedFilterOptionsData("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Gender");
			message = message+ ExcelUtility.droppedFilterOptionsData("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Product");
			message = message+ ExcelUtility.newAddedFilterOptionsData("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Product");
			message = message+ ExcelUtility.droppedFilterOptionsData("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Price");
			message = message+ ExcelUtility.newAddedFilterOptionsData("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Price");
			message = message+ ExcelUtility.droppedFilterOptionsData("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Size");
			message = message+ ExcelUtility.newAddedFilterOptionsData("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Size");
			message = message+ ExcelUtility.droppedFilterOptionsData("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Brand");
			message = message+ ExcelUtility.newAddedFilterOptionsData("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Brand");
			message = message+ ExcelUtility.thresholdVerification("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Gender");
			message = message+ ExcelUtility.thresholdVerification("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Product");
			message = message+ ExcelUtility.thresholdVerification("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Price");
			message = message+ ExcelUtility.thresholdVerification("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Size");
			message = message+ ExcelUtility.thresholdVerification("C:\\Opkey\\MonitoringThresholdQuantity\\"+gender, url, "Brand");
			
			if(message.equalsIgnoreCase(" "))
				//NXGReports.addStep("Verify threshold count for "+"http://abof.com"+url, "http://abof.com"+url, "It should verify filter options threshold count for "+ "http://abof.com"+url,message+" Threshold Monitoring passed", LogAs.PASSED, null);
				return "Threshold Monitoring passed";
			else
				BaseTest.emailMessageBody.put("http://abof.com"+url, message);
				return message;
			
				//NXGReports.addStep("Verify threshold count for "+"http://abof.com"+url, "http://abof.com"+url, "It should verify filter options threshold count for "+ "http://abof.com"+url,message, LogAs.FAILED, null);
			//throw new Exception();
		}
		catch (Exception exp){
			NXGReports.addStep("Verify threshold count for "+"http://abof.com"+url, "http://abof.com"+url, "It should verify filter options threshold count for "+ "http://abof.com"+url,message, LogAs.FAILED, null);
			return message;
			//throw exp;
		}
	}
	
	@SuppressWarnings("resource")
	public static String thresholdVerification(String filePath, String fileName, String sheet) {
		String message="", oldCount="", newCount="", value="", oldQtyData="", newQtyData="", url="";
		int positionOld, positionNew, diff = 0;
		float percentage = 0;
		Row row100, dataRange;
		boolean status=true;
		List<String> oldList = new ArrayList<String>();
		List<String> newList = new ArrayList<String>();
		List<String> common = new ArrayList<String>();
		DataFormatter formatter = new DataFormatter();
		try{
			fileName = fileName.replace("/", "");
			url = "\\"+fileName;
			// Create an object of File class to open xlsx file
			File file = new File(filePath + "\\" + fileName + ".xlsx");
			// Create an object of FileInputStream class to read excel file
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
			// If it is xlsx file then create object of XSSFWorkbook class
			workbook = new XSSFWorkbook(inputStream);
			// Read sheet inside the workbook by its name
			Sheet sheet1 = workbook.getSheet(sheet);
			row100 = sheet1.getRow(100);
			oldCount = formatter.formatCellValue(row100.getCell(1));
			newCount = formatter.formatCellValue(row100.getCell(3));
			for(int o=1; o<=Integer.parseInt(oldCount); o++){
			    	dataRange = sheet1.getRow(o);
			       	value =formatter.formatCellValue(dataRange.getCell(1));
			       	oldList.add(value);
			}
			for(int n=1; n<=Integer.parseInt(newCount); n++){
			      	dataRange = sheet1.getRow(n);
			      	value =formatter.formatCellValue(dataRange.getCell(3));
			       	newList.add(value);
			}
			if(newList.size()==oldList.size()){
				common = new ArrayList<String>(newList);
				common.retainAll(oldList);
				for(int p=0; p<common.size(); p++){
					positionOld = oldList.indexOf(common.get(p));
					Row oldDataRow = sheet1.getRow(positionOld+1);
					Cell oldDataCell = oldDataRow.getCell(2);
					oldQtyData = formatter.formatCellValue(oldDataCell);
					positionNew = newList.indexOf(common.get(p));
					Row newDataRow = sheet1.getRow(positionNew+1);
					Cell newDataCell = newDataRow.getCell(4);
					newQtyData = formatter.formatCellValue(newDataCell);
					if(sheet.toLowerCase().equalsIgnoreCase("price"))
					{
						if(!newQtyData.contentEquals("0")&&!oldQtyData.contentEquals("0")){
							diff = Integer.parseInt(newQtyData)-Integer.parseInt(oldQtyData);
							percentage = (diff*100)/Integer.parseInt(oldQtyData);
						}
					}
					else{
						diff = Integer.parseInt(newQtyData)-Integer.parseInt(oldQtyData);
						percentage = (diff*100)/Integer.parseInt(oldQtyData);
					}
										
					if((percentage > 15.00f&&diff>10)||percentage < -15.00f&&diff>10){
						message = message+"@"+sheet+":"+common.get(p).trim()+ " quantity changed from  "+oldQtyData+" to "+newQtyData+"\n"+" "+"<br>";
						if(status==true){
							status = false;
						}
					}
				}
			} else if(newList.size()>oldList.size()){
				common = new ArrayList<String>(newList);
				common.retainAll(oldList);
				for(int p=0; p<common.size(); p++){
					positionOld = oldList.indexOf(common.get(p));
					Row oldDataRow = sheet1.getRow(positionOld+1);
					Cell oldDataCell = oldDataRow.getCell(2);
					oldQtyData = formatter.formatCellValue(oldDataCell);
				//	oldQtyData = oldQtyData.replaceAll("s/([()])/g", "");
					positionNew = newList.indexOf(common.get(p));
					Row newDataRow = sheet1.getRow(positionNew+1);
					Cell newDataCell = newDataRow.getCell(4);
					newQtyData = formatter.formatCellValue(newDataCell);
					//newQtyData = newQtyData.replaceAll("s/([()])/g", "");
					diff = Integer.parseInt(newQtyData)-Integer.parseInt(oldQtyData);
					percentage = (diff*100)/Integer.parseInt(oldQtyData);
					if((percentage > 15.00f||percentage < -15.00f)&&diff>10){
						message = message+common.get(p).trim()+ " has  "+percentage+"% drop. "+"\n"+" "+"\t";
						if(status==true){
							status = false;
						}
					}
				}
			}
			else{
				common = new ArrayList<String>(oldList);
				common.retainAll(newList);
				for(int p=0; p<common.size(); p++){
					positionOld = oldList.indexOf(common.get(p).toString())+1;
					Row oldDataRow = sheet1.getRow(positionOld);
					Cell oldDataCell = oldDataRow.getCell(2);
					oldQtyData = formatter.formatCellValue(oldDataCell);
					positionNew = newList.indexOf(common.get(p).toString())+1;
					Row newDataRow = sheet1.getRow(positionNew);
					Cell newDataCell = newDataRow.getCell(4);
					newQtyData = formatter.formatCellValue(newDataCell);
					diff = Integer.parseInt(newQtyData)-Integer.parseInt(oldQtyData);
					percentage = (diff*100)/Integer.parseInt(oldQtyData);
					if((percentage > 15.00f||percentage < -15.00f)&&diff>10){
						message = message+common.get(p).trim()+ " has  "+percentage+"% drop. "+"\n"+" "+"\t";
						if(status==true){
							status = false;
						}
					}
				}
			}
			if(status){
				return "";
			}
			else{
				return message;
			}
		}
		catch(Exception e){
			return "Exception Occured : "+e.getMessage();
		}
	}
	
	@SuppressWarnings("resource")
	public static void moveYesterdayCategories(String filePath, String fileName, String gender){
		String  todayCategory="", yDayCount="";
		DataFormatter formatter = new DataFormatter();
		try{
			// Create an object of File class to open xlsx file
						File file = new File(filePath + "\\" + fileName + ".xlsx");

						// Create an object of FileInputStream class to read excel file
						FileInputStream inputStream = new FileInputStream(file);

						Workbook workbook = null;
					
							// If it is xlsx file then create object of XSSFWorkbook class
							workbook = new XSSFWorkbook(inputStream);
						

						// Read sheet inside the workbook by its name
						Sheet sheet1 = workbook.getSheet("Sheet1");
						if(gender.toLowerCase().equals("men")){
							Row men = sheet1.getRow(100);
							yDayCount = formatter.formatCellValue(men.getCell(1));
							Cell oldData = men.createCell(0);
					        oldData.setCellValue(yDayCount);
						}
						else{
							Row women = sheet1.getRow(100);
							yDayCount = formatter.formatCellValue(women.getCell(5));
							Cell oldData = women.createCell(4);
					        oldData.setCellValue(yDayCount);
						}
						for(int i=1; i<=Integer.parseInt(yDayCount); i++){
							Row category_Row = sheet1.getRow(i);
							if(gender.toLowerCase().equals("men")){
								todayCategory = category_Row.getCell(1).getStringCellValue().toString();
								Row row1 = sheet1.getRow(i);
						        Cell oldData = row1.createCell(0);
						        oldData.setCellValue(todayCategory);
							}else{
								todayCategory = category_Row.getCell(5).getStringCellValue().toString();	
								Row row1 = sheet1.getRow(i);
						        Cell oldData = row1.createCell(4);
						        oldData.setCellValue(todayCategory);
							}
							
						}	 
						    inputStream.close();
	
						    //Create an object of FileOutputStream class to create write data in excel file
						    FileOutputStream outputStream = new FileOutputStream(file);
	
						    //write data in the excel file
	
						    workbook.write(outputStream);
	
						    //close output stream
	
						    outputStream.close();
						    NXGReports.addStep("Move categories under "+gender+" in Categories.xlsx.",gender, "It should move old data in Categories.xlsx","It moved old data in Categories.xlsx successfully", LogAs.PASSED, null);
						    
						}	    
						catch(Exception exp){
							exp.printStackTrace();	
							NXGReports.addStep("Move categories under "+gender+" in Categories.xlsx.",gender, "It should move old data in Categories.xlsx","Moving old data in Categories.xlsx failed due to exception: "+exp.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
						}
	}


	@SuppressWarnings("resource")
	public static void droppedCategories(String filePath, String fileName, String gender){
		DataFormatter formatter = new DataFormatter();
		List<String> oldList = new ArrayList<String>();
		List<String> newList = new ArrayList<String>();
		List<String> notPresent = new ArrayList<String>();
		String message="Under "+gender+" :",oldCount="", newCount="", value="";
		Row row, dataRange;
		try{
			File file = new File(filePath + "\\" + fileName + ".xlsx");
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
			workbook = new XSSFWorkbook(inputStream);
			Sheet sheet1 = workbook.getSheet("Sheet1");
			if(gender.toLowerCase().equals("men")){
				row = sheet1.getRow(100);
		        Cell oldCountCell = row.getCell(0);
		        oldCount = formatter.formatCellValue(oldCountCell);
		        Cell newCountCell = row.getCell(1);
		        newCount = formatter.formatCellValue(newCountCell);
		        for(int o=1; o<=Integer.parseInt(oldCount); o++){
		        	dataRange = sheet1.getRow(o);
		        	value =formatter.formatCellValue(dataRange.getCell(0));
		        	oldList.add(value);
		        }
		        for(int n=1; n<=Integer.parseInt(newCount); n++){
		        	dataRange = sheet1.getRow(n);
		        	value =formatter.formatCellValue(dataRange.getCell(1));
		        	newList.add(value);
		        }
			}else{
				row = sheet1.getRow(100);
		        Cell todayCountCel = row.getCell(4);
		        oldCount = formatter.formatCellValue(todayCountCel);
		        Cell newCountCell = row.getCell(5);
		        newCount = formatter.formatCellValue(newCountCell);
		        for(int o=1; o<=Integer.parseInt(oldCount); o++){
		        	dataRange = sheet1.getRow(o);
		        	value =formatter.formatCellValue(dataRange.getCell(4));
		        	oldList.add(value);
		        }
		        for(int n=1; n<=Integer.parseInt(newCount); n++){
		        	dataRange = sheet1.getRow(n);
		        	value =formatter.formatCellValue(dataRange.getCell(5));
		        	newList.add(value);
		        }
			}
			notPresent = new ArrayList<String>(oldList);
			notPresent.removeAll(newList);
			for(int c=0; c<notPresent.size(); c++){
				message = message+(c+1)+". "+notPresent.get(c)+" "+"\n";
			}
			if(notPresent.isEmpty()){
				//return "Successfully verified no categories dropped.";
				NXGReports.addStep("Verify categories under "+gender+"  are dropped or Not?.", gender, "It should verify if any categories drop on megamenu.","It is verified no categories drop on megamenu.", LogAs.PASSED, null);
			}
			else{
				//return "These are categories dropped: "+message;
				NXGReports.addStep("Verify categories under "+gender+"  are dropped or Not?", gender, "It should verify if any categories drop on megamenu.",message+"\n"+"are dropped.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		}
		catch( Exception exp){
			//return "Exception occured. "+exp.getMessage();
			NXGReports.addStep("Verify categories under "+gender+"  are dropped or Not?", gender, "It should verify if any categories drop on megamenu.","Verification of "+gender+" categories dropp is failed, due to: ."+exp.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	@SuppressWarnings("resource")
	public static void  newAddedCategories(String filePath, String fileName, String gender){
		DataFormatter formatter = new DataFormatter();
		List<String> oldList = new ArrayList<String>();
		List<String> newList = new ArrayList<String>();
		List<String> notPresent = new ArrayList<String>();
		String message="Under "+gender+" :",oldCount="", newCount="", value="";
		Row row, dataRange;
		try{
			File file = new File(filePath + "\\" + fileName + ".xlsx");
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
			workbook = new XSSFWorkbook(inputStream);
			Sheet sheet1 = workbook.getSheet("Sheet1");
			if(gender.toLowerCase().equals("men")){
				row = sheet1.getRow(100);
		        Cell oldCountCell = row.getCell(0);
		        oldCount = formatter.formatCellValue(oldCountCell);
		        Cell newCountCell = row.getCell(1);
		        newCount = formatter.formatCellValue(newCountCell);
		        for(int o=1; o<=Integer.parseInt(oldCount); o++){
		        	dataRange = sheet1.getRow(o);
		        	value =formatter.formatCellValue(dataRange.getCell(0));
		        	oldList.add(value);
		        }
		        for(int n=1; n<=Integer.parseInt(newCount); n++){
		        	dataRange = sheet1.getRow(n);
		        	value =formatter.formatCellValue(dataRange.getCell(1));
		        	newList.add(value);
		        }
			}else{
				row = sheet1.getRow(100);
		        Cell todayCountCel = row.getCell(4);
		        oldCount = formatter.formatCellValue(todayCountCel);
		        Cell newCountCell = row.getCell(5);
		        newCount = formatter.formatCellValue(newCountCell);
		        for(int o=1; o<=Integer.parseInt(oldCount); o++){
		        	dataRange = sheet1.getRow(o);
		        	value =formatter.formatCellValue(dataRange.getCell(4));
		        	oldList.add(value);
		        }
		        for(int n=1; n<=Integer.parseInt(newCount); n++){
		        	dataRange = sheet1.getRow(n);
		        	value =formatter.formatCellValue(dataRange.getCell(5));
		        	newList.add(value);
		        }
			}
			notPresent = new ArrayList<String>(newList);
			notPresent.removeAll(oldList);
			for(int c=0; c<notPresent.size(); c++){
				
				message = message+(c+1)+". "+notPresent.get(c)+" "+"\n";
			}
			if(notPresent.isEmpty()){
				//return "Successfully verified no categories added.";
				NXGReports.addStep("Verify new categories under "+gender+"  are added or Not?", gender, "It should verify if any categories added on megamenu.","It is verified no categories added on megamenu.", LogAs.PASSED, null);
			}
			else{
				//return "These are categories added: "+message;
				NXGReports.addStep("Verify new categories under "+gender+"  are added or Not?", gender, "It should verify if any categories added on megamenu.",message+"\n"+"are added today.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		}
		catch( Exception exp){
			//return "Exception occured. "+exp.getMessage();
			NXGReports.addStep("Verify new categories under "+gender+"  are added or Not?", gender, "It should verify if any categories added on megamenu.","Verification of "+gender+" categories added is failed, due to: ."+exp.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	
		
	@SuppressWarnings("resource")
	public static String getCellValueFromExcel(String filePath, String fileName, int rowNum, int colNum){
		DataFormatter formatter = new DataFormatter();
		String cellValue="";
		try{
			File file = new File(filePath + "\\" + fileName + ".xlsx");
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
				workbook = new XSSFWorkbook(inputStream);
			Sheet sheet1 = workbook.getSheet("Sheet1");
			Row row = sheet1.getRow(rowNum);
			cellValue = formatter.formatCellValue(row.getCell(colNum));
			return cellValue;
		}
		catch( Exception exp){
			return "Exception occured. "+exp.getMessage();
		}
	}
	@SuppressWarnings("resource")
	public static String  getMenCategoriesCount(String filePath, String fileName){
		DataFormatter formatter = new DataFormatter();
		String count="";
		try{
			File file = new File(filePath + "\\" + fileName + ".xlsx");
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
				workbook = new XSSFWorkbook(inputStream);
			Sheet sheet1 = workbook.getSheet("Sheet1");
			Row row = sheet1.getRow(100);
			count = formatter.formatCellValue(row.getCell(1));
			return count;
		}
		catch( Exception exp){
			return "0";
		}
	}
	@SuppressWarnings("resource")
	public static String  getWomenCategoriesCount(String filePath, String fileName){
		DataFormatter formatter = new DataFormatter();
		String count="";
		try{
			File file = new File(filePath + "\\" + fileName + ".xlsx");
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
				workbook = new XSSFWorkbook(inputStream);
			Sheet sheet1 = workbook.getSheet("Sheet1");
			Row row = sheet1.getRow(100);
			count = formatter.formatCellValue(row.getCell(5));
			return count;
		}
		catch( Exception exp){
			return "0";
		}
	}
	

	@SuppressWarnings("resource")
	public static String moveFilterOptions(String filePath, String fileName, String sheet){
		String  oldCount="", yDayCount="", yDayOptionName="", yDayOptionQty="";
		int big;
		DataFormatter formatter = new DataFormatter();
		try{
				// Create an object of File class to open xlsx file
			File file = new File(filePath + "\\" + fileName + ".xlsx");
				// Create an object of FileInputStream class to read excel file
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
				// If it is xlsx file then create object of XSSFWorkbook class
			workbook = new XSSFWorkbook(inputStream);
				// Read sheet inside the workbook by its name
			Sheet sheet1 = workbook.getSheet(sheet);
			Row countRow = sheet1.getRow(100);
			oldCount = formatter.formatCellValue(countRow.getCell(1));
			yDayCount = formatter.formatCellValue(countRow.getCell(3));
			Cell oldData = countRow.createCell(1);
	        oldData.setCellValue(yDayCount);
	        if(Integer.parseInt(oldCount)>Integer.parseInt(yDayCount)){
	        	big=Integer.parseInt(oldCount);
	        	for(int c=0; c<big; c++){
		        	Row option_Row = sheet1.getRow(c+1);
						yDayOptionName = formatter.formatCellValue(option_Row.getCell(3));
						yDayOptionQty = formatter.formatCellValue(option_Row.getCell(4));
						if(c<Integer.parseInt(yDayCount)){
							Cell oldOptionData = option_Row.createCell(1);
							oldOptionData.setCellValue(yDayOptionName);
							Cell oldOptionQtyData = option_Row.createCell(2);
							oldOptionQtyData.setCellValue(yDayOptionQty);
						}else{
							Cell oldOptionData = option_Row.createCell(1);
							oldOptionData.setCellValue(" ");
							Cell oldOptionQtyData = option_Row.createCell(2);
							oldOptionQtyData.setCellValue(" ");
						}
		        }
	        }
	        else{
	        	big=Integer.parseInt(yDayCount);
	        	for(int c=0; c<big; c++){
		        	Row option_Row = sheet1.getRow(c+1);
						yDayOptionName = formatter.formatCellValue(option_Row.getCell(3));
						yDayOptionQty = formatter.formatCellValue(option_Row.getCell(4));
						Cell oldOptionData = option_Row.createCell(1);
						oldOptionData.setCellValue(yDayOptionName);
						Cell oldOptionQtyData = option_Row.createCell(2);
						oldOptionQtyData.setCellValue(yDayOptionQty);
						
		        }
	        }
	        
			inputStream.close();
			    //Create an object of FileOutputStream class to create write data in excel file
			FileOutputStream outputStream = new FileOutputStream(file);
				//write data in the excel file
		    workbook.write(outputStream);
			    //close output stream
		    outputStream.close();
		    return "Successfully moved "+sheet+" Categories text in excel.";
		}	    
		catch(Exception exp){
			return "Exception occured: "+exp.getMessage();	
		}
	}
	
	@SuppressWarnings({ "resource" })
	public static String saveFilterOptionsQtyInExcel(WebDriver driver, WebElement productHeaderCount, 
												List<WebElement> genderOptions, List<WebElement> genderOptionsQty, List<WebElement> productOptions, List<WebElement> productOptionsQty, 
												List<WebElement> priceOptions, List<WebElement> priceOptionsQty, List<WebElement> sizeOptions, List<WebElement> sizeOptionsQty,  
												List<WebElement> brandOptions, List<WebElement> brandOptionsQty, String filePath, String fileName){
		
		String linkText="";
		String product_grid_header_title="";
		List<String> genderOptionsList = new ArrayList<String>();
		List<String> genderOptionsQtyList = new ArrayList<String>();
		List<String> productOptionsList = new ArrayList<String>();
		List<String> productOptionsQtyList = new ArrayList<String>();
		List<String> priceOptionsList = new ArrayList<String>();
		List<String> priceOptionsQtyList = new ArrayList<String>();
		List<String> sizeOptionsList = new ArrayList<String>();
		List<String> sizeOptionsQtyList = new ArrayList<String>();
		List<String> brandOptionsList = new ArrayList<String>();
		List<String> brandOptionsQtyList = new ArrayList<String>();
		try{
			product_grid_header_title = productHeaderCount.getText();
			for(WebElement option:genderOptions){
				linkText = option.getText();
				genderOptionsList.add(linkText);
				
			}
			for(WebElement option:genderOptionsQty){
				linkText = option.getText();
				genderOptionsQtyList.add(linkText);
				
			}
			for(WebElement option:productOptions){
				linkText = option.getText();
				productOptionsList.add(linkText);
				
			}
			for(WebElement option:productOptionsQty){
				linkText = option.getText();
				productOptionsQtyList.add(linkText);
				
			}
			for(WebElement option:priceOptions){
				linkText = option.getText();
				priceOptionsList.add(linkText);
				
			}
			for(WebElement option:priceOptionsQty){
				linkText = option.getText();
				priceOptionsQtyList.add(linkText);
				
			}
			for(WebElement option:sizeOptions){
				linkText = option.getText();
				sizeOptionsList.add(linkText);
				
			}
			for(WebElement option:sizeOptionsQty){
				linkText = option.getText();
				sizeOptionsQtyList.add(linkText);
				
			}
			for(WebElement option:brandOptions){
				linkText = option.getText();
				brandOptionsList.add(linkText);
				
			}
			for(WebElement option:brandOptionsQty){
				linkText = option.getText();
				brandOptionsQtyList.add(linkText);
				
			}
			File file = new File(filePath + "\\" + fileName + ".xlsx");
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
				workbook = new XSSFWorkbook(inputStream);
			for(int i=0; i<genderOptions.size(); i++){
					Sheet sheet1 = workbook.getSheet("Gender");
					Row row = sheet1.getRow(i+1);
			        Cell name = row.createCell(3);
			        name.setCellValue(((JavascriptExecutor)driver).executeScript("return arguments[0].innerText;", genderOptions.get(i)).toString());
			        Cell qty = row.createCell(4);
			        qty.setCellValue(((JavascriptExecutor)driver).executeScript("return arguments[0].innerText;", genderOptionsQty.get(i)).toString());
			        if(i==genderOptions.size()-1){
			        	Row countRow = sheet1.getRow(100);
			        	Cell countCell = countRow.createCell(3);
			        	countCell.setCellValue(""+genderOptions.size());
			        }
			        
			}
			for(int i=0; i<productOptions.size(); i++){
				Sheet sheet1 = workbook.getSheet("Product");
				Row row = sheet1.getRow(i+1);
		        Cell name = row.createCell(3);
		        name.setCellValue(((JavascriptExecutor)driver).executeScript("return arguments[0].innerText;", productOptions.get(i)).toString());
		        Cell qty = row.createCell(4);
		        qty.setCellValue(((JavascriptExecutor)driver).executeScript("return arguments[0].innerText;", productOptionsQty.get(i)).toString());
		        if(i==productOptions.size()-1){
		        	Row countRow = sheet1.getRow(100);
		        	Cell countCell = countRow.createCell(3);
		        	countCell.setCellValue(""+productOptions.size());
		        }
			}
			for(int i=0; i<priceOptions.size(); i++){
				Sheet sheet1 = workbook.getSheet("Price");
				Row row = sheet1.getRow(i+1);
		        Cell name = row.createCell(3);
		        name.setCellValue(((JavascriptExecutor)driver).executeScript("return arguments[0].innerText;", priceOptions.get(i)).toString());
		        Cell qty = row.createCell(4);
		        qty.setCellValue(((JavascriptExecutor)driver).executeScript("return arguments[0].innerText;", priceOptionsQty.get(i)).toString());
		        if(i==priceOptions.size()-1){
		        	Row countRow = sheet1.getRow(100);
		        	Cell countCell = countRow.createCell(3);
		        	countCell.setCellValue(""+priceOptions.size());
		        }
			}
			for(int i=0; i<sizeOptions.size(); i++){
				Sheet sheet1 = workbook.getSheet("Size");
				Row row = sheet1.getRow(i+1);
		        Cell name = row.createCell(3);
		        name.setCellValue(((JavascriptExecutor)driver).executeScript("return arguments[0].innerText;", sizeOptions.get(i)).toString());
		        Cell qty = row.createCell(4);
		        qty.setCellValue(((JavascriptExecutor)driver).executeScript("return arguments[0].innerText;", sizeOptionsQty.get(i)).toString());
		        if(i==sizeOptions.size()-1){
		        	Row countRow = sheet1.getRow(100);
		        	Cell countCell = countRow.createCell(3);
		        	countCell.setCellValue(""+sizeOptions.size());
		        }
			}
			for(int i=0; i<brandOptions.size(); i++){
				Sheet sheet1 = workbook.getSheet("Brand");
				Row row = sheet1.getRow(i+1);
		        Cell name = row.createCell(3);
		        name.setCellValue(((JavascriptExecutor)driver).executeScript("return arguments[0].innerText;", brandOptions.get(i)).toString());
		        Cell qty = row.createCell(4);
		        qty.setCellValue(((JavascriptExecutor)driver).executeScript("return arguments[0].innerText;", brandOptionsQty.get(i)).toString());
		        if(i==brandOptions.size()-1){
		        	Row countRow = sheet1.getRow(100);
		        	Cell countCell = countRow.createCell(3);
		        	countCell.setCellValue(""+brandOptions.size());
		        }
			}
			//Close input stream
		    inputStream.close();
		    //Create an object of FileOutputStream class to create write data in excel file
		    FileOutputStream outputStream = new FileOutputStream(file);

		    //write data in the excel file
		    workbook.write(outputStream);

		    //close output stream
		    outputStream.close();
			return "Filter options quantity count collected.";
		} catch (FileNotFoundException e) {
            e.printStackTrace();
            return "File Not Found Exception occured. "+e.getMessage();
        } catch (IOException e) {
            e.printStackTrace();
            return "Exception occured. "+e.getMessage();
        } catch( Exception exp){
			exp.printStackTrace();
			return "Exception occured. "+exp.getMessage();
		}
	}
	@SuppressWarnings("resource")
	public static String droppedFilterOptions(String filePath, String fileName, String sheet){

		DataFormatter formatter = new DataFormatter();
		List<String> oldList = new ArrayList<String>();
		List<String> newList = new ArrayList<String>();
		List<String> notPresent = new ArrayList<String>();
		Row row, dataRange;
		String oldCount="", newCount="", value="", message="";
		try{
			File file = new File(filePath + "\\" + fileName + ".xlsx");
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
			workbook = new XSSFWorkbook(inputStream);
			Sheet sheet1 = workbook.getSheet(sheet);
			row = sheet1.getRow(100);
	        Cell oldCountCell = row.getCell(1);
	        oldCount = formatter.formatCellValue(oldCountCell);
	        Cell newCountCell = row.getCell(3);
	        newCount = formatter.formatCellValue(newCountCell);
	        for(int o=1; o<=Integer.parseInt(oldCount); o++){
	        	dataRange = sheet1.getRow(o);
	        	value =formatter.formatCellValue(dataRange.getCell(1));
	        	oldList.add(value);
	        }
	        for(int n=1; n<=Integer.parseInt(newCount); n++){
	        	dataRange = sheet1.getRow(n);
	        	value =formatter.formatCellValue(dataRange.getCell(3));
	        	newList.add(value);
	        }
			notPresent = new ArrayList<String>(oldList);
			notPresent.removeAll(newList);
			
			if(notPresent.isEmpty()){
				//Close input stream
			    inputStream.close();
			    //Create an object of FileOutputStream class to create write data in excel file
			    FileOutputStream outputStream = new FileOutputStream(file);

			    //write data in the excel file
			    workbook.write(outputStream);

			    //close output stream
			    outputStream.close();
				//return "Successfully verified no "+sheet+" filter options dropped.";
			    return "";
			}
			else{
				for(int c=0; c<notPresent.size(); c++){
					int size= newList.size()+c+1;
					Row row1 = sheet1.getRow(size);
			        Cell name = row1.createCell(3);
			        name.setCellValue(" ");
			        Cell qty = row1.createCell(4);
			        qty.setCellValue(" ");
					message = message+"("+(c+1)+"). "+notPresent.get(c)+" "+"\n";
				}
				//Close input stream
			    inputStream.close();
			    //Create an object of FileOutputStream class to create write data in excel file
			    FileOutputStream outputStream = new FileOutputStream(file);

			    //write data in the excel file
			    workbook.write(outputStream);

			    //close output stream
			    outputStream.close();
				return sheet+" filter options dropped: "+message;
			}
		}
		catch( Exception exp){
			return "Exception occured. "+exp.getMessage();
		}
	}
	
	@SuppressWarnings("resource")
	public static String newAddedFilterOptions(String filePath, String fileName, String sheet){

		DataFormatter formatter = new DataFormatter();
		List<String> oldList = new ArrayList<String>();
		List<String> newList = new ArrayList<String>();
		List<String> notPresent = new ArrayList<String>();
		Row row, dataRange;
		String oldCount="", newCount="", value="", message="";
		try{
			File file = new File(filePath + "\\" + fileName + ".xlsx");
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
			workbook = new XSSFWorkbook(inputStream);
			Sheet sheet1 = workbook.getSheet(sheet);
			row = sheet1.getRow(100);
	        Cell oldCountCell = row.getCell(1);
	        oldCount = formatter.formatCellValue(oldCountCell);
	        Cell newCountCell = row.getCell(3);
	        newCount = formatter.formatCellValue(newCountCell);
	        for(int o=1; o<=Integer.parseInt(oldCount); o++){
	        	dataRange = sheet1.getRow(o);
	        	value =formatter.formatCellValue(dataRange.getCell(1));
	        	oldList.add(value);
	        }
	        for(int n=1; n<=Integer.parseInt(newCount); n++){
	        	dataRange = sheet1.getRow(n);
	        	value =formatter.formatCellValue(dataRange.getCell(3));
	        	newList.add(value);
	        }
			notPresent = new ArrayList<String>(newList);
			notPresent.removeAll(oldList);
			
			for(int c=0; c<notPresent.size(); c++){
				message = message+"("+(c+1)+"). "+notPresent.get(c)+" "+"\n";
			}
			if(notPresent.isEmpty()){
				return "";
			}
			else{
				return sheet+" filter options added: "+message;
			}
		}
		catch( Exception exp){
			return "Exception occured. "+exp.getMessage();
		}
	}
	
	@SuppressWarnings("resource")
	public static String verifyQuantityChangePercentage(String filePath, String fileName, String sheet, String message){

		DataFormatter formatter = new DataFormatter();
		List<String> oldList = new ArrayList<String>();
		List<String> newList = new ArrayList<String>();
		List<String> common = new ArrayList<String>();
		Row row, dataRange;
		String oldCount="", newCount="", value="", oldQtyData="", newQtyData="";
		int positionOld, positionNew;			//, index=-1;
		float percentage;
		boolean status = true;
		try{
			File file = new File(filePath + "\\" + fileName + ".xlsx");
			FileInputStream inputStream = new FileInputStream(file);
			Workbook workbook = null;
			workbook = new XSSFWorkbook(inputStream);
			Sheet sheet1 = workbook.getSheet(sheet);
			row = sheet1.getRow(100);
	        Cell oldCountCell = row.getCell(1);
	        oldCount = formatter.formatCellValue(oldCountCell);
	        Cell newCountCell = row.getCell(3);
	        newCount = formatter.formatCellValue(newCountCell);
	        for(int o=1; o<=Integer.parseInt(oldCount); o++){
	        	dataRange = sheet1.getRow(o);
	        	value =formatter.formatCellValue(dataRange.getCell(1));
	        	oldList.add(value);
	        }
	        for(int n=1; n<=Integer.parseInt(newCount); n++){
	        	dataRange = sheet1.getRow(n);
	        	value =formatter.formatCellValue(dataRange.getCell(3));
	        	newList.add(value);
	        }
			if(newList.size()>oldList.size()){
				common = new ArrayList<String>(newList);
				common.retainAll(oldList);
				for(int p=0; p<common.size(); p++){
					positionOld = oldList.indexOf(common.get(p));
					Row oldDataRow = sheet1.getRow(positionOld);
					Cell oldDataCell = oldDataRow.getCell(2);
					oldQtyData = formatter.formatCellValue(oldDataCell);
					oldQtyData = oldQtyData.replaceAll("s/([()])/g", "");
					positionNew = newList.indexOf(common.get(p));
					Row newDataRow = sheet1.getRow(positionNew);
					Cell newDataCell = newDataRow.getCell(4);
					newQtyData = formatter.formatCellValue(newDataCell);
					newQtyData = newQtyData.replaceAll("s/([()])/g", "");
					percentage = Integer.parseInt(newQtyData)*100/Integer.parseInt(oldQtyData);
					if(percentage > 15){
						message = message+common.get(p).trim()+ " has morethan 15% drop. "+"\n"+" "+"\t";
					}
				}
			}
			else{

				common = new ArrayList<String>(oldList);
				common.retainAll(newList);
				
				for(int p=0; p<common.size(); p++){
					positionOld = oldList.indexOf(common.get(p).toString())+1;
					Row oldDataRow = sheet1.getRow(positionOld);
					Cell oldDataCell = oldDataRow.getCell(2);
					oldQtyData = formatter.formatCellValue(oldDataCell);
					oldQtyData = oldQtyData.replace("(", "");
					oldQtyData = oldQtyData.replace(")", "");
					positionNew = newList.indexOf(common.get(p).toString())+1;
					Row newDataRow = sheet1.getRow(positionNew);
					Cell newDataCell = newDataRow.getCell(4);
					newQtyData = formatter.formatCellValue(newDataCell);
					newQtyData = newQtyData.replace("(", "");
					newQtyData = newQtyData.replace(")", "");
					int diff = Integer.parseInt(newQtyData)-Integer.parseInt(oldQtyData);
					percentage = (diff*100)/Integer.parseInt(oldQtyData);
					if((percentage > 15.00f||percentage < -15.00f) && diff > 10){
						message = message+common.get(p).trim()+ " has  "+percentage+"% drop. "+"\n"+" "+"\t";
						if(status==true){
							status = false;
						}
					}		
				}
			}
			
			if(status){
				return "Successfully verified no "+sheet+" filter options added today.";
			}
			else{
				return sheet+" filter Qty changed: "+message;
			}
		}
		catch( Exception exp){
			return "Exception occured. "+exp.getMessage();
		}
	}	
}
