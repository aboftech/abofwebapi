package com.abof.web.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.util.StringUtils;
import org.testng.asserts.SoftAssert;

import com.abof.api.tests.ThresholdMonitoringAPI;
import com.abof.api.tests.ThresholdMonitoringAPI.entryCount4Facet;
import com.abof.web.testscripts.BaseTest;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;

import atu.testrecorder.ATUTestRecorder;

public class utility {
	


	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private Pattern pattern;
	private Matcher matcher;

	public String actualPageTitle;

	private static final String IMAGE_PATTERN = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";
	
	
	/*
	 * This method fetch iamge name Author : Shreekant R S
	 */

	public String fetchImageName(WebElement srcImage) throws Exception {

		String srcURL = srcImage.getAttribute("src");

		char splitchar = '/';

		String actualImageName = splitString(srcURL, splitchar);

		return actualImageName;

	}
	
	
	/*
	 * This method converts ecpection to string Author : Shreekant R S
	 */

	public static String exceptionsToString(Exception e) {

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();

	}

	
	

	/*
	 * This method verify page title Author : Shreekant R S
	 */
	public String fetchTextFromWebElement(WebElement element, String Text) {

		String value = "Not able to fetch " + Text;
		try {

			value = element.getText();
			System.out.println("Value is : " + value);
			if (StringUtils.isEmpty(value)) {
				return "Not able to fetch " + Text;
			}
			return value;
		}

		catch (Exception e) {

			String convertedString = exceptionsToString(e);
			return value + convertedString;
		}

	}
	
	/*
	 * This method validates image ending with .jpg Author : Shreekant R S
	 */

	public boolean validateImage(final String imageName) {


		System.out.println("String Iamge Name is: " + imageName);
		pattern = Pattern.compile(IMAGE_PATTERN);
		matcher = pattern.matcher(imageName);
		return matcher.matches();

	}

	
	public String splitStringByDot(String stringToSplit, char spiltChar, String containString) throws Exception {

		String str7Array[] = stringToSplit.split("\\."); // prints
															// [abc,
															// def, ghi]
		System.out.println("String with | delimiter: " + Arrays.toString(str7Array));
		int length = Arrays.toString(str7Array).length();
		System.out.println("Length is : " + length);
		String actualImageName = null;
		for (int k = 0; k < length; k++) {

			String iamgenames = str7Array[k];

			System.out.println("Image Extension name is " + iamgenames);

			for (int j = length; j <= length; j--) {

			}

			if (iamgenames.contains(containString)) {
				actualImageName = iamgenames;
				System.out.println("Actual Image Extension Name IS : " + actualImageName);
				break;
			}
		}
		return actualImageName;
	}
	
	/*
	 * This method splits string by char Author : Shreekant R S
	 */

	public String splitStringByPosition(String stringToSplit, char spiltChar, int i) throws Exception {

		String str7Array[] = stringToSplit.split("\\" + spiltChar);

		int length = str7Array.length;

		String finalValue = str7Array[length - i];

		System.out.println("Image NAme is: " + finalValue);

		return finalValue;
	}

	/*
	 * This method verify image extension Author : Shreekant R S
	 */

	public String verifyImageExtension(String imageName){

		String str7Array2[] = imageName.split("\\.");
		int length1 = str7Array2.length;
		String imageExtension = str7Array2[length1-1];
		System.out.println("Image Extension is: " + imageExtension);

		return imageExtension;
	}

	



	public String splitString(String stringToSplit, char spiltChar) throws Exception {

		String str7Array[] = stringToSplit.split("\\" + spiltChar);

		int length = str7Array.length;

		String finalValue = str7Array[length - 1];

		System.out.println("Image NAme is: " + finalValue);

		return finalValue;
	}
}
