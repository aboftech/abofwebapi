package com.abof.web.GTMTracking;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;

public class GTMTracking extends BaseTest{

	//https://abof-tracker.atlassian.net/browse/ABOF-7004
	public static String skult_url= "http://www.abof.com/skult";
	@Test
	public void TesttheGTMTagsforSkultPage() throws InterruptedException, IOException {

		driver.get(skult_url);
		JavascriptExecutor js = (JavascriptExecutor)driver;

		Thread thread = new Thread(){
			
				@Override
				public void run(){
					while(true){
//					System.out.println("Thread Running");
					analyzeLog();
				}
			}
		};

		thread.start();

		String text = Files.toString(new File("/Users/asifasunar/Documents/workspace/gtm/src/test/java/com/abof/browser/data-layer-helper.js"), Charsets.UTF_8);
		js.executeScript(text);
		js.executeScript("new DataLayerHelper(dataLayer, (model, message) => {console.log('aaa'+JSON.stringify(message))});");
		js.executeScript("new DataLayerHelper(dataLayer, (model, message) => {console.log('bbb'+JSON.stringify(model))});");
		js.executeScript("new DataLayerHelper(dataLayer, (model, message) => {console.log('ccc'+JSON.stringify(dataLayer))});");

		Thread.sleep(1000);

		String result = js.executeScript("return dataLayer;").toString();
		System.out.println(result);


		//				this.analyzeLog();
		//		
		//		Thread thread2 = new Thread(){
		//		    public void run(){
		//		      System.out.println("Thread Running");
		////		      js.executeScript("new DataLayerHelper(dataLayer, (model, message) => {console.log('----asifa2',message, model)});");
		//		      analyzeLog();
		//		    }
		//		  };
		//
		//		thread2.start();

		String view_all_products = "//button[@class='viewfullcollectionbtn']";
		WebElement viewAllProducts = driver.findElement(By.xpath(view_all_products));
		viewAllProducts.click();
		Thread.sleep(1000);
		js.executeScript(text);
		js.executeScript("new DataLayerHelper(dataLayer, (model, message) => {console.log('1111'+JSON.stringify(message))});");
		js.executeScript("new DataLayerHelper(dataLayer, (model, message) => {console.log('222'+JSON.stringify(model))});");
		js.executeScript("new DataLayerHelper(dataLayer, (model, message) => {console.log('333'+JSON.stringify(dataLayer))});");

		String result1 = js.executeScript("return dataLayer;").toString();
		System.out.println(result1);


		js.executeScript("window.scrollTo(0, document.body.scrollHeight/20)");
		Thread.sleep(1000);
		js.executeScript("window.scrollTo(document.body.scrollHeight/20, document.body.scrollHeight19)");
		Thread.sleep(1000);
		js.executeScript("window.scrollTo(document.body.scrollHeight/19, document.body.scrollHeight/18)");
		Thread.sleep(1000);
		js.executeScript("window.scrollTo(document.body.scrollHeight/18, document.body.scrollHeight/15)");
		Thread.sleep(1000);
		js.executeScript("window.scrollTo(document.body.scrollHeight/15, document.body.scrollHeight/12)");
		Thread.sleep(1000);
		js.executeScript("window.scrollTo(document.body.scrollHeight/12, document.body.scrollHeight/10)");
		Thread.sleep(1000);
		js.executeScript("window.scrollTo(document.body.scrollHeight/10, document.body.scrollHeight/8)");
		Thread.sleep(1000);
		js.executeScript("window.scrollTo(document.body.scrollHeight/8, document.body.scrollHeight/6)");
		Thread.sleep(1000);
		js.executeScript("window.scrollTo(document.body.scrollHeight/6, document.body.scrollHeight/4)");
		Thread.sleep(1000);
		js.executeScript("window.scrollTo(document.body.scrollHeight/4, document.body.scrollHeight/2)");
		Thread.sleep(1000);
		js.executeScript("window.scrollTo(document.body.scrollHeight/2, document.body.scrollHeight)");
		Thread.sleep(1000);


		System.out.println("***********************333333");



		//		List<String> gtmEvents = new ArrayList<String>();
		//		gtmEvents.add(arg0);
		//		if()


		//		String result1 = js.executeScript("return dataLayer;").toString();
		//		System.out.println("***********************4444444");
		//		System.out.println(result1);
		//		if (actualTitle.contentEquals(expectedTitle)){
		//			System.out.println("Test Passed!");
		//		} else {
		//			System.out.println("Test Failed");
		//		}
		thread.stop();
		//		System.out.println(driver.findElement.ByXpath("//script[contains(@src,'google-analytics.com/ga.js')]"));
		//		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@src, 'GTM-5H67HW')]")));
		//		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@src='https://www.googletagmanager.com/ns.html?id=GTM-5H67HW')]")));
		//		System.out.println(driver.getPageSource());

	}

	public void analyzeLog() {
//		System.out.println("*************************analyzing");
		LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
		for (LogEntry entry : logEntries) {
			System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
			//do something useful with the data
		}
	}
}
