package com.abof.web.GTMTracking;

import static com.jayway.restassured.RestAssured.given;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;


import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;

public class BaseTest {
	public static WebDriver driver;
	static WebElement wElement;
	static Dimension dSize;
	Wait wait;
	static int sStatusCnt = 0;

	@BeforeClass
	public void setUp() throws MalformedURLException {
		System.setProperty("webdriver.chrome.driver", "/Users/asifasunar/Downloads/chromedriver");
		ChromeOptions options = new ChromeOptions ();
		//		options.addExtensions (new File(“/path/to/extension.crx”));
		DesiredCapabilities caps = DesiredCapabilities.chrome();
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.BROWSER, Level.ALL);
        logPrefs.enable(LogType.DRIVER, Level.ALL);
        logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
        caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        driver = new ChromeDriver(caps);
        
//		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	
	public static Response postRequest(String url, String body, String WCToken, String WCTrustedToken, String userId) {
		Response PostResponse = given()
				.contentType(ContentType.JSON)
				.header(new Header("WCToken", WCToken))
				.header(new Header("WCTrustedToken", WCTrustedToken))
				.header(new Header("userId", userId))
				.body(body)
				.log()
				.everything()
				.expect()
				.log()
				.ifError()
				.when()
				.post(url);
		return PostResponse;
	}
	
	
	public static Response getRequest(String url, String WCToken, String WCTrustedToken, String userId) {
		Response getResponse = given()
				.contentType(ContentType.JSON)
				.header(new Header("WCToken", WCToken))
				.header(new Header("WCTrustedToken", WCTrustedToken))
				.header(new Header("userId", userId))
				.log()
				.everything()
				.expect()
				.log()
				.ifError()
				.when()
				.get(url);
		return getResponse;
	}
	
	public static Response deleteRequest(String url, String WCToken, String WCTrustedToken, String userId) {
		Response DeleteResponse = given()
				.header(new Header("WCToken", WCToken))
				.header(new Header("WCTrustedToken", WCTrustedToken))
				.header(new Header("userId", userId))
				.log()
				.everything()
				.expect()
				.log()
				.ifError()
				.when()
				.delete(url);
		return DeleteResponse;
	}
	

	public static void elementStatus(WebElement element, String elementName, String checkType)

	{
		switch (checkType) {
		case "displayed":
			try {
				element.isDisplayed();
				NXGReports.addStep(elementName + " is displayed", LogAs.PASSED, null);
			} catch (Exception e) {
				sStatusCnt++;
				NXGReports.addStep(elementName + " is not displayed", LogAs.FAILED,
						new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			break;
		case "enabled":
			try {
				element.isEnabled();
				NXGReports.addStep(elementName + " is enabled", LogAs.PASSED, null);
			} catch (Exception e) {
				sStatusCnt++;
				NXGReports.addStep(elementName + " is not enabled", LogAs.FAILED,
						new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			break;
		case "selected":
			try {
				element.isSelected();
				NXGReports.addStep(elementName + " is selected", LogAs.PASSED, null);
			} catch (Exception e) {
				sStatusCnt++;
				NXGReports.addStep(elementName + " is not selected", LogAs.FAILED,
						new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			break;
		}
	}

	public void visibilityOfElementWait(WebElement webElement, String elementName) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOf(webElement));
		} catch (Exception e) {
			NXGReports.addStep(elementName + " is not Visible", LogAs.FAILED, null);
		}
	}

	/*
	 * @author:Prerana Bhatt Description:check the list of webelemtns.
	 */
	public static void IsListDisplayed(List lst, String lstName) {
		try {
			if (lst.size() > 0) {
				NXGReports.addStep(lstName + " List is displayed", LogAs.PASSED, null);
			}
		} catch (Exception e) {
			NXGReports.addStep(lstName + " List is not displayed", LogAs.FAILED, null);
		}

	}

	public static void handleNavigation(WebElement ele) throws InterruptedException {
		while (!ele.isDisplayed()) {
			driver.navigate().back();
		}
	}

	@AfterMethod
	public void restapp() {
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}


}
